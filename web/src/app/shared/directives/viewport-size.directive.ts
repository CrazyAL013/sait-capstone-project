/**
 * Structural directive used to modify DOM structure based on viewport size.
 * Predefined sizes are contained in the ViewPortSizeEnum but the directive
 * will also accept any valid media query 
 * 
 * @see 
 * {@link https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries)}
 * 
 * @example
 * <div *ifViewportSize="'(max-width: 599.98px)'">SMALL</div>  
 * <div *ifViewportSize="viewportSize.Medium">MEDIUM</div>
 */
import { Directive, OnDestroy, Input, ViewContainerRef, TemplateRef } from "@angular/core";
import { BreakpointObserver, BreakpointState } from "@angular/cdk/layout";
import { Subscription } from "rxjs";
import { ViewportSizeEnum } from '@core/enums';

@Directive({
  selector: "[ifViewportSize]"
})
export class ViewportSizeDirective implements OnDestroy {
  private subscription = new Subscription();
  private context: any = {};

  @Input("ifViewportSize") 
  set viewportSize(context: ViewportSizeEnum | string) {
    this.context.$implicit = this.context.ngVar = context;
    this.subscription.unsubscribe();
    this.subscription = this.observer
      .observe([context])
      .subscribe(this.updateView);
  }

  constructor(
    private observer: BreakpointObserver,
    private viewContainer: ViewContainerRef,
    private templateRef: TemplateRef<any>
  ) {}

  updateView = ({ matches }: BreakpointState) => {
    if (matches && !this.viewContainer.length) {
      this.viewContainer.createEmbeddedView(this.templateRef, this.context);
    } else if (!matches && this.viewContainer.length) {
      this.viewContainer.clear();
    }
  };

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
