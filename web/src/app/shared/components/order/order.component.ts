import { NgModule,Component, OnInit, Input } from '@angular/core';
import { MaterialModule } from '@shared/material.module';

import { AuthService } from '@core/services/auth.service';
import { IOrder, IAccount, IProduct } from '@app/core/data';
import { environment } from '@env';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  @Input() order: IOrder;
  public account: IAccount;
  
  constructor(private authService: AuthService) {
    this.account = this.authService.accountValue;
  }

  ngOnInit(): void { }

  getImage(image: IProduct.IImage): string {
    if (image) {
      return (`${environment.apiEndpoint}${environment.images}${image.url}`);
    } else {
      return (`/assets/images/placeholder-image.png`);
    }
  }
}
