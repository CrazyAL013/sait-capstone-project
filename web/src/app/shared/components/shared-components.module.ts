import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'app/shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { LoginComponent } from './login/login.component';
import { OrderComponent } from './order/order.component';

const COMPONENTS = [
  ConfirmDialogComponent,
  LoginComponent,
  OrderComponent,
];

@NgModule({
  declarations: [...COMPONENTS ],
  imports: [CommonModule, MaterialModule, ReactiveFormsModule, RouterModule],
  exports: [...COMPONENTS]
})
export class SharedComponentsModule {}
