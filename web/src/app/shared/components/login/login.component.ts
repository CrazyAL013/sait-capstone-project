import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AuthService } from '@core/services';
import { $animations } from './login-animations';

export enum LoginAction {
  Register = 1,
  Login = 2,
  Logout = 3,
  ResetPassword = 4,
  ChangePassword = 5,
  ShowMessage = 6,
  AdminLogin = 7,
}

export class DialogData {
  constructor(
    public loginAction: LoginAction = LoginAction.Login,
    public returnUrl?: string
  ) { }
}

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: $animations,
})
export class LoginComponent {
  LoginAction = LoginAction;
  readonly form: FormGroup;

  public page: LoginAction = LoginAction.Login;
  public hidePassword = true;
  public hideNewPassword = true;
  public hideCurrentPassword = true;
  public error = null;
  public progress = false;

  public title: string;
  public message: string;
  public caption: string;

  private fullName: FormControl;
  private preferredName: FormControl;
  private username: FormControl;
  private email: FormControl;
  private password: FormControl;
  private currentPassword: FormControl;
  private newPassword: FormControl;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    // Create form group and form controls
    this.form = new FormGroup({});
    this.fullName = new FormControl(null, Validators.required);
    this.preferredName = new FormControl(null, Validators.required);
    this.email = new FormControl(null, [Validators.required, Validators.email]);
    this.password = new FormControl(null, Validators.required);
    this.currentPassword = new FormControl(null, Validators.required);
    this.newPassword = new FormControl(null, Validators.required);

    // Populates the form according to the page
    this.switchPage((this.page = this.data.loginAction));
  }

  ngOnInit() {
    this.dialogRef.keydownEvents().subscribe((event) => {
      if (event.key === 'Escape') {
        this.dialogRef.close(false);
      }
    });
  }

  public switchPage(loginAction: LoginAction) {
    // Reset the form and remove all the controls from the form group
    this.form.reset();
    Object.keys(this.form.controls).forEach((control) => {
      this.form.removeControl(control);
    });

    // Rebuild the form group based on the requested page
    switch ((this.page = loginAction)) {
      case LoginAction.Register:
        this.title = 'Register';
        this.message = 'Welcome. Please register to create an account.';
        this.caption = 'Register';
        this.form.addControl('fullName', this.fullName);
        this.form.addControl('preferredName', this.preferredName);
        this.form.addControl('email', this.email);
        this.form.addControl('password', this.password);
        break;
      case LoginAction.ResetPassword:
        this.title = 'Reset password';
        this.message = 'We will send you a password reset email';
        this.caption = 'Send email';
        this.form.addControl('email', this.email);
        break;
      case LoginAction.ChangePassword:
        if (this.authService.accountValue) {
          // Can only change password if logged in
          this.title = 'Change password';
          this.message = 'Change your current password';
          this.caption = 'Change password';
          this.form.addControl('currentPassword', this.currentPassword);
          this.form.addControl('newPassword', this.newPassword);
        } else {
          // Not logged in: this should never happen
          this.dialogRef.close(false);
        }
        break;
      case LoginAction.ShowMessage:
        this.caption = 'Done';
        break;
      case LoginAction.AdminLogin:
        this.title = 'Warehouse Login';
        this.message = 'Login to the warehouse';
        this.caption = 'Login';
        this.form.addControl('email', this.email);
        this.form.addControl('password', this.password);
        break;
      case LoginAction.Logout:
        this.logout();
        break;
      default:
      case LoginAction.Login:
        this.title = 'Sign in';
        this.message = 'Welcome back. Please sign in';
        this.caption = 'Sign in';
        this.form.addControl('email', this.email);
        this.form.addControl('password', this.password);
        break;
    }
  }

  private showError(error: string) {
    this.error = error;
    this.progress = false;
    setTimeout(() => (this.error = null), 5000);
  }

  public onSubmit(action: LoginAction) {
    this.progress = true;

    switch (action) {
      default:
      case LoginAction.AdminLogin:
      case LoginAction.Login:
        this.login(this.email.value, this.password.value);
        break;
      case LoginAction.Register:
        this.registerNew(
          this.email.value,
          this.password.value,
          this.fullName.value,
          this.preferredName.value
        );
        break;
      case LoginAction.ResetPassword:
        this.resetPassword(this.email.value);
        break;
      case LoginAction.ChangePassword:
        this.changePassword(this.currentPassword.value, this.newPassword.value);
        break;
      case LoginAction.ShowMessage:
        this.showMessage();
        break;
    }
  }

  private registerNew(
    email: string,
    password: string,
    fullName: string,
    preferredName: string
  ) {
    this.authService
      .register(email, password, fullName, preferredName)
      .pipe(first())
      .subscribe({
        next: () => {
          this.progress = false;
          this.title = `Thanks for registering, ${preferredName}`;
          this.message = "We've sent you an email to complete the registration process.";
          this.data.returnUrl = '/auth/login';
          this.switchPage(LoginAction.ShowMessage);
        },
        error: (error) => {
          this.showError(error);
        },
      });
  }

  private login(email: string, password: string) {
    this.authService
      .login(email, password)
      .pipe(first())
      .subscribe({
        next: () => {
          this.dialogRef.close(true);
          this.snackBar.open(`Welcome back, ${this.authService.accountValue.preferredName}.`, 'Sign In', { duration: 4000, });
          if (!this.data.returnUrl) this.data.returnUrl = '/';
          this.router.navigate([this.data.returnUrl]);
        },
        error: (error) => {
          this.showError(error);
        },
      });
  }

  private resetPassword(email: string) {
    this.authService
      .resetEmail(email)
      .pipe(first())
      .subscribe({
        next: () => {
          this.progress = false;
          this.title = 'Password reset';
          this.message = "We've sent you an email with instructions for resetting your password.";
          this.data.returnUrl = '/';
          this.switchPage(LoginAction.ShowMessage);
        },
        error: (error) => {
          this.showError(error);
        },
      });
  }

  private changePassword(currentPassword: string, newPassword: string) {
    this.authService
      .changePassword(
        this.authService.accountValue.username,
        currentPassword,
        newPassword
      )
      .pipe(first())
      .subscribe({
        next: () => {
          this.progress = false;
          this.title = 'Password changed';
          this.message = '';
          this.data.returnUrl = '/';
          this.switchPage(LoginAction.ShowMessage);
        },
        error: (error) => {
          this.showError(error);
        },
      });
  }

  private logout() {
    this.snackBar.open(`See you next time, ${this.authService.accountValue.preferredName}.`, 'Sign Out', { duration: 4000, });
    this.dialogRef.close(true);
    this.authService.logout();
    this.data.returnUrl = '/';
    this.router.navigate([this.data.returnUrl]);
  }

  private showMessage() {
    this.dialogRef.close(true);
    if (this.data.returnUrl) {
      this.router.navigate([this.data.returnUrl]);
    }
  }
}

// This component is used to launch the login dialog from a router
@Component({ template: '' })
export class LoginEntryComponent {

  constructor(public dialog: MatDialog, private router: Router, private route: ActivatedRoute, private location: Location) {
    this.route.paramMap.subscribe((paramMap) => {
      this.openDialog(paramMap.get('action'));
    });
  }

  openDialog(action: string): void {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    switch (action.toLowerCase().trim()) {
      case 'login':
        dialogConfig.data = new DialogData(LoginAction.Login, this.router.url);
        break;
      case 'logout':
        dialogConfig.data = new DialogData(LoginAction.Logout, this.router.url);
        break;
      case 'register':
        dialogConfig.data = new DialogData(LoginAction.Register, this.router.url);
        break;
      case 'change-password':
        dialogConfig.data = new DialogData(LoginAction.ChangePassword, this.router.url);
        break;
      case 'reset-password':
        dialogConfig.data = new DialogData(LoginAction.ResetPassword, this.router.url);
        break;
      default:
        return;
    }

    dialogConfig.disableClose = true;
    const dialogRef = this.dialog.open(LoginComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (!Boolean(dialogResult)) this.location.back();
      return Boolean(dialogResult);
    });
  }
}
