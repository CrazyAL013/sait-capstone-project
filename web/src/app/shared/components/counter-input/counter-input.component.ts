import { NgModule, Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MaterialModule } from '@shared/material.module';

@Component({
  selector: 'counter-input',
  templateUrl: './counter-input.component.html',
  styleUrls: ['./counter-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CounterInputComponent),
      multi: true
    }
  ]
})
export class CounterInputComponent implements OnInit {
  @Input() min: number;
  @Input() max: number;
  @Input() value: number = 0;
  @Output() changeEvent = new EventEmitter<number>();
  disabled: boolean = false;

  onChange: any = () => { };
  onTouched: any = () => { };

  constructor() { }

  ngOnInit(): void {   }

  increase() {
    if (typeof this.max === 'undefined') {
      this.value++;
      this.onChange(this.value);
      this.changeEvent.emit(this.value);
    } else if (this.value < this.max) {
      this.value++;
      this.onChange(this.value);
      this.changeEvent.emit(this.value);
    }
    
  }

  decrease() {
    if (typeof this.min === 'undefined') {
      this.value--;
      this.onChange(this.value);
      this.changeEvent.emit(this.value);
    } else if (this.value > this.min) {
      this.value--;
      this.onChange(this.value);
      this.changeEvent.emit(this.value);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}

@NgModule({
  declarations: [CounterInputComponent],
  imports: [MaterialModule],
  exports: [CounterInputComponent],
})
export class CounterInputModule { }