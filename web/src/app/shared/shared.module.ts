import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {
  ContentDirective,
  ViewportSizeDirective,
} from '@shared/directives';


import { SharedComponentsModule } from './components/shared-components.module';
import { ReplacePipe } from './pipes';

const PIPES = [
  ReplacePipe,
];

const DIRECTIVES = [
  ContentDirective,
  ViewportSizeDirective,
];

@NgModule({
  declarations: [ ...PIPES, ...DIRECTIVES],
  imports: [CommonModule, SharedComponentsModule],
  exports: [ ...PIPES, ...DIRECTIVES, SharedComponentsModule]
})
export class SharedModule {}
