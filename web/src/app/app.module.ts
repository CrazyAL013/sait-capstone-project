import { NgModule, APP_INITIALIZER } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { appInitializer } from '@core/app.initializer';

import { StorefrontAuthGuard, WarehouseAuthGuard, NoAuthGuard } from '@core';
import { WINDOW_PROVIDERS, AuthService } from '@core';
import { ErrorInterceptor, JwtInterceptor } from '@core';
import { SharedModule, MaterialModule, AddOnsModule } from '@shared';

@NgModule({
  declarations: [
    AppComponent, 
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    SharedModule,
    AddOnsModule,
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: appInitializer, multi: true, deps: [AuthService] },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    WINDOW_PROVIDERS,
    StorefrontAuthGuard,
    WarehouseAuthGuard,
    NoAuthGuard,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
