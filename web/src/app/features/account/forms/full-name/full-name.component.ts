import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import {
  ReactiveFormsModule,
  FormsModule,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { MaterialModule } from '@shared/material.module';
import { IAccount } from '@data/models';

@Component({
  selector: 'forms-full-name',
  templateUrl: './full-name.component.html',
  styleUrls: ['../forms.scss']
})
export class FullNameComponent implements OnInit {
  @Input() account: IAccount;
  @Input() form: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.buildForm();
  }

  get formControls() {
    return this.form.controls;
  }

  buildForm() {
    // Append the form controls to the form group supplied by the parent (dialog)
    this.form.addControl('fullName', new FormControl(this.account.fullName, Validators.required));
  }

  resetForm() {
    this.form.get('fullName').setValue(this.account.fullName);
  }

}

@NgModule({
  declarations: [FullNameComponent],
  imports: [CommonModule, ReactiveFormsModule, FormsModule, MaterialModule],
})
export class FullNameFormModule { }