import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import {
  ReactiveFormsModule,
  FormsModule,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { MaterialModule } from '@shared/material.module';
import { IAccount } from '@data/models';

@Component({
  selector: 'preferred-full-name',
  templateUrl: './preferred-name.component.html',
  styleUrls: ['../forms.scss']
})
export class PreferredNameComponent implements OnInit {
  @Input() account: IAccount;
  @Input() form: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.buildForm();
  }

  get formControls() {
    return this.form.controls;
  }

  buildForm() {
    // Append the form controls to the form group supplied by the parent (dialog)
    this.form.addControl('preferredName', new FormControl(this.account.preferredName, Validators.required));
  }

  resetForm() {
    this.form.get('preferredName').setValue(this.account.preferredName);
  }
}
@NgModule({
  declarations: [PreferredNameComponent],
  imports: [CommonModule, ReactiveFormsModule, FormsModule, MaterialModule],
})
export class PreferredNameFormModule { }
