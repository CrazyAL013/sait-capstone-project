import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import {
  ReactiveFormsModule,
  FormsModule,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { MaterialModule } from '@shared/material.module';
import { IAccount } from '@data/models';

@Component({
  selector: 'forms-email',
  templateUrl: './email.component.html',
  styleUrls: ['../forms.scss']
})
export class EmailComponent implements OnInit {
  @Input() account: IAccount;
  @Input() form: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.buildForm();
  }

  get formControls() {
    return this.form.controls;
  }

  buildForm() {
    // Append the form controls to the form group supplied by the parent (dialog)
    this.form.addControl('email', new FormControl(this.account.email, [Validators.required, Validators.email]));
  }

  resetForm() {
    this.form.get('email').setValue(this.account.email);
  }

}
@NgModule({
  declarations: [EmailComponent],
  imports: [CommonModule, ReactiveFormsModule, FormsModule, MaterialModule],
})
export class emailFormModule { }