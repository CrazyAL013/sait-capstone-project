import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ComponentFactory,
  ComponentFactoryResolver,
} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Globals } from '@core/globals';
import { IAccount } from '@data/models';
import { AccountService } from '@data/services';
import { AuthService } from '@core/services/auth.service';

import { FullNameComponent, PreferredNameComponent, EmailComponent } from './forms';
import { MatAccordion } from '@angular/material/expansion';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit, OnDestroy {
  // Reference to the <ng-template> element in the template
  @ViewChild('accordion') matAccordion: MatAccordion;
  @ViewChild('fullName', { read: ViewContainerRef }) fullNameContainerRef: ViewContainerRef;
  @ViewChild('preferredName', { read: ViewContainerRef }) preferredNameContainerRef: ViewContainerRef;
  @ViewChild('email', { read: ViewContainerRef }) emailContainerRef: ViewContainerRef;

  private formComponents: { [key: string]: ComponentRef<any> } = {};
  public form: FormGroup;
  account: IAccount;
  subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private accountService: AccountService,
    private spinner: NgxSpinnerService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private formBuilder: FormBuilder,
    public globals: Globals
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({});
    // Show the spinner
    this.spinner.show();
    this.loadAccount();
  }

  ngOnDestroy() {
    // Unsubscribe from all subscriptions
    this.subscriptions.forEach((s) => s.unsubscribe);
  }

  /**
   * Load the logged in account from authService
   */
  loadAccount() {
    if (this.authService.isLoggedIn) {
      this.account = this.authService.accountValue;
    }
  }

  /**
   * Save changes made to the account
   */
  saveAccount() {
    // Update the account
    this.account = Object.assign(this.account, this.form.value);
    this.account.username = this.account.email;

    this.subscriptions.push(
      this.accountService.updateAccount(this.account).subscribe()
    );

    // Close the open panel
    this.matAccordion._headers.find(h => h._isExpanded())._toggle();
  }


  public loadComponent(componentName: string) {
    // Reset all the form components to default state
    this.form.reset();
    for (var key in this.formComponents) {
      this.formComponents[key].instance.resetForm();
    }

    // Lazy load the requested form component
    switch (componentName) {
      case 'fullName': {
        if (this.fullNameContainerRef.length === 0) {
          import('./forms/full-name/full-name.component').then(({ FullNameComponent: component }) => {
            let factory: ComponentFactory<FullNameComponent> = this.componentFactoryResolver.resolveComponentFactory(component);
            let ref: ComponentRef<FullNameComponent> = this.fullNameContainerRef.createComponent(factory);
            ref.instance.account = this.account;
            ref.instance.form = this.form;
            this.formComponents[componentName] = ref;
          });
        }
      } break;
      case 'preferredName': {
        if (this.preferredNameContainerRef.length === 0) {
          import('./forms/preferred-name/preferred-name.component').then(({ PreferredNameComponent: component }) => {
            let factory: ComponentFactory<PreferredNameComponent> = this.componentFactoryResolver.resolveComponentFactory(component);
            let ref: ComponentRef<PreferredNameComponent> = this.preferredNameContainerRef.createComponent(factory);
            ref.instance.account = this.account;
            ref.instance.form = this.form;
            this.formComponents[componentName] = ref;
          });
        }
      } break;
      case 'email': {
        if (this.emailContainerRef.length === 0) {
          import('./forms/email/email.component').then(({ EmailComponent: component }) => {
            let factory: ComponentFactory<EmailComponent> = this.componentFactoryResolver.resolveComponentFactory(component);
            let ref: ComponentRef<EmailComponent> = this.emailContainerRef.createComponent(factory);
            ref.instance.account = this.account;
            ref.instance.form = this.form;
            this.formComponents[componentName] = ref;
          });
        }
      } break;
    }
  }
}
