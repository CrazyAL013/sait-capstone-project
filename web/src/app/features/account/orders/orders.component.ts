import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { IAccount, IOrder } from '@core/data';
import { AuthService } from '@core/services/auth.service';
import { OrderService } from '@data/services';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public orders$: Observable<IOrder[]>;
  private account: IAccount;

  constructor(
    private authService: AuthService,
    private orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  /**
   * Loads the data needed by the page
   */
  private loadData(): void {
    if (this.authService.isLoggedIn) {
      const subs: Observable<any>[] = [];
      this.account = this.authService.accountValue;
      this.orders$ = this.orderService.getOrders(this.account.id);
      this.subscriptions.push(this.orders$.subscribe());
    }
  }

  ngOnDestroy() {
    // Unsubscribe from all subscriptions
    this.subscriptions.forEach((s) => s.unsubscribe);
  }
}
