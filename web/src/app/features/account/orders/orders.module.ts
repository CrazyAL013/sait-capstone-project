import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '@shared/material.module';
import { OrdersComponent } from './orders.component';
import { SharedModule } from '@shared/shared.module';

const routes: Routes = [
  { path: '', component: OrdersComponent },
  { path: '**', redirectTo: '/account/orders', pathMatch: 'full' },
];

@NgModule({
  declarations: [OrdersComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule,
    SharedModule
  ]
})
export class OrdersModule { }
