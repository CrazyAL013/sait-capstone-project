import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';


import { AccountComponent } from './account.component';
import { MaterialModule } from '@shared/material.module';
import { AddOnsModule } from 'app/shared/addons.module';

const routes: Routes = [
  { path: '', component: AccountComponent },
  {
    path: 'orders',
    loadChildren: () =>
      import('./orders/orders.module').then((m) => m.OrdersModule),
  },
  {
    path: 'wishlists',
    loadChildren: () =>
      import('./wishlists/wishlists.module').then((m) => m.WishlistsModule),
  },
  {
    path: 'reviews',
    loadChildren: () =>
      import('./reviews/reviews.module').then((m) => m.ReviewsModule),
  },
  {
    path: 'addresses',
    loadChildren: () =>
      import('./addresses/addresses.module').then((m) => m.AddressesModule),
  },

];

@NgModule({
  declarations: [AccountComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule, 
    AddOnsModule
  ]
})
export class AccountModule { }
