import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '@shared/material.module';
import { WishlistsComponent } from './wishlists.component';

const routes: Routes = [
  { path: '', component: WishlistsComponent },
  {
    path: 'wishlist/:id',
    loadChildren: () =>
      import('./wishlist/wishlist.module').then((m) => m.WishlistModule),
  },
  { path: '**', redirectTo: '/account/wishlists', pathMatch: 'full' },
];

@NgModule({
  declarations: [WishlistsComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule
  ]
})
export class WishlistsModule { }
