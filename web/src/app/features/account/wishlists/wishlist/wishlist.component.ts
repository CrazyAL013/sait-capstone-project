import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {

  id: string;

  constructor(private route: ActivatedRoute) {
    // Get the 'id' parameter
    this.route.paramMap.subscribe((paramMap) => {
      this.id = paramMap.get('id');
    });
  }

  ngOnInit(): void {
  }

}
