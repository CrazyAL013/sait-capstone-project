import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '@shared/material.module';
import { WishlistComponent } from './wishlist.component';

const routes: Routes = [
  { path: '', component: WishlistComponent },
];

@NgModule({
  declarations: [WishlistComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule
  ]
})
export class WishlistModule { }
