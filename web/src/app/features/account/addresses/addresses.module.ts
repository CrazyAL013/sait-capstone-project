import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddressesComponent } from './addresses.component';
import { MaterialModule } from '@shared/material.module';
import { AddOnsModule } from 'app/shared/addons.module';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: AddressesComponent },
  {
    path: 'address',
    loadChildren: () =>
      import('./address/address.module').then((m) => m.AddressModule),
  },
  {
    path: 'address/:id',
    loadChildren: () =>
      import('./address/address.module').then((m) => m.AddressModule),
  },
  {
    path: 'address/:returnUrl',
    loadChildren: () =>
      import('./address/address.module').then((m) => m.AddressModule),
  }
];
@NgModule({
  declarations: [
    AddressesComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule,
    AddOnsModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AddressesModule { }
