import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { IAccount, IAddress } from '@core/data';
import { AuthService } from '@core/services/auth.service';
import { AccountService } from '@data/services';
import { AddressComponent } from './address/address.component';
import { Globals } from '@core/globals';
import { ConfirmDialogComponent, ConfirmDialogModel } from '@shared/components/confirm-dialog/confirm-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.scss']
})
export class AddressesComponent implements OnInit {
  @ViewChild('address', { read: ViewContainerRef }) addressContainerRef: ViewContainerRef = null;
  private addressComponentRef: ComponentRef<AddressComponent>;
  private account: IAccount;
  private subscriptions: Subscription[] = [];
  public addresses$: Observable<IAddress[]>;

  constructor(
    private authService: AuthService,
    private accountService: AccountService,
    private spinner: NgxSpinnerService,
    private readonly matDialog: MatDialog,
    private _snackBar: MatSnackBar,
    public globals: Globals
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.loadData();
  }

  /**
   * Loads the data needed by the page
   */
  private loadData(): void {
    if (this.authService.isLoggedIn) {
      const subs: Observable<any>[] = [];
      this.account = this.authService.accountValue;
      this.addresses$ = this.accountService.getAddresses(this.account, true);
      this.subscriptions.push(this.addresses$.subscribe());
    }
  }

  /**
   * Sets the default address
   * 
   * @param event The event that called this method
   * @param address The address to be set as default
   */
  defaultAddress(event: any, address: IAddress): void {
    event.preventDefault();
    event.stopPropagation();

    this.account.address = address;
    this.subscriptions.push(this.accountService.updateAccount(this.account).subscribe());
  }

  /**
   * Deletes an address
   * 
   * @param event The triggering event
   * @param address The address to be deleted
   */
  deleteAddress(event: any, address: IAddress): void {
    event.preventDefault();
    event.stopPropagation();

    //check if address is default address
    if (address.id === this.account.address.id) {
      // Address is default
        this._snackBar.open("Cannot delete the default Address", "Close", {
          duration: 4000,
        });
    } else {
      // Address is not a default
      // create the confirm dialog
      const dialogData = new ConfirmDialogModel(
        `Delete ${address.description}`,
        'Are you sure?'
      );
      const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
        data: dialogData,
        maxWidth: '80%'
      });

      dialogRef.afterClosed().subscribe((dialogResult) => {
        let result: boolean = dialogResult;
        if (result) {
          this.subscriptions.push(this.accountService.deleteAddress(address).subscribe());
        }
      });
    }
  }

  ngOnDestroy() {
    // Unsubscribe from all subscriptions
    this.subscriptions.forEach((s) => s.unsubscribe);
  }
}
