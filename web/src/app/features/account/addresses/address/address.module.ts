import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AddOnsModule } from 'app/shared/addons.module';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { MaterialModule } from '@shared/material.module';
import { AddressComponent } from './address.component';

const routes: Routes = [
  { path: '', component: AddressComponent },
];

@NgModule({
  declarations: [AddressComponent],
  imports: [
    RouterModule.forChild(routes),
    MaterialModule,
    AddOnsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ]
})
export class AddressModule { }
