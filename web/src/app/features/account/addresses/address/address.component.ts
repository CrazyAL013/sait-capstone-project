import { OnInit, Component, ViewChild, Input } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import * as uuid from 'uuid';

import { ILookup, Address, IAccount, IAddress } from '@core/data';
import { Globals } from '@core/globals';
import { AppService, AccountService } from '@data/services';
import { Observable, Subscription, forkJoin } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '@core/services/auth.service';
import { MatSlideToggle } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  @ViewChild('default') defaultCheckbox: MatSlideToggle;
  @ViewChild('region') regionDropdown: MatSelect;
  public account: IAccount;
  public address: IAddress;
  public address$: Observable<IAddress>;
  public form: FormGroup;
  public countries: ILookup.ICountry[] = [];
  public country: IAddress.ICountry;
  public load: boolean = false;
  public data$: Observable<any>;
  private subscriptions: Subscription[] = [];
  private id: string;
  private returnUrl: string = '/store/account/addresses';

  constructor(
    public globals: Globals,
    public router: Router,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private appService: AppService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _snackBar: MatSnackBar
  ) {
    // Get the 'id' parameter
    this.route.paramMap.subscribe((paramMap) => {
      this.id = paramMap.get('id');
      if (paramMap.has('returnUrl')) {
        this.returnUrl = paramMap.get('returnUrl');
      }
    });
    this.account = this.authService.accountValue;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({});
    this.spinner.show();
    this.loadData();
  }

  /**
   * Loads the data needed for this page
   */
  loadData(): void {
    const subs: Observable<any>[] = [];
    subs.push(this.appService.getCountries());
    if (this.id) subs.push(this.accountService.getAddress(this.id));

    this.data$ = forkJoin(subs);
    this.subscriptions.push(this.data$.subscribe(results => {
      this.countries = results[0] as ILookup.ICountry[];
      if (this.id) {
        this.address = results[1] as IAddress;
        this.country = this.countries.find(c => c.id === this.address.country.id);
      } else {
        // Create a new address object
        this.address = new Address({ id: uuid.v4(), accountId: this.account.id, country: this.countries[0] });
        this.country = this.countries[0];
      }
      this.buildForm();
      this.load = true;
    }));
  }

  /**
   * Compares two contries
   * 
   * @param country1 
   * @param country2 
   * @returns Both countries and a comparison result
   */
  compareCountries(country1: ILookup.ICountry, country2: ILookup.ICountry) {
    return (country1 && country2 && country1.id === country2.id);
  }

  /**
   * Compares two regions
   * 
   * @param region1 
   * @param region2 
   * @returns Both regions and a comparison result
   */
  compareRegions(region1: ILookup.IRegion, region2: ILookup.IRegion) {
    return (region1 && region2 && region1.id === region2.id);
  }

  buildForm(): void {
    // Append the form controls to the form group supplied by the parent (dialog)
    this.form.addControl('description', new FormControl(this.address.description, [Validators.required]));
    this.form.addControl('line1', new FormControl(this.address.line1, [Validators.required]));
    this.form.addControl('line2', new FormControl(this.address.line2));
    this.form.addControl('city', new FormControl(this.address.city, [Validators.required]));
    this.form.addControl('postalZip', new FormControl(this.address.postalZip, [Validators.required]));
    this.form.addControl('country', new FormControl(this.address.country, [Validators.required]));
    this.form.addControl('region', new FormControl(this.address.region, [Validators.required]));

    // If the country changes we need to re-populate the regions
    this.form.get('country').valueChanges.subscribe(country => {
      this.country = country;
      this.form.controls['region'].setValue(null);
      this.regionDropdown.focus();
    });
  }

  /**
   * Saves a new address
   */
  saveAddress(): void {
    let message: string;
    if (this.id) {
      // Existing address is being edited
      this.address = Object.assign(this.address, this.form.value);
      //delete this.address.country['regions'];
      this.subscriptions.push(this.accountService.updateAddress(this.address).subscribe());
      message = "Address Saved";
    } else {
      // New address is being created
      this.address = Object.assign(this.address, this.form.value);
      //delete this.address.country['regions'];
      this.subscriptions.push(this.accountService.addNewAddress(this.address).subscribe());
      message = "New Address Saved";
      if (this.account.address == null) {
          this.account.address = this.address;
      }
    }

    this.router.navigate([this.returnUrl]).then((navigated: boolean) => {
      if (navigated) {
        this._snackBar.open(message, "Close", {
          duration: 2000,
        });
      }
    });
  }

  ngOnDestroy() {
    // Unsubscribe from all subscriptions
    this.subscriptions.forEach((s) => s.unsubscribe);
  }
}
