import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '@shared/material.module';
import { ReviewComponent } from './review.component';

const routes: Routes = [
  { path: '', component: ReviewComponent },
];

@NgModule({
  declarations: [ReviewComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule
  ]
})
export class ReviewModule { }
