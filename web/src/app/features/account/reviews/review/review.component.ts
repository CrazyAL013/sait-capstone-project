import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit {
  id: string;

  constructor(private route: ActivatedRoute) {
    // Get the 'id' parameter
    this.route.paramMap.subscribe((paramMap) => {
      this.id = paramMap.get('id');
    });
  }

  ngOnInit(): void {}
}
