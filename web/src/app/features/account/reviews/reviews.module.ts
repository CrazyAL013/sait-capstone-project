import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '@shared/material.module';
import { ReviewsComponent } from './reviews.component';

const routes: Routes = [
  {
    path: '',
    component: ReviewsComponent,
  },
  {
    path: 'review/:id',
    loadChildren: () =>
      import('./review/review.module').then((m) => m.ReviewModule),
  },
  { path: '**', redirectTo: '/account/reviews', pathMatch: 'full' },
];

@NgModule({
  declarations: [ReviewsComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule
  ]
})
export class ReviewsModule { }