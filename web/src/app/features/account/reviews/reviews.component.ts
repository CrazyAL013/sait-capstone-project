import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { IAccount, IOrder, IProduct } from '@core/data';
import { AccountService } from '@data/services';
import { AuthService } from '@core/services/auth.service';
import { environment } from '@env';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public reviews$: Observable<IAccount.IReview[]>;
  private account: IAccount;
  public imageLocation: string = `${environment.apiEndpoint}${environment.images}`;

  constructor(
    private authService: AuthService,
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  /**
   * Loads the data needed by the page
   */
  private loadData(): void {
    if (this.authService.isLoggedIn) {
      const subs: Observable<any>[] = [];
      this.account = this.authService.accountValue;
      this.reviews$ = this.accountService.getReviews(this.account);
      this.subscriptions.push(this.reviews$.subscribe());
    }
  }

  getImage(image: IProduct.IImage): string {
    if (image) {
      return (`${environment.apiEndpoint}${environment.images}${image.url}`);
    } else {
      return (`/assets/images/placeholder-image.png`);
    }
  }

  onEditReview() {
  }

  ngOnDestroy() {
    // Unsubscribe from all subscriptions
    this.subscriptions.forEach((s) => s.unsubscribe);
  }
}
