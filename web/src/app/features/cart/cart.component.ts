import { Component, OnDestroy, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

import { Globals } from '@core/globals';
import { environment } from '@env';
import { AuthService, CartService } from '@core/services'
import { IAccount, ICartItem, IProduct } from '@data/models';

export enum StockStatusEnum {
  InStock,
  LowStock,
  OutOfStock
}
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  account: IAccount;
  cartItems: Map<string, ICartItem>;
  StockStatus: typeof StockStatusEnum = StockStatusEnum;
  ready$: Observable<boolean>;
  pluralMapping = { 'item': { '=0': '', '=1': '(1 item)', 'other': '(# items)' } };
  
  constructor(
    private cartService: CartService,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    public globals: Globals
  ) {
    this.subscriptions.push((this.ready$ = this.cartService.ready).subscribe());
    this.cartItems = this.cartService.Items;
    // Show the spinner
    this.spinner.show();
  }

  ngOnInit(): void {

  }

  /**
   * Get the image for specific product
   * 
   * @param image An image object
   * @returns The full path to the image
   */
  getImage(image: IProduct.IImage): string {
    if (image) {
      return (`${environment.apiEndpoint}${environment.images}${image.url}`);
    } else {
      return (`/assets/images/placeholder-image.png`);
    }
  }

  /**
   * Get the total number of items in the cart
   */
  public get itemCount(): number {
    return this.cartService.totalItems;
  }

  /**
   * Get the sub total cost of the cart
   */
  public get subTotal(): number {
    return this.cartService.totalCost;
  }

  public minQuantity(productItemId: string): number {
    const stock: number = this.cartItems.get(productItemId).productItem.stockQuantity;
    return ((stock === 0) ? 0 : 1);
  }

  /**
   * Computes the max quanitity for a product in the cart
   * 
   * @param productItemId The productItem to be used
   * @returns The max quantity for that productItem
   */
  public maxQuantity(productItemId: string): number {
    const item: ICartItem = this.cartItems.get(productItemId);
    const quantity: number = item.quantity;
    const stock: number = item.productItem.stockQuantity;
    const reorder: number = item.productItem.reorderLevel;
    let maxQty: number;

    if (stock == 0) {
      maxQty = 0;
    } else if (stock > 99) {
      maxQty = 99;
    } else {
      maxQty = stock;
    }
    return maxQty;
  }

  /**
   * Update the quanitity when changed
   * 
   * @param productItemId The productItem that was updated
   * @param quantity The new quantity
   */
  public onQuantityChanged(productItemId: string, quantity: number) {
    this.cartService.updateQuantity(productItemId, quantity);
  }

  /**
   * Removes a cart item when delete is clicked
   * 
   * @param productItemId The productItem to be deleted
   */
  public onDeleteItem(productItemId: string) {
    this.cartService.removeItem(productItemId);
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}
