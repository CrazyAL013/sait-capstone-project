import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { StorefrontAuthGuard } from '@core';
import { CartComponent } from './cart.component';
import { AddOnsModule } from '@shared/addons.module';
import { MaterialModule } from '@shared/material.module';
import { CounterInputModule } from '@shared/components/counter-input/counter-input.component';


const routes: Routes = [
  { path: '', component: CartComponent },
  {
    path: 'checkout',
    canActivate: [StorefrontAuthGuard],
    loadChildren: () => import('@app/features/checkout/checkout.module').then((m) => m.CheckoutModule),
  }
];
@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    AddOnsModule,
    MaterialModule,
    CounterInputModule
  ]
})
export class CartModule { }
