import { Component, OnInit, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { IProduct } from '@core/data';
import { ProductService } from '@core/data/services';

@Component({
  selector: 'app-product-reviews',
  templateUrl: './product-reviews.component.html',
  styleUrls: ['./product-reviews.component.scss']
})
export class ProductReviewsComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public reviews$: Observable<IProduct.IReview[]>;
  @Input() product: IProduct;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.reviews$ = this.productService.getProductReviews(this.product);
    this.subscriptions.push(this.reviews$.subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }

}
