import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { AddOnsModule } from 'app/shared/addons.module';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from '@shared/material.module';

import { ProductReviewsComponent } from './product-reviews/product-reviews.component';
import { CounterInputModule } from '@shared/components/counter-input/counter-input.component';

const routes: Routes = [
  { path: '', component: ProductComponent },
];
@NgModule({
  declarations: [
    ProductComponent, ProductReviewsComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    AddOnsModule,
    MaterialModule,
    CounterInputModule,
  ]
})
export class ProductModule { }
