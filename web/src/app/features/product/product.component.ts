import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { from, Observable } from 'rxjs';
import { Subscription } from 'rxjs';

import { IProduct } from '@data/models';
import { ProductService } from '@data/services';
import { AuthService } from '@core/services/auth.service';
import { CartService } from '@core/services';
import { environment } from '@env';

import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-item',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  private productId: string;
  private productItemId: string;
  public product$: Observable<IProduct>;
  public productItems: Map<string, IProduct.IProductItem> = new Map<string, IProduct.IProductItem>();
  public product: IProduct;
  public selected: IProduct.IProductItem;
  public load: boolean = false;
  private quantity: number = 1;

  private displayedImage: IProduct.IImage;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService,
    public authService: AuthService,
    private spinner: NgxSpinnerService,
    private _snackBar: MatSnackBar
    ) {
    this.spinner.show();
    this.subscriptions.push(this.route.paramMap.subscribe((paramMap) => {
      this.productId = paramMap.get('id');
      this.productItemId = paramMap.get('itemId');
    }));
  }

  ngOnInit() {
    this.loadProductItem(this.productId);
  }

  /**
   * Get the image path for a product
   * 
   * @param image The image to get a path for
   * @returns The path for an image
   */
  getImage(image: IProduct.IImage): string {
    if (image) {
      return (`${environment.apiEndpoint}${environment.images}${image.url}`);
    } else {
      return (`/assets/images/placeholder-image.png`);
    }
  }

  /**
   * load a product item
   * 
   * @param uid the uid of a product item
   */
  private loadProductItem(uid: string) {
    this.product$ = this.productService.getProductById(uid);
    this.subscriptions.push(this.product$.subscribe(product => {
      product.productItems.forEach((item) => {
        this.productItems.set(item.id, item);
        if (item.display && this.productItemId == null) this.selected = item;
        if (this.productItemId != null && this.productItemId == item.id) this.selected = item;
        this.load = true;
      })
      //TODO: take another look at this
      if (this.selected.images) {
        this.displayedImage = this.selected.images[0];
      }
    }));
  }

  /**
   * Change the selected product item
   * 
   * @param uid The new product item to display
   */
  private changeSelectedItem(uid: string) {
    this.selected = this.productItems.get(uid);
    //TODO: take another look at this
    if (this.selected.images) {
      this.displayedImage = this.selected.images[0];
    }
    this.quantity = 1;
  }

  /**
   * Change the siaplayed image
   * 
   * @param image The new displayed image
   */
  private changeImage(image: IProduct.IImage) {
    this.displayedImage = image;
  }

  /**
   * Add product item to cart
   * 
   * @param product The product to add
   * @param productItem The specific product item to add
   */
  private addToCart(product: IProduct, productItem: IProduct.IProductItem) {
    this.cartService.addItem(product, productItem, this.quantity);
    this.quantity = 1;

    let message: string = "Item added to cart!";
    this._snackBar.open(message, "Close", {
      duration: 2000,
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}