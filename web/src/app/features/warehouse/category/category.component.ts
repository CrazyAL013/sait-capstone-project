import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
  FormArray,
} from '@angular/forms';
import * as uuid from 'uuid';

import { Globals } from '@core';
import { ICategory, Category } from '@data/models';
import { CategoryService } from '@data/services';

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public category$: Observable<ICategory[]>;
  private category: ICategory;
  private uid: string;
  public categoryForm: FormGroup = null;
  public optionSelected = false;
  public editing = false;
  public newSelectedCategory = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    public globals: Globals
  ) {}

  ngOnInit(): void {
    this.subscriptions.push((this.category$ = this.categoryService.getCategories()).subscribe());
  }

  /**
   * Creates a new category and constructs the form
   */
  createCategory() {
    let element = document.getElementById('edit');
    element.classList.add('edit');
    this.editing = false;
    this.optionSelected = true;
    this.newSelectedCategory = true;
    this.category = new Category.Category({ id: uuid.v4() })
    this.buildForm(this.category);
  }

  /**
   * Reveals the select dropdown with all the current categories listed
   */
  editCategory() {
    this.editing = true;
    let element = document.getElementById('edit');
    element.classList.remove('edit');
    if((this.subcategoriesForm != undefined)) {
      this.subcategoriesForm.clear();
    }
  }

  /**
   * Constructs the form when a category to be edited has beeen selected
   * @param category The category that has been selected
   */
  categorySelect(category: ICategory) {
    this.optionSelected = true;
    this.newSelectedCategory = false;
    this.category = category;
    this.buildForm(this.category);
  }

  /**
   * Builds the category form
   * @param category The category the form is being built from
   */
  buildForm(category: ICategory): void {
    this.categoryForm = this.formBuilder.group({});
    this.categoryForm.removeControl('id');
    this.categoryForm.removeControl('name');
    this.categoryForm.removeControl('subcategories');

    this.categoryForm.addControl('id', new FormControl(category.id));
    this.categoryForm.addControl('name', new FormControl(category.name, [Validators.required]));
    this.categoryForm.addControl('subcategories', this.formBuilder.array([]));

    if (!this.newSelectedCategory) {
      this.subcategoriesForm.clear();
      if (category.subcategories != null) {
        for (let subcategory of category.subcategories) {
          this.addSubcategory(subcategory);
        }
      }
    }
  }

  public get subcategoriesForm(): FormArray {
    if (this.categoryForm != null)
    return this.categoryForm.get('subcategories') as FormArray;
  }

  /**
   * Adds a new subcategory if one is not passed in and constructs the form
   * @param subcategory The subcategory being added
   */
  public addSubcategory(subcategory: ICategory.ISubcategory) {
    if (!subcategory) {
      subcategory = new Category.Subcategory({ id: uuid.v4() });
    }

    const subcategoryGroup: FormGroup = this.formBuilder.group({});
    subcategoryGroup.addControl('id', new FormControl(subcategory.id));
    subcategoryGroup.addControl('name', new FormControl(subcategory.name, [Validators.required]));
    this.subcategoriesForm.push(subcategoryGroup);
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // TODO: MAKE WORK ---- Categories and subcategories can't be deleted if a product is associated with it. 
  /**
   * Deletes a category
   * @param category The category to be deleted
   */
  public deleteCategory(category) {
    this.subscriptions.push(this.categoryService.deleteCategory(category.id.value).subscribe());
  }

  /**
   * Deletes a subcategory
   * @param subcategory The subcategory to be deleted
   */
  public deleteSubcategory(subcategory) {
    // console.log(subcategory.controls.name.value, subcategory.controls.id.value, this.category.id);
    this.subscriptions.push(this.categoryService.deleteSubcategory(this.category.id, subcategory.controls.id.value).subscribe());
    this.buildForm(this.category);
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Gets the category form to be saved
   */
  onSubmit(): void {
    if (this.categoryForm.get("name").value != null) {
      this.saveCategory();
      this.router.navigate(['/warehouse/category']);
    } else {
      // TODO print message to screen
      console.log('Category must have a name');
    }
  }

  /**
   * Saves the category
   */
  saveCategory() {
    this.category = Object.assign(this.category, this.categoryForm.value);

    if (this.newSelectedCategory) {
      // Saves a new category
      this.subscriptions.push(this.categoryService.createCategory(this.category).subscribe());       
    } else {
      // Update the category
      this.subscriptions.push(this.categoryService.updateCategory(this.category).subscribe());
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}
