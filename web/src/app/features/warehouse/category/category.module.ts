import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MaterialModule, AddOnsModule } from '@shared';
import { CategoryComponent } from './category.component';


const routes: Routes = [
  { path: '', component: CategoryComponent },
];

@NgModule({
  declarations: [CategoryComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    AddOnsModule,
  ]
})
export class CategoryModule { }
