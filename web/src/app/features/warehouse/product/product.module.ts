import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MaterialModule, AddOnsModule } from '@shared';
import { ProductComponent } from './product.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';

const routes: Routes = [
  { path: '', component: ProductComponent },
  {
    path: 'imageUpload',
    loadChildren: () =>
      import('./image-upload/image-upload.module').then((m) => m.ImageUploadModule),
  }
];

@NgModule({
  declarations: [ProductComponent, ImageUploadComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    AddOnsModule,
  ]
})
export class ProductModule { }