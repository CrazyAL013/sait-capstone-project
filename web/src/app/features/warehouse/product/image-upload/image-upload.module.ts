import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ImageUploadComponent } from './image-upload.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule, AddOnsModule } from '@shared';

const routes: Routes = [
  { path: '', component: ImageUploadComponent },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    AddOnsModule,
  ]
})
export class ImageUploadModule { }
