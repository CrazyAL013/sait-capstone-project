import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss']
})
export class ImageUploadComponent implements OnInit {
  public selectedFile: File;
  public images: File[] = [];

  constructor(public dialogRef: MatDialogRef<ImageUploadComponent>) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
  }


  /**
   * When a new fie is uploaded add it the array of uploaded images
   * @param event The file upload event 
   */
  onFileChanged(event) {
    for (let index = 0; index < event.target.files.length; index++) {
      this.selectedFile = event.target.files[index]    
      this.images.push(this.selectedFile);
    }
  }

  /**
   * When the upload botton is clicked this closes the dialog and passes the array of uploaded images to the product component
   */
  onUpload() {
    this.dialogRef.close(this.images);
  }

  /**
   * Remove file from upload list
   * @param fileNumber Number of the file to remove from array.
   */
  removeFile(fileNumber: number) {
    this.images.splice(fileNumber, 1);
  }

  /**
   * Close the dialog without passing any uplaoded images back to the procuct component
   */
  close() {
    this.dialogRef.close();
  }

}
