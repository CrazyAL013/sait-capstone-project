import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription, forkJoin } from 'rxjs';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '@env';

import * as uuid from 'uuid';

import { AppService } from '@data/services';
import { IProduct, Product } from '@data/models';
import { ProductService } from '@data/services';
import { ImageUploadComponent } from './image-upload/image-upload.component'
import { ConfirmDialogComponent, ConfirmDialogModel } from '@shared/components/confirm-dialog/confirm-dialog.component';
 
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public imageLocation: string = `${environment.apiEndpoint}${environment.images}`; 
  public data$: Observable<any>;
  public product: IProduct;
  public productImages: IProduct.IImage[] = [];
  public suppliers: IProduct.ISupplier[];
  public manufacturers: IProduct.IManufacturer[];
  private uid: string;
  public productForm: FormGroup;
  public suppliersForm: FormGroup;
  public manufacturersForm: FormGroup;
  public productItemGroup: FormGroup = this.formBuilder.group({});
  public productItemSpecGroup: FormGroup = this.formBuilder.group({});
  public loaded = false;
  public newProduct = false;
  public url: string;


  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private appService: AppService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private cdref: ChangeDetectorRef
  ) {
    this.spinner.show();

    // Get the 'uid' parameter and load the product
    this.subscriptions.push(
      this.route.paramMap.subscribe((paramMap) => {
        if (paramMap.has('uid')) {
          // Existing product
          this.uid = paramMap.get('uid');
        } else {
          // New product
          this.newProduct = true;
        }
      })
    );
  }

  ngOnInit(): void {
    if (this.newProduct) {
      this.product = new Product({id: uuid.v4(), productItems: []});
    }
    this.loadData();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  /**
   * Gets the currently selected image for the product or a placeholder
   * @param image The currently selected product image
   * @returns The image url for the currently selected image or a placeholder
   */
  getImage(image: IProduct.IImage): string {
    if (image) {
      return (`${environment.apiEndpoint}${environment.images}${image.url}`);
    } else {
      return (`/assets/images/placeholder-image.png`);
    }
  }

  /**
   * Loads the product from a passed in UID and all the suppliers, manufacturers, and product images
   */
  private loadData() {
    const subs: Observable<any>[] = [];
    subs.push(this.appService.getSuppliers());
    subs.push(this.appService.getManufacturers());
    if (! this.newProduct) subs.push(this.productService.getProductById(this.uid));
    if (! this.newProduct) subs.push(this.productService.getProductImages(this.uid));

    this.data$ = forkJoin(subs);
    this.subscriptions.push(this.data$.subscribe(results => {
      this.suppliers = results[0] as IProduct.ISupplier[];
      this.manufacturers = results[1] as IProduct.IManufacturer[];
      if (! this.newProduct) {
        this.product = results[2] as IProduct;
        this.productImages = results[3] as IProduct.IImage[];
      }

      this.buildForm();
      this.loaded = true;
    }));
  }

  /**
   * Build the product form
   */
  buildForm(): void {

    this.productForm = this.formBuilder.group({});

    this.productForm.addControl('supplier', new FormControl(this.product.supplier, [Validators.required]));
    this.productForm.addControl('manufacturer', new FormControl(this.product.manufacturer, [Validators.required]));
    this.productForm.addControl('name', new FormControl(this.product.name, [Validators.required]));
    this.productForm.addControl('description', new FormControl(this.product.description, [Validators.required]));
    this.productForm.addControl('keywords', new FormControl(this.product.keywords, [Validators.required]));
    this.productForm.addControl('image', new FormControl(this.product.image));
    this.productForm.addControl('productItems', this.formBuilder.array([]));

    if (!this.newProduct) {
      // If this isn't a new product add product item form elements for each product item
      for (let item of this.product.productItems) {
        this.addProductItem(item);
      }
    } else {
      // Add one empty product item, every product must have 1 product item.
      const productItem = new Product.ProductItem({ id:uuid.v4(), images:[], specifications:[] });
      this.product.productItems.push(productItem);
      this.addProductItem(productItem);
    }
  }

  /**
   * 
   */
  public get productItemsForm(): FormArray {
    return this.productForm.get('productItems') as FormArray;
  }

  /**
   * Builds a product item form for each product item
   * @param item The product item
   */
  public addProductItem(item: IProduct.IProductItem) {

    const productItemGroup: FormGroup = this.formBuilder.group({});
    productItemGroup.addControl('id', new FormControl(item.id));
    productItemGroup.addControl('display', new FormControl(item.display, [Validators.required]));
    productItemGroup.addControl('model', new FormControl(item.model, [Validators.required]));
    productItemGroup.addControl('sku', new FormControl(item.sku));
    productItemGroup.addControl('upc', new FormControl(item.upc));
    productItemGroup.addControl('name', new FormControl(item.name, [Validators.required]));
    productItemGroup.addControl('description', new FormControl(item.description, [Validators.required]));
    productItemGroup.addControl('specifications', this.formBuilder.array([]));
    productItemGroup.addControl('keywords', new FormControl(item.keywords, [Validators.required]));
    productItemGroup.addControl('images', new FormControl(item.images));
    productItemGroup.addControl('unitPrice', new FormControl(item.unitPrice, [Validators.required, Validators.min(0)]));
    productItemGroup.addControl('weight', new FormControl(item.weight, [Validators.required, Validators.min(0)]));
    productItemGroup.addControl('stockQuantity', new FormControl(item.stockQuantity, [Validators.required, Validators.min(0)]));
    productItemGroup.addControl('reorderLevel', new FormControl(item.reorderLevel, [Validators.required, Validators.min(0)]));
    productItemGroup.addControl('rating', new FormControl(item.averageRating));
    
    // Add product item specifications
    let formArray = productItemGroup.get('specifications') as FormArray;
    if (!this.newProduct) {
      if (item.specifications) {
        for (let specification of item.specifications) {
          this.addProductItemSpec(formArray, specification);
        }
      }
    }

    this.productItemsForm.push(productItemGroup);
  }

  /**
   * Builds a specification form for each specification for the product item
   * @param specification A specification from the product item
   */
  public addProductItemSpec(formArray: FormArray, specification: IProduct.IProductItem.ISpecification) {
    if (!specification) {
      specification = new Product.ProductItem.Specification();
    }

    const productItemSpecGroup: FormGroup = this.formBuilder.group({});
    productItemSpecGroup.addControl('name', new FormControl(specification.name, [Validators.required]));
    productItemSpecGroup.addControl('value', new FormControl(specification.value, [Validators.required]));
    
    formArray.push(productItemSpecGroup);
  }

  /**
   * Opens image upload dialog
   * @param position The product item numeber, undefined if the image is for the product
   */
  openUploadDialog(): void {
    const dialogRef = this.dialog.open(ImageUploadComponent, {
      width: '750px',
    });
      dialogRef.afterClosed().subscribe(images => {
        if (images != undefined) {
          // If dialog is closed and images have been uploaded
          images.forEach(image => {
            // TODO: Check if images are square, reject if not
            let type: string = image.name.split('.').pop();
            let uid = uuid.v4();
            let fileName: string = (uid + '.' + type);
            let imageData: IProduct.IImage;
            imageData = new Product.Image({id: uid, productId: this.product.id, type: type});
            
            // Upload the image to the API file system
            this.productService.uploadImages(image, fileName).subscribe(data => {
              // Add the image information to the database
              this.productService.addImages(imageData).subscribe(data => {
                this.subscriptions.push(this.productService.getProductImages(this.product.id).subscribe(result => {
                  // Update the local image gallery
                  this.productImages = result;
                  this.snackBar.open("Image(s) Uploaded", "Dismiss", {duration: 4000,});
              }))});
              
            });
          });                
        }
      });
    }

  /**
   * Submits the form and calls saveProduct
   */
  onSubmit(): void {
    if (this.productItemGroup != undefined) {
      // Stops a product from beign saved with no product items
      this.saveProduct();
      this.snackBar.open("Product successfully saved", "Dismiss", {duration: 4000,});
    } else {
      this.snackBar.open("A product must have at least one product item.", "Dismiss", {duration: 4000,});
    }
  }

  /**
   * Chanegs the selected product image
   * @param image The new selected product image
   */
  onSelectProductImage(image: IProduct.IImage) {
    this.product.image = image;
    this.productForm.controls['image'].setValue(image);
  }

  /**
   * Set a product item image to be selected or deselected
   * @param productItem The product item thats images are being changed
   * @param image The image that has been selected or deselected
   * @param index The index of the product item
   */
  onSelectProductItemImage(productItem: IProduct.IProductItem, image: IProduct.IImage, index: number) {
    if(!productItem.images) productItem.images = [];
    if(productItem.images.some(i => i.id === image.id)) {
      // Image exists in array: remove it
      productItem.images = productItem.images.filter( i => i.id !== image.id);
    } else {
      // Image does not exist in array: add it
      productItem.images.push(image);
    }

    const productItemsFormArray: FormArray = this.productForm.get('productItems') as FormArray;
    productItemsFormArray.controls[index].get('images').setValue(productItem.images);
  }

  /**
   * Creates a new product item
   */
  onAddProductItem() {
    const productItem = new Product.ProductItem({ id:uuid.v4(), images:[], specifications:[] });
    this.product.productItems.push(productItem);
    this.addProductItem(productItem);
  }

  /**
   * Creates a new specification for a product item
   * @param productItem The product item the specification is being added to
   * @param index The index of the product item
   */
  onAddSpecification(productItem: IProduct.IProductItem, index: number) {
    const productItemsFormArray: FormArray = this.productForm.get('productItems') as FormArray;
    const specificationsFormArray: FormArray = productItemsFormArray.controls[index].get('specifications') as FormArray;

    const specification: IProduct.IProductItem.ISpecification = new Product.ProductItem.Specification();
    productItem.specifications.push(specification);
    this.addProductItemSpec(specificationsFormArray, specification);
  }

  /**
   * Deletes a specification from a product item
   * @param productItem The product item the specification is being deleted from
   * @param specification The specification being deleted
   * @param productItemIndex The index of the product item
   * @param specificationIndex The index of the specification
   */
  onDeleteSpecification(productItem: IProduct.IProductItem, specification: IProduct.IProductItem.ISpecification, productItemIndex: number, specificationIndex: number) {
    const dialogData = new ConfirmDialogModel('Delete specification?','Are you sure?');
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {data: dialogData, maxWidth: '80%'});

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let result: boolean = dialogResult;
      if (result) {
        // Remove the specification
        productItem.specifications = productItem.specifications.filter( s => s !== specification);
        const productItemsFormArray: FormArray = this.productForm.get('productItems') as FormArray;
        const specificationsFormArray: FormArray = productItemsFormArray.controls[productItemIndex].get('specifications') as FormArray;
        specificationsFormArray.removeAt(specificationIndex);
      }
    });
  }

  /**
   * Checks if the image already exists in the product item images array
   * @param productItem The procuct item the image exists on
   * @param image The image being checked
   * @returns True if the image exists in the product item images array
   */
  imageExists(productItem: IProduct.IProductItem, image: IProduct.IImage) {
    return(productItem.images?.some(i => i.id === image.id));
  }

  /**
   * Compares the supplier trying to be set to the suppliers in the select to find a match
   * @param supplier1 The supplier trying to be set
   * @param supplier2 The supplier in the select list
   * @returns True if the suppliers match
   */
  compareSuppliers(supplier1: IProduct.ISupplier, supplier2: IProduct.ISupplier) {
    return (supplier1 && supplier2 && supplier1.id === supplier2.id);
  }

  /**
   * Compares the manufacturer trying to be set to the manufactures in the select to fing a match
   * @param manufacturer1 The manufacturer trying to be set
   * @param manufacturer2 The manufacturer in the select list
   * @returns True if the manufacturers match
   */
  compareManufacturers(manufacturer1: IProduct.IManufacturer, manufacturer2: IProduct.IManufacturer) {
    return (manufacturer1 && manufacturer2 && manufacturer1.id === manufacturer2.id);
  }

  /**
   * Clears the product form and adds back the required 1 product item.
   */
  clearForm() {
    this.productItemsForm.clear();
    this.addProductItem(undefined);
  }

  /**
   * Assigns the form data to the product object and saves or updates the product 
   */
  saveProduct() {
    this.product = Object.assign(this.product, this.productForm.value);
    if (this.newProduct) {
      // Saves a new product
      this.subscriptions.push(
        this.productService.createProduct(this.product).subscribe( data => {
          // If this was a new product we need to go back and re-assign the images to the product.
          // This is a bit of a hack because when the images are first uploaded they can't be assigned
          // to a product since it doesn't exist yet. Chicken and egg...
          this.productImages.forEach(image => {
            image.productId = this.product.id;
            this.productService.updateImage(image).subscribe();
          });        
        })
      );      
    } else {
      // Update the product
      this.subscriptions.push(
        this.productService.updateProduct(this.product).subscribe()
      );
    }

  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}
