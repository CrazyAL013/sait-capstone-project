import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AccountsComponent } from './accounts.component';
import { MaterialModule, AddOnsModule } from '@shared';

const routes: Routes = [
  { path: '', component: AccountsComponent },
  {
    path: 'userAccount/:id',
    loadChildren: () =>
      import('./user-account/user-account.module').then((m) => m.UserAccountModule),
  }
];

@NgModule({
  declarations: [AccountsComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MaterialModule,
    AddOnsModule,
  ]
})
export class AccountsModule { }
