import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import * as uuid from 'uuid';

import { IAccount } from '@data/models';
import { AccountService } from '@data/services';

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public account$: Observable<IAccount>;
  private account: IAccount;
  public accounts: IAccount[] = [];
  public loaded = false;
  public accountForm: FormGroup;
  private id: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private accountService: AccountService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder
  ) {
    this.route.paramMap.subscribe((paramMap) => {
      this.id = paramMap.get('id');
    });
   }

  ngOnInit(): void {
    this.accountForm = this.formBuilder.group({});
    this.loadAccount();
  }

  private loadAccount() {
    this.subscriptions.push((this.account$ = this.accountService.getAccount(this.id)).subscribe(data => {
      this.account = data;
      this.loaded = true;
      this.buildForm();
    }));
  }

  public buildForm(): void {
    this.accountForm.addControl('id', new FormControl(this.account.id));
    this.accountForm.addControl('username', new FormControl(this.account.username, [Validators.required]));
    this.accountForm.addControl('fullName', new FormControl(this.account.fullName, [Validators.required]));
    this.accountForm.addControl('preferredName', new FormControl(this.account.preferredName, [Validators.required]));
    this.accountForm.addControl('email', new FormControl(this.account.email, [Validators.required]));
  }

  onSubmit(): void {
    this.updateAccount();
    // this.router.navigate(['/warehouse/accounts/userAccount/', this.account.id]);
    this.router.navigate(['/warehouse/accounts']);
  }

  updateAccount() {
    this.account = Object.assign(this.account, this.accountForm.value);
    this.subscriptions.push(this.accountService.updateAccount(this.account).subscribe()
      );
  }

}
