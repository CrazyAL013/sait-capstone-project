import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserAccountComponent } from './user-account.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule, AddOnsModule } from '@shared';

const routes: Routes = [
  { path: '', component: UserAccountComponent },
];

@NgModule({
  declarations: [UserAccountComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    AddOnsModule,
  ]
})
export class UserAccountModule { }
