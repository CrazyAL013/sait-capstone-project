import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import * as uuid from 'uuid';

import { IAccount } from '@data/models';
import { AccountService } from '@data/services';

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'user-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public account$: Observable<IAccount[]>;
  private account: IAccount;
  public accounts: IAccount[] = [];
  public loaded = false;
  private uid: string;
  public accountForm: FormGroup;
  displayedColumns: string[] = ['id', 'username', 'fullName', 'preferredName', 'email'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private accountService: AccountService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder
  ) {
    this.spinner.show();
  }

  ngOnInit(): void {
    this.loadAccounts();
  }

  private loadAccounts() {
    this.subscriptions.push((this.account$ = this.accountService.getAccounts()).subscribe(data => {
      data.forEach((item: IAccount) => {
        console.log(item);
        this.accounts.push(item);
      })
      this.loaded = true;
    }));
  }
}
