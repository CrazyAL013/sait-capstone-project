import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CheckoutComponent } from './checkout.component';
import { AddOnsModule } from '@shared/addons.module';
import { MaterialModule } from '@shared/material.module';
import { SharedModule } from '@shared/shared.module';
import { ShippingComponent } from './shipping/shipping.component';
import { BillingComponent } from './billing/billing.component';
import { PaymentComponent } from './payment/payment.component';
import { CompleteComponent } from './complete/complete.component';
import { SquareComponent } from './payment/square/square.component';

const routes: Routes = [
  { path: '', component: CheckoutComponent },
];
@NgModule({
  declarations: [
    CheckoutComponent,
    ShippingComponent,
    BillingComponent,
    PaymentComponent,
    CompleteComponent,
    SquareComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    AddOnsModule,
    MaterialModule,
    SharedModule,
  ]
})
export class CheckoutModule { }
