import { Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { MatStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';

import * as uuid from 'uuid';
import { environment } from '@env';
import { CheckoutService, ServiceStatus, StepDirection } from '@core/services'
import { Square } from '@app/core/data';

@Component({
  providers: [CheckoutService],
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit, OnDestroy {
  @ViewChild('stepper', { static: false }) stepper: MatStepper;
  private subscriptions: Subscription[] = [];
  public ServiceStatus = ServiceStatus;
  public serviceStatus$: Observable<ServiceStatus>;
  private step$: Observable<StepDirection>;
  private squareNonce$: Observable<Square.INonce>;

  constructor(
    private checkoutService: CheckoutService,
    private snackBar: MatSnackBar,
    private changeDetectorRef: ChangeDetectorRef) { 
      this.checkoutService.start();
    }

  ngOnInit(): void {
    // Unique key for this checkout
    this.checkoutService.idempotencyKey = uuid.v4();

    // Subscribe to step forward/back notifications
    this.subscriptions.push((this.step$ = this.checkoutService.step).subscribe(direction => {
      switch (direction) {
        case StepDirection.Previous:
          this.stepper.previous();
          break;
        case StepDirection.Next:
          this.stepper.next();
          break;
      }
    }));

    // Wait for the checkout service to be ready
    this.subscriptions.push((this.serviceStatus$ = this.checkoutService.status).subscribe(status => {
      this.onStatusChange(status);
    }));

    // Subscribe to the nonce received event after Square payment button is clicked
    this.subscriptions.push((this.squareNonce$ = this.checkoutService.squareNonce$).subscribe(nonce => {
      this.onNonceReceived(nonce);
    }));
  }

  /**
   * Recieved a nonce from Square
   * 
   * @param nonce 
   * @returns 
   */
  onNonceReceived(nonce: Square.INonce) {
    if (nonce.errors) {
      if (!environment.production) {
        console.error('Square payment (requestCardNonce) encountered errors:');
        nonce.errors.forEach(function (error) {
          console.error('  ' + error.message);
        });
      }
      this.snackBar.open(nonce.errors[0].message, 'Error', { duration: 4000, });
      return;
    }
  }

  /**
   * Called on visiting the previous step
   */
  onPreviousStep() {
    this.checkoutService.previousStep();
  }

  /**
   * Called on visiting the next step
   */
  onNextStep() {
    this.checkoutService.nextStep();
  }

  /**
   * used to perform actions on specific steps
   * 
   * @param status The step
   */
  onStatusChange(status: ServiceStatus) {
    switch (status) {
      case ServiceStatus.Loading:
        break;
      case ServiceStatus.Shipping:
        this.changeDetectorRef.detectChanges();
        break;
      case ServiceStatus.Billing:
        break;
      case ServiceStatus.Payment:
        break;
      case ServiceStatus.Complete:
        break;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
    // The checkout service is persisted globally so we need to
    // reset the status ready for the next checkout
    this.checkoutService.checkoutStep(ServiceStatus.Shipping);
  }
}
