import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { IAccount, IAddress } from '@core/data';
import { CheckoutService } from '@core/services';

@Component({
  selector: 'app-checkout-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  account: IAccount = this.checkoutService.account;
  addresses: IAddress[] = this.checkoutService.addresses;

  constructor(private checkoutService: CheckoutService) { }

  ngOnInit(): void { }

  /**
   * Handles selecting an address
   * 
   * @param event 
   * @param address The selected address
   */
  onSelectAddress(event: any, address: IAddress): void {
    this.checkoutService.billingAddress = address;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}
