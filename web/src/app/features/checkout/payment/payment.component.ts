import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { CheckoutService } from '@core/services';
import { EasyPost } from '@core/data';

@Component({
  selector: 'app-checkout-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  subscriptions: Subscription[] = [];
  shippingOptions: EasyPost.IShippingOption[] = this.checkoutService.shippingOptions;
  pluralMapping = { 'day': { '=0': '', '=1': '(1 day)', 'other': '(# days)' } };

  constructor(public checkoutService: CheckoutService) { }

  ngOnInit(): void {}

  /**
   * Get the logo of various carriers
   * 
   * @param carrier 
   * @returns The selected carrier logo
   */
  getCarrierLogo(carrier: string) {
    let logo: string = '';
    if(carrier.toLowerCase().includes("fedex")) {
      logo = 'fedex.svg';
    } else if (carrier.toLowerCase().includes("ups")) {
      logo = 'ups.svg';
    } else if (carrier.toLowerCase().includes("canada")) {
      logo = 'canada-post.svg';
    } else if (carrier.toLowerCase().includes("purolator")) {
      logo = 'purolator.svg';
    } else if (carrier.toLowerCase().includes("dhl")) {
      logo = 'dhl.svg';
    }
    return logo;
  }

  /**
   * Handles selecting a shipping option
   * 
   * @param event 
   * @param option The selected shipping option
   */
  onSelectShipping(event: any, option: EasyPost.IShippingOption): void {
    this.checkoutService.shippingOption = option;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}
