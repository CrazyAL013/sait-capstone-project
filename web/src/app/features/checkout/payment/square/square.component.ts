import { Component, OnInit, ElementRef } from '@angular/core';

import { environment } from '@env';
import { CheckoutService } from '@core/services';
import { Square } from '@data';

declare var SqPaymentForm: any;

type ReceiveNonce = (errors: any, nonce: string, cardData: any) => void;

@Component({
  selector: 'app-square-payment',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss'],
})
export class SquareComponent implements OnInit {

  constructor(
    public checkoutService: CheckoutService,
    private elementRef: ElementRef) { }

  ngOnInit(): void {
    this.checkoutService.squarePaymentForm = new SqPaymentForm({
      applicationId: environment.squareApplicationId,
      inputClass: 'sq-input',
      autoBuild: false,
      // Customize the CSS for SqPaymentForm iframe elements
      inputStyles: [{
        color: getComputedStyle(document.body).getPropertyValue('--color-text').trim(),
        fontSize: '15px',
        lineHeight: '1.75em',
        padding: '10px',
        placeholderColor: '#a0a0a0',
        backgroundColor: 'transparent'
      }],
      // Initialize the credit card placeholders
      cardNumber: {
        elementId: 'sq-card-number',
        placeholder: 'Card Number'
      },
      cvv: {
        elementId: 'sq-cvv',
        placeholder: 'CVV'
      },
      expirationDate: {
        elementId: 'sq-expiration-date',
        placeholder: 'MM/YY'
      },
      postalCode: {
        elementId: 'sq-postal-code',
        placeholder: 'Post Code'
      },
      callbacks: {
        cardNonceResponseReceived: this.receiveNonce
      }
    });
    this.checkoutService.squarePaymentForm.build();
  }

  ngAfterViewInit(): void {
    // Remove the Square submit button default click handler and add our own instead
    let element: Element | null = this.elementRef.nativeElement.querySelector('#sq-creditcard');
    element.setAttribute('onClick', '');
    element.addEventListener('click', this.onPay.bind(this));
  }

  onPay(event) {
    this.checkoutService.nextStep();
    event.preventDefault();
  }

  /**
   * Callback function to handle receipt of the Square nonce
   */
  private receiveNonce = (errors: any, nonce: string, cardData: any): void => {
    // Pass the nonce to the checkout service
    this.checkoutService.receiveNonce({ errors, nonce, cardData } as Square.INonce);
  }

  ngOnDestroy() {
    let element: Element | null = this.elementRef.nativeElement.querySelector('#sq-creditcard');
    element.removeEventListener('click', this.onPay.bind(this));
  }
}
