import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { IAccount, IAddress } from '@core/data';
import { CheckoutService } from '@core/services';

@Component({
  selector: 'app-checkout-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  account: IAccount = this.checkoutService.account;
  addresses: IAddress[] = this.checkoutService.addresses;
  ready$: Observable<boolean>;
  
  constructor(private checkoutService: CheckoutService) { }

  ngOnInit(): void { }

  /**
   * Handles an address being selected
   * 
   * @param event 
   * @param address The selected address
   */
  onSelectAddress(event: any , address: IAddress): void {
    this.checkoutService.shippingAddress = address;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}
