import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { CheckoutService } from '@core/services';

@Component({
  selector: 'app-checkout-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.scss']
})
export class CompleteComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  paymentComplete$: Observable<boolean>;
  public order = this.checkoutService.order;

  constructor(public checkoutService: CheckoutService) { }

  ngOnInit(): void {
    //this.subscriptions.push((this.paymentComplete$ = this.checkoutService.paymentComplete$).subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }

}
