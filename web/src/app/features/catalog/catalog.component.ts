import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { ICatalogItem } from '@data/models';
import { ProductService } from '@data/services';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
  encapsulation: ViewEncapsulation.None 
})
export class CatalogComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  isLoading: Observable<boolean>;
  readonly defaultPageSize: number = 10;
  readonly pageSizeOptions: number[] = [10, 25, 100];
  catalogItems$: Observable<ICatalogItem[]>;
  productCount: number = 0;
  subscriptions: Subscription[] = [];
  categoryUid: string = null;
  search: string = null;

  constructor(
    private route: ActivatedRoute, 
    private productService: ProductService,
    private snackBar: MatSnackBar,
    private spinner: NgxSpinnerService
  ) {
    // Show the spinner
    this.spinner.show();

    // Get the parameters
    this.subscriptions.push(this.route.paramMap.subscribe((paramMap) => {
      this.categoryUid = paramMap.get('category');
      this.search = paramMap.get('search');
    }));
  }

  ngOnInit() {
    // Request the initial page of products
    this.loadProducts(1, this.defaultPageSize);
  }

  ngAfterViewInit() {
    // Setup the (Material) paginator
    // this.subscriptions.push(
    //   this.paginator.page
    //     .pipe(
    //       tap(() => {
    //         this.loadProducts(
    //           this.paginator.pageIndex + 1,
    //           this.paginator.pageSize
    //         );
    //       })
    //     )
    //     .subscribe()
    // );
  }

  /**
   * Load all the products from the database 
   * @param num The page number
   * @param limit The limit of product per page
   */
  loadProducts(num: number, limit: number) {
    // Fetch a page of products
    if (this.search && this.categoryUid && this.categoryUid != "all") {
      this.catalogItems$ = this.productService.getCatalogByCategory(this.categoryUid, this.search);
    } else if (this.categoryUid && this.categoryUid != "all") {
      // In this case the categoryUid is the UID of a sub category
      this.catalogItems$ = this.productService.getCatalogBySubCategory(this.categoryUid);
    } else if (this.search) {
      this.catalogItems$ = this.productService.getCatalogBySearch(this.search);
    } else {
      this.catalogItems$ = this.productService.getCatalogAll();
    }
    this.subscriptions.push(this.catalogItems$.subscribe(data => {
      if (data.length === 0) {
        this.snackBar
          .open(`No results found.`, 'Search', { duration: 3000, verticalPosition: 'top' })
          .afterDismissed().subscribe( info => {
            (this.catalogItems$ = this.productService.getCatalogAll()).subscribe();
          });
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }
}
