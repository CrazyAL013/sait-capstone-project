import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogComponent } from './catalog.component';
import { CatalogItemComponent } from './catalog-item/catalog-item.component';
import { MaterialModule } from '@shared/material.module';
import { AddOnsModule } from '@shared/addons.module';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: CatalogComponent },
];
@NgModule({
  declarations: [
    CatalogComponent,
    CatalogItemComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule, 
    MaterialModule,
    AddOnsModule,
  ],
  exports: [
    CatalogComponent,
    CatalogItemComponent,
  ]

})
export class CatalogModule { }
