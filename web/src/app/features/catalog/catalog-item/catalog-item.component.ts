import { Component, Input, ViewEncapsulation } from '@angular/core';

import { ICatalogItem, IProduct } from '@data/models';
import { environment } from '@env';

@Component({
  selector: 'app-catalog-item',
  templateUrl: './catalog-item.component.html',
  styleUrls: ['./catalog-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CatalogItemComponent {
  @Input() catalogItem: ICatalogItem;

  constructor() { }

  /**
   * Gets the imaegs to be displayed on the catalog page
   * @param image The product image to be displayed
   * @returns The product image or a placeholder
   */
  getImage(image: IProduct.IImage): string {
    if (image) {
      return (`${environment.apiEndpoint}${environment.images}${image.url}`);
    } else {
      return (`/assets/images/placeholder-image.png`);
    }
  }

}
