import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { MaterialModule } from '@shared/material.module';
import { SharedModule } from 'app/shared';
import { CatalogModule } from '@features/catalog/catalog.module';

const routes: Routes = [
  { path: '', component: HomeComponent },
];
@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule, 
    MaterialModule, 
    SharedModule,
    CatalogModule
  ]
})
export class HomeModule {}
