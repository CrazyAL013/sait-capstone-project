export enum ViewportSizeEnum {
  XSmall = '(max-width: 599.98px)',
  Small = '(min-width: 600px) and (max-width: 959.98px)',
  Medium = '(min-width: 960px) and (max-width: 1279.98px)',
  Large = '(min-width: 1280px) and (max-width: 1919.98px)',
  XLarge = '(min-width: 1920px)',
  Handset = '(max-width: 599.98px) and (orientation: portrait), ' + '(max-width: 959.98px) and (orientation: landscape)',
  Tablet = '(min-width: 600px) and (max-width: 839.98px) and (orientation: portrait), ' + '(min-width: 960px) and (max-width: 1279.98px) and (orientation: landscape)',
  Web = '(min-width: 840px) and (orientation: portrait), ' + '(min-width: 1280px) and (orientation: landscape)',
  HandsetPortrait = '(max-width: 599.98px) and (orientation: portrait)',
  TabletPortrait = '(min-width: 600px) and (max-width: 839.98px) and (orientation: portrait)',
  WebPortrait = '(min-width: 840px) and (orientation: portrait)',
  HandsetLandscape = '(max-width: 959.98px) and (orientation: landscape)',
  TabletLandscape = '(min-width: 960px) and (max-width: 1279.98px) and (orientation: landscape)',
  WebLandscape = '(min-width: 1280px) and (orientation: landscape)'
};

// See database 'Role' lookup table
export enum UserRole {
  Useer = '2c1b718b-9da0-40f5-9b3c-061c2dde4396',
	Admin = '99052f08-1316-4de4-90f7-f85145e8af97',
	Super = 'fbbde3f4-721b-40d9-9819-aeeffb152617'
};

// See database 'OrderStatus' lookup table
export enum OrderStatus {
	New = '41509b3c-00fd-46ba-a389-ccd9f184a37d',
	InProgress = 'ce71a44e-6199-13eb-ae93-0242ac130062',
	Shipped = 'dd6b54f1-6199-17eb-ae93-0242ac140002',
	Cancelled = '014d3aae-619a-18eb-ae93-0242ac130502',
	Delayed = '06a23467-7ed7-2b25-809f-fff8c91342dd'
};

// See database 'PaymentProcessor' lookup table
export enum PaymentProcessor {
  Square = '8fe47cc2-6ff2-4918-8820-73c3bcf5359a'
}

// See database 'DiscountType' lookup table
export enum DiscountType {
  Shipping = '4f64e5f8-20a7-41d5-a9b5-edc777bdfec1',
	Net = 'be210eba-8a0d-42a5-a6f4-972dc6887307'
}