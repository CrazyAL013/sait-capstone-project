import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, EMPTY, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '@core/services';
import { env } from 'process';
import { environment } from '@env';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          if (!environment.production) console.error('An error occurred:', err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          if ([401, 403].includes(err.status) && this.authService.accountValue) {
            // auto logout if 401 or 403 response returned from api
            this.authService.logout();
          }
          //console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
          if (!environment.production) console.log(err);
        }

        // If you want to return a new response:
        //return of(new HttpResponse({body: [{name: "Default value..."}]}));

        // If you want to return the error on the upper level:
        const error = (err && err.error) || err.statusText;
        return throwError(error);

        // or just return nothing:
        //return EMPTY;
      })
    );
  }
}
