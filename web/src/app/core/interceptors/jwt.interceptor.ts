import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env';
import { AuthService } from '@core/services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to the api url
    const account = this.authService.accountValue;
    const isApiUrl = request.url.startsWith(environment.apiEndpoint);
    if (isApiUrl) {
      request = request.clone({
        setHeaders: { Authorization: `Bearer ${account?.accessToken}` },
      });
    }

    return next.handle(request);
  }
}
