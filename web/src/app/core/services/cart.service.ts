import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import * as uuid from 'uuid';

import { AuthService, AuthEventEnum } from '@core/services/auth.service'
import { CartService as CartDataService } from '@core/data/services/cart.service';
import { ProductService } from '@core/data/services/product.service';
import { IProduct, ICartItem, CartItem } from '@data/models';

interface ISavedCartItem { id: string, quantity: number }

@Injectable({
  providedIn: 'root'
})
export class CartService implements OnDestroy {
  readonly storageKey: string = 'cart';
  private cartItems: Map<string, ICartItem> = new Map<string, ICartItem>();
  private subscriptions: Subscription[] = [];
  private _ready: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private authService: AuthService,
    private productService: ProductService,
    private cartService: CartDataService
  ) {
    this.loadSavedCart();

    // Listen for user authentication events
    this.subscriptions.push(this.authService.account.subscribe(account$ => {
      if (this.authService.lastAuthEvent === AuthEventEnum.Login) {
        // User logged in: load the saved cart (if any)
        this.loadSavedCart();
      } else if (this.authService.lastAuthEvent === AuthEventEnum.Logout) {
        // User logged out: clear local cart
        this.clearCart();
      }
    }));
  }

  get ready(): Observable<boolean> {
    return this._ready as Observable<boolean>;
  }

  /**
   * Get all cart items
   */
  public get Items(): Map<string, ICartItem> {
    return this.cartItems;
  }

  /**
   * Get size of cart
   */
  public get totalItems(): number {
    return this.cartItems.size;
  }

  /**
   * Add a new item to the cart
   * 
   * @param product The Product to be added
   * @param productItem The specific product item to be added
   * @param quantity The quantity of the new item
   */
  public addItem(product: IProduct, productItem: IProduct.IProductItem, quantity: number = 0) {
    if (this.cartItems.has(productItem.id)) {
      // Product already in cart
      if (quantity === 0) {
        // No quantity specified: increase quantity by 1
        this.cartItems.get(productItem.id).quantity += 1;
      } else {
        // Otherwise update the quantity
        if ((this.cartItems.get(productItem.id).quantity + quantity) >= productItem.stockQuantity) {
          //new quantity exceeds stock level
          this.cartItems.get(productItem.id).quantity = productItem.stockQuantity;
        } else {
          //new quantity does not exceed stock level
          this.cartItems.get(productItem.id).quantity += quantity;
        }
      }
    } else {
      // Create a new cart item
      const cartItem: ICartItem = new CartItem({
        id: uuid.v4(),
        quantity: (quantity === 0) ? 1 : quantity,
        productId: product.id,
        productName: product.name,
        productImage: product.image,
        productItem: new CartItem.ProductItem({
          id: productItem.id,
          name: productItem.name,
          model: productItem.model,
          weight: productItem.weight,
          unitPrice: productItem.unitPrice,
          stockQuantity: productItem.stockQuantity,
          reorderLevel: productItem.reorderLevel
        })
      });
      this.cartItems.set(productItem.id, cartItem);
    }
    this.saveCart();
  }

  /**
   * Update the quantity of an item
   * 
   * @param productItemId The item whose quantity will be updated
   * @param quantity The new quantity
   */
  public updateQuantity(productItemId: string, quantity: number) {
    if (this.cartItems.has(productItemId)) {
      this.cartItems.get(productItemId).quantity = quantity;
      this.saveCart();
    }
  }

  /**
   * Remove item from cart
   * 
   * @param productItemId The item to be removed
   */
  public removeItem(productItemId: string) {
    if (this.cartItems.has(productItemId)) {
      this.cartItems.delete(productItemId);
      this.saveCart();
    }
  }

  /**
   * Calculate total cost of cart
   */
  public get totalCost(): number {
    let total = 0;
    this.cartItems.forEach((item => {
      total += (item.productItem.unitPrice * item.quantity);
    }));
    return total;
  }

  /**
   * Clears the local cart - done on logout
   */
  public clearCart() {
    this.cartItems.clear();
    localStorage.removeItem(this.storageKey);
  }

  /**
  * Logged-in users may have a saved cart that needs to
  * be combined with the current cart items.
  */
  private loadSavedCart() {
    // Load the saved cart from local storage (if one exists)
    let items: Map<string, ISavedCartItem> = new Map(JSON.parse(localStorage.getItem(this.storageKey)));

    // Load the saved cart from the database (if one exists and user is loggedin)
    if (this.authService.isLoggedIn) {
      // Get saved cart from database
      const dbItems: Map<string, ISavedCartItem> = new Map(JSON.parse(this.authService.accountValue.cart));

      // Combine local storage and saved cart
      items = new Map([...items, ...dbItems]);

      // Delete local storage cart
      localStorage.removeItem(this.storageKey);
    }

    // Populate the cart items with their full data
    if (items.size > 0) {
      const ids: string[] = [];
      items.forEach((value: ISavedCartItem, key: string) => {
        ids.push(key);
      });
      this.subscriptions.push(this.cartService.getCartItems(ids).subscribe(data => {
        data.forEach((item: ICartItem) => {
          // Transfer the saved Id and Quantity values to the new object
          item.id = items.get(item.productItem.id).id;
          item.quantity = items.get(item.productItem.id).quantity;
          this.cartItems.set(item.productItem.id, item);
        });
        // Save final cart to database
        this.saveCart();
        this._ready.next(true);
      }));
    } else {
      this._ready.next(true);
    }
  }

  /**
   * Saves the current cart locally or to the database
   */
  private saveCart() {
    const items: Map<string, ISavedCartItem> = new Map<string, ISavedCartItem>();
    this.cartItems.forEach((item => {
      const savedCartitem: ISavedCartItem = <ISavedCartItem>{ id: item.id, quantity: item.quantity };
      items.set(item.productItem.id, savedCartitem);
    }));
    if (this.authService.isLoggedIn) {
      // Logged in: save cart to database
      this.authService.accountValue.cart = JSON.stringify([...items]);
      this.cartService.updateCart(this.authService.accountValue).subscribe();

    } else {
      // Not logged in: save cart to local storage
      localStorage.setItem(this.storageKey, JSON.stringify([...items]));
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}
