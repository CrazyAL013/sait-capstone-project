import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, Observable, Subject, BehaviorSubject, forkJoin } from 'rxjs';

import * as uuid from 'uuid';
import { environment } from '@env';
import { AuthService } from '@core/services/auth.service';
import { CartService } from '@core/services/cart.service';
import { AppService, AccountService, PaymentService, ShippingService } from '@data/services';
import { ScriptLoaderService } from './script-loader.service';
import { OrderStatus, DiscountType, PaymentProcessor } from '@core/enums';
import {
  IAccount,
  ICartItem,
  IAddress,
  EasyPost,
  Square,
  IOrder, Order,
  IPayment, Payment,
  Shipment,
  ILookup
} from '@app/core/data';

export enum ServiceStatus {
  Loading = -1,
  Shipping = 0,
  Billing = 1,
  Payment = 2,
  Complete = 3
}

export enum StepDirection {
  Previous,
  Next,
}

/**
 * Note: This service would normally be global but due to a late-breaking issue 
 * where a customer with no addresses would add an address but the service would
 * never see it because addresses are cached, we had to modify the service to be
 * created (and destroyed) where it is used (Checkout module) rather than injecting 
 * it into 'root' as normal. Ideally we would restructure the service to allow 
 * addresses to be reloaded if something changes but there's no time to do that in 
 * this version.
 **/
@Injectable()
export class CheckoutService implements OnDestroy {
  private subscriptions: Subscription[] = [];
  private _status: BehaviorSubject<ServiceStatus> = new BehaviorSubject(ServiceStatus.Loading);
  private _step: Subject<StepDirection> = new Subject<StepDirection>();
  private _shippingOptions$: Observable<EasyPost.IShippingOption[]>;
  private _squareNonce: Subject<Square.INonce> = new Subject<Square.INonce>();
  private _currentStatus: number = 1;
  private _discountTypes: ILookup.IDiscountType[];
  private _orderStatuses: ILookup.IOrderStatus[];
  private _paymentProcessors: ILookup.IPaymentProcessor[];
  private _addresses: IAddress[];
  private _shippingAddress: IAddress = this.account.address;
  private _billingAddress: IAddress = this.account.address;
  private _shippingOption: EasyPost.IShippingOption;
  private _beforeTaxTotal: number;
  private _taxRate: number;
  private _taxTotal: number;
  private _grandTotal: number;
  private _paymentCard: IPayment.ICard;
  private _paymentId: string;
  private _order: IOrder;
  private _enablePayment: boolean = false;
  private _idempotencyKey: string = uuid.v4();
  public squarePaymentForm: any;
  public shippingOptions: EasyPost.IShippingOption[];

  constructor(
    private accountService: AccountService,
    private authService: AuthService,
    private paymentService: PaymentService,
    private shippingService: ShippingService,
    private cartService: CartService,
    private appService: AppService,
    private scriptLoaderService: ScriptLoaderService,
  ) {
    this.shippingAddress = this.account.address;
    this.billingAddress = this.account.address;

    const data$: Observable<any[]> = forkJoin([
      this.accountService.getAddresses(this.account),
      this.appService.getDiscountTypes(),
      this.appService.getOrderStatuses(),
      this.appService.getPaymentProcessors(),
      this.scriptLoaderService.loadScript('square-sandbox'),
    ]);
    this.subscriptions.push(data$.subscribe(results => {
      this._addresses = results[0] as IAddress[];
      this._discountTypes = results[1] as ILookup.IDiscountType[];
      this._orderStatuses = results[2] as ILookup.IOrderStatus[];
      this._paymentProcessors = results[3] as ILookup.IPaymentProcessor[];
      // Signal observers that we're ready
      // this.checkoutStep(ServiceStatus.Shipping);
    }));
  }

  public start(): void {
    this.subscriptions.push(this.accountService.getAddresses(this.account).subscribe( data => {
      this._addresses = data as IAddress[];
      // Signal observers that we're ready
      this.checkoutStep(ServiceStatus.Shipping);
    }));   
  }

  get status(): Observable<ServiceStatus> {
    return this._status as Observable<ServiceStatus>;
  }

  get step(): Observable<StepDirection> {
    return this._step as Observable<StepDirection>;
  }

  get squareNonce$(): Observable<Square.INonce> {
    return this._squareNonce;
  }

  get cartItems(): Map<string, ICartItem> {
    return this.cartService.Items;
  }

  get account(): IAccount {
    return this.authService.accountValue;
  }

  get currencyCode(): string {
    return this.billingAddress.country.currencyCode;
  }

  get currencySymbol(): string {
    return this.billingAddress.country.currencySymbol;
  }

  get addresses(): IAddress[] {
    return this._addresses;
  }

  get shippingAddress(): IAddress {
    return this._shippingAddress;
  }

  get billingAddress(): IAddress {
    return this._billingAddress;
  }

  get shippingOption(): EasyPost.IShippingOption {
    return this._shippingOption;
  }

  get subtotal(): number {
    return this.cartService.totalCost;
  }

  get discount(): number {
    //TODO V2: Discount not implemented
    return 0.00;
  }

  get taxTotal(): number {
    return this._taxTotal;
  }

  get beforeTaxTotal(): number {
    return this._beforeTaxTotal;
  }

  get grandTotal(): number {
    return this._grandTotal;
  }

  get paymentId(): string {
    return this._paymentId;
  }

  get order(): IOrder {
    return this._order;
  }

  get enablePayment(): boolean {
    return this._enablePayment;
  }

  set paymentId(id: string) {
    this._paymentId = id;
  }

  set idempotencyKey(key: string) {
    this._idempotencyKey = key;
  }

  set shippingAddress(address: IAddress) {
    this._shippingAddress = address;
  }

  set billingAddress(address: IAddress) {
    this._billingAddress = address;
    // Calculate the tax rate when billing address changes
    this._taxRate = ((address?.country.tax + address?.region.tax) / 100);
  }

  set shippingOption(option: EasyPost.IShippingOption) {
    this._shippingOption = option;
    this._enablePayment = true;
    // Recalculate totals when shipping option changes
    this._beforeTaxTotal = Math.round(((this.subtotal + this._shippingOption?.rate + this.discount) + Number.EPSILON) * 100) / 100;
    this._taxTotal = Math.round(((this._beforeTaxTotal * (this._taxRate)) + Number.EPSILON) * 100) / 100;
    this._grandTotal = Math.round(((this._beforeTaxTotal + this._taxTotal) + Number.EPSILON) * 100) / 100;
  }

  public paymentProcessor(id: string): ILookup.IPaymentProcessor {
    return this._paymentProcessors.find(item => item.id === id);
  }

  public orderStatus(id: string): ILookup.IOrderStatus {
    return this._orderStatuses.find(item => item.id === id);
  }

  public discountType(id: string): ILookup.IDiscountType {
    return this._discountTypes.find(item => item.id === id);
  }

  public previousStep() {
    this._step.next(StepDirection.Previous);
    this.checkoutStep(this._currentStatus - 1)
  }

  public nextStep() {
    this._step.next(StepDirection.Next);
    this.checkoutStep(this._currentStatus + 1)
  }

  public checkoutStep(status: ServiceStatus) {
    switch (status) {
      case ServiceStatus.Shipping:
        this._currentStatus = status;
        this._status.next(ServiceStatus.Shipping);
        break;
      case ServiceStatus.Billing:
        this._currentStatus = status;
        this._status.next(ServiceStatus.Billing);
        break;
      case ServiceStatus.Payment:
        this._currentStatus = status;
        this.loadShippingOptions();
        break;
      case ServiceStatus.Complete:
        this._currentStatus = status;
        this.squarePaymentForm.requestCardNonce();
        break;
    }
  }

  public receiveNonce(nonce: Square.INonce) {
    this._squareNonce.next(nonce);
    if (!nonce.errors) {
      // Save the card info
      const card: IPayment.ICard = new Payment.Card({
        brand: nonce.cardData.card_brand,
        last4Digits: nonce.cardData.last_4,
        expiryMonth: nonce.cardData.exp_month,
        expiryYear: nonce.cardData.exp_year,
        postalCode: nonce.cardData.billing_postal_code
      });
      this._paymentCard = card;

      // Create an order
      this._order = this.createOrder();

      // Create a Square payment
      const payment: Square.IPayment = new Square.Payment({
        nonce: nonce.nonce,
        idempotencyKey: this._idempotencyKey,
        locationId: environment.squareLocationId,
        amountMoney: new Square.Payment.Money({
          amount: (this._grandTotal * 100),
          currency: this.currencyCode
        }),
        order: this._order
      });

      // Send the payment to the API and get back the payment id
      this.subscriptions.push(this.paymentService.createSquarePayment(payment).subscribe(data => {
        this.paymentId = data.paymentId;
        this._squareNonce.next(nonce);
        this._status.next(ServiceStatus.Complete);
        this.cartService.clearCart();
      }));
    }
  }

  private loadShippingOptions() {
    this._shippingOption = null;
    this._enablePayment = false;
    this._shippingOptions$ = this.shippingService.getShippingCosts(this.shippingAddress);
    this.subscriptions.push(this._shippingOptions$.subscribe(data => {
      this.shippingOptions = data;
      this._status.next(ServiceStatus.Payment);
    }));
  }

  private createOrder(): IOrder {
    // Create the order items from the cart items
    const orderItems: IOrder.IOrderItem[] = [];
    this.cartItems.forEach((cartItem: ICartItem) => {
      const orderItem: IOrder.IOrderItem = new Order.OrderItem({
        id: uuid.v4(),
        quantity: cartItem.quantity,
        unitPrice: cartItem.productItem.unitPrice,
        productId: cartItem.productId,
        productName: cartItem.productName,
        productImage: cartItem.productImage,
        productItem: new Order.ProductItem({
          id: cartItem.productItem.id,
          name: cartItem.productItem.name,
          model: cartItem.productItem.model
        })
      })
      orderItems.push(orderItem);
    });

    // Create a new order
    const order: IOrder = new Order({
      id: uuid.v4(),
      account: this.account,
      status: this.orderStatus(OrderStatus.New),
      billingAddress: this.billingAddress,
      deliveryAddress: this.shippingAddress,
      discount: null, //TODO V2: Implement Discounts
      paymentProcessor: this.paymentProcessor(PaymentProcessor.Square),
      shipment: new Shipment({
        id: uuid.v4(),
        rateId: this.shippingOption.id,
        shipmentId: this.shippingOption.shipmentId,
        carrier: this.shippingOption.carrier,
        service: this.shippingOption.service,
        rate: this.shippingOption.rate,
        deliveryDays: this.shippingOption.deliveryDays
      }),
      subTotal: this.subtotal,
      beforeTaxTotal: this._beforeTaxTotal,
      shipTotal: this.shippingOption.rate,
      taxTotal: this._taxTotal,
      discountTotal: 0, // TODO V2: Implement Discounts
      grandTotal: this._grandTotal,
      paymentCardBrand: this._paymentCard.brand,
      paymentCardLast4Digits: this._paymentCard.last4Digits,
      paymentCardExpiryMonth: this._paymentCard.expiryMonth,
      paymentCardExpiryYear: this._paymentCard.expiryYear,
      items: orderItems
    });

    return order;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}
