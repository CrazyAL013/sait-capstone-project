import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as uuid from 'uuid';

import { environment } from '@env';
import { IAccount } from '@data/models';

export enum AuthEventEnum {
  None,
  Login,
  Logout,
  RefreshToken,
  ChangePassword,
  ResetEmail,
  Register,
  AdminLogin
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  public account: Observable<IAccount>;
  private accountSubject: BehaviorSubject<IAccount>;
  private refreshTokenTimeout: any;
  private lastEvent: AuthEventEnum = AuthEventEnum.None;

  constructor(private router: Router, private http: HttpClient) {
    this.accountSubject = new BehaviorSubject<IAccount>(null);
    this.account = this.accountSubject.asObservable();
  }

  /**
   * Gets the last auth event that occured
   */
  public get lastAuthEvent(): AuthEventEnum {
    return (this.lastEvent);
  }

  /**
   * Check if a user is logged in
   */
  public get isLoggedIn(): boolean {
    return (this.accountSubject.value != null && this.accountSubject.value.accessToken != null);
  }

  /**
   * Check if user is an admin
   */
  public get isAdmin(): boolean {
    const admin: boolean = (this.accountValue && (this.accountValue.roles.some(role => role.name === 'admin' || role.name === 'super')));
    return (this.accountSubject.value != null && this.accountSubject.value.accessToken != null && admin);
  }

  /**
   * Get the account data
   */
  public get accountValue(): IAccount {
    return this.accountSubject.value;
  }

  /**
   * Register a new user
   * 
   * @param username The entered username
   * @param password The entered password
   * @param fullName The entered full name
   * @param preferredName The entered preferred name
   */
  register(username: string, password: string, fullName: string, preferredName: string) {
    return this.http
      .post<any>(
        `${environment.apiEndpoint}/account`,
        { "id": uuid.v4(), "username": username, "email": username, "password": password, "fullName": fullName, "preferredName": preferredName }
      )
      .pipe(
        map((account) => {
          this.lastEvent = AuthEventEnum.Register;
          this.accountSubject.next(null);
        })
      );
  }

  /**
   * Reset the users email
   * 
   * @param username The username of the user
   */
  resetEmail(username: string) {
    //TODO V2:
    return this.http
      .post<any>(
        `${environment.apiEndpoint}/account/`,
        { username },
        { withCredentials: true }
      )
      .pipe(
        map((account) => {
          this.lastEvent = AuthEventEnum.ResetEmail;
          this.accountSubject.next(null);
          return account;
        })
      );
  }

  /**
   * Change the logged in users password
   * 
   * @param username The users username
   * @param currentPassword The users current password
   * @param newPassword The users new password
   */
  changePassword(username: string, currentPassword: string, newPassword: string) {
    return this.http
      .post<any>(
        `${environment.apiEndpoint}/account/changePassword`,
        { "username": username, "password": currentPassword, "newPassword": newPassword },
        { withCredentials: true }
      )
      .pipe(
        map((account) => {
          this.lastEvent = AuthEventEnum.ChangePassword;
          this.accountSubject.next(null);
          this.router.navigate(['/auth/login']);
          return account;
        })
      );
  }

  /**
   * Try to login a user
   * 
   * @param username The entered username
   * @param password The entered password
   */
  login(username: string, password: string) {
    if (this.isLoggedIn) this.logout();
    return this.http
      .post<IAccount>(
        `${environment.apiEndpoint}/account/login/`,
        { "username": username, "password": password },
        { withCredentials: true }
      )
      .pipe(
        map((account) => {
          this.lastEvent = AuthEventEnum.Login;
          this.accountSubject.next(account);
          this.startRefreshTokenTimer();
          return account;
        })
      );
  }

  /**
   * Logout the user
   */
  logout() {
    this.http
      .post<IAccount>(
        `${environment.apiEndpoint}/account/logout/`,
        this.accountValue,
        { withCredentials: true }
      )
      .subscribe();
    this.stopRefreshTokenTimer();
    this.lastEvent = AuthEventEnum.Logout;
    this.accountSubject.next(null);
    this.router.navigate(['/']);
  }

  /**
   * Refresh the access token
   * 
   */
  refreshToken() {
    return this.http
      .post<any>(
        `${environment.apiEndpoint}/account/refreshToken/`,
        {},
        { withCredentials: true }
      )
      .pipe(
        map((account) => {
          this.lastEvent = AuthEventEnum.RefreshToken;
          this.accountSubject.next(account);
          this.startRefreshTokenTimer();
          return account;
        })
      );
  }

  /**
   * Start the refresh token timer
   */
  private startRefreshTokenTimer() {
    // parse json object from base64 encoded jwt token
    const token = JSON.parse(atob(this.accountValue.accessToken.split('.')[1]));

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(token.exp * 1000);
    const timeout = expires.getTime() - Date.now() - 60 * 1000;
    this.refreshTokenTimeout = setTimeout(
      () => this.refreshToken().subscribe(),
      timeout
    );
  }

  /**
   * Stop the refresh token timer
   */
  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }
}
