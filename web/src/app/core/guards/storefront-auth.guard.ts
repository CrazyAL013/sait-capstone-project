import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AuthService } from '@core/services/auth.service';

import { DialogData, LoginAction, LoginComponent } from '@shared/components/login/login.component';

@Injectable({ providedIn: 'root' })
export class StorefrontAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private dialog: MatDialog
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const dialogConfig = new MatDialogConfig();
    const account = this.authService.accountValue;

    // Pre login-check validation
    switch (state.url) {
      case '/store/cart/checkout':
        // Navigation to checkout must only come from the cart
        if (this.router.url !== "/store/cart") {
          this.router.navigate(["/store/cart"]);          
        }
    }

    // Already logged in?
    if (account) return true;

    // Specific handling for certain routes
    switch (state.url) {
      case '/auth/login':
        state.url = this.router.url;
        break;
      case '/auth/register':
      case '/auth/reset-password':
        // No login required for these actions
        return true;
    }

    // Show the login dialog
    dialogConfig.data = new DialogData(LoginAction.Login, state.url);
    dialogConfig.disableClose = true;
    const dialogRef = this.dialog.open(LoginComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((dialogResult) => {
      return Boolean(dialogResult);
    });
  }
}
