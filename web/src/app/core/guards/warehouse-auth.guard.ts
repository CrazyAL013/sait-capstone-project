import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AuthService } from '@core/services/auth.service';

import {
  DialogData,
  LoginAction,
  LoginComponent,
} from '@shared/components/login/login.component';

@Injectable({ providedIn: 'root' })
export class WarehouseAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private dialog: MatDialog
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const dialogConfig = new MatDialogConfig();
    const account = this.authService.accountValue;

    const admin: boolean = (account && (account.roles.some(role => role.name === 'admin' || role.name === 'super')));
    if (account && admin) {
      // Already logged in
      return true;
    } else {
      // Show the login dialog
      dialogConfig.data = new DialogData(LoginAction.AdminLogin, state.url);
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(LoginComponent, dialogConfig);
      dialogRef.afterClosed().subscribe((dialogResult) => {
        return Boolean(dialogResult);
      });
    }
  }
}
