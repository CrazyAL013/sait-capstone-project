import { Injectable } from '@angular/core';

export class MediaState {
  XSmall: boolean = false;
  Small: boolean = false;
  Medium: boolean = false;
  Large: boolean = false;
  XLarge: boolean = false;
  Handset: boolean = false;
  Tablet: boolean = false;
  Web: boolean = false;
  HandsetPortrait: boolean = false;
  TabletPortrait: boolean = false;
  WebPortrait: boolean = false;
  HandsetLandscape: boolean = false;
  TabletLandscape: boolean = false;
  WebLandscape: boolean = false;
}

@Injectable({
  providedIn: 'root'
})
export class Globals {
  mediaState: MediaState;
  
  constructor() {
    this.mediaState = new MediaState();
  }
}
