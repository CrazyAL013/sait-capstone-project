import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '@env';
import { Square } from '@data/models';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  constructor(private readonly httpClient: HttpClient) {}

  /**
   * Creates a new Square payment
   * 
   * @param payment The payment to be used
   * @returns An Observable of tpye any
   */
  createSquarePayment(payment: Square.IPayment): Observable<any> {
    const url: string = `${environment.apiEndpoint}/square/payment/`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }),
      
    };
    return this.httpClient.post<any>(url, payment, httpOptions);
  }
}
