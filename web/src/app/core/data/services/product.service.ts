import {
  HttpClient,
  HttpResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';

import { environment } from '@env';
import { IProduct, ICatalogItem } from '@data/models';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private readonly httpClient: HttpClient) { }

  /**
  * Get reviews for the specified product
  */
  public getProductReviews(product: IProduct): Observable<IProduct.IReview[]> {
    const url: string = `${environment.apiEndpoint}/product/${product.id}/review`;
    return this.httpClient.get<IProduct.IReview[]>(url).pipe(
      map(data => data === null ? [] as IProduct.IReview[] : data)
    );
  }

  /**
   * Get a single Product (and all ProductItems) by ID
   */
  public getProductById(id: string): Observable<IProduct> {
    const url: string = `${environment.apiEndpoint}/product/${id}`;
    return this.httpClient.get<IProduct>(url);
  }

  /**
   * Get multiple Products (and all ProductItems) by ID's (array)
   */
  public getProductsById(id: string[]): Observable<IProduct[]> {

    //uid.forEach((value, index) => id[index] = `id=${value}`);

    const query: string = id.join('&id=');
    const url: string = `${environment.apiEndpoint}/product/?id=${query}`;
    console.log(url);
    debugger;

    return this.httpClient.get<IProduct[]>(url);
  }


  /**
   * Get all products (paginated)
   */
  public getAllProducts_Paged(num: number, limit: number): Observable<HttpResponse<IProduct[]>> {
    // const url: string = `${environment.apiUrl}/products`;
    const url: string = `${environment.apiEndpoint}/product/`;
    return this.getPaginatedResults(url, num, limit);
  }

  /**
   * Get multiple products by name (paginated)
   */
  public getProductsByName_Paged(searchString: string, num: number, limit: number): Observable<HttpResponse<IProduct[]>> {
    const url: string = `${environment.apiEndpoint}/catalog/search?terms=${searchString}&sort=''`;
    return this.getPaginatedResults(url, num, limit);
  }


  /**
   * Gets all catalog items
   * @param num Number of items to return
   * @param limit Max number of items to return
   * @returns All catalog items
   */
  public getCatalogAll(): Observable<ICatalogItem[]> {
    const url: string = `${environment.apiEndpoint}/catalog?sort=''`;
    // const url: string = `${environment.apiEndpoint}/product/`;
    //return this.getPaginatedResults(url, num, limit);
    return this.httpClient.get<ICatalogItem[]>(url);
  }

  /**
   * Returns multiple products (as a catalog) using the search criteria provided.
   * Catalog results include the Product data and data for a single ProductItem.
   * Results are paginated.
   */
  public getCatalogBySearch(search: string): Observable<ICatalogItem[]> {
    const url: string = `${environment.apiEndpoint}/catalog/search?terms='${search}'&sort=`;
    //return this.getPaginatedResults(url, num, limit);
    return this.httpClient.get<ICatalogItem[]>(url);
  }

  /**
   * Returns multiple products (as a catalog) using the category provided.
   * Catalog results include the Product data and data for a single ProductItem.
   * Results are paginated.
   */
  public getCatalogByCategory(categoryUid: string, search: string): Observable<ICatalogItem[]> {
    const url: string = `${environment.apiEndpoint}/catalog/searchCategory/${categoryUid}?terms='${search}'&sort=''`;
    //return this.getPaginatedResults(url, num, limit);
    return this.httpClient.get<ICatalogItem[]>(url);
  }

  /**
   * Returns multiple products (as a catalog) using the sub-category provided.
   * Catalog results include the Product data and data for a single ProductItem.
   * Results are paginated.
   */
  public getCatalogBySubCategory(subCategoryUid: string): Observable<ICatalogItem[]> {
    const url: string = `${environment.apiEndpoint}/catalog/subcategory/${subCategoryUid}?sort=''`;
    //return this.getPaginatedResults(url, num, limit);
    return this.httpClient.get<ICatalogItem[]>(url);
  }

  /**
   * Creates a new product
   * 
   * @param product product to be saved
   */
  createProduct(product: IProduct): Observable<IProduct> {
    const url: string = `${environment.apiEndpoint}/product/`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post<IProduct>(url, product, httpOptions);
  }

  /**
   * Update an existing product
   * 
   * @param product product to be updated
   **/
  updateProduct(product: IProduct): Observable<IProduct> {
    const url: string = `${environment.apiEndpoint}/product/${product.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    //TODO: remove console.log
    return this.httpClient.put<IProduct>(url, product, httpOptions).pipe(tap(_ => console.log(`updated product id=${product.id}`)));
  }

  /**
   * Upload an image to the API file system 
   */
  uploadImages(image: File, fileName: string): Observable<any> {

    const formData: FormData = new FormData();
    formData.append("image", image, fileName);
    console.log(formData);

    const url: string = `${environment.apiEndpoint}/product/uploadImage`;
    const httpOptions = {
      headers: new HttpHeaders({
      })
    };

    return this.httpClient.post<Object>(url, formData, httpOptions);
  }

  /**
   * Adds image information to the database 
   */
  addImages(image: IProduct.IImage): Observable<any> {
    const url: string = `${environment.apiEndpoint}/product/image`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post<IProduct.IImage>(url, image, httpOptions);
  }

  /**
   * Update an Image for a Product
   * 
   * @param image The image to update
   * @returns An Observable of type any
   */
  updateImage(image: IProduct.IImage): Observable<any> {
    const url: string = `${environment.apiEndpoint}/product/image/${image.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient.put<IProduct.IImage>(url, image, httpOptions);
  }

  /**
   * Get all images for a product
   * 
   * @param productUID The ID of the product
   * @returns An oBservable fo type any
   */
  getProductImages(productUID: string): Observable<any> {
    const url: string = `${environment.apiEndpoint}/product/productImages/${productUID}`;
    return this.httpClient.get<IProduct.IImage>(url);
  }

  /**
   * Download an image
   * 
   * @param imageName The name of the image to be downloaded
   * @returns An Observable of type any
   */
  downloadImages(imageName: string): Observable<any> {
    const url: string = `${environment.apiEndpoint}/product/downloadImage/${imageName}`;
    return this.httpClient.get<IProduct.IImage>(url);
  }

  /**
   * Create an image for a product item
   * 
   * @param productItemUID The ID of the product item
   * @param images The image to be assigned
   * @returns An Observable of type any
   */
  productItemImageCreate(productItemUID: string, images: IProduct.IImage[]): Observable<any> {
    const url: string = `${environment.apiEndpoint}/product/productItemImages/${productItemUID}`;
    return this.httpClient.post<Object>(url, images);
  }

  /**
   * Get products by page/limit (sorted by name)
   * 
   * @param num The page number to fetch
   * @param limit The number of records to fetch
   * @return An observable Product + HTTP response or null on error
  */
  private getPaginatedResults(url: string, num: number, limit: number): Observable<HttpResponse<ICatalogItem[]>> {
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type');
    const httpParams = new HttpParams()
      .set('_page', `${num}`)
      .set('_limit', `${limit}`)
      .set('_sort', 'title')
      .set('_order', 'asc');

    return this.httpClient
      .get<ICatalogItem[]>(url, {
        headers: httpHeaders,
        params: httpParams,
        responseType: 'json' as 'json',
        observe: 'response'
      })
      .pipe(catchError(err => of(null)));
  }

}
