import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';

import { environment } from '@env';
import { ICategory } from '@data/models';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private _getCategories: Observable<ICategory[]>;

  constructor(private readonly httpClient: HttpClient) { }

  /**
   * Get single Category by ID
   * 
   * @param id The ID of a category to get
   * @returns An Observable of type ICategory
   */
  getCategoryById(id: string): Observable<ICategory> {
    const url: string = `${environment.apiEndpoint}/category/${id}`;
    return this.httpClient.get<ICategory>(url);
  }

  /**
   * Get all categories and subcategories
   * Results are cached and reused unless reload flag is set
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data 
   * @returns An Observable of tpye ICategory[]
   */
  getCategories(reload: boolean = false): Observable<ICategory[]> {
    if (reload || !this._getCategories) {
      this._getCategories = this.httpClient
        .get<ICategory[]>(`${environment.apiEndpoint}/category`)
        .pipe(shareReplay());
    }
    return this._getCategories;
  }

  /**
   * Add new Category to DB
   * 
   * @param category The Category to be added
   * @returns An Observable of type ICategory
   */
  createCategory(category: ICategory): Observable<ICategory> {
    const url: string = `${environment.apiEndpoint}/category/`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.httpClient
      .post<ICategory>(url, category, httpOptions)
      .pipe(tap(this._getCategories = null));
  }

  /**
   * Update an existing category
   * 
   * @param category The category to be updated
   * @returns An Observable of type ICategory
   */
  updateCategory(category: ICategory): Observable<ICategory> {
    const url: string = `${environment.apiEndpoint}/category/${category.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.httpClient
      .put<ICategory>(url, category, httpOptions)
      .pipe(tap(this._getCategories = null));
  }

  /**
   * Delete a Category
   * 
   * @param categoryId The ID of the category to be deleted 
   * @returns An Observable of type ICategory
   */
  deleteCategory(categoryId: string): Observable<ICategory> {
    const url: string = `${environment.apiEndpoint}/category/${categoryId}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.httpClient
      .delete<ICategory>(url, httpOptions)
      .pipe(tap(this._getCategories = null));
  }

  /**
   * Create a new subcategory
   * 
   * @param categoryId The ID of the category the new subcategory belongs to
   * @param subcategory The subcategory to be created
   * @returns An Observable of type ICategory.ISubcategory
   */
  createSubcategory(categoryId: string, subcategory: ICategory.ISubcategory): Observable<ICategory.ISubcategory> {
    const url: string = `${environment.apiEndpoint}/category/${categoryId}/subcategory`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.httpClient
      .post<ICategory.ISubcategory>(url, subcategory, httpOptions)
      .pipe(tap(this._getCategories = null));
  }

  /**
   * Update an existing subcategory
   * 
   * @param categoryId The ID of the category the subcategory belongs to
   * @param subcategory The subcategory being updated
   * @returns An Observable of type Icategory.ISubcategory
   */
  updateSubcategory(categoryId: string, subcategory: ICategory.ISubcategory): Observable<ICategory.ISubcategory> {
    const url: string = `${environment.apiEndpoint}/category/${categoryId}/subcategory/${subcategory.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.httpClient
      .put<ICategory.ISubcategory>(url, subcategory, httpOptions)
      .pipe(tap(this._getCategories = null));
  }

  /**
   * Delete a subcategory
   * 
   * @param categoryId The ID of the category the subcategory belongs to
   * @param subcategoryId The Id of the subcategory
   * @returns An Observable of Type ICategory.ISubcategory
   */
  deleteSubcategory(categoryId: string, subcategoryId: string): Observable<ICategory.ISubcategory> {
    const url: string = `${environment.apiEndpoint}/category/${categoryId}/subcategory/${subcategoryId}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.httpClient
      .delete<ICategory.ISubcategory>(url, httpOptions)
      .pipe(tap(this._getCategories = null));
  }

}
