import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

import { environment } from '@env';
import { ILookup } from '@data/models';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private _getCountries: Observable<ILookup.ICountry[]>;
  private _getUserRoles: Observable<ILookup.IUserRole[]>;
  private _getOrderStatuses: Observable<ILookup.IOrderStatus[]>;
  private _getDiscountTypes: Observable<ILookup.IDiscountType[]>;
  private _getPaymentProcessors: Observable<ILookup.IPaymentProcessor[]>;
  private _getSuppliers: Observable<ILookup.ISupplier[]>;
  private _getManufacturers: Observable<ILookup.IManufacturer[]>;

  constructor(private readonly httpClient: HttpClient) { }

  /**
   * Get Countries
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data
   * @returns An Observable of type ILookup.ICountry[]
   */
  getCountries(reload: boolean = false): Observable<ILookup.ICountry[]> {
    if (reload || !this._getCountries) {
      this._getCountries = this.httpClient
        .get<ILookup.ICountry[]>(`${environment.apiEndpoint}/lookup/country`)
        .pipe(shareReplay());
    }
    return this._getCountries;
  }

  /**
   * Get User Roles
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data
   * @returns An Observable of type ILookup.IUserRole[]
   */
  getUserRoles(reload: boolean = false): Observable<ILookup.IUserRole[]> {
    if (reload || !this._getUserRoles) {
      this._getUserRoles = this.httpClient
        .get<ILookup.IUserRole[]>(`${environment.apiEndpoint}/lookup/userRole`)
        .pipe(shareReplay());
    }
    return this._getUserRoles;
  }

  /**
   * Get Order Satuses
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data 
   * @returns An Observable of type Ilookup.IOrderStatus[]
   */
  getOrderStatuses(reload: boolean = false): Observable<ILookup.IOrderStatus[]> {
    if (reload || !this._getOrderStatuses) {
      this._getOrderStatuses = this.httpClient
        .get<ILookup.IOrderStatus[]>(`${environment.apiEndpoint}/lookup/orderStatus`)
        .pipe(shareReplay());
    }
    return this._getOrderStatuses;
  }

  /**
   * Get Discount Types
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data
   * @returns An Observable of type ILookup.IDiscountType[]
   */
  getDiscountTypes(reload: boolean = false): Observable<ILookup.IDiscountType[]> {
    if (reload || !this._getDiscountTypes) {
      this._getDiscountTypes = this.httpClient
        .get<ILookup.IDiscountType[]>(`${environment.apiEndpoint}/lookup/discountType`)
        .pipe(shareReplay());
    }
    return this._getDiscountTypes;
  }

  /**
   * Get Payment Processors
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data
   * @returns An Observable of type Iloopkup.IPaymentProcessor[]
   */
  getPaymentProcessors(reload: boolean = false): Observable<ILookup.IPaymentProcessor[]> {
    if (reload || !this._getPaymentProcessors) {
      this._getPaymentProcessors = this.httpClient
        .get<ILookup.IPaymentProcessor[]>(`${environment.apiEndpoint}/lookup/paymentProcessor`)
        .pipe(shareReplay());
    }
    return this._getPaymentProcessors;
  }

  /**
   * Get Suppliers
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data 
   * @returns An Observable of type ILookup.ISupplier[]
   */
  getSuppliers(reload: boolean = false): Observable<ILookup.ISupplier[]> {
    if (reload || !this._getSuppliers) {
      this._getSuppliers = this.httpClient
        .get<ILookup.ISupplier[]>(`${environment.apiEndpoint}/lookup/supplier`)
        .pipe(shareReplay());
    }
    return this._getSuppliers;
  }

  /**
   * Get Manufacturers
   * 
   * @param reload Determines if we refresh local data from the database or serve cached data
   * @returns An Observable of type ILookup.IManufacturer[]
   */
  getManufacturers(reload: boolean = false): Observable<ILookup.IManufacturer[]> {
    if (reload || !this._getManufacturers) {
      this._getManufacturers = this.httpClient
        .get<ILookup.IManufacturer[]>(`${environment.apiEndpoint}/lookup/manufacturer`)
        .pipe(shareReplay());
    }
    return this._getManufacturers;
  }
}
