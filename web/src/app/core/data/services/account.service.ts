import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { shareReplay, map, tap, catchError } from 'rxjs/operators';
import * as uuid from 'uuid';

import { environment } from '@env';
import { IAccount, IAddress, Address } from '@data/models';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private _getAccount: Observable<IAccount>;
  private _getAddresses: Observable<IAddress[]>;
  private _getReviews: Observable<IAccount.IReview[]>;

  constructor(private readonly httpClient: HttpClient) { }

  /**
    * Gets an account
    * Results are cached and reused unless reload flag is set
    */
  getAccount(id: string, reload: boolean = false): Observable<IAccount> {
    if (reload || !this._getAccount) {
      this._getAccount = this.httpClient
        .get<IAccount>(`${environment.apiEndpoint}/account/${id}`)
        .pipe(shareReplay());
    }
    return this._getAccount;
  }

  getAccounts(): Observable<IAccount[]> {
    //TODO V2: THIS ENDPOINT DOES NOT EXIST
    return (this.httpClient.get<IAccount[]>(`${environment.apiEndpoint}/accounts/`))
  }

  getNewAddress(): Observable<IAddress> {
    let address = {} as IAddress;
    return of(address);
  }

  /**
  * Get all addresses for the specified account
  * Results are cached and reused unless reload flag is set
  */
  getAddresses(account: IAccount, reload: boolean = false): Observable<IAddress[]> {
    if (reload || !this._getAddresses) {
      this._getAddresses = this.httpClient
        .get<IAddress[]>(`${environment.apiEndpoint}/account/${account.id}/address`)
        .pipe(
          map(data => data === null ? [] as IAddress[] : data),
          shareReplay()
        );
    }
    return this._getAddresses;
  }

  getAddress(id: String): Observable<IAddress> {
    const url: string = `${environment.apiEndpoint}/address/${id}`;
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .set('Access-Control-Allow-Headers', 'Content-Type');

    return this.httpClient
      .get<IAddress>(url, {
        headers: httpHeaders,
        responseType: 'json' as 'json'
      })
      .pipe(catchError(err => of(null)));
  }

  /**
   * Updates an existing account
   * 
   * @param account The account to be updated + the updated information
   * @returns An Observable of type IAccount
   */
  updateAccount(account: IAccount): Observable<IAccount> {
    const url: string = `${environment.apiEndpoint}/account/${account.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient
      .put<IAccount>(url, account, httpOptions)
      .pipe(tap(this._getAccount = null));
  }

  /**
   * Updates an existing Address
   * 
   * @param address The address to be updated + the updated information
   * @returns An Observable of type IAddress
   */
  updateAddress(address: IAddress): Observable<IAddress> {
    const url: string = `${environment.apiEndpoint}/address/${address.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient
      .put<IAddress>(url, address, httpOptions)
      .pipe(tap(this._getAddresses = null));
  }

  /**
   * Adds a new address
   * 
   * @param address The address to be added
   * @returns An Observable of type IAddress
   */
  addNewAddress(address: IAddress): Observable<IAddress> {
    const url: string = `${environment.apiEndpoint}/address/`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    // assign new uid to address
    address.id = uuid.v4();

    return this.httpClient
      .post<IAddress>(url, address, httpOptions)
      .pipe(tap(this._getAddresses = null));
  }

  /**
   * Deletes an address
   * 
   * @param address The address to be deleted
   * @returns An Observable of type IAddress
   */
  deleteAddress(address: IAddress): Observable<IAddress> {
    const url: string = `${environment.apiEndpoint}/address/${address.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient
      .delete<Address>(url, httpOptions)
      .pipe(tap(this._getAddresses = null));
  }

  /**
   * Get all reviews for the specified account
   * Results are cached and reused unless reload flag is set
   * 
   * @param account The account that the fetched reviews belong too
   * @param reload 
   * @returns An Observable of type IAccount.IReview[]
   */
  getReviews(account: IAccount, reload: boolean = false): Observable<IAccount.IReview[]> {
    if (reload || !this._getAddresses) {
      this._getReviews = this.httpClient
        .get<IAccount.IReview[]>(`${environment.apiEndpoint}/account/${account.id}/review`)
        .pipe(
          map(data => data === null ? [] as IAccount.IReview[] : data),
          shareReplay()
        );
    }
    return this._getReviews;
  }

}
