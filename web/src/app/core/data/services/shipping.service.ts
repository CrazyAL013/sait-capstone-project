
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { EasyPost, IAddress } from '@data/models';
import { environment } from '@env';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {
  constructor(private readonly httpClient: HttpClient) { }

  /**
   * Get the shipping costs
   * 
   * @param address The address to be used
   * @returns An Observable of type EasyPost.IShippingOption[]
   */
  getShippingCosts(address: IAddress): Observable<EasyPost.IShippingOption[]> {
    const url: string = `${environment.apiEndpoint}/easypost/shipping/`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    };
    return this.httpClient
      .post<EasyPost.IShippingOption[]>(url, address, httpOptions);
  }

}
