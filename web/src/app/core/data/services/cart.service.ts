import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '@env';
import { ICartItem, IAccount } from '@data/models';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private readonly httpClient: HttpClient) { }

  /**
   * Get a single cart item
   * 
   * @param id The id of the cart item to get
   * @returns An observable for the http request
   */
  getCartItem(id: string): Observable<ICartItem> {
    const url: string = `${environment.apiEndpoint}/cart/${id}`;
    return this.httpClient.get<ICartItem>(url);
  }

  /**
   * Get multiple cart items
   * 
   * @param id An array of car item ids
   * @returns An observable for the http request
   */
  getCartItems(id: string[]): Observable<ICartItem[]> {
    for (let index = 0; index < id.length; index++) {
      id[index] = "'" + id[index] + "'";
    }
    const ids: string = id.join(",");
    const url: string = `${environment.apiEndpoint}/cart/${ids}`;
    return this.httpClient.get<ICartItem[]>(url);
  }

  /**
   * Updates the cart field in the database
   * 
   * @param account The account the cart belongs too
   * @returns An observable for the http request
   */
  updateCart(account: IAccount): Observable<IAccount> {
    const url: string = `${environment.apiEndpoint}/cart/${account.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient
      .put<IAccount>(url, account.cart, httpOptions)
      .pipe();
  }

  /**
   * Delete a cart from the database
   * 
   * @param account The account the cart belongs too
   * @returns An observable for the http request
   */
  deleteCart(account: IAccount) {
    const url: string = `${environment.apiEndpoint}/cart/${account.id}`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.delete(url, httpOptions);
  }

}
