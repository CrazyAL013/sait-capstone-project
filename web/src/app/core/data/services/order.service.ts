import {  HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { environment } from '@env';
import { IOrder } from '@data/models';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private readonly httpClient: HttpClient) { }

  /**
   * Get all orders beloinging to an account
   * 
   * @param accountId The ID of the account to be used
   * @returns An Observable of type IOrder[]
   */
  getOrders(accountId: string): Observable<IOrder[]> {
    const url: string = `${environment.apiEndpoint}/account/${accountId}/order`;
    return this.httpClient.get<IOrder[]>(url);
  }

  /**
   * Get a single order
   * 
   * @param id The ID of the order to get
   * @returns An Observalbe of type IOrder
   */
  getOrder(id: string): Observable<IOrder> {
    const url: string = `${environment.apiEndpoint}/order/${id}`;
    return this.httpClient.get<IOrder>(url);
  }
}
