import { IAddress } from "./address";

export interface IAccount {
    id: string;
    roles?: IAccount.IRole[];
    active: boolean;
    username: string;
    fullName: string;
    preferredName: string;
    email: string;
    cart?: string;
    address?: IAddress;
    accessToken?: string;
    refreshToken?: string;
}
export namespace IAccount {
    export interface IRole {
        id: string;
        name: string;
    }
    export interface IReview {
        id: string;
        title: string;
        description: string;
        rating: number;
        date: Date;
        verifiedPurchase: boolean;
        product: IAccount.IReview.IProduct;
    }
    export namespace IReview {
        export interface IProduct {
            id: string;
            name: string;
            image: string;
            item: IAccount.IReview.IProductItem;
        }

        export interface IProductItem {
            id: string;
            name: string;
            model: string;
        }
    }
    export interface IWishlist {
        id: string;
        title: string;
        productItem?: IAccount.IWishlist.IProductItem[];
    }
    export namespace IWishlist {
        export interface IProductItem {
            id: string;
            name: string;
            image: string;
        }
    }
}

export class Account implements IAccount {
    public id: string;
    public roles?: IAccount.IRole[];
    public active: boolean;
    public username: string;
    public fullName: string;
    public preferredName: string;
    public email: string;
    public cart?: string;
    public address?: IAddress;
    public accessToken?: string;
    public refreshToken?: string;

    constructor(params: IAccount = {} as IAccount) {
        let {
            id,
            roles,
            active,
            username,
            fullName,
            preferredName,
            email,
            cart,
            address,
            accessToken: idToken,
            refreshToken: accessToken
        } = params;

        this.id = id;
        this.roles = roles;
        this.active = active;
        this.username = username;
        this.fullName = fullName;
        this.preferredName = preferredName;
        this.email = email;
        this.cart = cart;
        this.address = address;
        this.accessToken = idToken;
        this.refreshToken = accessToken;
    }
}

export namespace Account {
    export class Review implements IAccount.IReview {
        public id: string;
        public title: string;
        public description: string;
        public rating: number;
        public date: Date;
        public verifiedPurchase: boolean;
        public product: IAccount.IReview.IProduct;

        constructor(params: IAccount.IReview = {} as IAccount.IReview) {
            let {
                id,
                title,
                description,
                rating,
                date,
                verifiedPurchase,
                product
            } = params;

            this.id = id;
            this.title = title;
            this.description = description;
            this.rating = rating;
            this.date = date;
            this.verifiedPurchase = verifiedPurchase;
            this.product = product;
        }
    }

    export class Wishlist implements IAccount.IWishlist {
        public id: string;
        public title: string;
        public productItem?: IAccount.IWishlist.IProductItem[];

        constructor(params: IAccount.IWishlist = {} as IAccount.IWishlist) {
            let {
                id,
                title,
                productItem
            } = params;

            this.id = id;
            this.title = title;
            this.productItem = productItem;
        }
    }
}