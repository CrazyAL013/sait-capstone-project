export namespace ILookup {

  export interface ICountry {
    id: number;
    name: string;
    code: string;
    tax: number;
    currencyCode: string;
    currencySymbol: string;
    regions: IRegion[];
  }

  export interface IRegion {
    id: number;
    countryId: number;
    name: string;
    code: string;
    tax: number;
  }

  export interface IUserRole {
    id: string;
    name: string;
  }

  export interface IOrderStatus {
    id: string;
    title: string;
    description: string;
  }

  export interface IDiscountType {
    id: string;
    name: string;
    description: string;
  }

  export interface IPaymentProcessor {
    id: string;
    name: string;
    logo: string;
  }

  export interface ISupplier {
    id: string;
    name: string;
    email: string;
    phone: string;
    fax: string;
    address: string;
    contacts: string;
  }

  export interface IManufacturer {
    id: string;
    name: string;
    website: string;
  }
}