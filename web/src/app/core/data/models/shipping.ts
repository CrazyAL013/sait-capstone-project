import { IAddress } from "./address";

export namespace EasyPost {
  export interface IShippingCriteria {
    toAddress: IAddress,
    name: string,
    phone: string,
    items: IShippingCriteria.IShippingItem[]
  }
  export namespace IShippingCriteria {
    export interface IShippingItem {
      weight: number
    }
  }

  export interface IShippingOption {
    id: string,
    shipmentId: string;
    service: string,
    carrier: string,
    logo: string,
    rate: number,
    currency: string,
    serviceCode: string,
    deliveryDays: number,
    deliveryDate: Date,
    deliveryDateGuaranteed: boolean,
    estDeliveryDays: number
  }
}

export interface IShipment {
  id: string;
  rateId: string;
  shipmentId: string;
  carrier: string;
  service: string;
  rate: number;
  trackingNumber?: string;
  deliveryDays: number;
  shipDate?: Date
}

export class Shipment implements IShipment {
  public id: string;
  public rateId: string;
  public shipmentId: string;
  public carrier: string;
  public service: string;
  public rate: number;
  public trackingNumber?: string;
  public deliveryDays: number;
  public shipDate?: Date;

  constructor(params: IShipment = {} as IShipment) {
    let {
      id,
      rateId,
      shipmentId,
      carrier,
      service,
      rate,
      trackingNumber,
      deliveryDays,
      shipDate
    } = params;

    this.id = id;
    this.rateId = rateId;
    this.shipmentId = shipmentId
    this.carrier = carrier;
    this.service = service;
    this.rate = rate;
    this.trackingNumber = trackingNumber;
    this.deliveryDays = deliveryDays;
    this.shipDate = shipDate;
  }
}
