import { IProduct } from "./product";

export interface ICatalogItem {
  id: string;
  productName?: string;
  image?: IProduct.IImage;
  productItem?: ICatalogItem.IProductItem;
}

export namespace ICatalogItem {
  export interface IProductItem {
    id?: string;
    name?: string;
    model?: string;
    totalSold?: number;
    unitPrice?: number;
    description?: string;
    reorderLevel?: number;
    averageRating: number;
    stockQuantity?: number;
  }
}