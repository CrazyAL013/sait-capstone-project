// Represents a product in the shopping cart.
// Primarily used for display purposes in the shopping
// cart, it is a combination of a ProductItem and a Product.

import { IProduct } from "./product";

export interface ICartItem {
  id: string;
  quantity: number;
  productId?: string;
  productName?: string;
  productImage?: IProduct.IImage;
  productItem?: ICartItem.IProductItem;
}

export namespace ICartItem {
  export interface IProductItem {
    id?: string;
    name?: string;
    model?: string;
    weight?: number;
    unitPrice?: number;
    stockQuantity?: number;
    reorderLevel?: number;
  }
}

export class CartItem implements ICartItem {
  public id: string;
  public quantity: number;
  public productId?: string;
  public productName?: string;
  public productImage?: IProduct.IImage;
  public productItem?: ICartItem.IProductItem;

  constructor(params: ICartItem = {} as ICartItem) {
    let {
      id,
      quantity,
      productId,
      productName,
      productImage,
      productItem
    } = params;

    this.id = id;
    this.quantity = quantity;
    this.productId = productId;
    this.productName = productName;
    this.productImage = productImage;
    this.productItem = productItem;
  }
}

export namespace CartItem {
  export class ProductItem {
    public id?: string;
    public name?: string;
    public model?: string;
    public weight?: number;
    public unitPrice?: number;
    public stockQuantity?: number;
    public reorderLevel?: number;

    constructor(params: ICartItem.IProductItem = {} as ICartItem.IProductItem) {
      let {
        id,
        name,
        model,
        weight,
        unitPrice,
        stockQuantity,
        reorderLevel
      } = params;

      this.id = id;
      this.name = name;
      this.model = model;
      this.weight = weight;
      this.unitPrice = unitPrice;
      this.stockQuantity = stockQuantity;
      this.reorderLevel = reorderLevel;
    }
  }
}