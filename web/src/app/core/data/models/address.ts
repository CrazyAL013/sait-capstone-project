export interface IAddress {
  id: string;
  accountId: string;
  description?: string;
  line1?: string;
  line2?: string;
  city?: string;
  postalZip?: string;
  region?: IAddress.IRegion;
  country?: IAddress.ICountry;
  primary?: boolean;
}

export namespace IAddress {

  export interface IRegion {
    id: number;
    countryId: number;
    name: string;
    code: string;
    tax: number;
  }

  export interface ICountry {
    id: number;
    name: string;
    code: string;
    tax: number;
    currencyCode: string;
    currencySymbol: string;
  }
}

export class Address implements IAddress {
  public id: string;
  public accountId: string;
  public description?: string;
  public line1?: string;
  public line2?: string;
  public city?: string;
  public postalZip?: string;
  public region?: IAddress.IRegion;
  public country?: IAddress.ICountry;
  public primary?: boolean;

  constructor(params: IAddress = {} as IAddress) {
    let {
      id,
      accountId,
      description,
      line1: line1,
      line2: line2,
      city,
      postalZip,
      region,
      country,
      primary
    } = params;
    this.id = id;
    this.accountId = accountId;
    this.description = description;
    this.line1 = line1;
    this.line2 = line2;
    this.city = city;
    this.postalZip = postalZip;
    this.region = region;
    this.country = country;
    this.primary = primary;
  }
}