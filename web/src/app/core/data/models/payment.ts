import { IOrder } from './order';

export namespace IPayment {
  export interface ICard {
    brand: string;
    last4Digits: number;
    expiryMonth: number;
    expiryYear: number;
    postalCode: string;
  }
}
export namespace Payment {
  export class Card implements IPayment.ICard {
    public brand: string;
    public last4Digits: number;
    public expiryMonth: number;
    public expiryYear: number;
    public postalCode: string;

    constructor(params: IPayment.ICard = {} as IPayment.ICard) {
      let {
        brand,
        last4Digits,
        expiryMonth,
        expiryYear,
        postalCode
      } = params;

      this.brand = brand;
      this.last4Digits = last4Digits;
      this.expiryMonth = expiryMonth;
      this.expiryYear = expiryYear;
      this.postalCode = postalCode;
    }
  }
}


export namespace Square {
  export interface INonce {
    errors: any, 
    nonce: string, 
    cardData: any
  };

  export interface IPayment {
    nonce: string;
    idempotencyKey: string;
    locationId: string;
    amountMoney: IPayment.IMoney;
    order: IOrder;
  }

  export namespace IPayment {
    export interface IMoney {
      amount: number;
      currency: string;
    }
  }

  export class Payment implements IPayment {
    public nonce: string;
    public idempotencyKey: string;
    public locationId: string;
    public amountMoney: IPayment.IMoney;
    public order: IOrder;

    constructor(params: IPayment = {} as IPayment) {
      let {
        nonce,
        idempotencyKey: idempotency_key,
        locationId: location_id,
        amountMoney: amount_money,
        order: order
      } = params;

      this.nonce = nonce;
      this.idempotencyKey = idempotency_key;
      this.locationId = location_id;
      this.amountMoney = amount_money;
      this.order = order;
    }
  }

  export namespace Payment {
    export class Money implements IPayment.IMoney {
      public amount: number;
      public currency: string;

      constructor(params: IPayment.IMoney = {} as IPayment.IMoney) {
        let {
          amount,
          currency
        } = params;

        this.amount = amount;
        this.currency = currency;
      }
    }
  }
}