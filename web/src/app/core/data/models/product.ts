import { ICategory } from './category';

export interface IProduct {
    id: string;
    supplier?: IProduct.ISupplier;
    manufacturer?: IProduct.IManufacturer;
    name?: string;
    description?: string;
    keywords?: string;
    averageRating?: number;
    categories?: ICategory[];
    image?: IProduct.IImage;
    productItems?: IProduct.IProductItem[];
}
export namespace IProduct {
    export interface IProductItem {
        id?: string,
        display?: boolean;
        model?: string;
        sku?: string;
        upc?: string;
        name?: string;
        description?: string;
        specifications?: IProductItem.ISpecification[];
        keywords?: string;
        images?: IProduct.IImage[];
        unitPrice?: number;
        weight?: number;
        stockQuantity?: number;
        reorderLevel?: number;
        totalSold?: number;
        averageRating?: number;
    }
    export namespace IProductItem {
        export interface ISpecification {
            name: string;
            value: string;
        }
    }
    export interface IImage {
        id: string;
        productId: string;
        type: string;
        location?: string;
        url?: string;
    }

    export interface IImageUpload {
        id: string;
        productId: string;
        type: string;
        location?: string;
        url?: string;
        data: string;
    }

    export interface ISupplier {
        id: string;
        name: string;
        email: string;
        phone: string;
        fax: string;
        address: string;
        contacts: string;
    }

    export interface IManufacturer {
        id: string;
        name: string;
        website: string;
    }

    export interface IReview {
        id: string;
        title: string;
        description: string;
        rating: number;
        date: Date;
        verifiedPurchase: boolean;
        name: string;
        region: string;
        country: string;

    }
}

export class Product implements IProduct {
    public id: string;
    public supplier?: IProduct.ISupplier;
    public manufacturer?: IProduct.IManufacturer;
    public name?: string;
    public description?: string;
    public keywords?: string;
    public averageRating?: number;
    public categories?: ICategory[];
    public image?: IProduct.IImage;
    public productItems?: IProduct.IProductItem[];

    constructor(params: IProduct = {} as IProduct) {
        let {
            id,
            supplier,
            manufacturer,
            name,
            description,
            keywords,
            averageRating,
            image,
            categories,
            productItems,
        } = params;

        this.id = id;
        this.supplier = supplier;
        this.manufacturer = manufacturer;
        this.name = name;
        this.description = description;
        this.keywords = keywords;
        this.averageRating = averageRating;
        this.image = image;
        this.categories = categories;
        this.productItems = productItems;
    }
}
export namespace Product {
    export class ProductItem implements IProduct.IProductItem {
        public id: string;
        public display?: boolean;
        public model?: string;
        public sku?: string;
        public upc?: string;
        public name?: string;
        public description?: string;
        public keywords?: string;
        public specifications?: IProduct.IProductItem.ISpecification[] = [];
        public images?: IProduct.IImage[] = [];
        public unitPrice?: number;
        public weight?: number;
        public stockQuantity?: number;
        public reorderLevel?: number;
        public totalSold?: number;
        public averageRating?: number;
        constructor(params: IProduct.IProductItem = {} as IProduct.IProductItem) {
            let {
                id,
                display,
                model,
                sku,
                upc,
                name,
                description,
                keywords,
                specifications,
                images,
                unitPrice,
                weight,
                stockQuantity,
                reorderLevel,
                totalSold,
                averageRating: averageRating,
            } = params;

            this.id = id;
            this.display = display;
            this.model = model;
            this.sku = sku;
            this.upc = upc;
            this.name = name;
            this.description = description;
            this.keywords = keywords;
            this.specifications = specifications;
            this.images = images;
            this.unitPrice = unitPrice;
            this.weight = weight;
            this.stockQuantity = stockQuantity;
            this.reorderLevel = reorderLevel;
            this.totalSold = totalSold;
            this.averageRating = averageRating;
        }
    }

    export class Image implements IProduct.IImage {
        public id: string;
        public productId: string;
        public type: string;
        public location: string;
        public url: string;
        constructor(params: IProduct.IImage = {} as IProduct.IImage) {
            let {
                id,
                productId,
                type,
                location,
                url
            } = params;

            this.id = id;
            this.productId = productId;
            this.type = type;
            this.location = location;
            this.url = url;
        }
    }

    export class ImageUpload implements IProduct.IImageUpload {
        public id: string;
        public productId: string;
        public type: string;
        public location: string;
        public url: string;
        public data: string;
        constructor(params: IProduct.IImageUpload = {} as IProduct.IImageUpload) {
            let {
                id,
                productId,
                type,
                location,
                url,
                data,
            } = params;

            this.id = id;
            this.productId = productId;
            this.type = type;
            this.location = location;
            this.url = url;
            this.data = data;
        }
    }
    
    export namespace ProductItem {
        export class Specification implements IProduct.IProductItem.ISpecification {
            public name: string;
            public value: string;
            constructor(params: IProduct.IProductItem.ISpecification = {} as IProduct.IProductItem.ISpecification) {
                let {
                    name,
                    value
                } = params;

                this.name = name;
                this.value = value;
            }
        }
    }
}
