import { IAccount } from './account';
import { IAddress } from './address';
import { IDiscount } from './discount';
import { IShipment } from './shipping';
import { ILookup } from './app';
import { IProduct } from './product';
import { IPayment } from './payment';

export interface IOrder {
  id: string;
  account: IAccount;
  status: ILookup.IOrderStatus;
  billingAddress?: IAddress;
  deliveryAddress?: IAddress;
  discount?: IDiscount;
  paymentProcessor?: ILookup.IPaymentProcessor;
  shipment?: IShipment;
  transactionId?: string;
  date?: Date;
  subTotal?: number;
  beforeTaxTotal?: number;
  shipTotal?: number;
  taxTotal?: number;
  discountTotal?: number;
  grandTotal?: number;
  paymentCardBrand?: string;
  paymentCardLast4Digits?: number;
  paymentCardExpiryMonth?: number;
  paymentCardExpiryYear?: number;
  items?: IOrder.IOrderItem[];
}
export namespace IOrder {
  export interface IOrderItem {
    id: string;
    quantity: number;
    unitPrice: number;
    productId: string;
    productName: string;
    productImage: IProduct.IImage;
    productItem: IOrder.IProductItem;
  }
  export interface IProductItem {
    id: string;
    name: string;
    model: string;
  }

}

export class Order implements IOrder {
  public id: string;
  public account: IAccount;
  public status: ILookup.IOrderStatus;
  public billingAddress?: IAddress;
  public deliveryAddress?: IAddress;
  public discount?: IDiscount;
  public paymentProcessor?: ILookup.IPaymentProcessor;
  public paymentCard?: IPayment.ICard;
  public shipment?: IShipment;
  public transactionId?: string;
  public date: Date;
  public subTotal?: number;
  public beforeTaxTotal?: number;
  public shipTotal?: number;
  public taxTotal?: number;
  public discountTotal?: number;
  public grandTotal?: number;
  public paymentCardBrand: string;
  public paymentCardLast4Digits: number;
  public paymentCardExpiryMonth: number;
  public paymentCardExpiryYear: number;  
  public items?: IOrder.IOrderItem[];

  constructor(params: IOrder = {} as IOrder) {
    let {
      id,
      account,
      status,
      billingAddress,
      deliveryAddress,
      discount,
      paymentProcessor,
      shipment,
      transactionId,
      date,
      subTotal,
      beforeTaxTotal,
      shipTotal,
      taxTotal,
      discountTotal,
      grandTotal,
      paymentCardBrand,
      paymentCardLast4Digits,
      paymentCardExpiryMonth,
      paymentCardExpiryYear,
      items
    } = params;

    this.id = id;
    this.account = account;
    this.status = status;
    this.billingAddress = billingAddress;
    this.deliveryAddress = deliveryAddress;
    this.discount = discount;
    this.paymentProcessor = paymentProcessor;
    this.shipment = shipment;
    this.transactionId = transactionId;
    this.date = date;
    this.subTotal = subTotal;
    this.beforeTaxTotal = beforeTaxTotal;
    this.shipTotal = shipTotal;
    this.taxTotal = taxTotal;
    this.discountTotal = discountTotal;
    this.grandTotal = grandTotal;
    this.paymentCardBrand = paymentCardBrand;
    this.paymentCardLast4Digits = paymentCardLast4Digits;
    this.paymentCardExpiryMonth = paymentCardExpiryMonth;
    this.paymentCardExpiryYear = paymentCardExpiryYear
    this.items = items;
  }
}

export namespace Order {
  export class OrderItem implements IOrder.IOrderItem {
    public id: string;
    public quantity: number;
    public unitPrice: number;
    public productId: string;
    public productName: string;
    public productImage: IProduct.IImage;
    public productItem: IOrder.IProductItem;

    constructor(params: IOrder.IOrderItem = {} as IOrder.IOrderItem) {
      let {
        id,
        quantity,
        unitPrice,
        productId,
        productName,
        productImage,
        productItem
      } = params;

      this.id = id;
      this.quantity = quantity;
      this.unitPrice = unitPrice;
      this.productId = productId;
      this.productName = productName;
      this.productImage = productImage;
      this.productItem = productItem;
    }
  }

  export class ProductItem implements IOrder.IProductItem {
    public id: string;
    public name: string;
    public model: string;

    constructor(params: IOrder.IProductItem = {} as IOrder.IProductItem) {
      let {
        id,
        name, 
        model
      } = params;

      this.id = id;
      this.name = name;
      this.model = model;
    }
  }
}

