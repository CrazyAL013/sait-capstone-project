
export interface ICategory {
    id: string;
    name?: string;
    subcategories?: ICategory.ISubcategory[];
}

export namespace ICategory {
    export interface ISubcategory {
        id: string;
        name?: string;
    }
}

export namespace Category {
    export class Category implements ICategory {
        public id: string;
        public name?: string;
        public subcategories?: ICategory.ISubcategory[];

        constructor(params: ICategory = {} as ICategory) {
            let {
                id,
                name,
                subcategories,
            } = params;

            this.id = id;
            this.name = name;
            this.subcategories = subcategories;
        }
    }

    export class Subcategory implements ICategory.ISubcategory {
        public id: string;
        public name?: string;

        constructor(params: ICategory.ISubcategory = {} as ICategory.ISubcategory) {
            let {
                id,
                name,
            } = params;

            this.id = id;
            this.name = name;
        }
    }
}