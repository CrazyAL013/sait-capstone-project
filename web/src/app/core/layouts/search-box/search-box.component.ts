import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Router } from '@angular/router';

import { CategoryService } from '@data/services';
import { ICategory } from '@data/models';

@Component({
  selector: 'search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SearchBoxComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  category$: Observable<ICategory[]>;
  selectedCategoryUid: string = "all";

  constructor(
    private categoryService: CategoryService,
    private router: Router) { }

  ngOnInit(): void {
    this.subscriptions.push((this.category$ = this.categoryService.getCategories()).subscribe());
  }

  onSearch(text: string) {
    if (text.trim().length > 2) {
      // Change the router reuse strategy to allow reloading for searches
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([`/store/catalog/${this.selectedCategoryUid}/${encodeURIComponent(text)}`]);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}
