import { CommonModule } from '@angular/common';
import { NgModule, Component, OnDestroy, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Subscription, Observable } from 'rxjs';

import { ViewportSizeEnum as ViewportSize } from '@core/enums';
import { AuthService, CartService } from '@core/services';
import { CategoryService } from '@data/services';
import { Globals } from '@core/globals';
import { IAccount, ICategory } from '@app/core/data';
import { MaterialModule } from '@shared';
import { SearchBoxComponent } from '../search-box/search-box.component';

@Component({
  selector: 'app-content-layout',
  templateUrl: './content-layout.component.html',
  styleUrls: ['./content-layout.component.scss'],
})
export class ContentLayoutComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  public account: IAccount;
  public category: ICategory;
  category$: Observable<ICategory[]>;
  public currentYear = new Date().getUTCFullYear();
  public floatingFooter: boolean = true;
  public closeMenuOnClick: boolean = false;
  private breakpoints: { [key: string]: boolean };
  public viewportSize = ViewportSize;

  // navItems = [
  //   { link: '/warehouse', title: 'Warehouse' }
  // ];

  constructor(
    public globals: Globals,
    private breakpointObserver: BreakpointObserver,
    public authService: AuthService,
    private categoryService: CategoryService,
    public cartService: CartService,
    private router: Router
  ) {
    // Listen for user authentication events
    this.subscriptions.push(this.authService.account.subscribe(account$ => {
      this.account = account$;
    }));
    // Set footer to float at the bottom of the page or be fixed at the bottom of the viewport
    this.floatingFooter = false;
    // Set the menu to close when an item is clicked
    this.closeMenuOnClick = true;
  }

  ngOnInit(): void {
    this.initMediaQueries();
    this.subscriptions.push((this.category$ = this.categoryService.getCategories()).subscribe());
  }

  onSideNavClosed() {
    this.category = null;
  }

  onCategorySelect(category: ICategory) {
    if (category) {
      this.category = category;
    } else {
      this.category = null;
    }
  }

  onSubcategorySelect(subcategory: ICategory.ISubcategory) {
    if (subcategory) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([`/store/catalog/${subcategory.id}`]);
    } else {
      console.log("ERROR");
    }

  }


  private initMediaQueries() {
    // Media queries are used to monitor screen size changes.
    // A BreakpointObserver defines the specific media queries to watch.
    // The media state is stored globally (globals.ts) and can be injected
    // into any component that needs to react to media state changes.
    this.subscriptions.push(this.breakpointObserver
      .observe([
        ViewportSize.XSmall,
        ViewportSize.Small,
        ViewportSize.Medium,
        ViewportSize.Large,
      ])
      .subscribe((state: BreakpointState) => {
        this.breakpoints = state.breakpoints;
        this.globals.mediaState.XSmall = state.breakpoints[ViewportSize.XSmall];
        this.globals.mediaState.Small = state.breakpoints[ViewportSize.Small];
        this.globals.mediaState.Medium = state.breakpoints[ViewportSize.Medium];
        this.globals.mediaState.Large = state.breakpoints[ViewportSize.Large];
      }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe())
  }
}


@NgModule({
  declarations: [ContentLayoutComponent, SearchBoxComponent],
  imports: [CommonModule, MaterialModule, RouterModule, FormsModule],
})
export class ContentLayoutModule { }