import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StorefrontAuthGuard, WarehouseAuthGuard, NoAuthGuard } from '@core';
import { ContentLayoutComponent } from '@layouts/content-layout1/content-layout.component';
import { LoginEntryComponent } from '@shared/components/login/login.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/store',
    pathMatch: 'full',
  },
  {
    path: 'auth/:action',
    canActivate: [StorefrontAuthGuard],
    component: LoginEntryComponent,
  },
  {
    path: 'store',
    component: ContentLayoutComponent,
    canActivate: [NoAuthGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('@features/home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'catalog',
        loadChildren: () => import('@app/features/catalog/catalog.module').then((m) => m.CatalogModule),
      },
      {
        path: 'catalog/:category/:search',
        loadChildren: () => import('@app/features/catalog/catalog.module').then((m) => m.CatalogModule),
      },
      {
        path: 'catalog/:category',
        loadChildren: () => import('@app/features/catalog/catalog.module').then((m) => m.CatalogModule),
      },
      {
        path: 'product/:id',
        loadChildren: () => import('@app/features/product/product.module').then((m) => m.ProductModule),
      },
      {
        path: 'product/:id/:itemId',
        loadChildren: () => import('@app/features/product/product.module').then((m) => m.ProductModule),
      },
      {
        path: 'cart',
        loadChildren: () => import('@app/features/cart/cart.module').then((m) => m.CartModule),
      },
      // {
      //   path: 'checkout',
      //   canActivate: [StorefrontAuthGuard],
      //   loadChildren: () => import('@app/features/checkout/checkout.module').then((m) => m.CheckoutModule),
      // },
      {
        path: 'account',
        canActivate: [StorefrontAuthGuard],
        loadChildren: () => import('@features/account/account.module').then((m) => m.AccountModule),
      }
    ],
  },
  {
    path: 'warehouse',
    component: ContentLayoutComponent,
    canActivate: [WarehouseAuthGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('@features/warehouse/warehouse.module').then((m) => m.WarehouseModule),
      },
    ],
  },
  { path: '**', redirectTo: '/store', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
