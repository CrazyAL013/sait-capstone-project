export const environment = {
  production: true,

  //Docker endpoint
  apiEndpoint: 'https://localhost:8080/v1',

  //apiEndpoint: 'https://10.232.175.39:8080/v1',


  images: '/images',

  squareApplicationId: 'sandbox-sq0idb-byixmdieP777rEreWL1nFg',
  squareLocationId: 'EAAAEGqae9spif465Tpz8e1ifbowTuajNcgPsaODJ625BnrjXaQIJ7WhAKViD0H5',
};
