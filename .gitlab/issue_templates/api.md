<_Use the following template to help create an API Call request._>

# API Call Name
<_Description of the API call_> 

#### Endpoints

| METHOD  | DESCRIPTION | EXAMPLE |
| ------- | ----------- | --------|
| `GET`   | [/v1/???](#retrieve_all) | `/v1/???` |
| `GET`   | [/v1/???/:id](#retrieve) | `/v1/???/b11faaed-58f1-4844-addd-3ab523c9b8f9` |
| `POST`  | [/v1/???](#create) | `/v1/???` |
| `PUT`   | [/v1/???/:id](#update) | `/v1/???/41509b3c-00fd-46ba-a389-ccd9f184a37d` |
| `DELETE`| [/v1/???/:id](#delete) | `/v1/???/b788304e-eb26-4821-8e1f-9d71ff50f4cf` |

#### Object
```
{
	uid: string;
	???: string;
	???: number;
	???: boolean;
}
```

### Create
Creates a new ???.

|     |     |
| --- | --- |
| Request Body | A JSON ??? object to be created. |
| Parameters   | n/a |
| Response [`200`] | Success: JSON response body contains the new ??? object. |
| Response [`500`] | Error: JSON response body contains details of the error. |

### Retrieve
Retrieves a single ??? by id.

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | `id` [required] String containing the UUID of the ???. |
| Response [`200`] | JSON response body contains a ??? object. |
| Response [`500`] | JSON response body contains details of the error. |

### Retrieve all
Retrieves all ??? (with optional pagination).

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | `page` [optional] Integer containing the (zero-based) page number.<br/>`limit` [optional] Integer specifying the number of results per page. |
| Response [`200`] | Success: JSON response body contains an array of ??? objects. |
| Response [`500`] | Error: JSON response body contains details of the error. |

### Update
Updates a single ???.

|     |     |
| --- | --- |
| Request Body | A JSON ??? object to be updated. |
| Parameters   | `id` [required] String containing the UUID of the ???. |
| Response [`200`] | Success: JSON response body contains the updated ??? object. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |

### Delete
Deletes the specified ???.

|     |     |
| --- | --- |
| Request Body | n/a |
| Parameters   | `id` [required] String containing the UUID of the ???. |
| Response [`204`] | Success: Response body is empty. |
| Response [`404`] | Not found: Response body is empty. |
| Response [`500`] | Error: JSON response body contains details of the error. |
