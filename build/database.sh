function run_sql() {
  mysql -u "$DATABASE_USER""$DATABASE_HOST" --password="$DATABASE_PASSWORD" < "$1"
}

cd ../database/Scripts/ddl/
run_sql create-database.sql
run_sql create-triggers.sql
run_sql load-test-data.sql
run_sql populate-lookups.sql

cd ../procs
run_sql account.sql
run_sql address.sql
run_sql cart.sql
run_sql category.sql
run_sql lookup.sql
run_sql order.sql
run_sql product.sql
run_sql sale.sql
run_sql search.sql

cd ../functions
run_sql functions.sql

cd ..
run_sql testing.sql
