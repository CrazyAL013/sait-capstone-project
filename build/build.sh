#!/bin/bash

# CONFIG
export DATABASE_USER='root'
export DATABASE_PASSWORD='password'
export DATABASE_HOST='localhost'
export SITE_NAME='123.com'
# END OF CONFIG

# function to find and kill tmux sessions
function tmux_new() {
  tmux ls | grep -q $1 && tmux send-keys -t $1 "$2" Enter || (tmux new-session -ds $1; tmux send-keys -t $1 "$2" Enter)
}

# populate database
tmux_new db ./database.sh

# build angular application
tmux_new rest ./web.sh

# build Java API
tmux_new java ./java.sh

sed "s/\$_SITE/$SITE_NAME/g" nginx-conf-sub.conf ${SITE_NAME}.conf
