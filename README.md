# SAIT Capstone Project

![](documentation/docs/resources/logo.png)

## Introduction
Our 2021 SAIT Captstone Project is to build an ecommerce website to support online product sales for a local aquarium services company.

The project will showcase several current technologies, including:

  - Angular single-page web application
  - Java RESTful API using the Spring framework
  - MySQL relational database

## Team
Alex Tompkins  
Daniel Tompkins  
Jack Graver  
Matthew Rempel  
Saurav Suthar  
Tait Hoyem  
Tommy Nguyen  

## Project Organization
The overall project is divided into four separate projects each contained within its own directory:

  - API
  - Database
  - Documentation
  - Web

## Running Using Docker
Navigate to the root of the project (where the `docker-compose.yml` exists) and run the following command:
```
docker-compose up
```
This will build the images and then run the containers. Once finished you will need to remove the containers and images.

## Screenshots

---
### Web
#### Catlog and product pages
![](documentation/docs/resources/screenshots/001.jpg)
![](documentation/docs/resources/screenshots/002.jpg)

---

#### Login dialog
![](documentation/docs/resources/screenshots/016.jpg)


---

#### User account pages
![](documentation/docs/resources/screenshots/003.jpg)
![](documentation/docs/resources/screenshots/004.jpg)
![](documentation/docs/resources/screenshots/005.jpg)
![](documentation/docs/resources/screenshots/006.jpg)
![](documentation/docs/resources/screenshots/007.jpg)

---

#### Cart and checkout pages
![](documentation/docs/resources/screenshots/008.jpg)
![](documentation/docs/resources/screenshots/009.jpg)
![](documentation/docs/resources/screenshots/010.jpg)
![](documentation/docs/resources/screenshots/011.jpg)
![](documentation/docs/resources/screenshots/012.jpg)

---

#### Category and subcategory search menu
![](documentation/docs/resources/screenshots/013.jpg)
![](documentation/docs/resources/screenshots/014.jpg)

---

#### Administration section
![](documentation/docs/resources/screenshots/018.jpg)
![](documentation/docs/resources/screenshots/019.jpg)
![](documentation/docs/resources/screenshots/020.jpg)
![](documentation/docs/resources/screenshots/021.jpg)
![](documentation/docs/resources/screenshots/022.jpg)

---
### Themed
![](documentation/docs/resources/screenshots/023.jpg)
![](documentation/docs/resources/screenshots/024.jpg)
![](documentation/docs/resources/screenshots/026.jpg)
![](documentation/docs/resources/screenshots/027.jpg)
![](documentation/docs/resources/screenshots/028.jpg)
![](documentation/docs/resources/screenshots/029.jpg)
![](documentation/docs/resources/screenshots/033.jpg)
![](documentation/docs/resources/screenshots/034.jpg)
![](documentation/docs/resources/screenshots/035.jpg)

### Mobile
#### Mobile catalog and product pages
![](documentation/docs/resources/screenshots/036.jpg)
![](documentation/docs/resources/screenshots/040.jpg)
![](documentation/docs/resources/screenshots/041.jpg)
![](documentation/docs/resources/screenshots/042.jpg)

---

#### Mobile user account page
![](documentation/docs/resources/screenshots/037.jpg)
![](documentation/docs/resources/screenshots/038.jpg)
![](documentation/docs/resources/screenshots/039.jpg)
![](documentation/docs/resources/screenshots/045.jpg)
![](documentation/docs/resources/screenshots/046.jpg)

---
#### Mobile category and subcategory search
![](documentation/docs/resources/screenshots/043.jpg)
![](documentation/docs/resources/screenshots/044.jpg)

---

#### Mobile password change
![](documentation/docs/resources/screenshots/047.jpg)
![](documentation/docs/resources/screenshots/048.jpg)

---

#### Mobile checkout process 
![](documentation/docs/resources/screenshots/049.jpg)
![](documentation/docs/resources/screenshots/050.jpg)
![](documentation/docs/resources/screenshots/051.jpg)
![](documentation/docs/resources/screenshots/052.jpg)
![](documentation/docs/resources/screenshots/053.jpg)
![](documentation/docs/resources/screenshots/054.jpg)
![](documentation/docs/resources/screenshots/055.jpg)

### Mobile Themed
![](documentation/docs/resources/screenshots/025.jpg)
![](documentation/docs/resources/screenshots/030.jpg)
![](documentation/docs/resources/screenshots/031.jpg)
![](documentation/docs/resources/screenshots/032.jpg)


## Useful Project Links
  [🔗 Git Repository](https://gitlab.com/CrazyAL013/sait-capstone-project)  
  [🔗 Developer Documentation](https://crazyal013.gitlab.io/sait-capstone-project)  
  [🔗 Issue Tracker](https://gitlab.com/CrazyAL013/sait-capstone-project/-/issues)  