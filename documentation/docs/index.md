![](./resources/logo.png){: align=right style="width:160px"}
# SAIT Capstone Project

## Developer Documentation
---

## Introduction
Our 2021 SAIT Captstone Project is to build an ecommerce website to support online product sales for a local aquarium services company.

The project will showcase several current technologies, including:

  - Angular single-page web application
  - Java RESTful API using the Spring framework
  - MySQL relational database

## Team
Alex Tompkins  
Daniel Tompkins  
Jack Graver  
Matthew Rempel  
Saurav Suthar  
Tait Hoyem  
Tommy Nguyen  

## Project Organization
The overall project is divided into four separate projects each contained within its own directory:

  - Database
  - Documentation
  - API
  - Web

## Useful Project Links
  [🔗 Git Repository](https://gitlab.com/CrazyAL013/sait-capstone-project)  
  [🔗 Developer Documentation](https://crazyal013.gitlab.io/sait-capstone-project)  
  [🔗 Issue Tracker](https://gitlab.com/CrazyAL013/sait-capstone-project/-/issues)  
