# Getting Started

## Get the Project
Clone the project from the [GitLab](https://gitlab.com) repo.

Using SSH:
```
git clone git@gitlab.com:CrazyAL013/sait-capstone-project.git
```
or HTTPS:
```
git clone https://gitlab.com/CrazyAL013/sait-capstone-project.git
```

The project contains four individual project directories:

- **database:** Database scripts
- **documentation:** Developer documentation
- **api:** REST API
- **web:** Angular single-page application

## Deployment

This project can be be deployed locally using Docker. It contains a `dockerfile` for each section of the project (web/api/database).

It also contains a `docker-compose` file at the top level that can be used to deploy the entire project. Please see the Docker section for more details.
## Documentation

This project uses MkDocs ([mkdocs.org](https://www.mkdocs.org/)) to provide a static documentation site. The markdown files used to generate the site are located in the `/documentation/docs` directory in the root of the project.

### Install MkDocs

```
pip install mkdocs-material --upgrade
pip install pymdown-extensions --upgrade
pip install Pygments --upgrade
pip install mkdocs-git-revision-date-localized-plugin --upgrade
```
### Running the Documentation Server Locally
To start a local copy of the documentation site:

```
cd documentation
mkdocs serve
```

A local server will be started on: [`http://localhost:8000`](http://localhost:8000).

### Hosted Documentation Site (GitLab)
The documentation website is also hosted on GitLab. [🔗](https://crazyal013.gitlab.io/sait-capstone-project)

#### GitLab CI/CD
The project uses GitLab continuous integration/deployment functionality to automatically build and deploy the documentation website whenever changes to the `/documentation` directory are committed to the repo. The website is typically updated within a few minutes of the commit.

The CI/CD Pipeline is defined in the `.gitlab-ci.yml` YAML configuration file in the project root.

=== ".gitlab-ci.yml"
    ``` yaml linenums="1"
    image: python:latest
    pages:
      stage: deploy
      only:
        changes:
        - documentation/**/*
      script:
        - pip install mkdocs-material pymdown-extensions Pygments mkdocs-git-revision-date-localized-plugin
        - cd documentation
        - mkdocs build --site-dir ../public
      artifacts:
        paths:
          - public
    ``` 

### Editing Documentation
We used [Visual Studio Code](https://code.visualstudio.com/) to create and edit the documentation. Opening the `documentation` directory in VSCode allows the entire project to edited in one place.

Starting a live local server allows you to view changes as they are saved to disk.

**NOTE**: The markdown flavour we are using is augmented by a number of extensions managed by MkDocs. Viewing the markdown in a standard editor may result in some of the markdown being rendered incorrectly.

## Source Code Management

In addition to Git, we decided to use [GitLab](https://gitlab.com) as our source code hosting and collaboration tool. 

### General SCM Guidelines
The following are general guidelines for using Git/GitLab for the capstone project.

- **Work on one thing at a time:** Wherever possible, try to ensure commits are always related to a single issue. If it is necessary to make changes than span more than one issue, stage and commit the changes separately for each issue.
- **Merge upstream changes regularly:** With a project that is actively under development, it is important to regularly pull upstream changes and merge them with your local repo. At a minimum you should pull on a daily basis.
- **Write good commit messages:** Your commit messages should inform other developers of the context of a change rather than simply what you changed. Always prefer a full title+body commit message and reserve the simplistic title-only commit messages only for the most straightforward changes.
- **Squash multiple local commits before pushing:** Complex changes may require multiple local commits as the work progresses. This type of incremental commit should be left locally and not be pushed to the remote until the change is complete. However, the incremental local commits create unnecessary clutter in the remote repository so best practice is to 'squash' the local commits into a single commit before pushing to the remote.


### Setup

#### Set local git user name/email
Make sure your git user name and email are set correctly:

``` bash
git config --global --list
```

If not, set them:

``` bash
git config --global user.name "Homer Simpson"
git config --global user.email "homer@simpsons.com"
```

#### Set default git text editor
Configure the default editor (replace `nano` with your editor of choice):
``` bash
git config --global core.editor nano
```

### Usage

#### Viewing the current state of the project
Before staging and committing, check the project status to ensure things are as expected:
``` bash
git status
```
#### Staging files for commit
Files are staged using `git add`. Each commit should consist ONLY of files related to the issue you are working on currently, so avoid using `git add .` which will stage all changed files indiscriminately. Some options for more selective staging are as follows:

##### Single file
``` bash
git add Scripts/ddl/storefront/create-database.sql
```
##### Single directory and all its contents
``` bash
git add database/\*
```

#### Squash multiple local commits before pushing
Multiple local commits for a single issue should be 'squashed' prior to pushing to the remote repo. Squashing the commits creates a single commit which represents the entire issue being resolved.
``` bash
git rebase -i origin/master
```
The editor will appear showing the local commits, for example:
``` text
pick 16b5fcc Code in, tests not passing
pick c964dea Getting closer
pick 06cf8ee Something changed
pick 396b4a3 Tests pass
pick 9be7fdb Better comments
```
Modify each entry EXCEPT the first, changing `pick` to `squash`:
``` text hl_lines="2 3 4 5"
pick 16b5fcc Code in, tests not passing
squash c964dea Getting closer
squash 06cf8ee Something changed
squash 396b4a3 Tests pass
squash 9be7fdb Better comments
```
Save the file and exit the editor. Git will open a new editor for you to type the new (single) commit message. After saving the message and exiting the editor, the commits will be squashed into a single commit, use `git log` to view the changes. You can now push the commit to the remote repo.

#### Writing good commit messages
There are lots of useful resources about writing good commit messages, [here's just one of them](https://chris.beams.io/posts/git-commit/).

#### Reference the GitLab issue tracker item
If the commit is related to an issue defined in the GitLab issue tracker, make sure you reference the issue number in the commit title:

``` bash
git commit -m"[#123] Fix typo in introduction to user guide"
```

GitLab will detect the reference and automatically close the referenced issue when the changes are pushed to the remote repo.

#### Simple commit
Simple commits require only a title (and issue number if applicable):
``` bash
git commit -m"[#123] Fix typo in introduction to user guide"
```
#### Complex commit
If the commit requires more explanation than just a simple title, omit the `-m` argument and git will open the configured global editor:
``` bash
git commit
```
``` text
[#123] This is the title for a more complex commit

This is the body where you explain things in more detail. Follow the best
practice guidelines to write useful commit messages. If the commit refers
to an issue in the issue tracker, put the reference in the title and also
at the end of the body message.

Resolves: #123
See also: #456
```

### Workflow
This is a suggested workflow for using git in the capstone project. This is just **one possible** workflow, there are others, lots and lots of them. 

It's important to understand that we're using this workflow because we're at the start of a project building version 1 and because we have relatively little time to complete things. Given these factors, we're trying to get features pushed as often as possible so other developers can integrate into their work. 

Normally we would probably be using branches and merging them as necessary but that's not really an option for the reasons already stated. So, we're all working on the main branch (master) and trying to ensure the local branch reflects the upstream branch as far as possible which is why we're using `git pull --rebase`. Rebase replaces merge and is useful for maintaining a linear project history because it applies all of your pending commits to the top of the remote tree.[^1]

  1.  **Always** check the git status to ensure you know what files are being tracked. This step is used to ensure you are only committing valid files to the repo:  
      `git status`
  2.  Stage local changes:  
      `git add <files>`
  3.  Commit the files locally (using a good commit message):  
      `git commit`
  4.  If you have any uncommitted local changes they must be stashed:  
      `git stash`
  4.  Now pull the remote branch to ensure you have any repo changes since your last pull:  
      `git pull --rebase origin master`
  5.  If git reports conflicts you now need to manually resolve those conflicts. The `rebase` command will merge one commit at a time so if you have multiple local commits you will need to resolve conflicts for the current commit and then move on to the next:  
      `git rebase --continue`
  6.  Now push your changes to the remote repo:  
      `git push origin master`
  7.  Lastly, restore stashed files (if any):  
      `git stash pop`

### Git Tips

#### Revert changes to working file
If you change a local file and need to revert the changes so that the file does not show up as modified:

``` bash
git checkout -- <file>
```

[^1]: [What's the difference between 'git merge' and 'git rebase'?](https://stackoverflow.com/a/16666418/4756724)
