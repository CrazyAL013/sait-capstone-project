# Code Snippets

## Adding a Delay to Data Retrieval
``` typescript linenums="1"
this.account$ = this.accountService.get(id).pipe(delay(5000));
```
You can add `.pipe(delay(x))` when retrieving data from a service in order to simulate the HTTP request lag/delay when work is being done locally.

## Returning an Empty Object Observable
``` typescript linenums="1"
  getNewAddress(): Observable<Address> { 
    let address = {} as Address;
    return of(address);
  }
```
When you want to generate a new object but the page you're loading relies on observables you can use a method like this in a service to return an empty object in an observable. The `of` keyword will take your object and return it as an observable.

## Environment: Development vs Production
It's useful to be able to execute certain statements based on whether the code is running in development or production environments.

``` ts 
import { environment } from '@env';
...
if (!environment.production) 
  console.log('DEBUG error message for development use only');
}
```

## Key-Value Pair Arrays
The Javascript `Map` object provides key-value pair array functionality and when coupled with Typescript also offers strict type checking too.

### Declaration
``` ts
private cartItems: Map<string, ICartItem>;
```

### Add Item
``` ts
this.cartItems.set(productItem.uid, cartItem);
```

### Delete Item
``` ts
this.cartItems.delete(productItem.uid);
```

### Find by Key
``` ts
if (this.cartItems.has(productItemId)) {
  ...
```

### Iterate
``` ts
this.cartItems.forEach((item => {
  ...
}));
```

### Clear
``` ts
this.cartItems.clear();
```





## Generate UUID's
[Already added to the project. Run `npm install` to update]  
Requires the [uuid npm package](https://www.npmjs.com/package/uuid):

``` bash
npm install uuid --save
npm install @types/uuid --save
```

Generate a (v4) UUID in code:

``` typescript
import * as uuid from 'uuid';
...
const id: string = uuid.v4();
```

## Synchronising Multiple Observable Subscriptions
If you need to execute two or more subscriptions and wait for both to complete before continuing (to render a template, for instance), there are two options:

### 1. Multiple Observables in `*ngIf`
To manage multiple observables in the HTML template, the following `*ngIf` syntax is available:

``` html+ng2 linenums="1"
<ng-container *ngIf="cartData$ | async, productData$ | async; else loading">
```

### 2. RXSJ `forkJoin()`
You can use the `forkJoin` function from the `rxjs` library to manage multiple observables in the component `.ts` code.

In the example below, we're creating multiple subscriptions by iterating over an array of Product Items to get id's and then calling the ProductService to retrieve each item from the REST API. 

``` typescript linenums="1"
private subscriptions: Subscription[] = [];
public allData$: Observable<any[]>;
...
private loadCart() {
  const subs: Observable<any>[] = [];
  subs.push(this.productService.getProductById(productId));
  subs.push(this.cartService.getCartItems());
  subs.push(this.accountService.getAddressBook(accountId));
  
  this.allData$ = forkJoin(subs);
  this.subscriptions.push(this.allData$.subscribe(results => {
    console.log(results);
  }));
}

ngOnDestroy() {
  this.subscriptions.forEach((s) => s.unsubscribe())
}
```
The example adds three observables to the `subs` array. Next `forkJoin` is called and the `Observable` array passed to it. The `forkJoin` function executes each observable concurrently and we can subscribe to it to get a single `Observable` result (`this.allData$`) when all observables have completed.

The `results` returned by subscribing to `forkJoin` are in the form of an array where `results[0]` is the result of the first observable, and so on.

We can use the `forkJoin` observable to control page rendering using Angular's `AsyncPipe`:

``` html+ng2 linenums="1"
<ng-container *ngIf="allData$ | async as data; else loading">
```