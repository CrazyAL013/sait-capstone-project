# Getting Started

This project provides an exemplar for creating scalable, enterprise-grade Angular projects. A number of Angular best-practices are encapsulated within the structure, including:

* routing-based navigation
* lazy-loaded child routes
* route guards
* authentication
* logging
* themes

The documentation is divided into two sections; Angular and Project. The Angular section contains notes about Angular features and best practices while the Project section contains information directly relevant to the project.

This documentation is not intended to be a tutorial. We tried to make notes on important topics as they came up and to provide some guidance for things we felt were complex or unusual in some way. 

## Get the Project
Follow [the instructions](../01_getting-started.md) to clone the project repository.

## Additional Setup
After cloning the project, change to the `web` project directory and install the required additional modules and packages:

```
npm install
npm audit fix
```

!!! note ""
    Node package managers `yarn` and `npm` are effectively interchangeable for most tasks. They are generally comparable in terms of features but it is recommended to use one or the other and not mix them.

## Angular Material UI
Angular Material is a UI component library built by the Angular team and ideally suited for use in Angular projects.

!!! note ""
    Angular Material is not the only option when choosing a UI component library for Angular. Other popular options include [Bulma](https://ng-bootstrap.github.io/#/home), [ng-Bootstrap](https://ng-bootstrap.github.io/#/home) and many more.

Material Components are styled internally which makes modifying the component styles more challenging that usual. Overriding default styling seems to be a hit and miss affair and is ultimately frustrating. Adding a custom class and associated styling does not generally have any affect. While it is certainly possible to override component styles, we did not pursue it any further.

Obviously the UI is designed to present a uniform experience so accepting the default styling is usually the path of least resistance.

## Running the code
Various scripts are defined in `package.json` to allow the application to be started during development.

To start the angular server and the Fake JSON API server concurrently:

In Linux/Mac:
```
npm start
```
In Windows:
```
npm run startwin
```


A local JSON server will be started on: [`http://localhost:3000`](http://localhost:3000).

A local Angular server will be started on: [`http://localhost:4200`](http://localhost:4200).

## Live Reload
The Angular server is configured to do a live reload when it detects any change to the project. In general this works well for most changes although we occasionally noticed that modifying some of the core app components (`app.module.ts`, `app.component.ts`, `app.routing.module.ts` and so on) required restarting the server before the changes would take effect.

## Debugging
Angular code can be debugged using the developer tools available in Firefox, Chrome or Chromium (possibly other browsers too).

The developer tools are activated using F12 and the code can be inspected, breakpoints set, and console output viewed as required. 

TypeScript is very useful for providing type-checking and other compile-time sanity checks but you should also get into the habit of checking the browser console for runtime errors.

Since Angular is an SPA, the application source code can be found in the following location:

 - Firefox: `Debugger` tab, `Webpack/src` entry.
 - Chrome/Chromium: `Sources` tab, `webpack/./src` entry.

!!! note ""
    Angular has the capability to 'lazy-load' modules on demand, so some code modules may not appear in the developer tools until the component has been loaded once.
 



