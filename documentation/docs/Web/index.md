# Introduction
## Developer Documentation: Web

Written by: Alex Tompkins & Daniel Tompkins

---

## Introduction
In preparation for our 2021 SAIT Captstone Project, we spent some time looking at Angular in an effort to provide some useful information to help the team get started with Angular development.

## Development Environment
This is our preferred development environment and the toolset we used:

 - OS: Linux ([Ubuntu 18.04/20.04](https://ubuntu.com/download/desktop))
 - Browser: Firefox, Chrome or Chromium
 - IDE: [Visual Studio Code](https://code.visualstudio.com/download)
 - SPA: [Angular 10](https://angular.io/guide/setup-local)
 - Language: [TypeScript](https://www.typescriptlang.org/)
 - CSS: [Sass](https://sass-lang.com/)
 - UI Components: [Angular Material UI](https://material.angular.io/)
 - Docs: [MkDocs](https://squidfunk.github.io/mkdocs-material/)

Although we chose to develop using Linux, the toolset should also be supported on Windows and Mac.

## Tutorial
Before going any further with this documentation, we recommend visiting the [Angular website](https://angular.io/), reading the [documentation](https://angular.io/docs) and working through the [Tour of Heroes](https://angular.io/tutorial) tutorial.


