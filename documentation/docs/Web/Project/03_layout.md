# Layout

Page layout defines the overall structure of a website. Modern websites utilise **responsive** layouts which allow them to adapt to the limitations imposed by different platforms such as desktop and mobile.

Numerous options exist for managing page layout, each with its own advantages and disadvantages. On balance, however, a straightforward CSS-based approach using Flexbox[^1] provides everything required to implement a responsive layout without the need for additional libraries. This is the approach used by this exemplar.

## Desktop-First vs Mobile-First
Layout design typically takes one of two approaches; desktop-first and mobile-first. Which one to use depends on the type of website and target audience. For the exemplar, we are designing a desktop-first layout and adapting for mobile afterwards.

Irrespective of the approach, mobile design tends towards stacking items in a single column with slide in/out menus.

## Responsive Layout
The exemplar uses a `BreakpointObserver` service from the `layout` package supplied with Angular Material[^2], to react to screen-size changes. Two separate methods are available: 

1. Using the `BreakpointObserver` service directly in a component - useful for selecting specific DOM element attribtues (i.e. CSS classes) based on viewport size.
2. Using the `ViewportSize` custom structural directive in a template - useful for rendering different DOM elements based on viewport size.

### BreakpointObserver Service
Angular Material provides the `BreakpointObserver` service to allow components to define observables and then react to screen-size changes.

The exemplar project implements the `BreakpointObserver` service in the layout component (`layouts` directory). The component defines the breakpoints that will be watched and then sets up the `BreakpointObserver` subscription for them. When the media state changes, the `BreakpointObserver` service calls subscribers and reports the changes. State changes are stored globally (`/core/globals.ts`) in the `mediaState` global variable which can be utilised in any component by injecting `Globals` in the constructor:

``` typescript linenums="1"
import { Globals } from '@core/globals';
...
export class SomeComponent {

  constructor( public globals: Globals) {...}

  someMethod() {
    if (this.globals.mediaState.XSmall) {
      ...
    }
  }
}
```

Once injected into the component, an associated template can also use the `mediaState` global variable to provide conditional rendering:

``` html+ng2 linenums="1"
<div [class.narrow]="globals.mediaState.Small">
  <span *ngIf="globals.mediastate.XSmall">...</span
</div>
```

### ViewPortSize Directive
The `ViewportSizeDirective` is a custom structural directive (like *ngFor and *ngIf) that is used to determine what DOM elements are rendered based on the viewport size.

Components use the directive in the same way as any structural directive. The example below demonstrates using the `max-width` media query[^3] to detect when the viewport size is less than or equal to 599.98px:

``` html+ng2
<div *ifViewportSize="'(max-width: 599.98px)'">SMALL</div>  
```

The `<div>` element is only rendered if the viewport size matches the media query.

Predefined sizes may also be used with the inclusion of the `ViewPortSizeEnum` in the component class:

``` ts linenums="1"
import { ViewportSizeEnum } from '@core/enums';
...
export class ContentLayoutComponent implements OnInit {
  public viewportSize = ViewportSizeEnum;
  ...
}
```

``` html+ng2
<div *ifViewportSize="viewportSize.Large">LARGE</div>
```

The `ViewPortSizeEnum` is located in `enums.ts` in the `core` directory and contains the full list of available predefined viewport sizes.




[^1]: [Flexbox Basics](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox), [Flexbox Guide](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox), [Flexbox Quick Reference](https://css-tricks.com/snippets/css/a-guide-to-flexbox/).
[^2]: [Angular Material Layout](https://material.angular.io/cdk/layout/overview).
[^3]: [CSS Media Queries](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries).