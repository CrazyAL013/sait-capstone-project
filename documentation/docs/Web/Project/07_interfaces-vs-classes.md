# Interface vs Classes

Javascript is not a class-based object oriented language.

When you write code like this: `let user = new User();` you are not actually creating a Customer object in the usual OO sense. Instead Javascript uses 'prototypal inheritance' which provides many similar features such as inheritance but does it in a different way. There are lots of resources about Javascript and prototypal inheritance.

Typsecript adds familiar OOP constructs as a veneer over Javascript, allowing code to operate in more traditional OO manner. However, Typescript must still transpile to plain Javascript so OO concepts are still constrained by the fact that inheritance is prototypal.

## Interface
An interface is a contract. It defines the shape of the object based on data contained within. Typescript uses interfaces to provide type-checking capabilities.

## Class
A class is a blueprint for creating reusable components. Classes allow us to use inheritance to create structured code.

## Interface vs Class
Use a class whenever you need to:

- Create (multiple) new instances
- Use inheritance
- Create singleton objects

For all other cases, use an interface.

When transpiled to Javascript, an interface generates considerably less code than a class and, by implication, reduces resource usage and performance impact. Never use a class to define a contract, use an interface instead.

## Class Usage
When creating a class, it is a good idea to also create a corresponding interface. This allows you to use either the class or interface as appropriate. 

The example below shows one method for creating classes in Typescript. This method allows fully optional named parameters to be used when calling the constructor and generally seems easier to use than other methods that try to overload the constructor.

``` ts linenums="1"
export interface IUser {
  id: string;
  username: string;
  fullName: string;
}

export class User implements IUser {
  public id: string;
  public username: string;
  public fullName: string;

  constructor(params: IUser = {} as IUser) {
    let {
      id,
      username,
      fullname
    } = params;

  this.id = id;
  this.username = username;
  this.fullName = fullName;
  )
}
```

Usage:

``` ts
const user = new User({ id: 'abc-123', fullName: 'Homer Simpson' });
```



## Declaration Merging
We can use namespace merging to provide the concept of nesting. This is particularly useful to avoid name collisions with class and interface names.

For instance, the code uses `ProductItem` in different ways depending on the situation. A `ProductItem` in a `Review` looks very different to a `ProductItem` in a `CartItem`. Rather than trying to name each uniquely (`ReviewProductItem`, `CartItemProductItem`) it's better to name them both `ProductItem` and use namespaces to differentiate.

Let's look at an example:

``` ts linenums="1"
export interface IAccount {
    id: string;
    admin: boolean;
    username: string;
    password: string;
    fullName: string;
    preferredName: string;
    email: string;
    idToken?: string;
}

export namespace IAccount {
    export interface IReview {
        id: string;
        title: string;
        description: string;
        rating: number;
        date: Date;
        verifiedPurchase: boolean;
        productItem: IProductItem;
    }

    export interface IProductItem {
        id: string;
        name: string;
        image: string;
    }
}
```

The example demonstrates namespace merging with interfaces but applies equally to classes. Notice that we have an interface and namespace named identically (`IAccount`). This is where the 'merge' takes place. We can use the exported interface/namespace as follows:

``` ts
let account: IAccount;
let review: IAccount.IReview
```

The first example uses the exported `IAccount` interface while the second uses the `IReview` interface contained within the `IAccount` namespace.

A more complex example using namespace merging with both interfaces and classes can be found in `cart-item.ts` in `/data/models`.