# Movies Page

## Components
```
/features/movies/movies.component.ts
/features/movies/movie-item/movie.item.component.ts
/features/movies/dialogs/movie-edit-dialog/movie-edit-dialog.component.ts
/features/movies/dialogs/movie-edit-dialog/movie-edit-form/movie-edit-form.component.ts
```

## Overview
The movies page demonstrates a full CRUD (Create Read Update Delete) implementation using a fake JSON REST API to provide data services.

The page is comprised of a main component `movies.component.ts` which is responsible for rendering a list of movies in paginated form. Each list item is a `movie.item.component.ts` which uses a Material Expansion Panel[^1] to display an individual movie entry.

Pagination is provided by a Material Paginator[^2] and loading utilises an NgxSpinner[^3] to provide user feedback while waiting for data to load.

Create and update functionality uses a Material Dialog[^4] to display an edit form which is used for both operations. The dialog component is implemented in `movie-edit-dialog.component.ts` and provides an example of component lazy-loading by loading its content (`movie-edit-form.component.ts`) on demand. The edit form uses a combination of sevaral Material form elements, including: Form Field[^5], Input[^6], and Select[^7].

## Context Menu
A context menu is implemented in `movies.component.ts` and triggered on each of the movie list elements. The menu provides `New`, `Edit`, and `Remove` functionality.

## Progress Indicator
The progress indicator used in the exemplar is a third-party add-on component called NgxSpinner[^3]. The spinner element `<ngx-spinner>` is used in the template (`movies.component.html`) and offers a number of attribute options to control its appearance/behaviour. The spinner is initialised in the `ngOnInit()` method of `movies.component.ts`.

To control whether the spinner is displayed, an `*ngIf` statement is used to determine whether the movies observable is still loading: 

```
<ng-container *ngIf="movies$ | async as movies; else loading">
```

If the data is still loading, an `<ng-template>` named `loading` (and containing the `<ngx-spinner>`) is rendered. Otherwise the movies list is rendered.

## Movie Edit Dialog
The movie edit dialog `movie-edit-dialog.component.ts` is perhaps the most complex part of the movie page exemplar. It combines separate dialog and form components and uses lazy-loading for the form component.

### Reactive Form
Angular reactive forms[^8] bind form controls to the data model and provides an immutable approach to managing state. Each form change creates a new state enabling the implementation of features such as undo/redo and background saves.

In the exemplar, the form spans two components, the dialog component `movie-edit-dialog.component.ts` contains the form element and managed the submit process while the body of the form is supplied by `movie-edit-form.component.ts` which contains the various form controls.

Reactive forms use a `FormGroup` object as a container for the form. An instance of a `FormGroup` is created in the dialog component and then passed down to the child component using an `@Input` property. This process is slightly complicated by the fact that the form component is lazy-loaded (see Lazy Loading for further details).

The form component takes the `FormGroup` reference and adds form controls (and any associated validations) to create the complete form.

### Lazy Loading
The dialog component `movie-edit-dialog.component.ts` is responsible for lazy-loading the form component `movie-edit-form.component.ts`. The `loadContentComponent` method handles the lazy loading operation using a combination of a dynamic import and a `ComponentFactory` to create an instance of the component and inject it into the `<ng-template>` element in the template for rendering. `@Input` properties are also passed down to the lazy-loaded component at this time.

=== "movie-edit-dialog.component.ts"
    ``` ts linenums="1"
      export class MovieEditDialogComponent implements OnInit {
        // Reference to the <ng-template> element in the template
        @ViewChild(TemplateRef, { read: ViewContainerRef })
        movieEditViewContainerRef: ViewContainerRef;

        ...

        private loadContentComponent(): void {
          // Dynamic import returns a Promise which (eventually) provides the requested module
          import('./movie-edit-form/movie-edit-form.component').then(
            ({ MovieEditFormComponent: MovieEditFormComponent }) => {
              // Get a ComponentFactory (used to dynamically load a component)
              let componentFactory: ComponentFactory<MovieEditFormComponent>;
              componentFactory = this.componentFactoryResolver.resolveComponentFactory(
                MovieEditFormComponent
              );
              // Use the ViewContainerRef to create and render the component into the DOM
              let componentRef: ComponentRef<MovieEditFormComponent>;
              componentRef = this.movieEditViewContainerRef.createComponent(
                componentFactory
              );
              // Pass the @Input variables for the (MovieEditFormComponent) component
              componentRef.instance.form = this.form;
              componentRef.instance.movie = this.movie;
              componentRef.instance.genres = this.genres;
            }
          );
        }
      }
    ``` 
=== "movie-edit-dialog.component.html"
    ``` html+ng2 linenums="1"
    <mat-dialog-content>
      <ng-template></ng-template>
    </mat-dialog-content>
    ```







[^1]: [Material Expansion Panel](https://material.angular.io/components/expansion/overview) is an Angular Material component.
[^2]: [Material Paginator](https://material.angular.io/components/paginator/overview) is an Angular Material component.
[^3]: [NgxSpinner](https://www.npmjs.com/package/ngx-spinner) is a third-party add-on component.
[^4]: [Material Dialog](https://material.angular.io/components/dialog/overview) is an Angular Material component.
[^5]: [Material Form Field](https://material.angular.io/components/form-field/overview) is an Angular Material component.
[^6]: [Material Input](https://material.angular.io/components/input/overview) is an Angular Material component.
[^7]: [Material Select](https://material.angular.io/components/select/overview) is an Angular Material component.
[^8]: [Angular Reactive Forms](https://angular.io/guide/reactive-forms).