# Organization
There is no single defined structure for an Angular application. The following represents an amalgamation of ideas from various sources along with best-practice guidelines from Angular.

## General
There are two ways to group features together in Angular; modules (`NgModule`) and barrels[^1] (`index.ts`). This structure uses barrels where lazy-loading is not required and modules where it is.

## Configuration
Angular projects are configured using `json` files located in the project root. Listed below are some of the more important files and their most notable uses.

=== "angular.json"
    Workspace and project configuration defaults. This is the core configuration file for an Angular project.[^2]

=== "package.json" 
    Contains npm package dependencies for the project and custom npm scripts. The `scripts` section defines npm scripts for performing various operations such as starting the Angular server, running the docs server, and so on.[^3]

=== "tsconfig.json"
    TypeScript configuration for use with the TypeScript compiler. The `paths` section contains entries which re-map imports relative to the `baseUrl`. This allows Angular imports to be simplified by using an `@` alias to represent a specific path within the project structure.[^4]

## Structure 

``` 
  .
  └── src
      ├── app
      │   └── core
      │   │   ├── data
      │   │   │   ├── models
      │   │   │   └── services
      │   │   ├── guards
      │   │   ├── interceptors
      │   │   ├── layouts
      │   │   │   └── partials       
      │   │   └── services  
      │   ├── features
      │   │   └── [home|about|contact...]
      │   └── shared
      │       ├── components      
      │       ├── directives
      │       └── pipes
      ├── assets
      ├── environments      
      └── styles
```

---

### Core
This directory contains classes used by the `AppModule` which should always be loaded, such as data models, route guards, HTTP interceptors, and application level services. Global variables (`globals.ts`) and enums (`enums.ts`) are also stored in the core directory.

=== "data"
    The data directory contains data models and associated data services.

=== "guards"
    Route guards are used to prevent users from navigating to parts of an app that require some kind of authentication.

=== "interceptors"
    Interceptors provide a way to intercept HTTP requests and responses and handle them before passing them along. Examples of usage include: converting HTTP to HTTPS, displaying a loader graphic to indicate the app is busy, converting returned data from one format to another, and so on.

=== "layouts"
    The layout directory contains components which act as a layout or parts of a layout. A `partials` subdirectory can be added to contain partial layout components that are used by the main layout components. Layout components must contain a `<router-outlet>` in their template and are eager-loaded from `app.module.ts`.

=== "services"
    Application level services (excluding data services) such as authentication and logging.
---

### Features
The features directory contains a collection of feature modules.

!!! note "Feature Module"
    A best practice used to organize components into a cohesive set of functionality providing modularity to an application.

Feature modules are generally analogous to individual web pages that provide each of the application's key features.

Feature modules, being separate from the root module and also independent of each other, provide the opportunity to implement a key piece of functionality: **lazy loading**. In addition to the usual module, component, template, and style files, a separate routing module is used to define the lasy-loaded child route or routes defined by the feature module.

A feature directory is named for the feature being implemented, i.e. `home`, `about`, `contact`, and so on. The feature directory contains the feature module and routing module along with the feature component, template and style files. Further associated feature modules may be nested below each feature directory.

---

### Shared
Components, directives and pipes that may be shared with other application components.

=== "components"
    Shared components implement functionality that may be used by one or more feature modules. Examples include a dialogs (login, message, confirmation etc.).

=== "directives"
    Directives are classes that provide additional functionality to templates. They are used to modify the behaviour and/or the structure of DOM page elements.  Angular provides a number of built-in directives.

=== "pipes"
    Pipes are classes that provide additional functionality to templates. A pipe is used to transform data for display purposes, such as formatting a date or currency value. Angular provides a number of built-in pipes.

---

## Angular CLI
The Angular CLI (Command Line Interface) provides `generate` functionality to create modules and components.

Start by generating a new `module` if one does not already exist:

``` bash
ng g m /features/account
```

This will create a new `account` module in the `features` directory (under `app`). An `account` directory will be created if necessary.

Next, create the components that belong to the `account` module:

``` bash
ng g c /features/account/orders
ng g c /features/account/orders/order
```

This will create two components, an `orders` component under the `account` module, and an `order` component under the `orders` component. Again, directories are created as necessary.

Using the Angular CLI will ensure that each component is also added to the parent module.

[^1]: [Barrels](https://basarat.gitbook.io/typescript/main-1/barrel).
[^2]: [Angular workspace configuration](https://angular.io/guide/workspace-config).
[^3]: [Angular npm dependencies](https://angular.io/guide/npm-packages).
[^4]: [Angular TypeScript configuration](https://angular.io/guide/typescript-configuration).

