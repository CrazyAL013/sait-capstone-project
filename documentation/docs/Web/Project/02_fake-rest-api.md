# Fake RESTful API

A typical SPA application requires a back-end REST API to deliver data to the front-end application.

In practice, however, development of the front-end and back-end components usually runs concurrently and the real REST API is simply not available as needed. Having a fake REST API that can be used for development and testing is a useful tool.

Using [JSON Server](https://www.npmjs.com/package/json-server) it is possible to rapidly deploy a local (to the application) REST API based on one or more `json` data files. The API will operate exactly like a real REST API, allowing the front-end code to contain actual data services which can be retasked to use the real API by simply changing a URL.