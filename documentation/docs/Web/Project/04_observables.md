# Observables

Observables are used to pass data between parts of the application. They are particularly useful for asynchromous progamming where the page must wait for data before rendering.

## REST API Communication
Communications between an application and a server backend process (REST API) are handled asynchronously. Javascript implements this communication using an observer pattern where the application makes a request to the server and supplies a callback function to be executed upon receipt of the response from the server.

### Controlling Rendering with Observables
Angular provides the 'async pipe' mechanism for controlling page rendering in the HTML template which watches the specified observable until it returns and then renders the elements. It also provides an `else` clause to cause something to be rendered (such as a spinner[^1]) while waiting.

=== "product.component.ts"
    ``` ts linenums="1"
    private subscriptions: Subscription[] = [];
    public product$: Observable<Product>;
    ...
    private loadProductItem(uid: string) {
      this.product$ = this.productService.getProductById(uid);
      this.subscriptions.push(this.product$.subscribe());
    }
    ```
=== "product.component.html"
    ``` html+ng2 linenums="1"
    <ng-container *ngIf="product$ | async as product; else loading">
      <h1>{{ product.name }}</h1>
    </ng-container>

    <ng-template #loading>
      <ngx-spinner bdColor="rgba(51,51,51,0.8)" size="medium" warn type="ball-clip-rotate">
      </ngx-spinner>
    </ng-template>
    ```

#### Multiple Observables
If multiple observables are involved, page rendering can wait for all to complete:

``` html+ng2 linenums="1"
<ng-container *ngIf="(product$ | async) as product, (categories$ | async) as categories; else loading">
  ...
</ng-container>
```

### Processing Observable Results
The pattern changes slightly if you need to process the results of an observable (in the component) when the subscription returns.

``` ts linenums="1" hl_lines="6"
  private subscriptions: Subscription[] = [];
  public product$: Observable<Product>;
  ...
  private loadProductItem(uid: string) {
    this.product$ = this.productService.getProductById(uid);
    this.subscriptions.push(this.product$.subscribe(results => {
      // Process the results here...
    }));
  }
```
Since we're processing the results of the subscription, the observable will return and trigger the async pipe in the template potentially before the results processing has completed. If the HTML template relies on the processing in some way, intermittent errors may occur as a result. This situation can be handled by an additional flag variable to indicate when the results processing is complete.

=== "product.component.ts"
    ``` ts linenums="1" hl_lines="2 8"
    public product$: Observable<Product>;
    public load: boolean = false;
    ...
    private loadProductItem(uid: string) {
      this.product$ = this.productService.getProductById(uid);
      this.subscriptions.push(this.product$.subscribe(results => {
        // Process the results here...
        this.load = true;
      }));
    }
    ```
=== "product.component.html"
    ``` html+ng2 linenums="1" hl_lines="1"
    <ng-container *ngIf="load && (product$ | async) as product; else loading">
      <h1>{{ product.name }}</h1>
    </ng-container>
    ```
### Managing Multiple Observables Concurrently
If a component requires multiple observables to be completed before performing a task, concurrent processing is the most effective solution.

To concurrently manage multiple observable you can use the `forkJoin` function from the `rxjs` library. The `forkJoin` function accepts an array of observables and subscribes to them all concurrently returning a single observable of its own when all subscriptions are complete.

``` typescript linenums="1"
private subscriptions: Subscription[] = [];
public allData$: Observable<any[]>;
...
private loadCart() {
  const subs: Observable<any>[] = [];
  subs.push(this.productService.getProductById(productId));
  subs.push(this.cartService.getCartItems());
  subs.push(this.accountService.getAddressBook(accountId));
  
  this.allData$ = forkJoin(subs);
  this.subscriptions.push(this.allData$.subscribe(results => {
    console.log(results);
  }));
}
```

The example adds three observables to the `subs` array which is then passed to the `forkJoin` function. The `forkJoin` function executes each observable concurrently and returns a single observable `allData$` when all observables have completed.

The `results` returned by subscribing to `forkJoin` are in the form of an array where `results[0]` is the result of the first observable, and so on.

We can use the `forkJoin` observable to control page rendering using Angular's 'async pipe'.

### Unsubscribing from Observables
It is important to unsubscribe from observables when the component is destroyed. Failure to do so will lead to memory leaks. The examples on this page push subscriptions to a class-level array which is then used in `ngOnDestroy` to unsubscribe from everything:

``` typescript linenums="1"
private subscriptions: Subscription[] = [];
...
ngOnDestroy() {
  this.subscriptions.forEach((s) => s.unsubscribe())
}
```




[^1]: The [ngx-spinner](https://www.npmjs.com/package/ngx-spinner) is a third-party addon.