# Authentication

We want to secure our REST API using JSON Web Tokens (JWT) and refresh tokens. With JWT, a user authenticates once and then receives a token from the API which it then uses whenever the two systems need to exchange data (instead of sending private credentials each time). 

The server issues two tokens during authentication: an `Id` token and a `Refresh` token. The Id token expires after a short time (i.e. 15 minutes) but, while the application is open in the browser, a timer will periodically poll the API and refresh it. 

Once the user closes the browser or tab, the Id token will become invalid and the application must send the Refresh token (from local storage) to the server and request a new Id token. A copy of the original Refresh token is stored on the server (in a database, for example) and the API checks it to ensure validity before issuing a new Id token. The Refresh token typically has a much longer expiry time (i.e. 7 days).

## Components

```
/core/app.initializer.ts
/core/guards/storefront-auth.guard.ts
/core/guards/warehouse-auth.guard.ts
/core/data/models/user.interface.ts
/core/interceptors/jwt.interceptor.ts
/core/interceptors/error.interceptor.ts
/core/interceptors/fake-jwt-auth.interceptor.ts
/core/services/auth.service.ts
/shared/components/login/login.component.ts
```

### App Initializer
An Angular [App Initializer](https://angular.io/api/core/APP_INITIALIZER) is a function that is injected at application startup and executed during application initialization. 

### Route Guards
Angular applications use [Route Guards](https://angular.io/guide/router#preventing-unauthorized-access) to secure specfific routes. When a specific route is requested, the Angular router calls a route guard to check for certain conditions. The guard then allows the routing request to proceed or redirects it elsewhere (to a login page, for instance).

### HTTP Interceptors
An Angular [Interceptor](https://angular.io/guide/http#intercepting-requests-and-responses) intercepts and handles an HTTPRequest or HttpResponse allowing the request/response to be transformed in some way before passing it along to the next interceptor in the chain.

Interceptors are created at startup by adding them to the root application injector. For the interceptors to work across the entire application, a single instance of `HttpClientModule` must be created in the `AppModule`.

!!! info "Fake JWT Authentication Interceptor"
    During development, we will use an HTTP interceptor (`fake-jwt-auth.interceptor.ts`) to intercept authentication requests from the app and return a 'fake' response. This test functionality will be removed once the REST API is able to handle authentication itself. The interceptor examines each request and handles any that are related to authentication. All other requests are simply passed along.

### Authentication Service
At the heart of the process is the authentication service (`auth.service.ts`). The service handles all authentication-related communication between the application and the API. The service is global to the application and components can inject it into their constructors to make use of functionality such as login and logout.

The authentication service exposes an observable `user` object (`Observable<User>`) which the component can monitor to receive notifications when a user logs in or out, or has their token refreshed.

### Login Component
The login component (`login.component.ts`) displays a login dialog which can handle multiple authentication-related operations:

1. Login
2. Create new account
3. Recover forgotten password
4. Change password
5. Admin login

## Process

### Step #1
When the Angular application runs, an app initializer (`app.initializer.ts`) is executed and attempts to automatically authenticate the user. If the user has logged in previously and the browser local storage still contains a valid refresh token, the initializer calls the API (via the authentication service) to request a fresh id token.

### Step #2
The authentication process is triggered in one of two ways:

1.  **The user tries to visit a page that requires authentication**  
    The Angular Router uses a Route Guard (`customer-auth.guard.ts` or `warehouse-auth.guard.ts`) to trigger the login process.
2.  **The user explicitly clicks a link to login**  
    The Authentication Service is called directly from the component to trigger the login process.

### Step #3:
The JWT Interceptor (`jwt.interceptor.ts`) intercepts the request to the API, checks local storage for a current id token and, if found, adds the token to the authorization header. 

1.  **The user is already logged in**  
    The id token is validated by the API and the request fulfilled.

2.  **The user is not already logged in**  
    The user is redirected to the login page to obtain the credentials. The API validates the user credentials and issues id and refresh tokens. The Authentication Service receives the tokens, saves the refresh token to local storage, and starts a refresh timer to renew the id token one minute before it expires. If the authentication request was generated by a Route Guard, the user is then redirected to the originally requested page.

3.  **The user's credentials are not accepted**  
    If the API rejects the user's credentials, a `401 Unauthorized` or `403 Forbidden` response is returned. The Error Interceptor (`error.interceptor.ts`) intercepts these error responses and automatically logs the user out of the application.

## Admininstrative Access
The authentication process can handle both regular and admin users. The authentication service (`auth.service.ts`) contains both `login()` and `adminLogin()` methods which allow the API to differentiate between login types.

!!! info "Storefront vs Warehouse"
    The website refers to administrative access as 'warehouse' which is separate and distinct from regular (storefront) user access. Separate accounts are required for storefront and warehouse access and only one account type may be logged-in at any one time. Both types of account use JWT authentication.

## Checking Authentication Status
The authentication status of a user can be checked using either a route guard or the authentication service.

### Route Guard
Two route guards are defined, Storefont (`storefront-auth.guard.ts`) and Warehouse (`warehouse-auth.guard.ts`). Adding one of these guards to a route will cause the login dialog to be displayed whenever an unauthenticated user tries to access the route.

``` typescript linenums="1" hl_lines="4"
{
    path: '',
    component: ContentLayoutComponent,
    canActivate: [StorefrontAuthGuard],
    children: [
      {
        path: 'account',
        loadChildren: () => import('@features/account/account.module').then((m) => m.AccountModule),
      },
    ],
  },
```

### Authentication Service
Whenever a component needs to explicitly check a user's authentication status, it can use the authentication service (`auth.service.ts`) to do so:

``` typescript linenums="1" hl_lines="7"
import { AuthService } from '@core/services';
...
export class SomeComponent{
  constructor( private authService: AuthService ) {...}

  someMethod() {
    if (this.authService.isLoggedIn) {
      ... 
    } 
  }
}
```

The authentication service also provides a way to check if the current user is an administrator:
``` typescript linenums="1"
if (this.authService.isLoggedIn && this.authService.isAdmin) {
  ... 
} 
```



## References
[Refresh Tokens: When to Use Them and How They Interact with JWTs](https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/)

[JWT Authentication with Refresh Tokens](https://jasonwatmore.com/post/2020/07/25/angular-10-jwt-authentication-with-refresh-tokens)