# Application

An Angular application is a hierarchy of components. At the root of the hierarchy is the parent ***Application*** component which gets rendered first when the application is bootstrapped.

As the parent, the Application component is also repsonsible for rendering any child components which, in turn, render their own child components. In this way the entire component hierarchy is recursively rendered.

## Example Application

The best way to visualize a component hierarchy is to look at an example application. Here's a mockup of an online store page:

![](../resources/angular-005.png)

### Components
Let's begin by identifying potential components:

![](../resources/angular-006.png)

There are four obvious candidates:

* **Navigation** - Visit other parts of the application
* **Breadcrumbs** - Show user's current location 
* **Product List** - A list of products  
* **Product** - An individual product

Theoretically it is possible to further divide the Product into components. The *image*, *SKU*, *product name*, *breadcrumb*, and *price* might conceivably make useful components however, at this point, it's not clear whether they offer any advantage over a single product component. 

### Component Hierarchy

Next we'll construct the component hierarchy:

![](../resources/angular-007.png)

### NgModules

With the components identified, we can now look at modules:

![](../resources/angular-008.png)

We have two NgModules:

* **Application** - The default NgModule for the entire application
* **Products** - An NgModule that groups the Product and Product List components

To identify NgModules we need to look for logical groupings of components. The application component is always contained in an application module which provides a global context for the entire Angular application. Within the application module we can create other modules to logically group functionality. 

The Navigation and Breadcrumbs components belong to the application module since they operate globally. The Product List and Product components are obviously related so we can logically group them together into a ***Products*** module.

## Summary
A mockup is a good starting point for designing an Angular application. The UI layout can be used to identify potential components and determine their place in the component hierarchy. Once the component hierarchy is complete it can be used to identify NgModules.

Together, NgModules and components provide the overall architecture for the application.
