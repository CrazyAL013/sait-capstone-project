# Routing
Angular routing determines which component to call based on a URL. The `<router-outlet>` template directive is used as a placeholder for the route and the router inserts the appropriate component after matching a route to the browser URL.

A separate routing module is used to define the routes available to the application. The routing modules contains a `Routes` array in which each route is defined. The order of routes is important because the `Router` uses a first-match-wins strategy when matching routes, so more specific routes should be placed above less specific routes.

## Routing Strategy
Two routing strategies are available; hash-style and HTML5. Hash-style routing uses `#` (anchor tag) characters in the URL to prevent the browser sending requests to the server (i.e. `https://example.com/#/about`). HTML5 routing uses regular URLs (i.e. `https://example.com/about`). Hash-style routing produces URLs that look unusual but avoids sending unnecessary requests to the server, HTML5 produces normal-looking URLs but requires the server to be configured to ignore the extraneous requests.

The type of routing used (HTML5 in our case) is specified in the module imports of `app.routing.module.ts`:

``` typescript linenums="1"
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  ...
```
## Child Routes
Routes can be nested by creating child routes. Child routes are relative to a component other than the root component. An eager-loaded child route requires adding an additional `<router-outlet>` to the application.

Child routes are specified in the `Routes` array by using a `children` array inside the parent route:

``` typescript linenums="1"
{
  path: '',
  component: ContentLayoutComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'dashboard',
      loadChildren: () =>
        import('@modules/home/home.module').then(m => m.HomeModule)
    },
    ...
```

## Lazy Loading
Lazy loaded modules improve application performance since they are only loaded as required. 

![](../resources/lazy-loading.png)

Whereas an eager-loaded route configuration specifies a component to be loaded, route configurations for lazy loading specify a module instead. When the route is loaded, the router loads the specified module which itself contains a reference to a separate routing module which is responsible for loading the child route component.

Angular identifies child routes using the module imports of the child routing module which specifies `.forChild` instead of `.forRoot` used by the root module routing. 
``` typescript linenums="1"
@NgModule({
  imports: [RouterModule.forChild(routes)],
  ...
```

### Lazy Loading Components
Typcially most lazy loading takes places at module-level during routing but it is also possible to lazy-load individual components. An example of where this might be useful is lazy-loading the content for a dialog, for example.

!!! note "Important"
    If a component is intended to be lazy-loaded, it is important to ensure that the component is never imported into a module anywhere in the application. Angular will automatically eager-load all imported components.
