# Boot Process
The application boot process is shown below:

![](../resources/boot-process.png)

The entry point to every Angular application is the `main.ts` file which contains this line:

``` ts
platformBrowserDynamic().bootstrapModule(AppModule)
```
The `platformBrowserDynamic` module indicates that we are about to boot Angular in a browser environment and the `bootstrapModule` method specifies the root module for the Angular application which, by convention, is named `AppModule`.

The  root module `bootstrap` property is used to specify the entry point component for the application, conventionally named `AppComponent`:

``` ts
bootstrap: [AppComponent]
```

Since the application is using the Angular router, the `AppModule` also imports `app-routing.module.ts` which contains route information. When the `AppComponent` loads, its HTML template contains a `<router-outlet>` tag which Angular replaces with the appropriate route component.

## Angular Window Provider
Referencing the global [`Window`](https://developer.mozilla.org/en-US/docs/Web/API/Window) object directly in an Angular component is considered poor practice. 

The exemplar offers one possible solution to resolve this problem by creating a service (`window.service.ts`) that can be used to inject `Window` into components.

The service is contained within the core module and exposed in `providers` as `WINDOW_PROVIDERS`.

The `WINDOW` injection token can then be used to inject the `window` into a component:

``` ts linenums="1"
import { WINDOW } from "../core/services/window.service";

export class IndexComponent {
  constructor(@Inject(WINDOW) private window: Window) {
    console.log(window);
  }
}
```

See [this article](https://brianflove.com/2018-01-11/angular-window-provider/) for more details about the service.
