# Angular Concepts


## What is Angular?
* A platform and framework for building single-page applications (SPA's).
* Uses HTML, CSS and Typescript.
* Libraries are imported to provide functionality.

## Application Concepts

![](../resources/angular-001.png)

* **NgModules** are containers for blocks of related functionality provided by components and services.
* **Components** are the basic building blocks for an application. Each NgModule contains the implementation for a piece of the UI known as a *view*.
* **Templates** define how a component view is rendered. They contain plain HTML and Angular elements which dynamically modify the HTML output in some way before rendering.
* **Services** are used by components to provide specific functionality not directly related to the view (database access, for example).
* **Decorators** are annotations that define how Angular should use a class.

---

### NgModules
Angular modules are classes that encapsulate a discrete piece of functionality. They can import additional functionality from other NgModules and export their own functionality to be used by other NgModules. In addition to the (well-known) reusability and organisational benefits gained from a modular approach to application design, Angular modules can also take advantage of a *lazy-loading* process which allows modules to be loaded on-demand, improving page load performance.

### Components and Templates
Components are classes that control a section of the UI called a *view*. Components contain the application logic required to support the view and define the associated *template*. Angular creates, updates, and destroys components as necessary.

![](../resources/angular-002.png)

A template combines HTML and Angular template syntax to create dynamically rendered content. Angular uses interpolation to incorporate calculated strings into HTML elements. Angular *template expressions* are used to generate the calculated strings.

#### Pipes and Directives
Pipes and directives are classes that provide additional functionality to templates. A pipe is used to transform data for display purposes, such as formatting a date or currency value. Directives are used to modify the behaviour and/or the structure of DOM page elements. Components are also directives. Angular provides a number of built-in pipes and directives.

### Services
Services are classes that provide functionality to an application. In keeping with a reusable and modular approach to design, services typically have a single purpose and operate independently of any NgModule or component.

![](../resources/angular-003.png)

#### Dependency Injection
As a general rule, Angular uses *dependency injection* (DI) to promote decoupling services from the modules that consume them. The Angular framework directly supports DI, allowing services to be injected into components via a constructor.

### Decorators
NgModules, components and services are simply classes. Decorators are used to mark the class type and provide metadata that tells Angular how to use them. Angular defines the following class decorators: `@Component()`, `@Directive()`, `@Pipe()`, `@Injectable()`, `@NgModule()`. Decorators may also be applied to class fields.

![](../resources/angular-004.png)

## Summary
Angular is used to build single-page applications (SPA's). Unlike a traditional web application which fulfills a request by loading entire new pages, an SPA loads into the browser as a single page and then dynamcially rewrites the page with new data from a web server.

An Angular application is comprised of at least one NgModule. NgModules are containers for groups of functionality implemented using Components and Services.

Components encapsulate both the functionality and display logic for a UI view. Templates combine HTML and Angular elements to define how a view is rendered.

Services provide specific, non-UI functionality to support components and are integrated into components using dependency injection. In dependency injection, the consumer class cedes responsibilty for object instantiation to some external component which, in turn, injects the new object instance into the consumer class via its constructor.

NgModules, Components and Services are all implemented as TypeScript classes. A class is annotated using decorators to tell Angular how it should be used.


