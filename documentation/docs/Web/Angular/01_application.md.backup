What is Angular?

Angular is used to build single-page applications (SPA's). Unlike a traditional web application which fulfills a request by loading entire new pages, an SPA loads into the browser as a single page and then dynamcially rewrites the page with new data from a web server.

An Angular application is comprised of at least one NgModule. NgModules are containers for groups of functionality implemented using Components and Services.

Components encapsulate both the functionality and display logic for a UI view. Templates combine HTML and Angular elements to define how a view is rendered.

Services provide specific, non-UI functionality to support components and are integrated into components using dependency injection. In dependency injection, the consumer class cedes responsibilty for object instantiation to some external component which, in turn, injects the new object instance into the consumer class via its constructor.

NgModules, Components and Services are all implemented as TypeScript classes. A class is annotated using decorators to tell Angular how it should be used.

    NgModules: Containers for blocks of related functionality provided by components and services.
        Components: The basic building blocks for an application. Each NgModule contains the implementation for a piece of the UI known as a view.
            Templates: Define how a component view is rendered. They contain plain HTML and Angular elements which dynamically modify the HTML output in some way before rendering.
        Services: Used by components to provide specific functionality not directly related to the view (database access, for example).
        Decorators: Annotations that define how Angular should use a class.

Components & Templates

Components are classes that control a section of the UI called a view. Components contain the application logic required to support the view and define the associated template. Angular creates, updates, and destroys components as necessary.

A template combines HTML and Angular template syntax to create dynamically rendered content. Angular uses interpolation to incorporate calculated strings into HTML elements. Angular template expressions are used to generate the calculated strings.
Services

Services are classes that provide functionality to an application. In keeping with a reusable and modular approach to design, services typically have a single purpose and operate independently of any NgModule or component.

Dependency Injection

As a general rule, Angular uses dependency injection (DI) to promote decoupling services from the modules that consume them. The Angular framework directly supports DI, allowing services to be injected into components via a constructor.
Decorators

NgModules, components and services are simply classes. Decorators are used to mark the class type and provide metadata that tells Angular how to use them. Angular defines the following class decorators: @Component(), @Directive(), @Pipe(), @Injectable(), @NgModule(). Decorators may also be applied to class fields.