# NgModules


## What are NgModules?
* Encapsulate a discrete piece of functionality
* Contain and provide scope for components and services
* Can import functionality from other NgModules
* Can export functionality for use by other NgModules

NgModules represent levels of abstraction. They are an organisational tool which promotes grouping components and services into cohesive blocks.

## Root Module
The first, or highest level of abstraction is the application itself. All Angular applications require a minimum of one NgModule which contains the entire application. This NgModule, known as the *root* module, provides the starting point or bootstrap for an Angular application and is typically named `AppModule`.

Simple applications typically require only the root module but as complexity increases collections of related functionality are refactored into additional *feature modules* which are then imported into the root module.

## @NgModule
Angular uses the `@NgModule` decorator to identify NgModules. An example `AppModule` is shown below:

``` ts linenums="1"
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports:      [ BrowserModule ],
  providers:    [ Logger ],
  declarations: [ AppComponent ],
  exports:      [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule {}
```
The import statements at the start of the code are regular JavaScript imports and provide access to symbols exported from other JavaScript files. They are not used by Angular.

The `@NgModule` decorator defines the TypeScript class as an NgModule and specifies a metadata object for Angular to use when initializing the class.

### Metadata

#### Imports
An array of (imported) NgModules used by the module. These imports are Angular-specific modules identified by the `@NgModule` decorator and are not related to the JavaScript imports.

#### Providers
An array of service providers used to configure the dependency injection system. The DI system creates instances of each specified provider and injects it into components, pipes and other services.

#### Declarations
An array of components, pipes and directives that belong to the module.

#### Exports
An array of *declarations* to be made visible to other modules.

#### Bootstrap
An array of components that Angular creates during the bootstrapping process and inserts into the `index.html` web page. While it's possible to have more than one component it's more usual to have only one which is typically named `AppComponent`. 