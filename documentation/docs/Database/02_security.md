# Security

Account security is provided using salted and hashed passwords. The hashed password field is sized to store the 256-bit hash values generated by SHA-256.

## API Access
The API will use a dedicated user account named `api` to access the databases. Currently a single account is planned but changing to two separate accounts would be trivial.

The API user account will have access restricted to SELECT, INSERT, UPDATE, and DELETE operations only.

The DDL script creates the API user without a password but also locks the user account to allow a secure password to be set manually (without exposing the password in a script).

The database uses a `Login` table to store user name, password salt, and password hash values. The `Login` table is linked to an `Account` table that stores the customer data. Additionally, a `Role` lookup table defines roles (user, admin etc.) for users. A user may belong to multiple roles (see the `UserRole` junction table).

