# Functions

Cuastom database functions can be used to simplify stored procedures and avoid repeating the same code.

## JSON Functions
Functions that return JSON can be used to simplify the confusing syntax generated when returning nested JSON in a stored procedure.

`GetAddressJSON(_AddressID int)`
 Given an Address ID, this function returns a JSON object representing an Address.

 `GetOrderItemsJSON(_OrderID int)`
 Given an Order ID, this function returns an array of JSON objects representing Order Items.


## ID/UID Functions
 `ID_FROM_UID(_UID varchar(36), _Table varchar(64))`
Given a UID and a table name, this function will return the corresponding integer ID value.

### Example:
```
SELECT ID_FROM_UID('ad9ddb60-4549-40cf-84ae-fd55fc688ebf', 'Account');
```
