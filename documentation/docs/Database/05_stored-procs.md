# Stored Procedures

## Organisation
Stored procedure can be grouped by operations (Category, Order, Account, and so on). A separate file should be created for each group of stored procedures.

The file should contain all procedures (CRUD) relating to the operation. So a group (and file) named `Category`, for instance, might contain separate stored procedures to manage creating, updating, retrieving and deleting categories.

## Naming Convention
Files are named for the operation they represent. A file containing stored procedures relating to Category operations should be named `category.sql`.

The individual stored procedures are named after the operation and the action and, optionally, a qualifier. Actions are one of: `Create`, `Update`, `Delete`, or `Get`.

`<operation><Create | Update | Delete | Get><qualifier>`

Qualifiers are used to indicate certain conditions and are usually associated with retieving data in specific ways.

For example:

- `CategoryCreate`: Create a new category
- `CategoryUpdate`: Update an existing category:
- `CategoryDelete`: UDelete an existing category:
- `CategoryGetById`: Retrieve a category by Id
- `CategoryGetAll`: Retrieve all categories

Note that stored procedure names all start with the 'group' name so that they will appear together when sorted.

## Header Comments
Each stored procedure should have a header comment which states its purpose and defines the paramaters and return value(s).

``` sql
/*
* Creates a category
* 
* @param {uuid} CategoryUID - The UUID of the category
* @param {string} Name - A string containing the category name
* @return {integer} - The ID of the new record
*/
```

## Formatting
The stored procedure source code should be formatted to make it easy to read: 

``` sql
DROP PROCEDURE IF EXISTS store.CategoryCreate;
DELIMITER //
CREATE PROCEDURE store.CategoryCreate (
	IN _CategoryUID varchar(36),
	IN _Name varchar(100),
	OUT _ID int
)
BEGIN
	INSERT INTO Category(
		UID,
		Name
	) VALUES (
		UUID_TO_BIN(_CategoryUID),
		_Name
	);
	SET _ID = LAST_INSERT_ID();
END //
DELIMITER ;
```

## Returning JSON
MySql has the capability to return results as formatted JSON. Interactions with the REST API that retrieve data (SELECT) should return results in JSON format.

``` mysql
/*
* Retrieves all categories and their subcategories
* 
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.CategoryGetAll;
DELIMITER //
CREATE PROCEDURE store.CategoryGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(UID),
		'name', Name,
		'subcategories', (
			SELECT JSON_ARRAYAGG(JSON_OBJECT(
				'id', BIN_TO_UUID(UID),
				'name', Name
			))
			FROM 
				Subcategory 
			WHERE 
				CategoryID = Subcategory.CategoryID
			ORDER BY Name
		)
	))
	FROM
		Category
	ORDER BY Name;
END //
DELIMITER ;
```

### Limitations

The `JSON_ARRAY_AGG` function does not guarantee sort order meaning that using it with sorted results does not work. The only option in this case is to return a normal resultset of ordered records and use `JSON_OBJECT` to create the records as valid JSON. The API will then need to convert the resultset into a JSON array. See `search.sql` for examples.