# Getting Started

The REST API will interact with the database using stored procedures which provide a greater degree of separation between the API code and the database than having embedded SQL queries within the code.

## Get the Project
Follow [the instructions](../01_getting-started.md) to clone the project repository.

## Project Organization
The database project is divided into `ddl`, `procs`, and `reports`, representing the Data Definition Language scripts for creating the database, the stored procedures for accessing the data, and business reports.

## Deploy Scripts
The database is defined by a number of scripts files each responsible for a particualr set of functionality (DDL, Functions, Stored Procedures etc.). To deploy the database these scripts need to be executed together.

It is important that the `create-database.sql` script (in `/ddl`) is executed first. The order of the remaining scripts is not important.

The following example commands assume MySQL is installed locally.

### Linux/Mac
```
cat ./Scripts/ddl/*.sql ./Scripts/functions/*.sql ./Scripts/procs/*.sql | mysql -u root -pYOURPASSWORD
```
