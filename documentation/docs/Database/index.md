# Introduction
## Developer Documentation: Database

Written by: Alex Tompkins & Daniel Tompkins

---

## Introduction
We selected MySQL as our database server because it is generally ubiquitous in hosting environments, offers a full-featured free edition, and has great community support.

## Development Environment
This is our preferred development environment and the toolset we used:

 - OS: Linux ([Ubuntu 18.04/20.04](https://ubuntu.com/download/desktop))
 - Host: Linux Containers - [LXD](https://ubuntu.com/server/docs/containers-lxd)
 - Database: [MySQL 8 Community Edition](https://dev.mysql.com/downloads/)
 - IDE: [DBeaver Community Edition](https://dbeaver.io/)
 - Docs: [MkDocs](https://squidfunk.github.io/mkdocs-material/)

We used a Linux container to host the database instance during development. The container was an Ubuntu 20.04 instance with MySql 8 installed.

Although we chose to develop using Linux, the toolset, with the exception of Linux Containers, should also be supported on Windows and Mac.



