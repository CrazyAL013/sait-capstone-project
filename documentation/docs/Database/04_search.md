# Search
Product search is crucial to operation of the website. Search must be fast and provide results ordered by closest match.

## MySQL Full-Text Search
Full-text queries perform linguistic searches against text data in full-text indexes by operating on words and phrases based on the rules of a particular language such as English. Full-text queries can include simple words and phrases or multiple forms of a word or phrase. A full-text query returns any documents that contain at least one match (also known as a hit). A match occurs when a target document contains all the terms specified in the full-text query, and meets any other search conditions, such as the distance between the matching terms

## Implementation
There are a couple of steps needed to implement full-text search.

### Table Structure
For best performance it is necessary to flatten (denormailize) the database structure to avoid table joins. We have implemented a single table `ProductSearch` dedicated to full-text search.

``` mysql
CREATE TABLE store.ProductSearch (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) NOT NULL,
  `ProductItemID` int(11) NOT NULL,
  `Keywords` longtext NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ProductSearch_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```
The table contains foreign key references to the Product and ProductItem tables and a `Keywords` field that contains all relevant keyword data for each product.

To implement full-text search we need a `FULLTEXT` index to be added to the `Keyword` field:

``` mysql
ALTER TABLE store.ProductSearch ADD FULLTEXT (Keywords);
```

### Triggers
To ensure the `ProductSearch` table is kept up-to-date we have implemented triggers to handle `INSERT`, `UPDATE`, and `DELETE` operations.

Whenever a record in the `ProductItem` table is inserted, updated, or deleted, a trigger will automatically make the corresponding changes to the `ProductSearch` table.

``` mysql
CREATE TRIGGER store.ProductItemInsert
	AFTER INSERT ON store.ProductItem FOR EACH ROW
		INSERT INTO ProductSearch
			(ProductID, ProductItemID, Keywords)
		SELECT
			NEW.ProductID,
			NEW.ID,
			CONCAT(LOWER(P.Keywords), " ", LOWER(NEW.Keywords))
		FROM 
			Product P, ProductItem PI
		WHERE 
			P.ID = PI.ProductID AND
			P.ID = NEW.ProductID AND
			PI.ID = NEW.ID;

CREATE TRIGGER store.ProductItemUpdate
	AFTER UPDATE ON store.ProductItem FOR EACH ROW
		UPDATE ProductSearch
		SET Keywords = 
		(
			SELECT
				CONCAT(LOWER(P.Keywords), " ", LOWER(NEW.Keywords))
			FROM 
				Product P, ProductItem PI
			WHERE 
				P.ID = PI.ProductID AND
				PI.ID = NEW.ID
		)
		WHERE ID = NEW.ID;

CREATE TRIGGER store.ProductItemDelete
	AFTER DELETE ON store.ProductItem FOR EACH ROW
		DELETE FROM ProductSearch
		WHERE ProductItemID = OLD.ID;
```

### Limitations
Full-text search operates on words. Punctuation and special charcters are removed before indexing. There are also predefined limitations such as a minimum word length (4 by default) which can be changed if required.

This behaviour presents a problem when dealing with things such as model numbers where `ap-3`, for example, would be treated as `ap` and `3` separately and then ignored completely since neither word is 4 or more characters in length.

One solution to this problem is to replace characters such as the hypen with a character that is recognised as a letter. MySql defines the underscore as a character and does not modify strings containing it. Therefore converting `ap-3` to `ap_3` before saving to the database would result in the word `ap_3` being indexed correctly. Of course this means that search keywords must also be modified in the same way to ensure a correct match.

Since the product data entry takes place in the website, the keywords will need to be converted before being sent to the API. Assuming that users are enetering space-separated keywords, the code would need to replace all non-alphanumeric characters with underscores.