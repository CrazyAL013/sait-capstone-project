# Docker

## Intro

First, [install Docker on your machine](https://docs.docker.com/get-docker/).

Then install `Docker Compose`:
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

Then it is highly recommended to install the Docker extension for `Visual Studio Code`. This will make managing the container much easier.

```
ms-azuretools.vscode-docker
```

This project makes use of a Docker compose file to automate the building of the three individual Docker containers. After installing Docker run the following command in `/sait-capstone-project`:
```
docker-compose build --no-cache && docker-compose up -d --force-recreate
```

This will run the compose file which will in turn run the the Docker build files located in each section of the project. (web, api and database)

## Building Individual Containers
To build an individual container navigate to the same folder as the `dockerfile` then use this:
```
docker build -t sait-capstone-front .
```

## Additional Information

If you change any of the api code please recreate the jar file.
```
./mvnw clean package
```
At this point in time I wasn't able to get the Docker container to do this for me.

## Other Commands



## Cleanup
After you have finished using the containers you will need to remove the containers and the images. This can be done easily through the Docker VSCode extension or the command line.

(At some point I'll see if this can be automated.)