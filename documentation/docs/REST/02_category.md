# Category

The ```category``` object is the data for a single category

## End Points

- ```GET /v1/category/```
- ```POST /v1/category/```
- ```GET /v1/category/{id}```
- ```PUT /v1/category/{id}```
- ```DELETE /v1/category/{id}```

## The Category Object 

=== "category.json"
    ``` json linenums="1"
    {
        "id": "34fdd45c-5ae6-11eb-ae93-0242ac130002",
        "name": "Plants",
    }
    ```
---
```id``` *String*
: The UUID for the object
---
```name``` *String*
: The name of the category
--- 

## Get All Categories 

#### End Point
```GET /v1/category/```

#### Response
- ```200```: An array of all the categories 
- ```404```: No categories found

--- 

## Create a Category

#### End Point
```POST /v1/category/```

#### Request Body

=== "new_category.json"
    ``` json linenums="1"
        {
            "name": "Coral",
        }
    ```
#### Parameters 
- ```"key": "token 5429835702893"```

#### Response
- ```200``` The category was created

--- 

## Get Specific Category

#### End Point
```GET /v1/category/{id}```

#### Path variables
- ```{id}```: The UUID of the category

#### Response
- ```200```: A category object

--- 

## Update Specific Category

#### End Point
```PUT /v1/category/{id}```

#### Path variables
- ```{id}```: The UUID of the category

#### Response
- ```200```: A category object


