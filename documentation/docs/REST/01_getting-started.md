# Getting Started

## Get the Project

Follow [the instructions](../01_getting-started.md) to clone the project repository.

## Requirements

- [VSCode](https://code.visualstudio.com)
- [Java 11](https://www.java.com/en/download/)

VSCode Extensions (These are not mandatory but if you intend to contribute to the development they are very helpful)

- [Spring Initializer Java Support](https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-spring-boot)
- [Spring Boot Tools](https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-spring-boot)
- [Spring Boot Dashboard](https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-spring-boot)

## Running the Project

Navigate to the `api/store/` directory and run the project

```
$ cd api/store/
$ ./mvnw clean spring-boot:run
```

Navigating to [http://localhost:8080/v1/category/](http://localhost:8080/v1/category/) should now show you a JSON list of all the categories in the database
### Possible Problems

If you get the following error

`com.mysql.cj.jdbc.exceptions.CommunicationsException: Communications link failure`

You will need to ensure the database is running on your local machine with the following specifications.

```
url=jdbc:mysql://localhost:3306/
username=root
password=password
```

You can also optionally change the database that Spring Boot will attempt to connect to in the application.properties file
