# Introduction
## Developer Documentation: REST API


---

## Introduction
...

## Development Environment
This is our preferred development environment and the toolset we used:

 - OS: Linux ([Ubuntu 18.04/20.04](https://ubuntu.com/download/desktop))
 - Browser: Firefox, Safari, Chrome or Chromium
 - IDE: [VSCode](https://code.visualstudio.com)
 - Language: [Java 11](https://www.java.com/en/download/)
 - REST Framework: [Spring Boot 2.4.2](https://spring.io/projects/spring-boot)
 - API Tools: [Postman](https://www.postman.com/)
 - Docs: [MkDocs](https://squidfunk.github.io/mkdocs-material/)

Although we chose to develop using Linux, the toolset should also be supported on Windows and Mac.


