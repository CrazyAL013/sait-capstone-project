# Documentation
This project uses MkDocs ([mkdocs.org](https://www.mkdocs.org/)) to provide a static documentation site.

---

## Install
Install MkDocs (and the Material theme):

```
pip install mkdocs-material --upgrade
pip install pymdown-extensions --upgrade
pip install Pygments --upgrade
pip install mkdocs-git-revision-date-localized-plugin --upgrade
```

**NOTE:** You may need to substitute `pip3` instead of `pip`, depending on your system set-up.

---

## Serve
To start a local copy of the documentation site (from `/documentation`):

```
mkdocs serve
```

A local server will be started on: http://127.0.0.1:8000.
