package com.aquarium;

import com.easypost.EasyPost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Main application class for running the SpringBoot Application 
 */
@SpringBootApplication
public class Application {

	/**
	 * Main java method to run the application 
	 * @param args Arguments passed in to running the application
	 */
	public static void main(String[] args) {
		EasyPost.apiKey = "EZTK8ba9f862f019463bbdb284a48cc71d88CNXilk0N64oQpHOXdFvmyQ";
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Configuration to allow or disallow based on CORS
	 * @return
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowCredentials(true).allowedOrigins("http://localhost:4200")
						.allowedHeaders("*").allowedMethods("OPTIONS", "GET", "POST", "PUT", "DELETE");
			}
		};
	}
}
