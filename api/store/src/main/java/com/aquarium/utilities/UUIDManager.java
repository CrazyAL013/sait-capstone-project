package com.aquarium.utilities;

import java.nio.ByteBuffer;
import java.util.UUID;

public class UUIDManager {
    /**
     * Convert Byte[] to a UUID object
     * @param bytes of the UUID
     * @return UUID 
     */
    public UUID getUUIDFromBytes(byte[] bytes) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        Long high = byteBuffer.getLong();
        Long low = byteBuffer.getLong();
        return new UUID(high, low);
    }
}
