package com.aquarium.utilities;

import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Will unwrap and retrieve data from stored procedure output
 */
public class CallStatementUnwrapper {

    /**
     * Will extract a result set and return it as an object
     * @param resultSetData The result set data
     * @return The extracted result set
     * @throws IndexOutOfBoundsException if there is no result set
     */
    public Object extractResultSet(Map<String, Object> resultSetData) throws IndexOutOfBoundsException {
        List<Map<String, Object>> results = (List<Map<String, Object>>) resultSetData.get("#result-set-" + 1);

        Map<String, Object> map = results.get(0);
        Object actualData = null;
        for (Object value : map.values()) {
            actualData = value;
        }

        return actualData;
    }

    /**
     * Will extract a full result set and return it as an object
     * @param resultSetData The result set data
     * @return The extracted result set
     */
    public Map<String, Object> extractFullResultSet(Map<String, Object> resultSetData) {
        List<Map<String, Object>> results = (List<Map<String, Object>>) resultSetData.get("#result-set-" + 1);
        try {
            Map<String, Object> map = results.get(0);
            return map; 
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts the resultset into a JSON array (to retain sort order)
     * Calls database procedure 'SearchBySubcategory()'
     * 
     * @param resultset - a resultset Map 
     * @return A JsonNode object containing the JSON array
     */
    @SuppressWarnings("unchecked")
    public JsonNode buildJsonArray(Map<String, Object> resultset) {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();
        List<Map<String, Object>> results = (List<Map<String, Object>>) resultset.get("#result-set-1");
        results.forEach(product -> {
            try {
                JsonNode node = mapper.readTree((String) product.get("ProductJSON"));
                arrayNode.add(node);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        return arrayNode;
    }

    /**
     * Get the update count from an SQL operation
     * @param resultset The result set data
     * @return the update count
     */
    public String extractUpdateCount(Map<String, Object> resultset) {
        return String.valueOf(resultset.get("#update-count-1"));
    }

    /**
     * Check that there is a result set
     * @param resultset to check
     * @return if there is a result set
     */
    public Boolean hasResultSet(Map<String, Object> resultset) {
        List<Map<String, Object>> results = (List<Map<String, Object>>) resultset.get("#result-set-" + 1);
        return !results.isEmpty();
    }

    /**
     * Check that there is a valid token
     * @param resultset to check
     * @return if there is a valid token
     */
    public Boolean hasValidToken(Map<String, Object> resultset) {
        List<Map<String, Object>> results = (List<Map<String, Object>>) resultset.get("#result-set-" + 1);
        try {
            Map<String, Object> map = results.get(0);
            return (Boolean) map.get("IsValidToken"); 
        } catch (Exception e) {
            return false;
        }
    }

}
