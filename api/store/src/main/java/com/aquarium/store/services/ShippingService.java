package com.aquarium.store.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aquarium.store.models.Country;
import com.aquarium.store.models.Region;
import com.easypost.exception.EasyPostException;
import com.easypost.model.Address;
import com.easypost.model.Rate;
import com.easypost.model.Shipment;

import org.springframework.stereotype.Service;

/**
 * Service for Shipping
 */
@Service
public class ShippingService {

    /**
     * Creates a new shipment
     * @param from where shipment is coming from
     * @return shipment rates
     * @throws EasyPostException
     */
    public List<Rate> createShipment(com.aquarium.store.models.Address from) throws EasyPostException {
        Map<String, Object> parcelMap = new HashMap<String, Object>();
        parcelMap.put("height", 5);
        parcelMap.put("width", 10.9);
        parcelMap.put("length", 20.2);
        parcelMap.put("weight", 65.9);

        // Parcel parcel = Parcel.create(parcelMap);

        Map<String, Object> toAddressMap = new HashMap<String, Object>();
        toAddressMap.put("name", "TODO: Get Name");
        toAddressMap.put("street1", from.getLine1());
        toAddressMap.put("street2", from.getLine2());
        toAddressMap.put("city", from.getCity());
        toAddressMap.put("state", from.getRegion().getCode());
        toAddressMap.put("country", from.getCountry().getCode());
        toAddressMap.put("zip", from.getPostalZip());
        toAddressMap.put("phone", "1234567890");

        Map<String, Object> fromAddressMap = new HashMap<String, Object>();
        fromAddressMap.put("name", "Benjamin's Aquarium Services");
        fromAddressMap.put("street1", "184 Sherwood Square NW");
        fromAddressMap.put("street2", "");
        fromAddressMap.put("city", "Calgary");
        fromAddressMap.put("state", "AB");
        fromAddressMap.put("country", "CA");
        fromAddressMap.put("zip", "T3R0R7");
        fromAddressMap.put("phone", "4034782296");
        fromAddressMap.put("company", "Benjamin's Aquarium Services");


        // Map<String, Object> customsInfoMap = new HashMap<String, Object>();

        Address toAddress = Address.create(toAddressMap);
        System.out.println(toAddress.getId());

        Address fromAddress = Address.create(fromAddressMap);
        System.out.println(fromAddress.getId());

        Map<String, Object> shipmentMap = new HashMap<String, Object>();
        shipmentMap.put("to_address", toAddressMap);
        shipmentMap.put("from_address", fromAddressMap);
        shipmentMap.put("parcel", parcelMap);
        // shipmentMap.put("customs_info", customsInfoMap);

        Shipment shipment = Shipment.create(shipmentMap);

        List<Rate> rates = shipment.getRates();

        // Rate personalRate = new Rate("", "Benjamin's Aquarium Services", "", 25.00, "CAD", 25.00, "CAD", 25.00, "CAD", 1, "", true, 1, "", "");
        // Rate personalRate = new Rate("id", "carrier", "service", 25.00, "CAD", 25.00, "CAD", 25.00, "CAD", 1, "", true, 1, "none", "none");
        // Rate personalRate = new Rate();
        // rates.add(personalRate);

        System.out.println(shipment.toString());

        return rates;
    }

    /**
     * Creates a postage label for shipment
     * 
     * @param rateID id of rate
     * @param shipmentID id of shipment
     * @return the postage label for shipment
     * @throws EasyPostException
     */
    public Object purchaseRate(String rateID, String shipmentID) throws EasyPostException {
        Shipment shipment = Shipment.retrieve(shipmentID);
        Map<String, Object> buyMap = new HashMap<String, Object>();
        List<Rate> rates = shipment.getRates();

        for (Rate rate : rates) {
            if (rate.getId().equals(rateID)) {
                buyMap.put("rate", rate);
                shipment.buy(buyMap);
            }
        }

        // System.out.println(shipment);

        return shipment.getPostageLabel();
    }

    /**
     * Tests if EasyPost is active
     * @return boolean for active or not
     * @throws Exception
     */
    public Boolean easyPostIsUp() throws Exception {
        com.aquarium.store.models.Address from = new com.aquarium.store.models.Address();
        Region region = new Region(1, 0.05f, "AB", "Alberta", 1);
        Country country = new Country(1, "Canada", "CA", "CA", "$", 0.05f);

        from.setLine1("333 Edge Park Blvd. NW");
        from.setLine2("333 Edge Park Blvd. NW");
        from.setCity("Calgary");
        from.setCountry(country);
        from.setRegion(region);
        from.setPostalZip("T3A 4K4");

        createShipment(from);

        return true;
    }
}
