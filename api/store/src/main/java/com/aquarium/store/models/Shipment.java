package com.aquarium.store.models;

import java.util.Date;

/**
 * Model for Shipment data
 */
public class Shipment {
    private String id;
    private String carrier;
    private String service;
    private int rate;
    private String trackingNumber;
    private int deliveryDays;
    private Date shipDate;
    private String rateId;
    private String shipmentId;

    /**
     * Constructor for Shipment objects
     * @param id shipment id
     * @param carrier shipment carrier
     * @param service shipment service
     * @param rate rate of shipment
     * @param trackingNumber shipment tracking number
     * @param deliveryDays days for delivery
     * @param shipDate date it was shipped
     * @param rateId the id of shipping rate
     * @param shipmentId shipment id
     */
    public Shipment(String id, String carrier, String service, int rate, String trackingNumber, int deliveryDays,
            Date shipDate, String rateId, String shipmentId) {
        this.id = id;
        this.carrier = carrier;
        this.service = service;
        this.rate = rate;
        this.trackingNumber = trackingNumber;
        this.deliveryDays = deliveryDays;
        this.shipDate = shipDate;
        this.rateId = rateId;
        this.shipmentId = shipmentId;
    }

    /**
     * ID getter method
     * @return shipment id
     */
    public String getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new shipment id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Carrier getter method
     * @return shipment carrier
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Carrier setter method
     * @param carrier new shipment carrier
     */
    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    /**
     * Service getter method
     * @return shipment service
     */
    public String getService() {
        return service;
    }

    /**
     * Service setter method
     * @param service new shipment service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * Rate getter method
     * @return shipment rate
     */
    public int getRate() {
        return rate;
    }

    /**
     * Rate setter method
     * @param rate new shipment rate 
     */
    public void setRate(int rate) {
        this.rate = rate;
    }

    /**
     * Tracking number getting method
     * @return shipment tracking number
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * Tracking number setter method
     * @param trackingNumber new shipment tracking number
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * Delivery days getter method
     * @return total days for shipment
     */
    public int getDeliveryDays() {
        return deliveryDays;
    }

    /**
     * Delivery days setter method
     * @param deliveryDays new total days for shipment
     */
    public void setDeliveryDays(int deliveryDays) {
        this.deliveryDays = deliveryDays;
    }

    /**
     * Ship date getter method
     * @return date of shipment
     */
    public Date getShipDate() {
        return shipDate;
    }
    
    /**
     * Ship date setter method
     * @param shipDate new date for shipment
     */
    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    /**
     * Rate id getter method
     * @return shipment rate id
     */
    public String getRateId() {
        return rateId;
    }

    /**
     * Rate id setter method
     * @param rateId new shipment rate id
     */
    public void setRateId(String rateId) {
        this.rateId = rateId;
    }

    /**
     * Shipment id getter method
     * @return shipment id
     */
    public String getShipmentId() {
        return shipmentId;
    }

    /**
     * Shipment id setter method
     * @param shipmentId new shipment id
     */
    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

}
