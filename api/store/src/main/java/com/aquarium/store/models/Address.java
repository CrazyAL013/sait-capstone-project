package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for address data
 */
public class Address {
    private UUID id;
    private UUID accountId;
    private String description;
    private String line1;
    private String line2;
    private String city;
    private String postalZip;
    private Region region;
    private Country country;

    /**
     * Default constructor for Address objects
     */
    public Address() {}

    /**
     * Full constructor for Address objects
     * 
     * @param id the id for this address
     * @param accountId the account id for this address
     * @param description the description of the address
     * @param line1 the first line of the address
     * @param line2 the second line of the address
     * @param city the city for this address
     * @param postalZip the postal code for this address
     * @param region the region for this address
     * @param country the country for this adderss
     */
    public Address(UUID id, UUID accountId, String description, String line1, String line2, String city,
            String postalZip, Region region, Country country) {
        this.id = id;
        this.accountId = accountId;
        this.description = description;
        this.line1 = line1;
        this.line2 = line2;
        this.city = city;
        this.postalZip = postalZip;
        this.region = region;
        this.country = country;
    }

    /**
     * Id getter method
     * @return return address id
     */
    public UUID getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id the new id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * Account id getter method
     * @return the account id for this address
     */
    public UUID getAccountId() {
        return accountId;
    }

    /**
     * Account id setter method
     * @param accountId the new account id for this address
     */
    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }
    
    /**
     * Description getter method
     * @return the description for this address
     */
    public String getDescription() {
        return description;
    }

    /**
     * Description setter method
     * @param description the new description for this address
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Line 1 getter method
     * @return the first line of the address
     */
    public String getLine1() {
        return line1;
    }

    /**
     * Line 1 setter method
     * @param line1 new value for line 1
     */
    public void setLine1(String line1) {
        this.line1 = line1;
    }

    /**
     * Line 2 getter method
     * @return the second line of the address
     */
    public String getLine2() {
        return line2;
    }

    /**
     * Line 2 setter method
     * @param line2 new value for line 2
     */
    public void setLine2(String line2) {
        this.line2 = line2;
    }

    /**
     * City getter method
     * @return the city of this address
     */
    public String getCity() {
        return city;
    }

    /**
     * City setter method
     * @param city new value for address city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Postal code getter method
     * @return the postal code of this address
     */
    public String getPostalZip() {
        return postalZip;
    }

    /**
     * Postal code setter method
     * @param postalZip new value for postal code
     */
    public void setPostalZip(String postalZip) {
        this.postalZip = postalZip;
    }

    /**
     * Region getter method
     * @return the region of this address
     */
    public Region getRegion() {
        return region;
    }

    /**
     * Region setter method
     * @param region new value for address region
     */
    public void setRegion(Region region) {
        this.region = region;
    }

    /**
     * Country getter method
     * @return the country of this address
     */
    public Country getCountry() {
        return country;
    }

    /**
     * Country setter method
     * @param country new value for address country
     */
    public void setCountry(Country country) {
        this.country = country;
    }
}