package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for ProductItem data
 */
public class ProductItem {

    private UUID id;
    private boolean display;
    private String model;
    private String sku;
    private String upc;
    private String name;
    private String description;
    private Specification[] specifications;
    private String keywords;
    private Image[] images;
    private double unitPrice;
    private double weight;
    private int stockQuantity;
    private int reorderLevel;
    private int totalSold;
    private int averageRating;

    /**
     * Default constructor for ProductItem objects
     */
    public ProductItem() {}

    /**
     * Full constructor for ProductItem objects
     * @param id product item id
     * @param display boolean to display or not
     * @param model product item model number
     * @param sku product item sku number
     * @param name product item name
     * @param description product item description
     * @param specifications product item specifications data
     * @param keywords product item keywords
     * @param images product item images
     * @param unitPrice product item unit price 
     * @param weight product item weight
     * @param stockQuantity number of product item in stock
     * @param reorderLevel product item reorder level
     * @param totalSold number of product item sold
     * @param averageRating average rating of product item
     */
    public ProductItem(UUID id, boolean display, String model, String sku, String name,
                       String description, Specification[] specifications, String keywords, Image[] images, double unitPrice,
                       double weight, int stockQuantity, int reorderLevel, int totalSold, int averageRating) {
        this.id = id;
        this.display = display;
        this.model = model;
        this.sku = sku;
        this.name = name;
        this.description = description;
        this.specifications = specifications;
        this.keywords = keywords;
        this.images = images;
        this.unitPrice = unitPrice;
        this.weight = weight;
        this.stockQuantity = stockQuantity;
        this.reorderLevel = reorderLevel;
        this.totalSold = totalSold;
        this.averageRating = averageRating;
    }

    /**
     * Display setter method
     * 
     * Display (true) or don't display (false)
     * 
     * @param display new boolean value for display
     */
    public void setDisplay(boolean display) {
        this.display = display;
    }

    /**
     * Display getter method
     * @return display boolean for product item
     */
    public boolean getDisplay() {
        return this.display;
    }

    /**
     * Model setter method
     * @param model new model number for product item
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Model number getter method
     * @return product item model number
     */
    public String getModel() {
        return this.model;
    }

    /**
     * SKU setter method
     * @param sku new sku for product item
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * SKU getter method
     * @return product item sku
     */
    public String getSku() {
        return this.sku;
    }

    /**
     * UPC setter method
     * @param upc new product item upc
     */
    public void setUpc(String upc) {
        this.upc = upc;
    }

    /**
     * UPC getter method
     * @return product item upc
     */
    public String getUpc() {
        return this.upc;
    }

    /** 
     * Name setter method
     * @param name new product item name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Name getter method
     * @return product item name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Description setter method
     * @param description new product item description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Description getter method
     * @return product item description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Specifications data setter method
     * @param specifications new specifications object list for product item
     */
    public void setSpecifications(Specification[] specifications) {
        this.specifications = specifications;
    }
    
    /**
     * Specifications data getter method
     * @return list of specification objects for product item
     */
    public Specification[] getSpecifications() {
        return this.specifications;
    }

    /**
     * Keywords setter method
     * @param keywords new keywords for product item
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * Keywords getter method
     * @return product item keywords
     */
    public String getKeywords() {
        return this.keywords;
    }

    /**
     * Images data setter method
     * @param images new list of images for product item
     */
    public void setImages(Image[] images) {
        this.images = images;
    }

    /**
     * Images getter method
     * @return product item list of images
     */
    public Image[] getImages() {
        return this.images;
    }

    /**
     * Unit price setter method
     * @param unitPrice new unit price for product item
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * Unit price getter method
     * @return product item unit price
     */
    public double getUnitPrice() {
        return this.unitPrice;
    }

    /**
     * Weight setter method
     * @param weight new product item weight
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Weight getter method 
     * @return product item weight
     */
    public double getWeight() {
        return this.weight;
    }

    /**
     * Stock Quantity setter method
     * @param stockQuantity new in stock quantity of product item
     */
    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    /**
     * Stock Quantity getter method
     * @return current number of product item in stock
     */
    public int getStockQuantity() {
        return this.stockQuantity;
    }

    /**
     * Reorder level setter method
     * @param reorderLevel new reorder level for product item
     */
    public void setReorderLevel(int reorderLevel) {
        this.reorderLevel = reorderLevel;
    }

    /**
     * Reorder level getter method
     * @return product item reorder level
     */
    public int getReorderLevel() {
        return this.reorderLevel;
    }

    /**
     * Total sold getter method
     * @return total product item sold
     */
    public int getTotalSold() {
        return totalSold;
    }

    /**
     * Total sold setter method
     * @param totalSold new total sold of product item
     */
    public void setTotalSold(int totalSold) {
        this.totalSold = totalSold;
    }

    /**
     * ID getter method
     * @return product item id
     */
    public UUID getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new product item id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * Average rating getter method
     * @return product item average rating
     */
    public int getAverageRating() {
        return averageRating;
    }

    /**
     * Average rating setter method
     * @param averageRating new product item average rating
     */
    public void setAverageRating(int averageRating) {
        this.averageRating = averageRating;
    }
}
