package com.aquarium.store.exception;
/**
 * This class is used in the case of a File Storage Exception
 */
public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
