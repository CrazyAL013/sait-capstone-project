package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for Product data
 */
public class Product {

    private UUID id;
    private Supplier supplier;
    private Manufacturer manufacturer;
    private String name;
    private String description;
    private String keywords;
    private Image image;
    private ProductItem[] productItems;

    /**
     * Default constructor for Product objects
     */
    public Product() {}

    /**
     * Full constructor for Product objects
     * @param id product UUID
     * @param supplier supplier data
     * @param manufacturer manufacturer data
     * @param name product name
     * @param description product description
     * @param keywords keywords for product
     * @param image product item
     * @param productItems productitem for product
     */
    public Product(UUID id, Supplier supplier, Manufacturer manufacturer, String name, String description, String keywords,
            Image image, ProductItem[] productItems) {
        this.id = id;
        this.supplier = supplier;
        this.manufacturer = manufacturer;
        this.name = name;
        this.description = description;
        this.keywords = keywords;
        this.image = image;
        this.productItems = productItems;
    }

    /**
     * Name getter method
     * @return product name
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter method
     * @param name new name for product
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Description getter method
     * @return product description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Description setter method
     * @param description new description for product
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Keywords getter method
     * @return product keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * Keywords setter method
     * @param keywords new keywords for product
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * Image getter method
     * @return product image
     */
    public Image getImage() {
        return image;
    }

    /**
     * Image setter method
     * @param image new image for product
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * ProductItems getter method
     * @return productitem of product
     */
    public ProductItem[] getProductItems() {
        return productItems;
    }

    /**
     * ProductItems setter method
     * @param productItems new product items for product
     */
    public void setProductItems(ProductItem[] productItems) {
        this.productItems = productItems;
    }

    /**
     * ID getter method
     * @return product id
     */
    public UUID getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new product id 
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * Supplier data getter method
     * @return product supplier data
     */
    public Supplier getSupplier() {
        return supplier;
    }

    /**
     * Supplier data setter method
     * @param supplier new supplier data object for product
     */
    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
    
    /**
     * Manufacturer data getter method
     * @return product manufacturer data
     */
    public Manufacturer getManufacturer() {
        return manufacturer;
    }
    
    /**
     * Manufacturer data setter method
     * @param manufacturer new manufacturer data object for product
     */
    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }
}
