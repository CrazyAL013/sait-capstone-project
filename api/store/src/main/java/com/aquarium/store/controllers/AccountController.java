package com.aquarium.store.controllers;

import java.util.NoSuchElementException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.aquarium.store.models.Account;
import com.aquarium.store.models.ChangePassword;
import com.aquarium.store.models.JwtRequest;
import com.aquarium.store.services.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService service;

    /**
     * Will attempt to login a user
     * @param authenticationRequest Contains the user credentials
     * @param response For storing the httpOnly cookie
     * @return A responseEntity to define headers and body data
     * @throws Exception
     */
    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest,
            HttpServletResponse response) throws Exception {
        try {
            Object[] accountData = service.login(authenticationRequest);
            Object account = accountData[0];
            String refreshToken = accountData[1].toString();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            
            Cookie cookie = new Cookie("refreshToken", refreshToken);
            cookie.setMaxAge(7 * 24 * 60 * 60); // 7 Days
            cookie.setSecure(true);
            cookie.setHttpOnly(true);
            cookie.setPath("/");
            response.addCookie(cookie);

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Username or password is incorrect", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * Will attempt to log out a user
     * @param account That is trying to log out
     * @param refreshToken To verify the account
     * @param response to store the Http Only cookie
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/logout")
    public ResponseEntity<?> logout(@RequestBody Account account, @CookieValue(value = "refreshToken", defaultValue = "No Cookie") String refreshToken,
            HttpServletResponse response) {
        try {

            if (refreshToken.equals("No Cookie")) {
                return new ResponseEntity<>(HttpStatus.OK);
            }

            // Revoke refresh token (delete from db)
            service.deleteRefreshToken(account.getId(), refreshToken);

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            Cookie cookie = new Cookie("refreshToken", null);
            cookie.setMaxAge(0); // 7 Days
            cookie.setSecure(true);
            cookie.setHttpOnly(true);
            cookie.setPath("/");
            response.addCookie(cookie);

            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);                

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    /**
     * Create a user account
     * @param account Data to create
     * @param response to store the Http only cookie
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("")
    public ResponseEntity<?> register(@RequestBody Account account, HttpServletResponse response) {
        try {
            Object[] accountData = service.register(account);
            Object newAccount = accountData[0];
            String refreshToken = accountData[1].toString();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            Cookie cookie = new Cookie("refreshToken", refreshToken);
            cookie.setMaxAge(7 * 24 * 60 * 60); // 7 Days
            cookie.setSecure(true);
            cookie.setHttpOnly(true);
            cookie.setPath("/");
            response.addCookie(cookie);

            return new ResponseEntity<>(newAccount, responseHeaders, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Will get a new refresh token for the user
     * @param refreshToken to verify the user
     * @param response to store the http only cookie
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/refreshToken")
    public ResponseEntity<?> newRefreshToken(@CookieValue(value = "refreshToken", defaultValue = "No Cookie") String refreshToken,
            HttpServletResponse response) {
        try {

            if (refreshToken.equals("No Cookie")) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            Object[] accountData = service.generateNewTokens(refreshToken);
            Object account = accountData[0];
            String newRefreshToken = accountData[1].toString();

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            Cookie cookie = new Cookie("refreshToken", newRefreshToken);
            cookie.setMaxAge(7 * 24 * 60 * 60); // 7 Days
            cookie.setSecure(true);
            cookie.setHttpOnly(true);
            cookie.setPath("/");
            response.addCookie(cookie);

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    /**
     * Use for a user to change their password
     * @param changePassword entity to store password data
     * @param response to store the http only cookie
     * @return A responseEntity to define headers and body data
     * @throws Exception If there is an error changing the password
     */
    @PostMapping("/changePassword")
    public ResponseEntity<?> changePassword(@RequestBody ChangePassword changePassword,
            HttpServletResponse response) throws Exception {
        try {
            service.changePassword(changePassword);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }


    /**
     * Get the account of the given UID
     *
     * @param accountUID of user to get
     * @return A responseEntity to define headers and body data (account object)
     */
    @GetMapping("/{accountUID}")
    public ResponseEntity<Object> get(@PathVariable String accountUID) {
        try {
            Object account = service.getAccount(accountUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get all the addresses of an account
     *
     * @param accountUID of the user to get
     * @return A responseEntity to define headers and body data (account addresses)
     */
    @GetMapping("/{accountUID}/address")
    public ResponseEntity<Object> getAddress(@PathVariable String accountUID) {
        try {
            Object account = service.getAllAccountAddress(accountUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get all the primary address of an account
     *
     * @param accountUID of the user address to get
     * @return A responseEntity to define headers and body data (account primary address)
     */
    @GetMapping("/{accountUID}/address/primary")
    public ResponseEntity<Object> getAddressPrimary(@PathVariable String accountUID) {
        try {
            Object account = service.getAccountAddressPrimary(accountUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete the account 
     *
     * @param accountUID of the account to delete
     * @return A responseEntity to define headers and body data
     */
    @DeleteMapping("/{accountUID}")
    public ResponseEntity<Object> deleteAccount(@PathVariable String accountUID) {
        try {
            Object account = service.deleteAccount(accountUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update the given account
     *
     * @param accountUID of the account to update
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{accountUID}")
    public ResponseEntity<Object> updateAccount(@PathVariable String accountUID, @RequestBody Account account) {
        try {
            String updateCount = service.updateAccount(account);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(updateCount, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update the given account primary address
     *
     * @param accountUID of the account to update
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{accountUID}/address/primary/{addressUID}")
    public ResponseEntity<Object> updateAccountPrimaryAddress(@PathVariable String accountUID,
            @PathVariable String addressUID) {
        try {
            String updateCount = service.updateAccountAddressPrimary(accountUID, addressUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(updateCount, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get the account orders 
     * 
     * @param accountUID of the account orders to get
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{accountUID}/order")
    public ResponseEntity<Object> getOrders(@PathVariable String accountUID) {
        try {
            Object account = service.getAccountOrders(accountUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    /**
     * Get the account reviews
     * 
     * @param accountUID of the account reviews to get
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{accountUID}/review")
    public ResponseEntity<Object> getReviews(@PathVariable String accountUID) {
        try {
            Object account = service.getAccountReviews(accountUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException|IndexOutOfBoundsException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
