package com.aquarium.store.services;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import com.easypost.exception.EasyPostException;
import com.easypost.model.Rate;

import com.aquarium.store.models.Error;
import com.aquarium.store.models.Order;
import com.aquarium.store.models.OrderItem;
import com.aquarium.store.models.Payment;
import com.aquarium.store.models.PaymentResponse;
import com.aquarium.store.models.Shipment;
import com.aquarium.utilities.CallStatementUnwrapper;
import com.squareup.square.Environment;
import com.squareup.square.SquareClient;
import com.squareup.square.api.PaymentsApi;
import com.squareup.square.exceptions.ApiException;
import com.squareup.square.models.CreatePaymentRequest;
import com.squareup.square.models.CreatePaymentResponse;
import com.squareup.square.models.Money;

/**
 * Service for Payment
 */
@Service
@Transactional
public class PaymentService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // private static final String SQUARE_ACCESS_TOKEN_ENV_VAR =
    // "EAAAEKAmcfihSFs8xCYguEBbLA5xhvu7xPTS7hnCxFszMhX72QtK0YvE2Sd4Yw1F";
    private static final String SQUARE_ACCESS_TOKEN_ENV_VAR = "EAAAEGqae9spif465Tpz8e1ifbowTuajNcgPsaODJ625BnrjXaQIJ7WhAKViD0H5";

    // private static final String SQUARE_APP_ID_ENV_VAR =
    // "sandbox-sq0idb-0rvCDwY2vhJi-4iyDTT_TA";
    private static final String SQUARE_APP_ID_ENV_VAR = "sandbox-sq0idb-byixmdieP777rEreWL1nFg";

    // private static final String SQUARE_ENV_ENV_VAR = "production";
    private static final String SQUARE_ENV_ENV_VAR = "sandbox";

    /**
     * Make a payment for order
     * @param payment payment being made
     * @return PaymentReponse data or custom error messages
     */
    @Async
    public Object makePayment(Payment payment) {

        Long amount = payment.getAmount_money().getAmount();
        String currency = payment.getAmount_money().getCurrency();
        String nonce = payment.getNonce();
        String idempotencyKey = payment.getIdempotency_key();

        SquareClient squareClient = new SquareClient.Builder().environment(Environment.fromString(SQUARE_ENV_ENV_VAR))
                .accessToken(SQUARE_ACCESS_TOKEN_ENV_VAR).build();

        Money amountMoney = new Money.Builder().amount(amount).currency(currency).build();

        CreatePaymentRequest body = new CreatePaymentRequest.Builder(nonce, idempotencyKey, amountMoney).build();

        PaymentsApi paymentsApi = squareClient.getPaymentsApi();

        try {
            CreatePaymentResponse result = paymentsApi.createPayment(body);
            System.out.println("Payment Success!");
            System.out.println(result);
            String paymentId = result.getPayment().getId();
            payment.getOrder().setTransactionId(paymentId);

            purchaseRate(payment.getOrder().getShipment().getRateId(),
                    payment.getOrder().getShipment().getShipmentId());

            // Save this shipment
            String shipmentId = saveShipment(payment.getOrder().getShipment());
            System.out.println("Shipment saved to Database!");
            // payment.getOrder().getShipment().setId(shipmentId);

            // Save the order in the database
            String orderId = saveOrder(payment.getOrder());

            // Save each order item
            for (OrderItem orderItem : payment.getOrder().getItems()) {
                saveOrderItem(orderItem, payment.getOrder().getId());
            }

            System.out.println("Order saved to Database!");

            // TODO: Notify admin that an order has been placed

            return new PaymentResponse(paymentId, orderId, shipmentId);
        } catch (EasyPostException e) {
            System.out.println("Failed to make the request: EasyPostException");
            System.out.println(e.getLocalizedMessage());
            System.out.println(String.format("Exception: %s", e.getMessage()));
            e.printStackTrace();

            ArrayList<Error> customErrors = new ArrayList<>();
            Error newError = new Error("EasyPostException", e.getLocalizedMessage(),
                    "EasyPostException. Order not processed");
            customErrors.add(newError);

            return customErrors;
        } catch (ApiException e) {
            System.out.println("Failed to make the request: API Exception");
            System.out.println(e.getLocalizedMessage());
            System.out.println(String.format("Exception: %s", e.getMessage()));
            e.printStackTrace();

            List<com.squareup.square.models.Error> errors = e.getErrors();
            ArrayList<Error> customErrors = new ArrayList<>();

            for (com.squareup.square.models.Error error : errors) {
                Error newError = new Error(error.getCode(), error.getDetail(), error.getClass().toString());
                customErrors.add(newError);

            }

            return customErrors;
        } catch (IOException e) {
            System.out.println("Failed to make the request: IOException");
            System.out.println(e.getLocalizedMessage());
            System.out.println(String.format("Exception: %s", e.getMessage()));
            e.printStackTrace();

            ArrayList<Error> customErrors = new ArrayList<>();
            Error newError = new Error("IOException", e.getLocalizedMessage(),
                    "Square Payment error. Payment not processed.");
            customErrors.add(newError);

            return customErrors;
        }
    }

    /**
     * Creates a postage label for shipping
     * @param rateID id of rate
     * @param shipmentID shipment id
     * @return the postage label
     * @throws EasyPostException
     */
    public Object purchaseRate(String rateID, String shipmentID) throws EasyPostException {
        com.easypost.model.Shipment shipment = com.easypost.model.Shipment.retrieve(shipmentID);
        Map<String, Object> buyMap = new HashMap<String, Object>();
        List<Rate> rates = shipment.getRates();

        for (Rate rate : rates) {
            if (rate.getId().equals(rateID)) {
                buyMap.put("rate", rate);
                shipment.buy(buyMap);
            }
        }

        System.out.println(shipment);

        return shipment.getPostageLabel();
    }

    /**
     * Save a shipment in database
     * @param shipment the shipment data to be saved
     * @return the saved shipment
     */
    private String saveShipment(Shipment shipment) {
        String sql = "{CALL OrderShipmentCreate(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        final java.sql.Date shipDate = ((shipment.getShipDate() != null)
                ? new java.sql.Date(shipment.getShipDate().getTime())
                : new java.sql.Date(new Date().getTime()));

        // if (shipment.getShipDate() != null) {
        // shipDate = new java.sql.Date(shipment.getShipDate().getTime());
        // }

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ShipmentUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _RateID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ShipmentID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Carrier
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Service
        paramList.add(new SqlParameter(Types.DECIMAL)); // _Rate
        paramList.add(new SqlParameter(Types.VARCHAR)); // _TrackingNumber
        paramList.add(new SqlParameter(Types.INTEGER)); // _DeliveryDays
        paramList.add(new SqlParameter(Types.DATE)); // _ShipDate
        paramList.add(new SqlOutParameter("id", Types.INTEGER));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, shipment.getId());
            callableStatement.setString(2, shipment.getShipmentId());
            callableStatement.setString(3, shipment.getRateId());
            callableStatement.setString(4, shipment.getCarrier());
            callableStatement.setString(5, shipment.getService());
            callableStatement.setInt(6, shipment.getRate());
            callableStatement.setString(7, shipment.getTrackingNumber());
            callableStatement.setInt(8, shipment.getDeliveryDays());
            callableStatement.setDate(9, shipDate);
            callableStatement.registerOutParameter(10, Types.INTEGER);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Save order in database
     * @param order order to be saved
     * @return the saved order
     */
    private String saveOrder(Order order) {
        String sql = "{CALL OrderCreate(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _OrderUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _AccountUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _BillingAddressUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _DeliveryAddressUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _DiscountUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ShipmentUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _PaymentProcessor
        paramList.add(new SqlParameter(Types.VARCHAR)); // _TransactionID
        paramList.add(new SqlParameter(Types.DECIMAL)); // _SubTotal
        paramList.add(new SqlParameter(Types.DECIMAL)); // _BeforeTaxTotal
        paramList.add(new SqlParameter(Types.DECIMAL)); // _ShipTotal
        paramList.add(new SqlParameter(Types.DECIMAL)); // _TaxTotal
        paramList.add(new SqlParameter(Types.DECIMAL)); // _DiscountTotal
        paramList.add(new SqlParameter(Types.DECIMAL)); // _GrandTotal
        paramList.add(new SqlParameter(Types.VARCHAR)); // _PaymentCardBrand
        paramList.add(new SqlParameter(Types.INTEGER)); // _PaymentCardLast4Digits
        paramList.add(new SqlParameter(Types.INTEGER)); // _PaymentCardExpiryMonth
        paramList.add(new SqlParameter(Types.INTEGER)); // _PaymentCardExpiryYear

        paramList.add(new SqlOutParameter("id", Types.INTEGER));

        String orderUID = order.getId();
        String accountUID = order.getAccount().getId();
        String billingAddressUID = order.getBillingAddress().getId().toString();
        String deliveryAddressUID = order.getDeliveryAddress().getId().toString();
        String discountUid = (order.getDiscount() != null ? order.getDiscount().getUid().toString() : null);
        String shipmentUID = order.getShipment().getId();
        String paymentProcessor = order.getPaymentProcessor().getId().toString();
        String transactionID = order.getTransactionId();
        double subTotal = order.getSubTotal();
        double beforeTaxTotal = order.getBeforeTaxTotal();
        double shipTotal = order.getShipTotal();
        double taxTotal = order.getTaxTotal();
        double discountTotal = order.getDiscountTotal();
        double grandTotal = order.getGrandTotal();
        String paymentCardBrand = order.getPaymentCardBrand();
        int paymentCardLast4Digits = order.getPaymentCardLast4Digits();
        int paymentCardExpiryMonth = order.getPaymentCardExpiryMonth();
        int paymentCardExpiryYear = order.getPaymentCardExpiryYear();

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, orderUID); // _OrderUID
            callableStatement.setString(2, accountUID); // _AccountUID
            callableStatement.setString(3, billingAddressUID); // _BillingAddressUID
            callableStatement.setString(4, deliveryAddressUID); // _DeliveryAddressUID
            callableStatement.setString(5, discountUid); // _DiscountUID
            callableStatement.setString(6, shipmentUID); // _ShipmentUID
            callableStatement.setString(7, paymentProcessor); // _PaymentProcessor
            callableStatement.setString(8, transactionID); // _TransactionID
            callableStatement.setDouble(9, subTotal); // _SubTotal
            callableStatement.setDouble(10, beforeTaxTotal); // _BeforeTaxTotal
            callableStatement.setDouble(11, shipTotal); // _ShipTotal
            callableStatement.setDouble(12, taxTotal); // _TaxTotal
            callableStatement.setDouble(13, discountTotal); // _DiscountTotal
            callableStatement.setDouble(14, grandTotal); // _GrandTotal
            callableStatement.setString(15, paymentCardBrand); // _PaymentCardBrand
            callableStatement.setInt(16, paymentCardLast4Digits); // _PaymentCardLast4Digits
            callableStatement.setInt(17, paymentCardExpiryMonth); // _PaymentCardExpiryMonth
            callableStatement.setInt(18, paymentCardExpiryYear); // _PaymentCardExpiryYear
            callableStatement.registerOutParameter(19, Types.INTEGER);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Save order item in order
     * @param orderItem order item to be saved
     * @param orderUID UUID to save in
     * @return the updated order
     */
    private String saveOrderItem(OrderItem orderItem, String orderUID) {
        String sql = "{CALL OrderItemCreate(?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _OrderItemUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _OrderUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID
        paramList.add(new SqlParameter(Types.INTEGER)); // _Quantity
        paramList.add(new SqlParameter(Types.DECIMAL)); // _UnitPrice
        paramList.add(new SqlOutParameter("id", Types.INTEGER)); // _ID

        String orderItemUID = orderItem.getId();
        String productItemUID = orderItem.getProductItem().getId().toString();
        int quantity = orderItem.getQuantity();
        double unitPrice = orderItem.getUnitPrice();

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, orderItemUID); // _OrderItemUID
            callableStatement.setString(2, orderUID); // _OrderUID
            callableStatement.setString(3, productItemUID); // _ProductItemUID
            callableStatement.setInt(4, quantity); // _Quantity
            callableStatement.setDouble(5, unitPrice); // _UnitPrice
            callableStatement.registerOutParameter(6, Types.INTEGER); // _ID
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

}
