package com.aquarium.store.models;

/**
 * Model for application images
 */
public class Image {
    private String id;
    private String productId;
    private String type;
    private String location;

    /**
     * Constructor for Image objects
     * 
     * Only initializes id and location
     * 
     * @param id the image id
     * @param location the file location of the image
     */
    public Image(String id, String location) {
        this.id = id;
        this.location = location;
    }

    /**
     * Image id getter method
     * @return the id of the image
     */
    public String getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new id of the image
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Product id getter method
     * @return the product id of this image
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Product id setter method
     * @param productId new product id for this image
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Type getter method
     * @return the type of this image
     */
    public String getType() {
        return this.type;
    }

    /**
     * Type setter method
     * @param type new type of this image
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Location getter method
     * @return the file location of this image
     */
    public String getLocation() {
        return location;
    }

    /**
     * Location setter method
     * @param location the new file location of this image
     */
    public void setLocation(String location) {
        this.location = location;
    }
}
