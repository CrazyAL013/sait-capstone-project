package com.aquarium.store.models;

/**
 * Model for OrderItem data
 */
public class OrderItem {
    private String id;
    private String productId;
    private int quantity;
    private double unitPrice;
    private String productName;
    private ProductItem productItem;

    /**
     * Constructor for OrderItems objects
     * @param id order item id
     * @param productId product id
     * @param quantity quantity in order
     * @param unitPrice price of unit
     * @param productName name of product
     * @param productItem the product data
     */
    public OrderItem(String id, String productId, int quantity, double unitPrice, String productName, ProductItem productItem) {
        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.productName = productName;
        this.productItem = productItem;
    }

    /**
     * ID getter method
     * @return order item id
     */
    public String getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new order item id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * ProductItem id getter method
     * @return productitem id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * ProductItem id setter method
     * @param productItemId new id for productitem
     */
    public void setProductId(String productItemId) {
        this.productId = productItemId;
    }

    /**
     * Quantity getter method
     * @return the quantity in order
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Quantity setter method
     * @param quantity new quantity in order
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Unit price getter method
     * @return price of unti
     */
    public double getUnitPrice() {
        return unitPrice;
    }

    /**
     * Unit price setter method
     * @param unitPrice new unit price
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * Product name getter method
     * @return product name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Product name setter method
     * @param productName new product name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * ProductItem getter method
     * @return the orderitem productitem
     */
    public ProductItem getProductItem() {
        return productItem;
    }

    /**
     * ProductItem setter method
     * @param productItem new productitem for orderitem
     */
    public void setProductItem(ProductItem productItem) {
        this.productItem = productItem;
    }

}
