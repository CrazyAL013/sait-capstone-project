package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for Supplier data
 */
public class Supplier {
  private UUID id;
  private UUID supplierId;
  private String name;
  private String email;
  private String phone;
  private String fax;
  private String address;
  private String contacts;

  /**
   * Default constructor for Supplier objets
   */
  public Supplier() {}

  /**
   * Full constructor for Supplier objets
   * @param id suppplier id
   * @param supplierId supplier id
   * @param name supplier name
   * @param email supplier email
   * @param phone supplier phone
   * @param fax supplier fax
   * @param address supplier address
   * @param contacts supplier contacts
   */
  public Supplier(UUID id, UUID supplierId, String name, String email, String phone, String fax, String address, String contacts) {
    this.id = id;
    this.supplierId = supplierId;
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.fax = fax;
    this.address = address;
    this.contacts = contacts;
  }

  /**
   * ID getter method
   * @return supplier id
   */
  public UUID getId() {
    return this.id;
  }

  /**
   * ID setter method 
   * @param id new supplier id
   */
  public void setId(UUID id) {
    this.id = id;
  }

  /**
   * Supplier id getter method
   * @return supplier id
   */
  public UUID getSupplierId() {
    return this.supplierId;
  }

  /**
   * Supplier id setter method
   * @param supplierId new supplier id
   */
  public void setSupplierId(UUID supplierId) {
    this.supplierId = supplierId;
  }

  /**
   * Name getter method
   * @return supplier name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Name setter method
   * @param name new supplier name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Email getter method
   * @return supplier email
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * Email setter method
   * @param email new supplier email
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * Phone getter method
   * @return supplier phone
   */
  public String getPhone() {
    return this.phone;
  }

  /**
   * Phone setter method
   * @param phone new supplier phone
   */
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * Fax getter method
   * @return supplier fax
   */
  public String getFax() {
    return this.fax;
  }

  /**
   * Fax setter method
   * @param fax new supplier fax
   */
  public void setFax(String fax) {
    this.fax = fax;
  }

  /**
   * Address getter method
   * @return supplier address
   */
  public String getAddress() {
    return this.address;
  }

  /**
   * Adress setter method
   * @param address new supplier address
   */
  public void setAddress(String address) {
    this.address = address;
  }

  /**
   * Contacts getter method
   * @return supplier contacts
   */
  public String getContacts() {
    return this.contacts;
  }

  /**
   * Contacts setter method
   * @param contacts new supplier contacts
   */
  public void setContacts(String contacts) {
    this.contacts = contacts;
  }

}

