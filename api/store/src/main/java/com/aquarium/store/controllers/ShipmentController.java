package com.aquarium.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import com.aquarium.store.models.Address;
import com.aquarium.store.models.Error;
import com.aquarium.store.services.ShippingService;

@RestController
@RequestMapping("/easypost/shipping")
public class ShipmentController {

    @Autowired
    ShippingService service;

    /**
     * Get the possible shipments for an address
     * @param fromAddress of the shipment
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/")
    public DeferredResult<Object> getShipments(@RequestBody Address fromAddress) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        final DeferredResult<Object> deferredResult = new DeferredResult<>();

        try {
            Object rates = service.createShipment(fromAddress);
            ResponseEntity re = new ResponseEntity<>(rates, responseHeaders, HttpStatus.OK);
            deferredResult.setResult(re);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            Error error = new Error("", e.getLocalizedMessage(), "EasyPost Error");
            ResponseEntity re = new ResponseEntity<>(error, responseHeaders, HttpStatus.SERVICE_UNAVAILABLE);
            deferredResult.setResult(re);
        }

        return deferredResult;
    }

    /**
     * Buy and shipment rate
     * @param shipmentID of the shipment
     * @param rateID of the rate
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/{shipmentID}/{rateID}/buy-rate")
    public DeferredResult<Object> buyRate(@PathVariable String shipmentID, @PathVariable String rateID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        final DeferredResult<Object> deferredResult = new DeferredResult<>();
        try {
            Object labelURL = service.purchaseRate(rateID, shipmentID);
            ResponseEntity re = new ResponseEntity<>(labelURL, responseHeaders, HttpStatus.OK);
            deferredResult.setResult(re);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            Error error = new Error("", e.getLocalizedMessage(), "EasyPost Error");
            ResponseEntity re = new ResponseEntity<>(error, responseHeaders, HttpStatus.SERVICE_UNAVAILABLE);
            deferredResult.setResult(re);
        }

        return deferredResult;
    }

    /**
     * Get the status of easypost
     * @returnA A boolean of the status
     */
    @GetMapping("/status")
    public ResponseEntity<?> getStatus() {
        try {
            service.easyPostIsUp();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }

    }
}
