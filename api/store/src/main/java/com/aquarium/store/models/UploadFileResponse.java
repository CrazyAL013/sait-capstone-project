package com.aquarium.store.models;

/**
 * Model for application UploadFileResponse data
 */
public class UploadFileResponse {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;

    /**
     * Constructor for UploadFileReponse objects
     * @param fileName the name of the upload file
     * @param fileDownloadUri the uri for download
     * @param fileType the type of file
     * @param size the size of the file
     */
    public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
    }

    /**
     * File name getter method
     * @return name of the file
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * File name setter method
     * @param fileName new name for file
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * File Download URI getter method
     * @return download uri for file
     */
    public String getFileDownloadUri() {
        return fileDownloadUri;
    }

    /**
     * File Download URI setter method
     * @param fileDownloadUri new download uri for file
     */
    public void setFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
    }
    
    /**
     * File type getter method 
     * @return the type of file
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * File type setter method
     * @param fileType new file type for file
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * Size getter method
     * @return size of file
     */
    public long getSize() {
        return size;
    }

    /**
     * Size setter method
     * @param size new size of file
     */
    public void setSize(long size) {
        this.size = size;
    }
}
