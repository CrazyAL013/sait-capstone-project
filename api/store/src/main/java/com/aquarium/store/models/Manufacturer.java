package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for Manufacturer data
 */
public class Manufacturer {
    private UUID id;
    private UUID manufacturerId;
    private String name;
    private String website;

    /**
     * Default contructor for Manufacturer objects
     */
    public Manufacturer() {}

    /**
     * Full constructor for Manufacturer objects
     * @param id the manufacturer id
     * @param manufacturerId the manufacturer id
     * @param name the name of the manufacturer
     * @param website the manufacturer website
     */
    public Manufacturer(UUID id, UUID manufacturerId, String name, String website) {
      this.id = id;
      this.manufacturerId = manufacturerId;
      this.name = name;
      this.website = website;
    }

    /**
     * Id getter method
     * @return the manufacturer id
     */
    public UUID getId() {
      return this.id;
    }

    /**
     * Id setter method
     * @param id new manufacturer id
     */
    public void setId(UUID id) {
      this.id = id;
    }

    /**
     * Manufacturer id getter method
     * @return the manufacturer id
     */
    public UUID getManufacturerId() {
      return this.manufacturerId;
    }

    /**
     * Manufacturer id setter method
     * @param manufacturerId new manufacturer id
     */
    public void setManufacturerId(UUID manufacturerId) {
      this.manufacturerId = manufacturerId;
    }

    /**
     * Name getter mehtod
     * @return manufacturer name
     */
    public String getName() {
      return this.name;
    }

    /**
     * Name setter method
     * @param name new manufacturer name
     */
    public void setName(String name) {
      this.name = name;
    }

    /**
     * Website getter method
     * @return the manufacturer website
     */
    public String getWebsite() {
      return this.website;
    }

    /**
     * Website setter method
     * @param website new manufacturer website
     */
    public void setWebsite(String website) {
      this.website = website;
    }
}
