package com.aquarium.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import com.aquarium.store.models.Payment;
import com.aquarium.store.models.PaymentResponse;
import com.aquarium.store.services.PaymentService;

@RestController
@RequestMapping("/square/payment")
public class PaymentController {

    @Autowired
    PaymentService service;

    /**
     * Get a payment
     * @param payment to process
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/")
    public DeferredResult<Object> get(@RequestBody Payment payment) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        if (payment.getOrder() == null) {
            final DeferredResult<Object> deferredResult = new DeferredResult<>();
            ResponseEntity re = new ResponseEntity<>("BAD REQUEST", responseHeaders, HttpStatus.BAD_REQUEST);
            deferredResult.setResult(re);
            return deferredResult;
        }

        Object result = service.makePayment(payment);
        
        final DeferredResult<Object> deferredResult = new DeferredResult<>();
        if (result instanceof PaymentResponse) {
            ResponseEntity re = new ResponseEntity<>(result, HttpStatus.OK);
            deferredResult.setResult(re);
        } else {
            ResponseEntity re = new ResponseEntity<>(result, HttpStatus.SERVICE_UNAVAILABLE);
            deferredResult.setResult(re);
        }
        return deferredResult;
    }

    
}
