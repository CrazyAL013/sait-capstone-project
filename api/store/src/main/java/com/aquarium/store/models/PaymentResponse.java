package com.aquarium.store.models;

/**
 * Model for PaymentResponse data
 */
public class PaymentResponse {
    String paymentId;
    String orderId;
    String shipmentId;

    /**
     * Constructor for PaymentResponse objects
     * @param paymentId payment id for response
     * @param orderId order id for response
     * @param shipmentId shipment id for response
     */
    public PaymentResponse(String paymentId, String orderId, String shipmentId) {
        this.paymentId = paymentId;
        this.orderId = orderId;
        this.shipmentId = shipmentId;
    }

    /**
     * Payment id getter method
     * @return response payment id
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * Payment id setter method
     * @param paymentId new payment id
     */
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * Order is getter method
     * @return response order id
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Order id setter method
     * @param orderId new order id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * Shipment id getter method
     * @return response shipment id
     */
    public String getShipmentId() {
        return shipmentId;
    }

    /**
     * Shipment id setter method
     * @param shipmentId new shipment id 
     */
    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

}
