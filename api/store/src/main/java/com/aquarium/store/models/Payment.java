package com.aquarium.store.models;

/**
 * Model for Payment data
 */
public class Payment {
    String nonce;
    String idempotencyKey;
    String locationId;
    AmountMoney amountMoney;
    Order order;
    
    /**
     * Constructor for Payment objects
     * @param nonce nonce for payment
     * @param idempotencyKey idempotency key for payment
     * @param locationId the location id for payment
     * @param amountMoney the amount paid in payment
     * @param order the order for the payment
     */
    public Payment(String nonce, String idempotencyKey, String locationId, AmountMoney amountMoney, Order order) {
        this.nonce = nonce;
        this.idempotencyKey = idempotencyKey;
        this.locationId = locationId;
        this.amountMoney = amountMoney;
        this.order = order;
    }

    /**
     * Nonce getter method
     * @return the payment nonce
     */
    public String getNonce() {
        return nonce;
    }

    /**
     * Nonce setter method
     * @param nonce new nonce for payment
     */
    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    /**
     * Idempotency key getter method
     * @return payment idempotency key
     */
    public String getIdempotency_key() {
        return idempotencyKey;
    }

    /**
     * Idempotency key setter method
     * @param idempotency_key new idempotency key for payment
     */
    public void setIdempotency_key(String idempotency_key) {
        this.idempotencyKey = idempotency_key;
    }

    /**
     * Location id getter method
     * @return payment location id
     */
    public String getLocation_id() {
        return locationId;
    }

    /**
     * Location id setter method
     * @param location_id new location id for paymnet
     */
    public void setLocation_id(String location_id) {
        this.locationId = location_id;
    }

    /**
     * AmountMoney data etter method
     * @return payment amountMoney data
     */
    public AmountMoney getAmount_money() {
        return amountMoney;
    }

    /**
     * Amount money setter method
     * @param amount_money new AmountMoney data for payment
     */
    public void setAmount_money(AmountMoney amount_money) {
        this.amountMoney = amount_money;
    }

    /**
     * Order getter method
     * @return order for payment
     */
    public Order getOrder() {
        return order;
    }

    /**
     * Order setter method
     * @param order new order for payment
     */
    public void setOrder(Order order) {
        this.order = order;
    }


}
