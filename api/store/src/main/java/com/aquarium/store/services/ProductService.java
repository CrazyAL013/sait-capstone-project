package com.aquarium.store.services;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aquarium.store.models.Image;
import com.aquarium.store.models.Product;
import com.aquarium.store.models.ProductItem;
import com.aquarium.store.models.Specification;
import com.aquarium.utilities.CallStatementUnwrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for Product
 * Performs CRUD on Product data
 */
@Service
@Transactional
public class ProductService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${file.upload-dir}")
    private String uploadDir;

    /**
     * Get all products
     * 
     * @return all found products
     */
    public Object getProducts() throws IndexOutOfBoundsException {
        String sql = "{CALL ProductGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
    * Finds one product based on UUID
    * @param productUUID the UUID of product to search for
    * @return the found product
    * @throws IndexOutOfBoundsException if there is no product
    */
    public Object getProduct(String productUUID) throws IndexOutOfBoundsException {
        String sql = "{CALL ProductGetById(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productUUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Save a new product
     * 
     * @param product the product data to save
     * @return the inserted product
     */
    public String saveProduct(Product product) {
        String sql = "{CALL ProductCreate(?, ?, ?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _SupplierUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ManufacturerUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Name
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Description
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Keywords
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Image
        paramList.add(new SqlOutParameter("result", Types.INTEGER));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, product.getId().toString()); // _ProductUID
            callableStatement.setString(2, product.getSupplier().getId().toString()); // _SupplierUID
            callableStatement.setString(3, product.getManufacturer().getId().toString()); // _ManufacturerUID
            callableStatement.setString(4, product.getName()); // _Name
            callableStatement.setString(5, product.getDescription()); // _Description
            callableStatement.setString(6, product.getKeywords()); // _Keywords
            callableStatement.setString(7, (product.getImage() != null ) ? product.getImage().getId() : null); // _Image
            callableStatement.registerOutParameter(8, Types.INTEGER);
            return callableStatement;
        }, paramList);

        // Loop through the product items and save them
        for (ProductItem productItem : product.getProductItems()) {
            saveProductItem(productItem, product.getId().toString());
        }


        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Update a product
     * @param product new Product data
     * @param productUUID UUID of product to update
     * @return the updated product
     */
    public String updateProduct(Product product, String productUUID) {
        String sql = "{CALL ProductUpdate(?, ?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _SupplierUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ManufacturerUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Name
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Description
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Keywords
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Image

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productUUID); // _ProductUID
            callableStatement.setString(2, product.getSupplier().getId().toString()); // _SupplierUID
            callableStatement.setString(3, product.getManufacturer().getId().toString()); // _ManufacturerUID
            callableStatement.setString(4, product.getName()); // _Name
            callableStatement.setString(5, product.getDescription()); // _Description
            callableStatement.setString(6, product.getKeywords()); // _Keywords
            callableStatement.setString(7, product.getImage().getId()); // _Image
            return callableStatement;
        }, paramList);

        // Loop through the product items and update them
        for (ProductItem productItem : product.getProductItems()) {
            updateProductItem(productItem, productItem.getId().toString(), productUUID);
        }

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Deletes a product based on UUID
     * @param productUID the UUID of product to delete
     * @return the deleted product
     */
    public String deleteProduct(String productUID) {
        String sql = "{CALL ProductDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    // MARK: - Product Images
    /**
     * Creates a new image
     * @param image image to create
     * @return the created image
     */
    public String productImageCreate(Image image) {
        image.setLocation("/");

        String sql = "{CALL ProductImageCreate(?, ?, ?, ?, ?)}";

        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ImageUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Type
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Location
        paramList.add(new SqlOutParameter("result", Types.INTEGER)); // _ID

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, image.getId()); // _ImageUID
            callableStatement.setString(2, image.getProductId()); // _ProductUID
            callableStatement.setString(3, image.getType()); // _Type
            callableStatement.setString(4, image.getLocation()); // _Location
            callableStatement.registerOutParameter(5, Types.INTEGER); // _ID
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Updates an image
     * @param image image to update
     * @return the updated image
     */
    public String productImageUpdate(Image image) {
        String sql = "{CALL ProductImageUpdate(?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ImageUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Type
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Location

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, image.getId()); // _ImageUID
            callableStatement.setString(2, image.getProductId()); // _ProductUID
            callableStatement.setString(3, image.getType()); // _Type
            callableStatement.setString(4, image.getLocation()); // _Location
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Deletes an image
     * @param imageUID UUID of image to delete
     * @return the deleted image 
     */
    public String productImageDelete(String imageUID) {
        String sql = "{CALL ProductImageDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ImageUID

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, imageUID); // _ImageUID
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    

    // MARK: - Product Items
    /**
    * Retrieves a product item
    * @param productItemUUID UUID Of product item to search for
    * @return the found product item
    * @throws IndexOutOfBoundsException if no product item has matching UUID
    */
    public Object getProductItem(String productItemUUID) throws IndexOutOfBoundsException {
        String sql = "{CALL ProductItemGetById(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productItemUUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Saves a new product item
     * @param productItem Product Item data to save
     * @param productUID UUID of product
     * @return the saved product item
     */
    public String saveProductItem(ProductItem productItem, String productUID) {
        String sql = "{CALL ProductItemCreate(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductUID
        paramList.add(new SqlParameter(Types.BOOLEAN)); // _Display
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Model
        paramList.add(new SqlParameter(Types.VARCHAR)); // _SKU
        paramList.add(new SqlParameter(Types.VARCHAR)); // _UPC
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Name
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Description
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Keywords
        paramList.add(new SqlParameter(Types.DOUBLE)); // _UnitPrice
        paramList.add(new SqlParameter(Types.DOUBLE)); // _Weight
        paramList.add(new SqlParameter(Types.INTEGER)); // _StockQuantity
        paramList.add(new SqlParameter(Types.INTEGER)); // _ReorderLevel
        paramList.add(new SqlParameter(Types.INTEGER)); // _TotalSold
        paramList.add(new SqlOutParameter("result", Types.INTEGER)); // _ID

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);

            callableStatement.setString(1, productItem.getId().toString()); // _ProductItemUID
            callableStatement.setString(2, productUID); // _ProductUID
            callableStatement.setBoolean(3, productItem.getDisplay()); // _Display
            callableStatement.setString(4, productItem.getModel()); // _Model
            callableStatement.setString(5, productItem.getSku()); // _SKU
            callableStatement.setString(6, productItem.getUpc()); // _UPC
            callableStatement.setString(7, productItem.getName()); // _Name
            callableStatement.setString(8, productItem.getDescription()); // _Description
            callableStatement.setString(9, productItem.getKeywords()); // _Keywords
            callableStatement.setDouble(10, productItem.getUnitPrice()); // _UnitPrice
            callableStatement.setDouble(11, productItem.getWeight()); // _Weight
            callableStatement.setInt(12, productItem.getStockQuantity()); // _StockQuantity
            callableStatement.setInt(13, productItem.getReorderLevel()); // _ReorderLevel
            callableStatement.setInt(14, productItem.getTotalSold()); // _TotalSold
            callableStatement.registerOutParameter(15, Types.INTEGER);

            return callableStatement;
        }, paramList);

        // Create the specifications
        for (Specification specification : productItem.getSpecifications()) {
            updateSpecification(productItem.getId().toString(), specification);
        }

        // Link the images
        for (Image image : productItem.getImages()) {
            productItemImageCreate(image.getId(), productItem.getId().toString());
        }

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Upates an existing product item
     * @param productItem new product item data
     * @param productItemUUID product item UUID to update
     * @param productUUID product UUID for product item
     * @return updated product item
     */
    public String updateProductItem(ProductItem productItem, String productItemUUID, String productUUID) {
        String sql = "{CALL ProductItemUpdate(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();


        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductUID
        paramList.add(new SqlParameter(Types.BOOLEAN)); // _Display
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Model
        paramList.add(new SqlParameter(Types.VARCHAR)); // _SKU
        paramList.add(new SqlParameter(Types.VARCHAR)); // _UPC
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Name
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Description
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Keywords
        paramList.add(new SqlParameter(Types.DOUBLE)); // _UnitPrice
        paramList.add(new SqlParameter(Types.DOUBLE)); // _Weight
        paramList.add(new SqlParameter(Types.INTEGER)); // _StockQuantity
        paramList.add(new SqlParameter(Types.INTEGER)); // _ReorderLevel
        paramList.add(new SqlParameter(Types.INTEGER)); // _TotalSold

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productItemUUID); // _ProductItemUID
            callableStatement.setString(2, productUUID); // _ProductUID
            callableStatement.setBoolean(3, productItem.getDisplay()); // _Display
            callableStatement.setString(4, productItem.getModel()); // _Model
            callableStatement.setString(5, productItem.getSku()); // _SKU
            callableStatement.setString(6, productItem.getUpc()); // _UPC
            callableStatement.setString(7, productItem.getName()); // _Name
            callableStatement.setString(8, productItem.getDescription()); // _Description
            callableStatement.setString(9, productItem.getKeywords()); // _Keywords
            callableStatement.setDouble(10, productItem.getUnitPrice()); // _UnitPrice
            callableStatement.setDouble(11, productItem.getWeight()); // _Weight
            callableStatement.setInt(12, productItem.getStockQuantity()); // _StockQuantity
            callableStatement.setInt(13, productItem.getReorderLevel()); // _ReorderLevel
            callableStatement.setInt(14, productItem.getTotalSold()); // _TotalSold
            return callableStatement;
        }, paramList);

        // Delete all the existing specifications: not the right way to do it but time is short...
        deleteSpecifications(productItemUUID);

        // Update the specifications
        for (Specification specification : productItem.getSpecifications()) {
            updateSpecification(productItemUUID, specification);
        }

        // Link the images
        if (productItem.getImages() != null) {
            for (Image image : productItem.getImages()) {
                productItemImageCreate(image.getId(), productItem.getId().toString());
            }
        }

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Deletes a product item
     * @param productItemUID UUID o product item to delete
     * @return deleted product item
     */
    public String deleteProductItem(String productItemUID) {
        String sql = "{CALL ProductItemDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productItemUID);
            return callableStatement;
        }, paramList);


        // Remove the SpecificationDeleteByProductItem
        String specificationCreateSQL = "{CALL SpecificationDeleteByProductItem(?)}";
        List<SqlParameter> specParamList = new ArrayList<>();

        specParamList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(specificationCreateSQL);
            callableStatement.setString(1, productItemUID); // _ProductItemUID
            return callableStatement;
        }, specParamList);

        // TODO: Remove the image link
        
        return unwrapper.extractUpdateCount(out);
    }

    // MARK: - Product Item Images
    /**
     * Creates an image for product item
     * @param imageUID UUID of image to be used
     * @param productItemUID UUID of product item to create image for
     * @return the created image for product item
     */
    public String productItemImageCreate(String imageUID, String productItemUID) {
        String sql = "{CALL ProductItemImageCreate(?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ImageUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _Location
        paramList.add(new SqlOutParameter("result", Types.INTEGER)); // _ID

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, imageUID); // _ImageUID
            callableStatement.setString(2, productItemUID); // _Location
            callableStatement.registerOutParameter(3, Types.INTEGER); // _ID
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Delete product item image
     * @param imageUID UUID of image to delete
     * @param productItemUID UUID of product item to delete from
     * @return updated product item
     */
    public String productItemImageDelete(String imageUID, String productItemUID) {
        String sql = "{CALL ProductItemImageDelete(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _ImageUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, imageUID); // _ImageUID
            callableStatement.setString(2, productItemUID); // _ProductItemUID
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Gets all images for a product
     * @param productUUID UUID of product to find images for
     * @return the images for the product
     * @throws IndexOutOfBoundsException if there is no product with matching UUID
     */
    public Object getProductImages(String productUUID) throws IndexOutOfBoundsException {
        String sql = "{CALL ProductImageGetAll(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productUUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }
    
    // MARK: - Product Sub Category 
    /**
     * Add a subcategory to a product
     * @param productUID product UUID to add subcategory to
     * @param categoryUID category UUID for subcategory
     * @param subcategoryUID UUID of subcategory to add
     */
    public void productAddSubCategory(String productUID, String categoryUID, String subcategoryUID) {
        String specificationCreateSQL = "{CALL ProductAddSubCategory(?, ?, ?)}";
        List<SqlParameter> specParamList = new ArrayList<>();

        specParamList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID
        specParamList.add(new SqlParameter(Types.VARCHAR)); // _CategoryUID
        specParamList.add(new SqlParameter(Types.BOOLEAN)); // _SubcategoryUID

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(specificationCreateSQL);
            callableStatement.setString(1, productUID); // _ProductItemUID
            callableStatement.setString(2, categoryUID); // _CategoryUID
            callableStatement.setString(3, subcategoryUID); // _SubcategoryUID
            return callableStatement;
        }, specParamList);
    }
    
    // TODO: Implement this method
    /**
     * Remvoe a subcategory from product
     * @param productUID UUID of product to remove from
     * @param subcategoryUID UUID of subcategory to remove
     */
    public void productRemoveSubCategory(String productUID, String subcategoryUID) {
        String specificationCreateSQL = "{CALL ProductRemoveSubCategory(?, ?, ?)}";
        List<SqlParameter> specParamList = new ArrayList<>();

        specParamList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID
        specParamList.add(new SqlParameter(Types.BOOLEAN)); // _SubcategoryUID

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(specificationCreateSQL);
            callableStatement.setString(1, productUID); // _ProductItemUID
            callableStatement.setString(2, subcategoryUID); // _SubcategoryUID
            return callableStatement;
        }, specParamList);
    }

    // MARK: - Product Specification
    /**
     * Update product specification 
     * @param productUID UUID of product to update
     * @param specification new specification data
     * @return the updated product specifications 
     */
    public String updateSpecification(String productUID, Specification specification) {
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        String specificationCreateSQL = "{CALL SpecificationCreate(?, ?, ?)}";
        List<SqlParameter> specParamList = new ArrayList<>();

        specParamList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID
        specParamList.add(new SqlParameter(Types.VARCHAR)); // _ProductUID
        specParamList.add(new SqlParameter(Types.BOOLEAN)); // _Display

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(specificationCreateSQL);
            callableStatement.setString(1, productUID); // _ProductItemUID
            callableStatement.setString(2, specification.getName()); // _ProductUID
            callableStatement.setString(3, specification.getValue()); // _Display
            return callableStatement;
        }, specParamList);

        return unwrapper.extractUpdateCount(out); 
    }

    /**
     * Delete product item specifications
     * @param productItemUID UUID pf product item to delete from
     * @return the updated product item
     */
    public String deleteSpecifications(String productItemUID) {
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        String specificationDeleteSQL = "{CALL SpecificationDeleteByProductItem(?)}";
        List<SqlParameter> specParamList = new ArrayList<>();

        specParamList.add(new SqlParameter(Types.VARCHAR)); // _ProductItemUID

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(specificationDeleteSQL);
            callableStatement.setString(1, productItemUID); // _ProductItemUID
            return callableStatement;
        }, specParamList);

        return unwrapper.extractUpdateCount(out); 
    }

    /**
     * Gets all review for product
     * @param productUUID UUID of product to search for
     * @return the reviews for the product
     * @throws IndexOutOfBoundsException if no product matches UUID
     */
    public Object getProductReviews(String productUUID) throws IndexOutOfBoundsException {
        String sql = "{CALL ProductReviewsGetAll(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productUUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }
}
