package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for PaymentProcessor data
 */
public class PaymentProcessor {
    private UUID id;
    private String logo;
    private String name;

    /**
     * Constructor for PaymentProcessor data
     * @param id payment processor id
     * @param logo payment processor logo
     * @param name payment processor nane
     */
    public PaymentProcessor(UUID id, String logo, String name) {
        this.id = id;
        this.logo = logo;
        this.name = name;
    }

    /**
     * Payment Processor id getter method
     * @return payment processor id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Payment Processor id setter method
     * @param id new payment processor id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * Payment Processor logo getter method
     * @return the logo of the payment processor
     */
    public String getLogo() {
        return logo;
    }

    /**
     * Payment Processor logo setter method
     * @param logo new logo for payment processor
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * Payment Processor name getter method
     * @return name of payment processor
     */
    public String getName() {
        return name;
    }

    /**
     * Payment Processor name setter
     * @param name new name for payment processor
     */
    public void setName(String name) {
        this.name = name;
    }

}
