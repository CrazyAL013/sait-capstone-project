package com.aquarium.store.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.aquarium.store.models.Image;
import com.aquarium.store.models.Product;
import com.aquarium.store.models.ProductItem;
import com.aquarium.store.models.UploadFileResponse;
import com.aquarium.store.services.FileStorageService;
import com.aquarium.store.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService service;

    @Autowired
    FileStorageService fileStorageService;

    /**
     * Get products
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/")
    public ResponseEntity<Object> getProducts() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            Object products = service.getProducts();
            return new ResponseEntity<>(products, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get a specific product
     * @param productUUID of the product
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{productUUID}")
    public ResponseEntity<Object> getProduct(@PathVariable String productUUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            Object product = service.getProduct(productUUID);
            return new ResponseEntity<>(product, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Add a product
     * @param product to add
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/")
    public ResponseEntity<Object> add(@RequestBody Product product) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.saveProduct(product);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update a product
     * @param productUUID of the product to update
     * @param product to update
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{productUUID}")
    public ResponseEntity<Object> updateProduct(@PathVariable String productUUID, @RequestBody Product product) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.updateProduct(product, productUUID);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete a product
     * @param productUUID of the product to delete
     * @return A responseEntity to define headers and body data
     */
    @DeleteMapping("/{productUUID}")
    public ResponseEntity<Object> deleteProduct(@PathVariable String productUUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.deleteProduct(productUUID);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    // MARK: - Product Items

    /**
     * Get a product item
     * @param productUUID of the product to get
     * @param itemUUID of the item
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{productUUID}/productItem/{itemUUID}")
    public ResponseEntity<Object> getProductItem(@PathVariable String productUUID, @PathVariable String itemUUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            Object product = service.getProductItem(itemUUID);
            return new ResponseEntity<>(product, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Add a new product item 
     * @param productUUID of the product
     * @param productItem to add
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/{productUUID}/productItem")
    public ResponseEntity<Object> add(@PathVariable String productUUID, @RequestBody ProductItem productItem) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.saveProductItem(productItem, productUUID);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update a product item
     * @param productUUID of the product to update
     * @param itemUUID of the item to update
     * @param productItem to update
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{productUUID}/productItem/{itemUUID}")
    public ResponseEntity<Object> updateProductItem(@PathVariable String productUUID, @PathVariable String itemUUID,
            @RequestBody ProductItem productItem) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.updateProductItem(productItem, itemUUID, productUUID);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete a product item
     * @param productUUID of the product to delete
     * @param itemUUID of the item to delete
     * @return A responseEntity to define headers and body data
     */
    @DeleteMapping("/{productUUID}/productItem/{itemUUID}")
    public ResponseEntity<Object> deleteProductItem(@PathVariable String productUUID, @PathVariable String itemUUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.deleteProductItem(itemUUID);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    // MARK: - Images
    /**
     * Upload an image
     * @param file to upload
     * @return A responseEntity to define headers and body data
     */
    @PostMapping(value = "/uploadImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public UploadFileResponse uploadImage(@RequestParam(value = "image", required = false) MultipartFile file) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "multipart/form-data;");
        String fileName = fileStorageService.storeFile(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/product/downloadImage/")
                .path(fileName).toUriString();

        System.out.println(fileDownloadUri);

        return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
    }

    /**
     * Will download a file
     * @param fileName to find and download
     * @param request to store the content type
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/downloadImage/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
    /**
     * Add an image to the database
     * @param image to add
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/image")
    public ResponseEntity<Object> addImage(@RequestBody Image image) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.productImageCreate(image);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update a product image
     * @param imageUUID of the image
     * @param image to update
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/image/{imageUUID}")
    public ResponseEntity<Object> updateProductImage(@PathVariable String imageUUID, @RequestBody Image image) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.productImageUpdate(image);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Delete a product image
     * @param imageUUID of the image
     * @return A responseEntity to define headers and body data
     */
    @DeleteMapping("/image/{imageUUID}")
    public ResponseEntity<Object> deleteProductImage(@PathVariable String imageUUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            String rowChangeCount = service.productImageDelete(imageUUID);
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Gets all images associated with a product
     * @param productUUID of the product
     * @return 
     */
    @GetMapping("/productImages/{productUUID}")
    public ResponseEntity<Object> getProductImages(@PathVariable String productUUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            Object products = service.getProductImages(productUUID);
            return new ResponseEntity<>(products, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Create association between images and a product item
     * @param images to add
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/productItemImages/{productItemUUID}")
    public ResponseEntity<Object> addproductItemImage(@PathVariable String productItemUUID, @RequestBody Image[] images) {
        HttpHeaders responseHeaders = new HttpHeaders();

        try {
            for (int i = 0; i < images.length; i++) {
                service.productItemImageCreate(images[i].getId(), productItemUUID);
            }
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get product reviews
     * @param productUID of the product
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{productUID}/review")
    public ResponseEntity<Object> getProductReviews(@PathVariable String productUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        try {
            Object account = service.getProductReviews(productUID);
            return new ResponseEntity<>(account, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            e.printStackTrace();
            return new ResponseEntity<>(responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }
}
