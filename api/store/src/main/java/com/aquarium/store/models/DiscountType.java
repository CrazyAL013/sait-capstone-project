package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for DiscountType data
 */
public class DiscountType {
    
    private UUID uid;
    private int id;
    private String name;
    private String description;

    /**
     * Default constructor for DiscountType data
     */
    public DiscountType() {}

    /**
     * Full constructor for DiscountType data
     * @param id the discount type id
     * @param uid the discount type uid
     * @param name the name for the discount type
     * @param description the discount type description
     */
    public DiscountType(int id, UUID uid, String name, String description) {
        this.id = id;
        this.uid = uid;
        this.name = name;
        this.description = description;
    }

    /**
     * Id setter method
     * @param id new value for discount type id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Id getter method
     * @return the discount type id
     */
    public int getId() {
        return this.id;
    }

    /**
     * UID setter method
     * @param uid new UUID for discount type
     */
    public void setUid(UUID uid) {
        this.uid = uid;
    }

    /**
     * UID getter method
     * @return the discount type uid
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Name setter method
     * @param name the discount type name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Name getter method
     * @return the discount type name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Description setter method
     * @param description new description for discount type
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Description getter method
     * @return the discount type description
     */
    public String getDescription() {
        return this.description;
    }

}
