package com.aquarium.store.controllers;

import com.aquarium.store.services.CatalogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * RestController for catalog
 * Retrieves data from catagories and subcategories based on keyword search and sorting the returned data
 */
@RestController
@RequestMapping("/catalog")
public class CatalogController {

    @Autowired
    CatalogService service;

     /**
     * Returns all products
     * 
     * @param RequestParam sort - the method to sort the data by
     * @return the products in JSON format
     */
    @GetMapping("")
    public ResponseEntity<Object> searchAll(@RequestParam String sort) {
        Object results = service.searchAll(sort);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(results, responseHeaders, HttpStatus.OK);
    }

    /**
     * Searches for all products based on keywords
     * 
     * @param RequestParam terms - the keywords to search for
     * @param RequestParam sort - the method to sort the data by
     * @return the products in JSON format
     */
    @GetMapping("/search")
    public ResponseEntity<Object> searchByKeyword(@RequestParam String terms, @RequestParam String sort) {
        Object results = service.searchByKeyword(terms, sort);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(results, responseHeaders, HttpStatus.OK);
    }

     /**
     * Searches for products in a category by keywords
     * 
     * @param categoryUID - The UUID for the category to search by
     * @param RequestParam terms - the keywords to search by
     * @param RequestParam sort - the method to sort the data by
     * @return the products in JSON format
     */
    @GetMapping("/searchCategory/{categoryUID}")
    public ResponseEntity<Object> searchByCategoryKeyword(@PathVariable String categoryUID, @RequestParam String terms, @RequestParam String sort) {
        Object results = service.searchByCategoryKeyword(categoryUID, terms, sort);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(results, responseHeaders, HttpStatus.OK);
    }

    /**
     * Searches for products in subcategories by keywords
     * 
     * @param subcategoryUID - the UUID for the subcategory to search by
     * @param RequestParam terms - the keywords to search by
     * @param RequestParam sort - the method to sort the data by
     * @return the products in JSON format
     */
    @GetMapping("/searchSubcategory/{subcategoryUID}")
    public ResponseEntity<Object> searchBySubcategoryKeyword(@PathVariable String subcategoryUID, @RequestParam String terms, @RequestParam String sort) {
        Object results = service.searchBySubcategoryKeyword(subcategoryUID, terms, sort);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(results, responseHeaders, HttpStatus.OK);
    }

     /**
     * Returns all products in a category
     * 
     * @param PathVariable categoryUID - the UUID of the category to search in
     * @param RequestParam sort - the method to sort the data by
     * @return the products in JSON format
     */
    @GetMapping("/category/{categoryUID}")
    public ResponseEntity<Object> searchByCategoryKeyword(@PathVariable String categoryUID, @RequestParam String sort) {
        Object results = service.searchByCategory(categoryUID, sort);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(results, responseHeaders, HttpStatus.OK);
    }

     /**
     * Returns all products in a subcategory
     * 
     * @param PathVariable subcategoryUID - the UUID of the subcategory to search in
     * @param RequestParam sort - the method to sort the data by
     * @return the products in JSON format
     */
    @GetMapping("/subcategory/{subcategoryUID}")
    public ResponseEntity<Object> searchBySubcategory(@PathVariable String subcategoryUID, @RequestParam String sort) {
        Object results = service.searchBySubcategory(subcategoryUID, sort);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(results, responseHeaders, HttpStatus.OK);
    }
}