package com.aquarium.store.models;

import java.util.ArrayList;

/**
 * Model for Category data
 */
public class Category {
    private String name;
    private ArrayList<Subcategory> subcategories;
    private String id;

    /**
     * Default constructor for Category objects
     */
    public Category() {}

    /**
     * Full constructor for Category Objects
     * @param name the name of the category
     * @param subcategories the subcategories for the category
     * @param id the category id
     */
    public Category(String name, ArrayList<Subcategory> subcategories, String id) {
        this.name = name;
        this.subcategories = subcategories;
        this.id = id;
    }

    /**
     * Name getter method
     * @return the name of the category
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter method
     * @param name the new name for the category
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Subcategories getter method
     * @return the category subcategories 
     */
    public ArrayList<Subcategory> getSubcategories() {
        return subcategories;
    }

    /**
     * Subcategories setter method
     * @param subcategories list of new subcategories
     */
    public void setSubcategories(ArrayList<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

    /**
     * ID getter method
     * @return the category id
     */
    public String getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id the new category ID
     */
    public void setId(String id) {
        this.id = id;
    }
}