package com.aquarium.store.models;

/**
 * Model for User data
 */
public class User {

    private String id;
    private Account account;
    private String password;

    /**
     * Constructor for User objects
     * @param id user id
     * @param account account data for user
     * @param password user password
     */
    public User(String id, Account account, String password) {
        this.id = id;
        this.account = account;
        this.password = password;
    }

    /**
     * ID getter method
     * @return user id
     */
    public String getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new id for user
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Account getter method
     * @return account data for user
     */
    public Account getAccount() {
        return account;
    }

    /**
     * Account setter method
     * @param account new Account data object for user
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Password getter method
     * @return user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Password setter method 
     * @param password new password for user
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
