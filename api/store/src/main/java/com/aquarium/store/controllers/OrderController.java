package com.aquarium.store.controllers;

import com.aquarium.store.services.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/order")
public class OrderController {
    
    @Autowired
    OrderService service;

    /**
     * Get an order based on uid
     * @param orderUUID of the order
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{orderUUID}")
    public ResponseEntity<Object> getOrder(@PathVariable String orderUUID) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

        try {
            Object order = service.getOrder(orderUUID);
            return new ResponseEntity<>(order, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(responseHeaders, HttpStatus.NOT_FOUND);
        }
    }
}