package com.aquarium.store.models;

/**
 * Model for application errors 
 */
public class Error {
    private String code;
    private String detail;
    private String category;

    /**
     * Constructor for Errors 
     * @param code the error code
     * @param detail the details of the error
     * @param category the category of the error
     */
    public Error(String code, String detail, String category) {
        this.code = code;
        this.detail = detail;
        this.category = category;
    }

    /**
     * Code getter method
     * @return the code of the error
     */
    public String getCode() {
        return code;
    }

    /**
     * Code setter method
     * @param code new code for the error
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Detail getter method
     * @return the detail of the error
     */
    public String getDetail() {
        return detail;
    }
    
    /**
     * Detail setter method
     * @param detail new details for the error
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * Category getter method
     * @return the category of the error
     */
    public String getCategory() {
        return category;
    }

    /**
     * Category setter method
     * @param category new category of the error
     */
    public void setCategory(String category) {
        this.category = category;
    }

}
