package com.aquarium.store.services;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aquarium.utilities.CallStatementUnwrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for catalog and searching for products in categories and subcategories based on keywords
 */
@Service
@Transactional
public class CatalogService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

     /**
     * Returns all products
     * Calls database procedure 'SearchAll()'
     * 
     * @param sort - the method to sort the data by
     * @return the results from the stored procedure
     */
    public Object searchAll(String sort) {
        String sql = "{CALL SearchAll(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, sort);
            return callableStatement;
        }, paramList);

        return unwrapper.buildJsonArray(out);
    }

    /**
     * Searches for all products based on keywords
     * Calls database procedure 'SearchByKeyword()'
     * 
     * @param keyword - the keywords to search for
     * @param sort - the method to sort the data by
     * @return the results from the stored procedure
     */
    public Object searchByKeyword(String keyword, String sort) {
        String sql = "{CALL SearchByKeyword(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, keyword);
            callableStatement.setString(2, sort);
            return callableStatement;
        }, paramList);

        return unwrapper.buildJsonArray(out);
    }

    /**
     * Searches for products in a category by keywords
     * Calls database procedure 'SearchByCategoryKeyword()'
     * 
     * @param categoryUID - the UUID of the category to search in
     * @param keyword - the keywords to search by
     * @param sort - the method to sort the data by
     * @return the results from the stored procedure
     */
    public Object searchByCategoryKeyword(String categoryUID, String keyword, String sort) {
        String sql = "{CALL SearchByCategoryKeyword(?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, categoryUID);
            callableStatement.setString(2, keyword);
            callableStatement.setString(3, sort);
            return callableStatement;
        }, paramList);

        return unwrapper.buildJsonArray(out);
    }

    /**
     * Searches for products in subcategories by keywords
     * Calls database procedure 'SearchBySUbcategoryKeyword()'
     * 
     * @param subcategoryUID - the UUID Of the subcategory to search in
     * @param keyword - the keywords to search by
     * @param sort - the method to sort the data by
     * @return the results from the stored procedure
     */
    public Object searchBySubcategoryKeyword(String subcategoryUID, String keyword, String sort) {
        String sql = "{CALL SearchBySubcategoryKeyword(?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, subcategoryUID);
            callableStatement.setString(2, keyword);
            callableStatement.setString(3, sort);
            return callableStatement;
        }, paramList);

        return unwrapper.buildJsonArray(out);
    }

    /**
     * Returns all products in a category
     * Calls database procedure 'searchByCategory()'
     * 
     * @param categoryUID - the UUID of the category to search in
     * @param sort - the method to sort the data by
     * @return the results from the stored procedure
     */
    public Object searchByCategory(String categoryUID, String sort) {
        String sql = "{CALL SearchByCategory(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, categoryUID);
            callableStatement.setString(2, sort);
            return callableStatement;
        }, paramList);

        return unwrapper.buildJsonArray(out);
    }

    /**
     * Returns all products in a subcategory
     * Calls database procedure 'SearchBySubcategory()'
     * 
     * @param subcategoryUID - the UUID of the subcategory to search in 
     * @param sort - the method to sort the data by
     * @return the results from the stored procedure
     */
    public Object searchBySubcategory(String subcategoryUID, String sort) {
        String sql = "{CALL SearchBySubcategory(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, subcategoryUID);
            callableStatement.setString(2, sort);
            return callableStatement;
        }, paramList);

        return unwrapper.buildJsonArray(out);
    }
}
