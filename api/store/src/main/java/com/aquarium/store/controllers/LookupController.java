package com.aquarium.store.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.aquarium.store.services.LookupService;


/**
 * Rest controller for lookup
 * Mapping to /v1/lookup
 */
@RestController
@RequestMapping("/lookup")
public class LookupController {

    @Autowired
    LookupService service;

    /**
     * Gets all countries
     * @return countries data retrieved
     */
    @GetMapping("/country")
    public ResponseEntity<Object> country() {
        Object countries = service.getCountry();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(countries, responseHeaders, HttpStatus.OK);
    }

    /**
     * GEts all user roles
     * @return user roles data retrieved
     */
    @GetMapping("/userRole")
    public ResponseEntity<Object> userRoles() {
        Object userRoles = service.getUserRoles();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(userRoles, responseHeaders, HttpStatus.OK);
    }

    /**
     * Gets all order status
     * @return order status data retrieved
     */
    @GetMapping("/orderStatus")
    public ResponseEntity<Object> orderStatus() {
        Object orderStatuses = service.getOrderStatus();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(orderStatuses, responseHeaders, HttpStatus.OK);
    }

    /**
     * Gets all discount types
     * @return discount types data retreived
     */
    @GetMapping("/discountType")
    public ResponseEntity<Object> discountTypes() {
        Object discountTypes = service.getDiscountTypes();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(discountTypes, responseHeaders, HttpStatus.OK);
    }

    /**
     * Gets all payment processors
     * @return payment processors data retrieved
     */
    @GetMapping("/paymentProcessor")
    public ResponseEntity<Object> paymentProcessor() {
        Object paymentProcessors = service.getPaymentProcessor();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(paymentProcessors, responseHeaders, HttpStatus.OK);
    }

    /**
     * Gets all suppliers
     * @return suppliers data retrieved
     */
    @GetMapping("/supplier")
    public ResponseEntity<Object> supplier() {
        Object paymentProcessors = service.getSuppleirs();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(paymentProcessors, responseHeaders, HttpStatus.OK);
    }

    /**
     * Gets all manufacturers
     * @return manufacturers data retrieved
     */
    @GetMapping("/manufacturer")
    public ResponseEntity<Object> manufacturer() {
        Object paymentProcessors = service.getManufacturers();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(paymentProcessors, responseHeaders, HttpStatus.OK);
    }


}
