package com.aquarium.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aquarium.utilities.CallStatementUnwrapper;

/**
 * Service class for lookup
 */
@Service
@Transactional
public class LookupService {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Gets all contries
     * @return countries data
     */
    public Object getCountry() {
        String sql = "{CALL CountryGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Gets all user roles
     * @return user roles data
     */
    public Object getUserRoles() {
        String sql = "{CALL UserRoleGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        
        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Gets all order statuses
     * @return order statuses data
     */
    public Object getOrderStatus() {
        String sql = "{CALL OrderStatusGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        
        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }
    
    /**
     * Gets all discount types
     * @return discount types data 
     */
    public Object getDiscountTypes() {
        String sql = "{CALL DiscountTypeGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        
        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Gets all payment processors
     * @return payment processors data
     */
    public Object getPaymentProcessor() {
        String sql = "{CALL PaymentProcessorGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        
        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Gets all suppliers
     * @return suppliers data
     */
    public Object getSuppleirs() {
        String sql = "{CALL SupplierGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        
        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Gets all manufacturers
     * @return manufacturers data
     */
    public Object getManufacturers() {
        String sql = "{CALL ManufacturerGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();
        
        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }
}
