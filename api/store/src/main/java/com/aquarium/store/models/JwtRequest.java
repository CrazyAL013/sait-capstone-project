package com.aquarium.store.models;

import java.io.Serializable;

/**
 * Model for application JwtRequest data
 */
public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    private String username;
    private String password;

    /**
     * Default constructor for JwtRequest objects
     */
    public JwtRequest() {}

    /**
     * Constructor for JwtRequest objects
     * @param username the username of the requet
     * @param password the password of the request
     */
    public JwtRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    /** 
     * Username getter method
     * @return the request username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Username setter method
     * @param username new username for this request
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Password getter method
     * @return the request password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Password setter method
     * @param password new password for this request
     */
    public void setPassword(String password) {
        this.password = password;
    }
}