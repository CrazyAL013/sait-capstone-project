package com.aquarium.store.services;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aquarium.utilities.CallStatementUnwrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for order
 * Only purpose is to retrieve an order from UID
 */
@Service
@Transactional
public class OrderService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Retrieves order data based on provided UID
     * @param orderUUID UID to search for
     * @return Order returned
     * @throws IndexOutOfBoundsException if there is no order
     */
    public Object getOrder(String orderUUID) throws IndexOutOfBoundsException {
        String sql = "{CALL OrderGetById(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, orderUUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }
}