package com.aquarium.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.UUID;

import com.aquarium.store.models.Address;
import com.aquarium.store.services.AddressService;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    AddressService service;

    /**
     * Will get an address by UID
     * 
     * @param type of address to get
     * @param addressUID The uid of the address to get
     * @return
     */
    @GetMapping("/{addressUID}")
    public ResponseEntity<Object> get(String type, @PathVariable String addressUID) {
        try {
            Object category = service.getAddressByAddressUID(addressUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(category, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     *  Add an address 
     * 
     * @param address To add
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/")
    public ResponseEntity<Object> add(@RequestBody Address address) {
        try {
            String rowChangeCount = service.saveAddress(address);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update an address
     * 
     * @param address data to update
     * @param addressUID of the address to update
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{addressUID}")
    public ResponseEntity<Object> update(@RequestBody Address address, @PathVariable UUID addressUID) {
        try {
            String rowChangeCount = service.updateAddress(address, addressUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete an address
     * @param addressUID of the address to delete
     * @return A responseEntity to define headers and body data
     */
    @DeleteMapping("/{addressUID}")
    public ResponseEntity<Object> delete(@PathVariable UUID addressUID) {
        try {
            String rowChangeCount = service.deleteAddress(addressUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
