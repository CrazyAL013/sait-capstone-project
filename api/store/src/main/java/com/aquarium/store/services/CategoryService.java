package com.aquarium.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.aquarium.store.models.Category;
import com.aquarium.store.models.Subcategory;
import com.aquarium.utilities.CallStatementUnwrapper;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * List all of the categories
     * @return list of all categories found
     */
    public Object listAllCategories() {
        String sql = "{CALL CategoryGetAll()}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        Map<String, Object> out = jdbcTemplate.call(connection -> connection.prepareCall(sql), paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Finds a category baesd on UID
     * 
     * @param uid UID to search by
     * @return the found category
     */
    public Object getCategory(String uid) {
        String sql = "{CALL CategoryGetById(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid);
            return callableStatement;
        }, paramList);
        
        return unwrapper.extractResultSet(out);
    }

    /**
     * Insert a new category into databaes
     * @param category Object of category to save
     * @return the number or rows created
     */
    public String saveCategory(Category category) {
        String sql = "{CALL CategoryCreate(?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlOutParameter("result", Types.INTEGER));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, category.getId());
            callableStatement.setString(2, category.getName());
            callableStatement.registerOutParameter(3, Types.INTEGER);
            return callableStatement;
        }, paramList);

        for (Subcategory subcategory : category.getSubcategories()) {
            this.createOrUpdateSubcategory(subcategory, category.getId());
        }

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Update an existing category and return it
     * @param category the new Category data
     * @param uid the UID of the category to update
     * @return return the new category
     */
    public String updateCategory(Category category, UUID uid) {
        String sql = "{CALL CategoryUpdate(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out =  jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid.toString());
            callableStatement.setString(2, category.getName());
            return callableStatement;
        }, paramList);

        for (Subcategory subcategory : category.getSubcategories()) {
            this.createOrUpdateSubcategory(subcategory, uid.toString());
        }

        return unwrapper.extractUpdateCount(out);
    }
    
    /**
     * Creates a new SubCategory if it does not already exist, otherwise updates the subcategory
     * @param subcategory the subcategory data
     * @param uid the UID of the subcategory to update if needed
     * @return the created or updated subcategory
     */
    private String createOrUpdateSubcategory(Subcategory subcategory, String id) {
        String sql = "{CALL SubcategoryCreateOrUpdate(?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlOutParameter("id", Types.INTEGER));

        Map<String, Object> out =  jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, subcategory.getId());
            callableStatement.setString(2, id);
            callableStatement.setString(3, subcategory.getName());
            callableStatement.registerOutParameter(4, Types.INTEGER);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Delete a category with the given uid
     * @param uid UID of category to delete
     */
    public String deleteCategory(UUID uid) {
        String sql = "{CALL CategoryDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid.toString());
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Insert a subcategory
     * @param uid UID of category for subcategory
     * @param subcategory new subcategory data
     * @return the inserted subcategory
     */
    public String saveSubCategory(UUID categoryUID, Subcategory subcategory) {
        String sql = "{CALL SubcategoryCreate(?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlOutParameter("id", Types.INTEGER));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, subcategory.getId());
            callableStatement.setString(2, categoryUID.toString());
            callableStatement.setString(3, subcategory.getName());
            callableStatement.registerOutParameter(4, Types.INTEGER);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Update a provided subcategory
     * @param categoryUuid UID of category for subcategory
     * @param subcategory new subcategory data
     * @param subcategoryUid UID of subcategory to be updated
     * @return the updated subcategory
     */
    public String updateSubcategory(UUID categoryUuid, Subcategory subcategory, UUID subcategoryUid) {
        String sql = "{CALL SubcategoryUpdate(?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, subcategoryUid.toString());
            callableStatement.setString(2, categoryUuid.toString());
            callableStatement.setString(3, subcategory.getName());
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Delete a subcategory
     * @param subcategoryUid UID of subcategory to delete
     * @return deleted subcategory
     */
    public String deleteSubcategory(UUID subcategoryUid) {
        String sql = "{CALL SubcategoryDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, subcategoryUid.toString());
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

}