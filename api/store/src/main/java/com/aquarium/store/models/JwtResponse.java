package com.aquarium.store.models;

import java.io.Serializable;

/**
 * Model for applicatoin JwtResponse data 
 */
public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String authToken;
    private final String refreshToken;

    /**
     * Constructor for JwtResponse
     * @param authToken authenitcation token from response
     * @param refreshToken refresh token from response
     */
    public JwtResponse(String authToken, String refreshToken) {
        this.authToken = authToken;
        this.refreshToken = refreshToken;
    }

    /**
     * Authentication Token getter method 
     * @return the response AuthToken
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Refresh Token getter method
     * @return the response RefreshToken
     */
    public String getRefreshToken() {
        return refreshToken;
    }


}
