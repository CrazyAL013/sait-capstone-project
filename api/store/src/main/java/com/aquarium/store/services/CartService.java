package com.aquarium.store.services;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aquarium.utilities.CallStatementUnwrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;

/**
 * Service for Cart
 * Performs CRUD operations on Cart data
 */
@Service
public class CartService {

    @Autowired
    JdbcTemplate jdbcTemplate;


    /**
     * Returns product items from a cart
     * @param productItemList product items to retrieve
     * @return the found product items
     */
    public Object getCartItems(String productItemList) {
        String sql = "{CALL CartGetItems(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, productItemList);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Updates an accounts cart
     * @param uid the UID of the account to update
     * @param cart the new data for the cart
     * @return the number of rows affected by update
     */
    public String updateCart(String uid, String cart) {
        String sql = "{CALL CartUpdate(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid);
            callableStatement.setString(2, cart);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Deletes an accounts cart
     * @param uid the UID Of the account to delete the cart
     * @return the number of rows affected by delete
     */
    public String deleteCart(String uid) {
        String sql = "{CALL CartDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }
    
}
