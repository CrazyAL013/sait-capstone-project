package com.aquarium.store.models;

/**
 * Model for Region data
 */
public class Region {

    private int id;
    private Float tax;
    private String code;
    private String name;
    private int countryId;

    /**
     * Constructor for Region objets
     * @param id region id
     * @param tax the tax in the region
     * @param code the region code
     * @param name the name of the region
     * @param countryId the id of the country of the region
     */
    public Region(int id, Float tax, String code, String name, int countryId) {
        this.id = id;
        this.tax = tax;
        this.code = code;
        this.name = name;
        this.countryId = countryId;
    }

    /**
     * ID getter method
     * @return region id
     */
    public int getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new id of the region
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Tax getter method
     * @return region tax value
     */
    public Float getTax() {
        return tax;
    }

    /**
     * Tax setter method
     * @param tax new region tax value
     */
    public void setTax(Float tax) {
        this.tax = tax;
    }

    /**
     * Code getter method
     * @return region code
     */
    public String getCode() {
        return code;
    }

    /**
     * Code setter method
     * @param code new region code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Name getter method
     * @return region name
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter method
     * @param name new region name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Country id getter method
     * @return id of region country
     */
    public int getCountryId() {
        return countryId;
    }

    /**
     * Country id setter method
     * @param countryId new id of region country
     */
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

}
