package com.aquarium.store.models;

/**
 * Model for Role data
 */
public class Role {
    private String id;
    private String name;

    /**
     * Constructor for Role objects
     * @param id role id
     * @param name name of role
     */
    public Role(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * ID getter method
     * @return role id
     */
    public String getId() {
        return id;
    }

    /**
     * ID setter method
     * @param id new id of role
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Name getter method
     * @return name of role
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter method
     * @param name new name of role
     */
    public void setName(String name) {
        this.name = name;
    }

}
