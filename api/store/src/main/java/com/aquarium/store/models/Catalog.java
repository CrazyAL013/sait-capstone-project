package com.aquarium.store.models;

import java.util.UUID;

/**
 * Model for Catalog data
 */
public class Catalog {
    
    private int id;
    private UUID uid;
    private int supplierID;
    private String name;
    private String description;
    private String keywords;
    private int rating;
    private String image;
    private ProductItem[] items;

    /**
     * Default constructor for Catalog objects
     */
    public Catalog() {}

    /**
     * Full constructor for Catalog objects
     * @param id the catalog id
     * @param uid the catalog uid
     * @param supplierID the supplier id
     * @param name the name of the catalog
     * @param description the catalog description
     * @param keywords the keywords for this catalog
     * @param rating the catalog rating
     * @param image the image for this catalog
     * @param items the items in this catalog
     */
    public Catalog(int id, UUID uid, int supplierID, String name, String description, String keywords, 
                   int rating, String image, ProductItem[] items) {
        this.id = id;
        this.uid = uid;
        this.supplierID = supplierID;
        this.name = name;
        this.description = description;
        this.keywords = keywords;
        this.rating = rating;
        this.image = image;
        this.items = items;
    }

    /**
     * ID setter method
     * @param id new value for catalog id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * ID getter method
     * @return the catalog id
     */
    public int getId() {
        return this.id;
    }

    /**
     * UID setter method
     * @param uid new catalog uid
     */
    public void setUid(UUID uid) {
        this.uid = uid;
    }

    /**
     * UID getter method
     * @return the catalog UID
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Supplier id setter method
     * @param supplierID new supplier id value
     */
    public void setSupplierId(int supplierID) {
        this.supplierID = supplierID;
    }

    /**
     * Supplier id getter method
     * @return the catalog supplier id
     */
    public int getSupplierId() {
        return this.supplierID;
    }

    /**
     * Name setter method
     * @param name new value for catalog name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Name getter method
     * @return the name of this catalog
     */
    public String getName() {
        return this.name;
    }

    /**
     * Description setter method 
     * @param description new value for catalog description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Description getter method
     * @return the catalog description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Keywords setter method
     * @param keywords the new keywords for this catalog
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * Keywords getter method
     * @return the keywords for this catalog
     */
    public String getKeywords() {
        return this.keywords;
    }

    /**
     * Ratings setter method
     * @param rating new rating for catalog
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     * Rating getter method
     * @return the catalog rating
     */
    public int getRating() {
        return this.rating;
    }

    /**
     * Image setter method 
     * @param image new image for catalog
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Image getter method
     * @return image for this catalog
     */
    public String getImage() {
        return this.image;
    }

    /**
     * Items setter method
     * @param items new items for catalog
     */
    public void setItems(ProductItem[] items) {
        this.items = items;
    }

    /**
     * Items getter method
     * @return items in the catalog
     */
    public ProductItem[] getItems() {
        return this.items;
    }
}
