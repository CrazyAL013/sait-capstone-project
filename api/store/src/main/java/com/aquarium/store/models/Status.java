package com.aquarium.store.models;

/**
 * Model for Status data
 */
public class Status {
    private String id; 
    private String title; 
    private String description;

    /**
     * Constructor for Status object
     * @param id status id
     * @param title status title
     * @param description status description
     */
    public Status(String id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }
    
    /**
     * ID getter method
     * @return status id
     */
    public String getId() {
        return id;
    }
    
    /**
     * ID setter method
     * @param id new id for status
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     * Title getter method
     * @return status title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Title setter method
     * @param title new status title
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * Description getter method
     * @return status description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Description setter method
     * @param description new status description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
