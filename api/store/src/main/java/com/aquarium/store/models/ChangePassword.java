package com.aquarium.store.models;

/**
 * Model for tracking password changes
 */
public class ChangePassword {

    private String username;
    private String password;
    private String newPassword;

    /**
     * Constructor for ChangePassword objects
     * @param username the username being changed
     * @param password the old password
     * @param newPassword the new password
     */
    public ChangePassword(String username, String password, String newPassword) {
        this.username = username;
        this.password = password;
        this.newPassword = newPassword;
    }
    
    /** 
     * Username getter method
     * @return the username being changed
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username setter method
     * @param username new value for username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Old password getter method
     * @return the old password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Old password setter method
     * @param password new value for old password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * New password getter method
     * @return the new password
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * New password setter method
     * @param newPassword new value for new password
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}
