package com.aquarium.store.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.UUID;

import com.aquarium.store.models.Category;
import com.aquarium.store.models.Subcategory;
import com.aquarium.store.services.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService service;

    /**
     * Get all categories
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("")
    public ResponseEntity<Object> list() {
        Object categories = service.listAllCategories();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(categories, responseHeaders, HttpStatus.OK);
    }

    /**
     * Get a specific category by it's uid
     * @param uid Of the category
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{categoryUID}")
    public ResponseEntity<Object> get(@PathVariable String categoryUID) {
        try {
            Object category = service.getCategory(categoryUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(category, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Save the new category and return it once created
     * @param category to create
     * @return  A responseEntity to define headers and body data
     */
    @PostMapping("/")
    public ResponseEntity<Object> add(@RequestBody Category category) {
        try {
            String rowChangeCount = service.saveCategory(category);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update a specific category with the given uid
     * @param category to update
     * @param uid of the category
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{categoryUID}")
    public ResponseEntity<Object> update(@RequestBody Category category, @PathVariable UUID categoryUID) {
        try {
            String rowChangeCount = service.updateCategory(category, categoryUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes a category with the given UID
     * @param categoryUID of the category to delete
     */
    @DeleteMapping("/{categoryUID}")
    public ResponseEntity<Object> delete(@PathVariable UUID categoryUID) {
        try {
            String rowChangeCount = service.deleteCategory(categoryUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Creates a new subcategory
     * @param subcategory to create
     * @param uid of the category
     * @return A responseEntity to define headers and body data
     */
    @PostMapping("/{categoryUID}/subcategory")
    public ResponseEntity<Object> addSubCategory(@RequestBody Subcategory subcategory, @PathVariable UUID categoryUID) {
        try {
            String rowChangeCount = service.saveSubCategory(categoryUID, subcategory);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Update a subcategory
     * @param subcategory to create
     * @param uid of the category
     * @param subcategoryUID of the subcategory
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{categoryUID}/subcategory/{subcategoryUID}")
    public ResponseEntity<Object> updateSubcategory(@RequestBody Subcategory subcategory, @PathVariable UUID categoryUID, @PathVariable UUID subcategoryUID) {
        try {
            String rowChangeCount = service.updateSubcategory(categoryUID, subcategory, subcategoryUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete a subcategory
     * @param categoryUID of the category
     * @param subcategoryUID of the subcategory
     */
    @DeleteMapping("/{categoryUID}/subcategory/{subcategoryUID}")
    public ResponseEntity<Object>  deleteSubcategory(@PathVariable UUID categoryUID, @PathVariable UUID subcategoryUID) {
        try {
            String rowChangeCount = service.deleteSubcategory(subcategoryUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
