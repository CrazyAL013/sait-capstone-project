package com.aquarium.store.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Model for Order data
 */
public class Order {

  private String id;
  private Account account;
  private Status status;
  private Address billingAddress;
  private Address deliveryAddress;
  private DiscountType discount;
  private PaymentProcessor paymentProcessor;
  private Shipment shipment;
  private String transactionId;
  private Date date;
  private double subTotal;
  private double beforeTaxTotal;
  private double shipTotal;
  private double taxTotal;
  private double discountTotal;
  private double grandTotal;
  private String paymentCardBrand;
  private int paymentCardLast4Digits;
  private int paymentCardExpiryMonth;
  private int paymentCardExpiryYear;
  private ArrayList<OrderItem> items;

  /**
   * Constructor for Order objects
   * @param id order id
   * @param account the account of the account
   * @param status the order status
   * @param billingAddress the order billing address
   * @param deliveryAddress the order delivery address
   * @param discount the discount on the order
   * @param paymentProcessor the payment processor used
   * @param shipment the shipment data for the order
   * @param transactionId the order transaction id
   * @param date the order data
   * @param subTotal the order subtotal
   * @param beforeTaxTotal the order total before tax
   * @param shipTotal the order shipping total
   * @param taxTotal the order tax total
   * @param discountTotal the order discount total
   * @param grandTotal the total amount for the order
   * @param paymentCardBrand the payment card brand
   * @param paymentCardLast4Digits last 4 digits of payment card
   * @param paymentCardExpiryMonth expiry month of payment card
   * @param paymentCardExpiryYear expiry year of payment card
   * @param items the order items
   */
  public Order(String id, Account account, Status status, Address billingAddress, Address deliveryAddress,
      DiscountType discount, PaymentProcessor paymentProcessor, Shipment shipment, 
      String transactionId, Date date, double subTotal, double beforeTaxTotal, double shipTotal, double taxTotal, 
      double discountTotal, double grandTotal, String paymentCardBrand, int paymentCardLast4Digits,
      int paymentCardExpiryMonth, int paymentCardExpiryYear, ArrayList<OrderItem> items) {
    this.id = id;
    this.account = account;
    this.status = status;
    this.billingAddress = billingAddress;
    this.deliveryAddress = deliveryAddress;
    this.discount = discount;
    this.paymentProcessor = paymentProcessor;
    this.shipment = shipment;
    this.transactionId = transactionId;
    this.date = date;
    this.subTotal = subTotal;
    this.beforeTaxTotal = beforeTaxTotal;
    this.shipTotal = shipTotal;
    this.taxTotal = taxTotal;
    this.discountTotal = discountTotal;
    this.grandTotal = grandTotal;
    this.paymentCardBrand = paymentCardBrand;
    this.paymentCardLast4Digits = paymentCardLast4Digits;
    this.paymentCardExpiryMonth = paymentCardExpiryMonth;
    this.paymentCardExpiryYear = paymentCardExpiryYear;
    this.items = items;
  }

  /**
   * ID getter method
   * @return the order id
   */
  public String getId() {
    return id;
  }

  /**
   * ID setter method
   * @param id new order id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Account getter method
   * @return the account that placed placed the order
   */
  public Account getAccount() {
    return account;
  }

  /**
   * Account setter method
   * @param account new account that placed the order
   */
  public void setAccount(Account account) {
    this.account = account;
  }

  /**
   * Billing address getter method
   * @return the order billing address
   */
  public Address getBillingAddress() {
    return billingAddress;
  }

  /**
   * Billing address setter method
   * @param billingAddress new order billing address
   */
  public void setBillingAddress(Address billingAddress) {
    this.billingAddress = billingAddress;
  }

  /**
   * Delivery address getter method
   * @return the order delivery address
   */
  public Address getDeliveryAddress() {
    return deliveryAddress;
  }

  /**
   * Delivery address setter method
   * @param deliveryAddress new order delivery address
   */
  public void setDeliveryAddress(Address deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }

  /**
   * Discount type getter method
   * @return the type of discount applied
   */
  public DiscountType getDiscount() {
    return discount;
  }

  /**
   * Discount type setter method
   * @param discount new discount type that was applied
   */
  public void setDiscount(DiscountType discount) {
    this.discount = discount;
  }

  /**
   * Shipment data getter method
   * @return the shipment data
   */
  public Shipment getShipment() {
    return shipment;
  }

  /**
   * Shipment setter method
   * @param shipment new shipment data
   */
  public void setShipment(Shipment shipment) {
    this.shipment = shipment;
  }

  /**
   * Transaction id getter method
   * @return the order transaction id
   */
  public String getTransactionId() {
    return transactionId;
  }

  /**
   * Transaction id setter method
   * @param transactionId new order transaction id
   */
  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  /**
   * Date getter method
   * @return order date
   */
  public Date getDate() {
    return date;
  }

  /**
   * Date setter method
   * @param date new order date
   */
  public void setDate(Date date) {
    this.date = date;
  }

  /**
   * Sub total getter method
   * @return the order sub total
   */
  public double getSubTotal() {
    return subTotal;
  }

  /**
   * Sub total setter method
   * @param subTotal new order sub total
   */
  public void setSubTotal(double subTotal) {
    this.subTotal = subTotal;
  }

  /**
   * Before tax total getter method
   * @return the total before tax
   */
  public double getBeforeTaxTotal() {
    return beforeTaxTotal;
  }

  /**
   * Before tax setter method
   * @param beforeTaxTotal new total before tax
   */
  public void setBeforeTaxTotal(double beforeTaxTotal) {
    this.beforeTaxTotal = beforeTaxTotal;
  }

  /**
   * Shipping total getter method
   * @return the shipping total
   */
  public double getShipTotal() {
    return shipTotal;
  }

  /**
   * Shipping total setter method
   * @param shipTotal new shipping total
   */
  public void setShipTotal(double shipTotal) {
    this.shipTotal = shipTotal;
  }

  /**
   * Tax total getter method
   * @return order tax total
   */
  public double getTaxTotal() {
    return taxTotal;
  }

  /**
   * Tax total setter method
   * @param taxTotal new order tax total
   */
  public void setTaxTotal(double taxTotal) {
    this.taxTotal = taxTotal;
  }

  /**
   * Discount total getter method
   * @return the total discount amount
   */
  public double getDiscountTotal() {
    return discountTotal;
  }

  /**
   * Discount total setter method
   * @param discountTotal new discount total amoutn 
   */
  public void setDiscountTotal(double discountTotal) {
    this.discountTotal = discountTotal;
  }

  /**
   * Grand total getter method
   * @return the order grand total
   */
  public double getGrandTotal() {
    return grandTotal;
  }

  /**
   * Grand total setter method
   * @param grandTotal new order grand total
   */
  public void setGrandTotal(double grandTotal) {
    this.grandTotal = grandTotal;
  }

  /**
   * Items getter method
   * @return the order items
   */
  public ArrayList<OrderItem> getItems() {
    return items;
  }

  /**
   * Items setter method
   * @param items new items for the order
   */
  public void setItems(ArrayList<OrderItem> items) {
    this.items = items;
  }

  /**
   * Status getter method
   * @return order status
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Status setter method
   * @param status new order status
   */
  public void setStatus(Status status) {
    this.status = status;
  }

  /**
   * Payment processor getter method
   * @return order payment processor
   */
  public PaymentProcessor getPaymentProcessor() {
    return paymentProcessor;
  }

  /**
   * Payment processor setter method
   * @param paymentProcessor new order payment processor
   */
  public void setPaymentProcessor(PaymentProcessor paymentProcessor) {
    this.paymentProcessor = paymentProcessor;
  }

  /**
   * Payment card brand getter method
   * @return the payment card brand
   */
  public String getPaymentCardBrand() {
    return this.paymentCardBrand;
  }

  /**
   * Payment card brand setter method
   * @param paymentCardBrand new payment card brand
   */
  public void setPaymentCardBrand(String paymentCardBrand) {
    this.paymentCardBrand = paymentCardBrand;
  }
  
  /**
   * Payment card last 4 digits getter method
   * @return the last 4 digits of payment card
   */
  public int getPaymentCardLast4Digits() {
    return this.paymentCardLast4Digits;
  }

  /**
   * Payment card last 4 digits setter method
   * @param paymentCardLast4Digits new last 4 digits of payment card
   */
  public void setPaymentCardLast4Digits(int paymentCardLast4Digits) {
    this.paymentCardLast4Digits = paymentCardLast4Digits;
  }

  /**
   * Payment card expiry month getter method
   * @return expiry month of payment card
   */
  public int getPaymentCardExpiryMonth() {
    return this.paymentCardExpiryMonth;
  }

  /**
   * Payment card expiry month setter method
   * @param paymentCardExpiryMonth new expiry month of payment card
   */
  public void setPaymentCardExpiryMonth(int paymentCardExpiryMonth) {
    this.paymentCardExpiryMonth = paymentCardExpiryMonth;
  }

  /**
   * Payment card expiry year getter mthod
   * @return expiry year of payment card
   */
  public int getPaymentCardExpiryYear() {
    return this.paymentCardExpiryYear;
  }

  /**
   * Payment card expiry year setter method
   * @param paymentCardExpiryYear new expiry year of payment card
   */
  public void setPaymentCardExpiryYear(int paymentCardExpiryYear) {
    this.paymentCardExpiryYear = paymentCardExpiryYear;
  }

}
