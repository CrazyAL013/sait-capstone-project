package com.aquarium.store.models;

/**
 * Model for subcategory data
 */
public class Subcategory {

	private String name;
	private int categoryID;
	private String id;

	/**
	 * Default constructor for Subcategory objets
	 */
	public Subcategory() {}

	/**
	 * Full constructor for Subcategory objects
	 * @param name subcategory name
	 * @param categoryID subcategory cateogry id
	 * @param id subcategory id
	 */
	public Subcategory(String name, int categoryID, String id) {
		this.name = name;
		this.categoryID = categoryID;
		this.id = id;
	}

	/**
	 * Name getter method
	 * @return subcategory
	 */
	public String getName() {
		return name;
	}

	/** 
	 * Name setter method 
	 * @param name new subcategory name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Category ID getter method
	 * @return category id for subcategory
	 */
	public int getCategoryID() {
		return categoryID;
	}

	/**
	 * Category ID setter method
	 * @param categoryID new category id for subcategory
	 */
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	/**
	 * ID getter method
	 * @return subcategory id
	 */
	public String getId() {
		return id;
	}

	/** 
	 * ID setter method
	 * @param id new subcategory id
	 */
	public void setId(String id) {
		this.id = id;
	}

}
