package com.aquarium.store.models;

import java.util.Date;
import java.util.UUID;

/**
 * Model for Review data
 */
public class Review {

    private UUID id;
    private Account account;
    private Product product;
    private ProductItem productItem;
    private String title;
    private String description;
    private int rating;
    private Date date;
    private boolean verifiedPurchase;

    /** 
     * Constructor for Review objects
     * @param id review id
     * @param account account that placed reviewed
     * @param product product that received review
     * @param productItem product item that recevied review
     * @param title title of review
     * @param description description of review
     * @param rating review rating
     * @param date review date
     * @param verifiedPurchase if the review is from a real purchase
     */
    public Review(UUID id, Account account, Product product, ProductItem productItem, 
                  String title, String description, int rating, Date date, boolean verifiedPurchase) {
        this.id = id;
        this.account = account;
        this.product = product;
        this.productItem = productItem;
        this.title = title;
        this.description = description;
        this.rating = rating;
        this.date = date;
        this.verifiedPurchase = verifiedPurchase;
    }

    /**
     * ID getter method
     * @return review id
     */
    public UUID getId() {
        return this.id;
    }

    /**
     * ID setter method
     * @param id new review id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * Account getter method
     * @return account that posted review
     */
    public Account getAccount() {
        return this.account;
    }

    /**
     * Account setter method
     * @param account new account for review
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Product getter method
     * @return product that received review
     */
    public Product getProduct() {
        return this.product;
    }

    /**
     * Product setter method
     * @param product new product for review
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * ProductItem getter mehod
     * @return productitem that reveived review
     */
    public ProductItem getProductItem() {
        return this.productItem;
    }

    /**
     * ProductItem setter method
     * @param productItem new productitem for review
     */
    public void setProductItem(ProductItem productItem) {
        this.productItem = productItem;
    }

    /**
     * Title getter method 
     * @return review title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Title setter method 
     * @param title new title for review
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Description getter method
     * @return review description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Description setter method
     * @param description new description for review
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Rating getter method
     * @return review rating
     */
    public int getRating() {
        return this.rating;
    }

    /**
     * Rating setter method
     * @param rating new review rating
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     * Date getter method
     * @return review date
     */
    public Date getDate() {
        return this.date;
    }

    /**
     * Date setter method
     * @param date new date of reivew
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Verified Purchase getter method
     * @return if the review was from a real purchase
     */
    public boolean isVerifiedPurchase() {
        return this.verifiedPurchase;
    }

    /**
     * Verfied Purchase getter method
     * @return verified purchase boolean
     */
    public boolean getVerifiedPurchase() {
        return this.verifiedPurchase;
    }

    /**
     * Verified Purchase setter method
     * @param verifiedPurchase change if it is a verified purchase
     */
    public void setVerifiedPurchase(boolean verifiedPurchase) {
        this.verifiedPurchase = verifiedPurchase;
    }
}
