package com.aquarium.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.aquarium.store.models.Address;
import com.aquarium.utilities.CallStatementUnwrapper;

/**
 * Service for Address
 * Purpose is performing CRUD on Address data
 */
@Service
public class AddressService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Finds an address based on UID
     * @param uid the UID to search by
     * @return the address found
     */
    public Object getAddressByAddressUID(String uid) {
        String sql = "{CALL AddressGetById(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Insert new address into database
     * @param address Address object to be insert
     * @return the number of rows inserted into database
     */
    public String saveAddress(Address address) {
        String sql = "{CALL AddressCreate(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.INTEGER));
        paramList.add(new SqlParameter(Types.INTEGER));
        paramList.add(new SqlOutParameter("result", Types.INTEGER));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, address.getId().toString());
            callableStatement.setString(2, address.getAccountId().toString());
            callableStatement.setString(3, address.getDescription());
            callableStatement.setString(4, address.getLine1());
            callableStatement.setString(5, address.getLine2());
            callableStatement.setString(6, address.getCity());
            callableStatement.setString(7, address.getPostalZip());
            callableStatement.setString(8, String.valueOf(address.getRegion().getId()));
            callableStatement.setString(9, String.valueOf(address.getCountry().getId()));
            callableStatement.registerOutParameter(10, Types.INTEGER);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Update address data in database
     * @param address the new address data
     * @param uid the UID of address to be updated
     * @return the number of rows affected by update
     */
    public String updateAddress(Address address, UUID uid) {
        String sql = "{CALL AddressUpdate(?, ?, ?, ?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.INTEGER));
        paramList.add(new SqlParameter(Types.INTEGER));

        Map<String, Object> out =  jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid.toString());
            callableStatement.setString(2, address.getDescription());
            callableStatement.setString(3, address.getLine1());
            callableStatement.setString(4, address.getLine2());
            callableStatement.setString(5, address.getCity());
            callableStatement.setString(6, address.getPostalZip());
            callableStatement.setString(7, String.valueOf(address.getRegion().getId()));
            callableStatement.setString(8, String.valueOf(address.getCountry().getId()));
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Deletes an address from database 
     * @param uid UID of address to delete
     * @return number of address' deleted
     */
    public String deleteAddress(UUID uid) {
        String sql = "{CALL AddressDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid.toString());
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }
}
