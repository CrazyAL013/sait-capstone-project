package com.aquarium.store.controllers;

import java.util.NoSuchElementException;

import com.aquarium.store.services.CartService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/cart")
public class CartController {
    
    @Autowired
    CartService service;

    /**
     * Get product items from a item list
     * @param productItemList The list to get the items from
     * @return A responseEntity to define headers and body data
     */
    @GetMapping("/{productItemList}")
    public ResponseEntity<Object> getProductItems(@PathVariable String productItemList) {
        try {
            Object category = service.getCartItems(productItemList);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");

            return new ResponseEntity<>(category, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update a cart
     * @param accountUID Account of the cart
     * @param cartJSON Cart data
     * @return A responseEntity to define headers and body data
     */
    @PutMapping("/{accountUID}")
    public ResponseEntity<Object> updateCart(@PathVariable String accountUID, @RequestBody String cartJSON) {
        try {
            String rowChangeCount = service.updateCart(accountUID, cartJSON);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete a cart
     * @param accountUID of the cart to delete
     * @return A responseEntity to define headers and body data
     */
    @DeleteMapping("/{accountUID}")
    public ResponseEntity<Object> deleteCart(@PathVariable String accountUID) {
        try {
            String rowChangeCount = service.deleteCart(accountUID);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "application/json; charset=UTF-8");
            responseHeaders.set("Update-Count", rowChangeCount);
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
