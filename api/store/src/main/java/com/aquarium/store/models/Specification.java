package com.aquarium.store.models;

/**
 * Model for Specification data
 */
public class Specification {
    private String name;
    private String value;

    /**
     * Constructor for Specification object
     * @param name specification name
     * @param value specification value
     */
    public Specification(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Name getter method
     * @return specification name
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter method
     * @param name new specification name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Value getter method
     * @return specification value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Value setter method
     * @param value new specification value
     */
    public void setValue(String value) {
        this.value = value;
    }

    

}
