package com.aquarium.store.models;

/**
 * Class for tracking money amounts 
 */
public class AmountMoney {
    Long amount;
    String currency;

    /**
     * Constructor for AmountMoney
     * @param amount the amount being tracked
     * @param currency the type of currency used
     */
    public AmountMoney(Long amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    /**
     * Amount getter method
     * @return the amount
     */
    public Long getAmount() {
        return amount;
    }

    /**
     * Amount setter method
     * @param amount new value for amount
     */
    public void setAmount(Long amount) {
        this.amount = amount;
    }

    /**
     * Currency getter method
     * @return the currency being used
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Currency setter method
     * @param currency the new currency being used
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}