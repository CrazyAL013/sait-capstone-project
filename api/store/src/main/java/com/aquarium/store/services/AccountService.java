package com.aquarium.store.services;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aquarium.authentication.JwtTokenUtil;
import com.aquarium.store.models.Account;
import com.aquarium.store.models.ChangePassword;
import com.aquarium.store.models.JwtRequest;
import com.aquarium.utilities.CallStatementUnwrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

/**
 * Service for Account
 */
@Service
@Transactional
public class AccountService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     * Goes into databsae and check for a matching user to log into application
     * 
     * @param jwtRequest contains username and password
     * @return account and refresh token in array
     * @throws UsernameNotFoundException
     */
    public Object[] login(JwtRequest jwtRequest) throws UsernameNotFoundException {
        String username = jwtRequest.getUsername();
        String password = jwtRequest.getPassword();

        String sql = "{CALL AccountLogin(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, username);
            return callableStatement;
        }, paramList);

        Map<String, Object> rs = unwrapper.extractFullResultSet(out); // IndexOutOfBoundsException will be thrown if
                                                                      // username not found

        if (rs == null) {
            throw new UsernameNotFoundException("Invalid Credentials");
        }

        String hash = (String) rs.get("PasswordHash");
        String accountUID = (String) rs.get("BIN_TO_UUID(Account.UID)");

        // If there is no result set then there is no valid user account for the given
        // credentials
        if (!unwrapper.hasResultSet(out)) {
            throw new UsernameNotFoundException("Invalid Credentials");
        }

        // Verify the hash
        Argon2 argon2 = Argon2Factory.create();
        Boolean isCorrect = argon2.verify(hash, password.toCharArray());

        if (!isCorrect) {
            // Wrong password
            throw new UsernameNotFoundException("Invalid Credentials");
        }

        UserDetails userDetails = new User(accountUID, "", new ArrayList<>());

        String accessToken = jwtTokenUtil.generateToken(userDetails);
        String refreshToken = generateRefreshToken(accountUID);

        addRefreshToken(accountUID, refreshToken);

        Object accountJSON = getAccount(accountUID);
        accountJSON = insertAccessToken(accessToken, accountJSON);

        // Since we need to return 2 things here return them inside an array
        Object ar[] = new Object[2];
        ar[0] = accountJSON;
        ar[1] = refreshToken;

        return ar;
    }

    /**
     * Generates refresh tokens
     * @param accountUID UUID of account to generate for
     * @return the generated refresh tokens
     */
    public String generateRefreshToken(String accountUID) {
        Map<String, Object> claims = new HashMap<>();
        final String refreshToken = jwtTokenUtil.doGenerateRefreshToken(claims, accountUID);

        return refreshToken;
    }

    /**
     * Verifies an access token
     * 
     * @param authToken token to verify
     * @return if the token is valid 
     */
    public Boolean verifyAccessToken(String authToken) {
        try {
            return jwtTokenUtil.validateToken(authToken);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Generates new tokens
     * 
     * @param refreshToken current refresh tokens
     * @return account and new refresh tokens in array
     * @throws UsernameNotFoundException
     */
    public Object[] generateNewTokens(String refreshToken) throws UsernameNotFoundException {
        String accountUID = jwtTokenUtil.getUserIDFromToken(refreshToken);
        Boolean validRefreshToken = verifyRefreshToken(accountUID, refreshToken);
        Boolean isExpired = jwtTokenUtil.isTokenExpired(refreshToken);

        // Check that the refresh token is valid
        if (!validRefreshToken || isExpired) {
            throw new UsernameNotFoundException("Invalid Refresh Token");
        }

        // Create new tokens
        UserDetails userDetails = new User(accountUID, "", new ArrayList<>());
        String newAccessToken = jwtTokenUtil.generateToken(userDetails);
        String newRefreshToken = generateRefreshToken(accountUID);

        // Replace the old refresh token with the new one
        updateRefreshToken(accountUID, refreshToken, newRefreshToken);

        // Get the account object to return
        Object accountJSON = getAccount(accountUID);
        accountJSON = insertAccessToken(newAccessToken, accountJSON);

        // Since we need to return 2 things here return them inside an array
        Object ar[] = new Object[2];
        ar[0] = accountJSON;
        ar[1] = newRefreshToken;

        return ar;
    }

    /**
     * Registers a new account
     * @param account account data being registered
     * @return account and refresh tokens in array
     * @throws UsernameNotFoundException
     */
    public Object[] register(Account account) throws UsernameNotFoundException {
        String accountUID = account.getId();

        // Hash the password
        Argon2 argon2 = Argon2Factory.create();
        char[] password = account.getPassword().toCharArray();
        String hash = argon2.hash(4, 1024 * 1024, 8, password);

        // Store the new account in the database
        String sql = "{CALL AccountCreate(?, ?, ?, ?, ?, ?)}";
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR)); // _AccountUID
        paramList.add(new SqlParameter(Types.VARCHAR)); // _UserName
        paramList.add(new SqlParameter(Types.VARCHAR)); // _FullName
        paramList.add(new SqlParameter(Types.VARCHAR)); // _PreferredName
        paramList.add(new SqlParameter(Types.VARCHAR)); // _EMail
        paramList.add(new SqlParameter(Types.VARCHAR)); // _PasswordHash

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID); // _AccountUID
            callableStatement.setString(2, account.getUsername()); // _UserName
            callableStatement.setString(3, account.getFullName()); // _FullName
            callableStatement.setString(4, account.getPreferredName()); // _PreferredName
            callableStatement.setString(5, account.getEmail()); // _EMail
            callableStatement.setString(6, hash); // _PasswordHash
            return callableStatement;
        }, paramList);

        // Create the tokens
        final UserDetails userDetails = new User(accountUID, hash, new ArrayList<>());
        Map<String, Object> claims = new HashMap<>();

        final String accessToken = jwtTokenUtil.generateToken(userDetails);
        final String refreshToken = jwtTokenUtil.doGenerateRefreshToken(claims, accountUID);

        // Get the account object to return
        Object accountJSON = getAccount(accountUID);
        accountJSON = insertAccessToken(accessToken, accountJSON);

        Object ar[] = new Object[2];
        ar[0] = accountJSON;
        ar[1] = refreshToken;

        return ar;
    }

    /**
     * Inserts access tokens for account
     * 
     * @param token current tokens 
     * @param accountJSON account in JSON
     * @return string with tokens
     */
    private String insertAccessToken(String token, Object accountJSON) {
        System.out.println(accountJSON);
        String accountString = accountJSON.toString();

        accountString = accountString.replace("\"accessToken\": \"\"", "\"accessToken\": \"" + token + "\"");

        return accountString;
    }

    /**
     * Changes an account password
     * @param changePassword new password
     */
    public void changePassword(ChangePassword changePassword) {
        String username = changePassword.getUsername();
        String password = changePassword.getPassword();
        String newPassword = changePassword.getNewPassword();

        // Verify that the username and password are correct
        String sql = "{CALL AccountLogin(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, username);
            return callableStatement;
        }, paramList);

        Map<String, Object> rs = unwrapper.extractFullResultSet(out); // IndexOutOfBoundsException will be thrown if
                                                                      // username not found

        if (rs == null) {
            throw new UsernameNotFoundException("Invalid Credentials");
        }

        String hash = (String) rs.get("PasswordHash");
        String accountUID = (String) rs.get("BIN_TO_UUID(Account.UID)");

        // If there is no result set then there is no valid user account for the given
        // credentials
        if (!unwrapper.hasResultSet(out)) {
            throw new UsernameNotFoundException("Invalid Credentials");
        }

        // Verify the hash
        Argon2 argon2 = Argon2Factory.create();
        Boolean isCorrect = argon2.verify(hash, password.toCharArray());

        if (!isCorrect) {
            // Wrong password
            throw new UsernameNotFoundException("Invalid Credentials");
        }

        // Hash the password
        String newHash = argon2.hash(4, 1024 * 1024, 8, newPassword.toCharArray());

        // Store the new account in the database
        String changePasswordSQL = "{CALL AccountPasswordChange(?, ?)}";
        List<SqlParameter> changePasswordParamList = new ArrayList<>();

        changePasswordParamList.add(new SqlParameter(Types.VARCHAR)); // _AccountUID
        changePasswordParamList.add(new SqlParameter(Types.VARCHAR)); // _PasswordHash

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(changePasswordSQL);
            callableStatement.setString(1, accountUID); // _AccountUID
            callableStatement.setString(2, newHash); // _PasswordHash
            return callableStatement;
        }, paramList);
    }
    
    /**
     * Get the account by the given UID
     * 
     * @param accountUID of the account to get
     * @return found account
     */
    public Object getAccount(String accountUID) throws IndexOutOfBoundsException {
        String sql = "{CALL AccountGetById(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Update account
     * @param account updated account data
     * @return updated account
     */
    public String updateAccount(Account account) {
        String sql = "{CALL AccountUpdate(?, ?, ?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, account.getId());
            callableStatement.setString(2, account.getFullName());
            callableStatement.setString(3, account.getPreferredName());
            callableStatement.setString(4, account.getEmail());
            callableStatement.setString(5, account.getPhoneNumber());
            return callableStatement;
        }, paramList);

        // Update username in user table
        updateAccountUserName(account);

        // Update the primary address
        if (account.getAddress() != null) {
            updatePrimaryAddress(account);
        }

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Update account username
     * @param account new account data with changed username
     */
    private void updateAccountUserName(Account account) {
        String sql = "{CALL AccountUserNameChange(?, ?)}";
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, account.getId());
            callableStatement.setString(2, account.getUsername());
            return callableStatement;
        }, paramList);

    }

    /**
     * Update account primary address
     * @param account new account data with updated primary address
     */
    private void updatePrimaryAddress(Account account) {
        String sql = "{CALL AddressUpdatePrimary(?, ?)}";
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, account.getId());
            callableStatement.setString(2, account.getAddress().getId().toString());
            return callableStatement;
        }, paramList);

    }

    /**
     * Deletes an account 
     * @param uid UUID of account to delete
     * @return deleted account
     */
    public String deleteAccount(String uid) {
        String sql = "{CALL AccountDelete(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Will get all the address' for a given account
     * 
     * @param uid uuid of account to get address for
     * @return list of addresses
     */
    public Object getAllAccountAddress(String uid) {
        String sql = "{CALL AddressGetAll(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Will get all the primary address for a given account
     * 
     * @param uid UUID Of account to get primary address for
     * @return the account primary address
     */
    public Object getAccountAddressPrimary(String uid) {
        String sql = "{CALL AddressGetPrimary(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, uid);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Update primary address of account
     * 
     * @param accountUID UUID of account to update
     * @param addressUID UUID of address to use
     * @return updated account
     */
    public String updateAccountAddressPrimary(String accountUID, String addressUID) {
        String sql = "{CALL AddressUpdatePrimary(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            callableStatement.setString(2, addressUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Add refresh token to account
     * 
     * @param accountUID UUID of account to add to
     * @param token new refresh token
     * @return updated account
     */
    public String addRefreshToken(String accountUID, String token) {
        String sql = "{CALL RefreshTokenCreate(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            callableStatement.setString(2, token);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Verify account refresh token
     * @param accountUID UUID of account to verify
     * @param token token to verify
     * @return if the refresh token is valid
     */
    public Boolean verifyRefreshToken(String accountUID, String token) {
        String sql = "{CALL RefreshTokenValidate(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            callableStatement.setString(2, token);
            return callableStatement;
        }, paramList);

        return unwrapper.hasValidToken(out);
    }
    
    /**
     * Delete account refresh token
     * @param accountUID UUID of account to delete from
     * @param token token to delete
     */
    public void deleteRefreshToken(String accountUID, String token) {
        String sql = "{CALL RefreshTokenDelete(?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            callableStatement.setString(2, token);
            return callableStatement;
        }, paramList);

    }

    /**
     * Update account refresh token
     * @param accountUID UUID of account to update
     * @param oldToken value of old token
     * @param newToken new token value
     * @return updated account
     */
    public String updateRefreshToken(String accountUID, String oldToken, String newToken) {
        String sql = "{CALL RefreshTokenUpdate(?, ?, ?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));
        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            callableStatement.setString(2, oldToken);
            callableStatement.setString(3, newToken);
            return callableStatement;
        }, paramList);

        return unwrapper.extractUpdateCount(out);
    }

    /**
     * Get all orders for account
     * @param accountUID UUID of account to search for
     * @return the found orders
     * @throws IndexOutOfBoundsException if no account has matching UUID
     */
    public Object getAccountOrders(String accountUID) throws IndexOutOfBoundsException {
        String sql = "{CALL AccountOrderGetAll(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }

    /**
     * Get all reviews from account
     * @param accountUID UUID of account to search
     * @return all found reviews
     * @throws IndexOutOfBoundsException if no account has matching UUID
     */
    public Object getAccountReviews(String accountUID) throws IndexOutOfBoundsException {
        String sql = "{CALL AccountReviewGetAll(?)}";
        CallStatementUnwrapper unwrapper = new CallStatementUnwrapper();
        List<SqlParameter> paramList = new ArrayList<>();

        paramList.add(new SqlParameter(Types.VARCHAR));

        Map<String, Object> out = jdbcTemplate.call(connection -> {
            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, accountUID);
            return callableStatement;
        }, paramList);

        return unwrapper.extractResultSet(out);
    }
}