package com.aquarium.store.models;

/**
 * Model for Country Data
 */
public class Country {

    private int id;
    private String name;
    private String code;
    private String currencyCode;
    private String currencySymbol;
    private double tax;

    /**
     * Default constructor for Country objects
     */
    public Country(){}

    /**
     * Full constructor for Country objects
     * @param id the country id
     * @param name the name of the country
     * @param code the code of the country
     * @param currencyCode the currency code for the country
     * @param currencySymbol the currency symbol for the country
     * @param tax the tax value for the country
     */
    public Country(int id, String name, String code, String currencyCode, String currencySymbol, double tax) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.currencyCode = currencyCode;
        this.currencySymbol = currencySymbol;
        this.tax = tax;
    }

    /**
     * Id setter method
     * @param id new value for country id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Id getter method
     * @return the country id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Name setter method
     * @param name new name for the country
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Name getter method
     * @return the name of the country
     */
    public String getName() {
        return this.name;
    }

    /**
     * Code setter method
     * @param code new code for the country
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Code getter method
     * @return the country code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Currency code setter method
     * @param currencyCode the new currency code for the country
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Currency code getter method
     * @return the country currency code
     */
    public String getCurrencyCode() {
        return this.currencyCode;
    }

    /**
     * Currency symbol setter method
     * @param currencySymbol the new currency symbol for the country
     */
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    /**
     * Currency symbol getter method
     * @return the country currency symbol
     */
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    /**
     * Tax setter method
     * @param tax the new tax value for the country
     */
    public void setTax(double tax) {
        this.tax = tax;
    }

    /**
     * Tax getter method
     * @return the tax value for the country
     */
    public double getTax() {
        return this.tax;
    }
}
