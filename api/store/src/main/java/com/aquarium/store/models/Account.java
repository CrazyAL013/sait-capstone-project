package com.aquarium.store.models;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Model for Account data
 */
public class Account {

    private String id;
    private String username;
    private String idToken;
    private String fullName;
    private String preferredName;
    private String email;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSSSSS")
    private Date joinDate;
    
    private String phoneNumber;
    private Role[] role;
    private Address address;
    private String password;


    /**
     * Default constructor for Account objects
     */
    public Account() {}

    /**
     * Full contructor for an Account objects
     * 
     * @param id the id of the account
     * @param username the account username
     * @param idToken the account id token
     * @param fullName the user for this accounts full name
     * @param preferredName the user for this accoutns preferred name 
     * @param email he account email
     * @param joinDate the date they joined the service
     * @param phoneNumber the account phone number
     * @param role the account role
     * @param address the account address
     * @param password the account password
     */
    public Account(String id, String username, String idToken, String fullName, String preferredName, String email,
            Date joinDate, String phoneNumber, Role[] role, Address address, String password) {
        this.id = id;
        this.username = username;
        this.idToken = idToken;
        this.fullName = fullName;
        this.preferredName = preferredName;
        this.email = email;
        this.joinDate = joinDate;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.address = address;
        this.password = password;
    }

    /**
     * ID setter method
     * @param id the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * ID getter method
     * @return the account id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Full name setter
     * @param fullName the new full name
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * Full name getter method
     * @return the full name for this account
     */
    public String getFullName() {
        return this.fullName;
    }

    /**
     * Preferred name setter method 
     * @param preferredName the new value for preferred name
     */
    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    /**
     * Preferred name getter method
     * @return the preferred name for this account
     */
    public String getPreferredName() {
        return this.preferredName;
    }

    /**
     * Email setter method
     * @param email the new email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Email getter method
     * @return the email for this account
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Join date setter method
     * @param joinDate the new date for join date
     */
    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    /**
     * Join date getter method
     * @return the date they joined the service
     */
    public Date getJoinDate() {
        return this.joinDate;
    }

    /**
     * Phone number getter method
     * @param phoneNumber the phone number for this account
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Phone number getter method
     * @return the phone number for this account
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    
    /**
     * Username getter method
     * @return the username for this account
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username setter method
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Id token getter method
     * @return the ID Token for this account
     */
    public String getIdToken() {
        return idToken;
    }

    /**
     * Id token setter method
     * @param idToken the new id token
     */
    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    /**
     * Role getter method
     * @return list of roles for this account
     */
    public Role[] getRole() {
        return role;
    }

    /**
     * Role setter method
     * @param role the new list of roles for this account
     */
    public void setRole(Role[] role) {
        this.role = role;
    }

    /**
     * Address getter method
     * @return the address for this account
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Address setter method
     * @param address the new address for this account
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Password getter method
     * @return the password for this account
     */
    public String getPassword() {
        return password;
    }

    /**
     * Password setter method
     * @param password the new password for this account
     */
    public void setPassword(String password) {
        this.password = password;
    }

}