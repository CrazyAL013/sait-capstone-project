# Development Environemnt
storefront/create
back/create
load test data
stored procs

## Build Instructions
You will need the following commands available to build this project:
* mvn (maven)
* npm
* tmux
* mysql (mysql-client)

You need either a local mysql server, or the authentication to use a remote one. Setup information in the build.sh file.
