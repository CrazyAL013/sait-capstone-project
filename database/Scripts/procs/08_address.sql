#-------------------------------------------------------------------------------
/*
* All operations related to customer (storefront) addresses.
*
* Content:	AddressGetById
* 			AddressCreate
* 			AddressUpdate
* 			AddressDelete
*/
#-------------------------------------------------------------------------------
/*
* Retrieves an Address
*
* @param {uuid} AddressUID - The UUID for the Address to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.AddressGetById;
DELIMITER //
CREATE PROCEDURE store.AddressGetById(
	IN _AddressUID varchar(36)
)
BEGIN
	SELECT 
		GetAddressJSON(Address.ID)
	FROM
		Address
	WHERE
		UID = UUID_TO_BIN(_AddressUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Address
*
* @param {uuid} AddressUID - The UUID of the Address
* @param {uuid} AccountUID - The UUID of the Account
* @param {string} Description - A string containing the address description
* @param {string} Line1 - A string containing line 1 of the address
* @param {string} Line2 - A string containing line 2 of the address
* @param {string} City - A string containing the city
* @param {string} PostalZip - A string containing the postal code/zip
* @param {integer} RegionID - A int containing ID of the region
* @param {integer} CountryID - A int containing ID of the country
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.AddressCreate;
DELIMITER //
CREATE PROCEDURE store.AddressCreate(
	IN _AddressUID varchar(36),
	IN _AccountUID varchar(36),
	IN _Description varchar(50),
	IN _Line1 varchar(100),
	IN _Line2 varchar(100),
	IN _City varchar(100),
	IN _PostalZip char(15),
	IN _RegionID int,
	IN _CountryID int,
	OUT _ID int
	)
BEGIN
	
	INSERT INTO Address(
		UID,
		AccountID,
		Description,
		Line1,
		Line2,
		City,
		PostalZip,
		RegionID,
		CountryID
	)
	SELECT
		UUID_TO_BIN(_AddressUID),
		Account.ID,
		_Description,
		_Line1,
		_Line2,
		_City,
		_PostalZip,
		_RegionID,
		_CountryID
	FROM
		Account
	WHERE
		Account.UID = UUID_TO_BIN(_AccountUID);

	SET _ID = LAST_INSERT_ID();

	IF (SELECT ISNULL(Account.AddressID) FROM Account WHERE Account.UID = UUID_TO_BIN(_AccountUID)) THEN
		UPDATE Account SET AddressID = _ID WHERE Account.UID = UUID_TO_BIN(_AccountUID);
	END IF;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Address
*
* @param {uuid} AddressUID - The UUID of the Address
* @param {string} Description - A string containing the address description
* @param {string} Line1 - A string containing line 1 of the address
* @param {string} Line2 - A string containing line 2 of the address
* @param {string} City - A string containing the city
* @param {string} PostalZip - A string containing the postal code/zip
* @param {integer} RegionID - A string containing (integer) ID of the region
* @param {integer} CountryID - A string containing (integer) ID of the country
* @param {boolean} Primary - A boolean flag indicating a primary address
*/
DROP PROCEDURE IF EXISTS store.AddressUpdate;
DELIMITER //
CREATE PROCEDURE store.AddressUpdate(
	IN _AddressUID varchar(36),
	IN _Description varchar(50),
	IN _Line1 varchar(100),
	IN _Line2 varchar(100),
	IN _City varchar(100),
	IN _PostalZip char(15),
	IN _RegionID int,
	IN _CountryID int
)
BEGIN

	UPDATE Address
	SET
		Description = _Description,
		Line1 = _Line1,
		Line2 = _Line2,
		City = _City,
		PostalZip = _PostalZip,
		RegionID = _RegionID,
		CountryID = _CountryID
	WHERE
		UID = UUID_TO_BIN(_AddressUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes an Address
*
* @param {uuid} AddressUID - The UUID for the Address to be deleted
*/
DROP PROCEDURE IF EXISTS store.AddressDelete;
DELIMITER //
CREATE PROCEDURE store.AddressDelete(
	IN _AddressUID varchar(36)
)
BEGIN
	DELETE
	FROM
		Address
	WHERE
		UID = UUID_TO_BIN(_AddressUID);
END //
DELIMITER ;
