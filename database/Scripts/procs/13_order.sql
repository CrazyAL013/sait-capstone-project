#-------------------------------------------------------------------------------
/*
* All operations related to customer orders.
* 
* Content:	OrderGetAll
* 					OrderGetById
* 					OrderCreate
* 					OrderUpdate
* 					OrderDelete
* 					OrderItemCreate
* 					OrderItemUpdate
* 					OrderShipmentCreate
*           OrderShipmentUpdate
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Orders
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.OrderGetAll;
DELIMITER //
CREATE PROCEDURE store.OrderGetAll()
BEGIN
	
	SELECT JSON_ARRAYAGG(
		GetOrderJSON(`Order`.ID)
	)
	FROM 
		`Order`
	ORDER BY
		`Date`, OrderStatusID;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------

/*
* Retrieves an Order
*
* @param {uuid} OrderUID - The UUID for the Order to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.OrderGetById;
DELIMITER //
CREATE PROCEDURE store.OrderGetById(
	IN _OrderUID varchar(36)
)
BEGIN
	SELECT 
		GetOrderJSON(`Order`.ID)
	FROM 
		`Order`
	WHERE 
		`Order`.UID = UUID_TO_BIN(_OrderUID);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Order
*
* @param {uuid} OrderUID - The UUID of the Order
* @param {uuid} AccountUID - the UUID of the account to which the order belongs
* @param {uuid} BillingAddressUID - the UUID of the billing address
* @param {uuid} DeliveryAddressUID - the UUID of the delivery address
* @param {uuid} DiscountUID - the UUID of the discount
* @param {uuid} ShipmentUID - the UUID of the shipment
* @param {string} PaymentProcessorID - the UID of the payment processor
* @param {string} TransactionID - the transaction/payment ID (from the processor)
* @param {float} SubTotal - the sub total
* @param {float} BeforeTaxTotal - the before tax total
* @param {float} ShipTotal - the shipping total
* @param {float} TaxTotal - the tax total
* @param {float} DiscountTotal - the discount total
* @param {float} GrandTotal - the grand total
* @param {string} PaymentCardBrand - the brand of the payment card
* @return {integer} PaymentCardLast4Digits - The last 4 digits of the payment card
* @return {integer} PaymentCardExpiryMonth - The expiry month of the payment card
* @return {integer} PaymentCardExpiryYear - The expiry year of the payment card
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.OrderCreate;
DELIMITER //
CREATE PROCEDURE store.OrderCreate(
	IN _OrderUID varchar(36),
	IN _AccountUID varchar(36),
	IN _BillingAddressUID varchar(36),
	IN _DeliveryAddressUID varchar(36),
	IN _DiscountUID varchar(36),
	IN _ShipmentUID varchar(36),
	IN _PaymentProcessorID varchar(36),
	IN _TransactionID varchar(500),
	IN _SubTotal decimal(15,2),
	IN _BeforeTaxTotal decimal(15,2),
	IN _ShipTotal decimal(15,2),
	IN _TaxTotal decimal(15,2),
	IN _DiscountTotal decimal(15,2),
	IN _GrandTotal decimal(15,2),
	IN _PaymentCardBrand varchar(50),
	IN _PaymentCardLast4Digits int,
	IN _PaymentCardExpiryMonth int,
	IN _PaymentCardExpiryYear int,
	OUT _ID int
)
BEGIN
	
	INSERT INTO `Order`(
		UID,
		AccountID,
		BillingAddressID,
		DeliveryAddressID,
		DiscountID,
		ShipmentID,
		PaymentProcessorID,
		TransactionID,
		SubTotal,
		BeforeTaxTotal,
		ShipTotal,
		TaxTotal,
		DiscountTotal,
		GrandTotal,
		PaymentCardBrand,
		PaymentCardLast4Digits,
		PaymentCardExpiryMonth,
		PaymentCardExpiryYear
	)
	SELECT
		UUID_TO_BIN(_OrderUID),
		ID_FROM_UID(_AccountUID, 'Account'),
		ID_FROM_UID(_BillingAddressUID, 'Address'),
		ID_FROM_UID(_DeliveryAddressUID, 'Address'),
		ID_FROM_UID(_DiscountUID, 'Discount'),
		ID_FROM_UID(_ShipmentUID, 'Shipment'),
		ID_FROM_UID(_PaymentProcessorID, 'PaymentProcessor'),
		_TransactionID,
		_SubTotal,
		_BeforeTaxTotal,
		_ShipTotal,
		_TaxTotal,
		_DiscountTotal,
		_GrandTotal,
		_PaymentCardBrand,
		_PaymentCardLast4Digits,
		_PaymentCardExpiryMonth,
		_PaymentCardExpiryYear;

	SET _ID = LAST_INSERT_ID();

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Order
*
* @param {uuid} OrderUID - The UUID of the Order
* @param {uuid} AccountUID - the UUID of the account to which the order belongs
* @param {uuid} OrderStatusUID - the UUID of the order status
* @param {uuid} BillingAddressUID - the UUID of the billing address
* @param {uuid} DeliveryAddressUID - the UUID of the delivery address
* @param {uuid} DiscountUID - the UUID of the discount
* @param {uuid} ShipmentUID - the UUID of the shipment
* @param {string} PaymentProcessor - the UUID of the payment processor
* @param {string} TransactionID - the transaction/payment ID (from the processor)
* @param {float} SubTotal - the sub total
* @param {float} BeforeTaxTotal - the before tax total
* @param {float} ShipTotal - the shipping total
* @param {float} TaxTotal - the tax total
* @param {float} DiscountTotal - the discount total
* @param {float} GrandTotal - the grand total
* @param {string} PaymentCardBrand - the brand of the payment card
* @return {integer} PaymentCardLast4Digits - The last 4 digits of the payment card
* @return {integer} PaymentCardExpiryMonth - The expiry month of the payment card
* @return {integer} PaymentCardExpiryYear - The expiry year of the payment card
*/
DROP PROCEDURE IF EXISTS store.OrderUpdate;
DELIMITER //
CREATE PROCEDURE store.OrderUpdate(
	IN _OrderUID varchar(36),
	IN _AccountUID varchar(36),
	IN _OrderStatusUID varchar(36),
	IN _BillingAddressUID varchar(36),
	IN _DeliveryAddressUID varchar(36),
	IN _DiscountUID varchar(36),
	IN _ShipmentUID varchar(36),
	IN _PaymentProcessor varchar(36),
	IN _TransactionID varchar(500),
	IN _SubTotal decimal(15,2),
	IN _BeforeTaxTotal decimal(15,2),	
	IN _ShipTotal decimal(15,2),
	IN _TaxTotal decimal(15,2),
	IN _DiscountTotal decimal(15,2),
	IN _GrandTotal decimal(15,2),
	IN _PaymentCardBrand varchar(50),
	IN _PaymentCardLast4Digits int,
	IN _PaymentCardExpiryMonth int,
	IN _PaymentCardExpiryYear int
)
BEGIN
		
	UPDATE 
		`Order`
  SET
		AccountID =	ID_FROM_UID(_AccountUID, 'Account'),
		OrderStatusID = ID_FROM_UID(_OrderStatusUID, 'OrderStatus'),
		BillingAddressID = ID_FROM_UID(_BillingAddressUID, 'Address'),
		DeliveryAddressID = ID_FROM_UID(_DeliveryAddressUID, 'Address'),
		DiscountID = ID_FROM_UID(_DiscountUID, 'Discount'),
		ShipmentID = ID_FROM_UID(_ShipmentUID, 'Shipment'),
		PaymentProcessorID = ID_FROM_UID(_PaymentProcessor, 'PaymentProcessor'),
		TransactionID = _TransactionID,
		SubTotal = _SubTotal,
		BeforeTaxTotal = _BeforeTaxTotal,
		ShipTotal = _ShipTotal,
		TaxTotal = _TaxTotal,
		DiscountTotal = _DiscountTotal,
		GrandTotal = _GrandTotal,
		PaymentCardBrand = _PaymentCardBrand,
		PaymentCardLast4Digits = _PaymentCardLast4Digits,
		PaymentCardExpiryMonth = _PaymentCardExpiryMonth,
		PaymentCardExpiryYear = _PaymentCardExpiryYear
  WHERE 
  	`Order`.UID = UUID_TO_BIN(_OrderUID);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes (cancels) an Order
*
* @param {uuid} OrderUID - The UUID for the Order to be cancelled
*/
DROP PROCEDURE IF EXISTS store.OrderDelete;
DELIMITER //
CREATE PROCEDURE store.OrderDelete(
	IN _OrderUID varchar(36)
)
BEGIN

	UPDATE 
		`Order`
  SET
		OrderStatusID = (SELECT ID FROM OrderStatus WHERE Title LIKE 'cancel')
  WHERE 
  	`Order`.UID = UUID_TO_BIN(_OrderUID);
  
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Order Item
*
* @param {uuid} OrderItemUID - The UUID of the Order Item
* @param {uuid} OrderUID - The UUID of the Order
* @param {uuid} ProductItemUID - The UUID of the Product Item
* @param {integer} Quantity - the quantity (of the item)
* @param {float} UnitPrice - the unit price of the item
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.OrderItemCreate;
DELIMITER //
CREATE PROCEDURE store.OrderItemCreate(
	IN _OrderItemUID varchar(36),
	IN _OrderUID varchar(36),
	IN _ProductItemUID varchar(36),
	IN _Quantity int,
	IN _UnitPrice decimal(15,2),
	OUT _ID int
)
BEGIN
	
	-- Update the product item stock quantity to reflect the amount ordered
	UPDATE ProductItem SET StockQuantity = (StockQuantity - _Quantity) WHERE ProductItem.UID = UUID_TO_BIN(_ProductItemUID);
	
	INSERT INTO OrderItem(
		UID,
		OrderID,
		ProductItemID,
		Quantity,
		UnitPrice
	)
	VALUES (
		UUID_TO_BIN(_OrderItemUID),
		ID_FROM_UID(_OrderUID, 'Order'),
		ID_FROM_UID(_ProductItemUID, 'ProductItem'),
		_Quantity,
		_UnitPrice
	);

	SET _ID = LAST_INSERT_ID();

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Order Item
*
* @param {uuid} OrderItemUID - The UUID of the Order Item
* @param {uuid} OrderUID - The UUID of the Order
* @param {uuid} ProductItemUID - The UUID of the Product Item
* @param {integer} Quantity - the quantity (of the item)
* @param {float} UnitPrice - the unit price of the item
*/
DROP PROCEDURE IF EXISTS store.OrderItemUpdate;
DELIMITER //
CREATE PROCEDURE store.OrderItemUpdate(
	IN _OrderItemUID varchar(36),
	IN _OrderUID varchar(36),
	IN _ProductItemUID varchar(36),
	IN _Quantity int,
	IN _UnitPrice decimal(15,2)
)
BEGIN
		
	UPDATE 
		OrderItem
  SET
		OrderID = ID_FROM_UID(_OrderUID, 'Order'),
	  ProductItemID =	ID_FROM_UID(_ProductItemUID, 'ProductItem'),
	  Quantity = _Quantity,
	  UnitPrice = _UnitPrice
  WHERE 
  	OrderItem.UID = UUID_TO_BIN(_OrderItemUID);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Order Shipment
*
* @param {uuid} ShipmentUID - The UUID of the Shipment
* @param {string} RateID - the EasyPost Rate Id
* @param {string} ShipmentID - the EasyPost Shipment Id
* @param {string} Carrier - the name of the shipment carrier
* @param {string} Service - the name of the shipment service level
* @param {float} Rate - the shipping rate
* @param {string} TrackingNumber - the tracking number for the shipment
* @param {integer} DeliveryDays - the number of days for delivery
* @param {date} ShipDate - the shipping date
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.OrderShipmentCreate;
DELIMITER //
CREATE PROCEDURE store.OrderShipmentCreate(
	IN _ShipmentUID varchar(36),
	IN _RateID varchar(37),
	IN _ShipmentID varchar(37),
	IN _Carrier varchar(100),
	IN _Service varchar(100),
	IN _Rate decimal(15,2),
	IN _TrackingNumber varchar(500),
	IN _DeliveryDays int,
	IN _ShipDate Date,
	OUT _ID int
)
BEGIN
	
	INSERT INTO Shipment(
		UID,
		RateID,
		ShipmentID,
		Carrier,
		Service,
		Rate,
		TrackingNumber,
		DeliveryDays,
		ShipDate
	)
	VALUES (
		UUID_TO_BIN(_ShipmentUID),
		_RateID,
		_ShipmentID,
		_Carrier,
		_Service,
		_Rate,
		_TrackingNumber,
		_DeliveryDays,
		_ShipDate
	);

	SET _ID = LAST_INSERT_ID();

END //
DELIMITER ;

#-------------------------------------------------------------------------------
/*
* Updates an Order Shipment
*
* @param {uuid} ShipmentUID - The UUID of the Shipment to change
* @param {string} RateID - the EasyPost Rate Id
* @param {string} ShipmentID - the EasyPost Shipment Id
* @param {string} Carrier - the new name of the shipment carrier
* @param {string} Service - the new name of the shipment service level
* @param {float} Rate - the new shipping rate
* @param {string} TrackingNumber - the new tracking number for the shipment
* @param {integer} DeliveryDays - the new number of days for delivery
* @param {date} ShipDate - the new shipping date
*/
DROP PROCEDURE IF EXISTS store.OrderShipmentUpdate;
DELIMITER //
CREATE PROCEDURE store.OrderShipmentUpdate(
  IN _ShipmentUID varchar(36),
	IN _RateID varchar(37),
	IN _ShipmentID varchar(37),  
	IN _Carrier varchar(100),
	IN _Service varchar(100),
	IN _Rate decimal(15,2),
	IN _TrackingNumber varchar(500),
	IN _DeliveryDays int,
	IN _ShipDate Date
)
BEGIN
	
  UPDATE 
  	Shipment
  SET 
  	RateID = _RateID,
  	ShipmentID = _ShipmentID,
  	Carrier = _Carrier,
		Service = _Service,
		Rate = _Rate,
		TrackingNumber = _TrackingNumber,
		DeliveryDays = _DeliveryDays,
		ShipDate = _ShipDate
  WHERE 
 		UID = UUID_TO_BIN(_ShipmentUID);

END //
DELIMITER ;
