#-------------------------------------------------------------------------------
/*
* All operations related to customer accounts.
*
* Content:		AccountGetAll
* 				AccountGetById
* 				AccountCreate
* 				AccountUpdate
* 				AccountDelete
* 				AccountUserUpdate
* 				AccountUserRoleCreate
* 				AccountUserRoleDelete
* 				ReviewGetAll
* 				ReviewGetById
* 				ReviewCreate
* 				ReviewUpdate
* 				ReviewDelete
* 				WishlistGetAll
* 				WishlistGetById
* 				WishlistCreate
* 				WishlistUpdate
* 				WishlistDelete
* 				WishListItemCreate
* 				WishListItemDelete
* 				AddressGetAll
* 				AddressGetPrimary
* 				AddressUpdatePrimary
*				AccountOrderGetAll
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all accounts
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.AccountGetAll;
DELIMITER //
CREATE PROCEDURE store.AccountGetAll()
BEGIN

	SELECT JSON_ARRAYAGG(
		GetAccountJSON(Account.ID)
	)
  FROM 
 		Account JOIN User ON Account.ID = User.AccountID;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves an account
*
* @param {uuid} AccountUID - The UUID for the account to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.AccountGetById;
DELIMITER //
CREATE PROCEDURE store.AccountGetById(
	IN _AccountUID varchar(36)
)
BEGIN
	SELECT 
   		GetAccountJSON(Account.ID)
	FROM
		Account, User
	WHERE
		Account.ID = User.AccountID AND
		Account.UID = UUID_TO_BIN(_AccountUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an account
*
* @param {uuid} AccountUID - The UUID of the account
* @param {string} UserName - A string containing the login user name (e-mail)
* @param {string} FullName - A string containing the full name
* @param {string} PreferredName - A string containing the preferredName
* @param {string} Email - A string containing the e-mail address
* @param {string} PasswordHash - A string containing the hashed password
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.AccountCreate;
DELIMITER //
CREATE PROCEDURE store.AccountCreate(
   IN _AccountUID varchar(36),
   IN _UserName varchar(100),
   IN _FullName varchar(100),
   IN _PreferredName varchar(50),
   IN _EMail varchar(100),
   IN _PasswordHash char(98)
)
BEGIN

	# Create the account
	INSERT INTO Account
		(UID, FullName, PreferredName, EMail)
	VALUES (
		UUID_TO_BIN(_AccountUID), 
		_FullName, 
		_PreferredName, 
		LOWER(_EMail)
	);
	
	# Create the user
	INSERT INTO User
		(AccountID, UserName, PasswordHash)
	VALUES (
		ID_FROM_UID(_AccountUID, 'Account'),
		LOWER(_UserName),
		_PasswordHash
	);

	SET @UserID = LAST_INSERT_ID();

	# Create the default user role (ID=1)
	INSERT INTO UserRole
		(UserID, RoleID)
	VALUES 
		(@UserID, 1);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an account
*
* @param {uuid} AccountUID - The UUID of the account
* @param {string} FullName - A string containing the full name
* @param {string} PreferredName - A string containing the preferredName
* @param {string} Email - A string containing the e-mail address
* @param {string} PhoneNumber - A string containing the phone number
*/
DROP PROCEDURE IF EXISTS store.AccountUpdate;
DELIMITER //
CREATE PROCEDURE store.AccountUpdate(
   IN _AccountUID varchar(36),
   IN _FullName varchar(100),
   IN _PreferredName varchar(50),
   IN _Email varchar(100),
   IN _PhoneNumber varchar(30)
)
BEGIN

	UPDATE 
		Account
	SET 
		FullName = _FullName,
		PreferredName = _PreferredName,
		EMail = LOWER(_Email),
		PhoneNumber = _PhoneNumber
	WHERE 
		Account.UID = UUID_TO_BIN(_AccountUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes an account (by deactivating it rather than actually deleting it)
*
* @param {uuid} AccountUID - The UUID for the account to be deleted
*/
DROP PROCEDURE IF EXISTS store.AccountDelete;
DELIMITER //
CREATE PROCEDURE store.AccountDelete(
 	IN _AccountUID varchar(36)
)
BEGIN

   UPDATE 
   	Account
   SET 
   	Active = false
   WHERE 
  	Account.UID = UUID_TO_BIN(_AccountUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an account user
*
* @param {uuid} AccountUID - The UUID of the account
* @param {uuid} RoleUID - The user role
* @param {string} UserName - A string containing the user name
* @param {string} PasswordHash - A string containing the hashed password
*/
DROP PROCEDURE IF EXISTS store.AccountUserUpdate;
DELIMITER //
CREATE PROCEDURE store.AccountUserUpdate(
   IN _AccountUID varchar(36),
   IN _RoleUID varchar(36),   
   IN _UserName varchar(100),
   IN _PasswordHash char(98)
)
BEGIN	
	UPDATE 
		User
	SET 
		UserName = LOWER(_UserName),
		PasswordHash = _PasswordHash
	WHERE 
		User.AccountID = ID_FROM_UID(_AccountUID, 'Account');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Adds an account user role
*
* @param {uuid} AccountUID - The UUID of the account
* @param {uuid} RoleUID - The user role
*/
DROP PROCEDURE IF EXISTS store.AccountUserRoleCreate;
DELIMITER //
CREATE PROCEDURE store.AccountUserRoleCreate(
   IN _AccountUID varchar(36),
   IN _RoleUID varchar(36)
)
BEGIN	
	INSERT INTO UserRole
		(UserID, RoleID)
	SELECT 
		User.ID, 
		ID_FROM_UID(_RoleUID, 'Role')
	FROM 
		User
	WHERE
		User.AccountID = ID_FROM_UID(_AccountUID, 'Account');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Removes an account user role
*
* @param {uuid} AccountUID - The UUID of the account
* @param {uuid} RoleUID - The user role
*/
DROP PROCEDURE IF EXISTS store.AccountUserRoleDelete;
DELIMITER //
CREATE PROCEDURE store.AccountUserRoleDelete(
   IN _AccountUID varchar(36),
   IN _RoleUID varchar(36)
)
BEGIN	
	DELETE FROM 
		UserRole
	WHERE 
		UserID = (SELECT ID FROM User WHERE User.AccountID = ID_FROM_UID(_AccountUID, 'Account')) AND 
		RoleID = ID_FROM_UID(_RoleUID, 'Role');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all Reviews (for an account)
*
* @param {uuid} AccountUID - The UUID for the Account containing the Reviews
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.AccountReviewGetAll;
DELIMITER //
CREATE PROCEDURE store.AccountReviewGetAll(
   IN _AccountUID varchar(36)
)
BEGIN

  SELECT JSON_ARRAYAGG(
  	GetReviewJSON(Review.ID)
  )
  FROM
  	Review, Account
  WHERE
  	Review.AccountID = Account.ID AND 	
  	Account.ID = ID_FROM_UID(_AccountUID, 'Account'); 
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a Review
*
* @param {uuid} ReviewUID - The UUID for the Review to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.ReviewGetById;
DELIMITER //
CREATE PROCEDURE store.ReviewGetById(
  IN _ReviewID varchar(36)
)
BEGIN

  SELECT 
  	GetReviewJSON(Review.ID)
  FROM 
  	Review
  WHERE
  	Review.ID = ID_FROM_UID(_ReviewUID, 'Review');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Review
*
* @param {uuid} ReviewUID - The UUID of the Review
* @param {uuid} AccountUID - The UUID of the Account
* @param {uuid} productItemUID - The UUID of the ProductItem
* @param {string} Title - A string containing review title
* @param {string} Description - A string containing the review dascription
* @param {integer} Rating - An integer containing the rating (1-5)
* @param {boolean} VerifiedPurchase - True if the review is for a verified purchase
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.ReviewCreate;
DELIMITER //
CREATE PROCEDURE store.ReviewCreate(
  IN _ReviewUID varchar(36),
  IN _AccountUID varchar(36),
  IN _ProductUID varchar(36),
  IN _ProductItemUID varchar(36),
  IN _Title varchar(100),
  IN _Description varchar(1000),
  IN _Rating tinyint,
  IN _VerifiedPurchase boolean
)
BEGIN

  -- TODO: vertified purchase
    INSERT INTO Review (
      `UID`, `AccountID`, `ProductID`, `ProductItemID`, `Title`, `Description`, `Rating`, `VerifiedPurchase`
    ) 
    VALUES (
      UUID_TO_BIN(_ReviewUID), 
      UUID_TO_BIN(_AccountUID), 
      UUID_TO_BIN(_ProductUID), 
      UUID_TO_BIN(_ProductItemUID), 
      _Title, 
      _Description, 
      _Rating,  
      _VerifiedPurchase
    );

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Review
*
* @param {uuid} ReviewUID - The UUID of the Review
* @param {string} Title - A string containing review title
* @param {string} Description - A string containing the review dascription
* @param {integer} Rating - An integer containing the rating (1-5)
*/
DROP PROCEDURE IF EXISTS store.ReviewUpdate;
DELIMITER //
CREATE PROCEDURE store.ReviewUpdate(
  IN _ReviewUID varchar(36),
  IN _Title varchar(100),
  IN _Description varchar(1000),
  IN _Rating tinyint
)
BEGIN

  UPDATE Review SET 
    `Title` = _Title,
    `Description` = _Description,
    `Rating` = _Rating
  WHERE `UID` = UUID_TO_BIN(_RewviewUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes an Review
*
* @param {uuid} ReviewUID - The UUID for the Review to be deleted
*/
DROP PROCEDURE IF EXISTS store.ReviewDelete;
DELIMITER //
CREATE PROCEDURE store.ReviewDelete(
 	IN ReviewUID binary(16)
)
BEGIN

  DELETE FROM Review
  WHERE `UID` = ReviewUID;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all Wishlists (for an account)
*
* @param {UUID} - The account UID to pull all wishlists from
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.WishlistGetAll;
DELIMITER //
CREATE PROCEDURE store.WishlistGetAll(
  IN _AccountUID varchar(36)
)
BEGIN

  SELECT JSON_ARRAYAGG(JSON_OBJECT(
    'id', BIN_TO_UUID(UID),
    'title', Title,
    'productItem', (SELECT JSON_ARRAYAGG(JSON_OBJECT(
        'id', Product.UID,
        'name', Product.Name,
        'image', Product.Image
      ))
      FROM store.Product
      JOIN store.Wishlist ON Wishlist.AccountID = Account.ID
    )
  ))
  FROM store.Wishlist
  WHERE `AccountID` = UUID_TO_BIN(AccountUID);

	/*
	JSON:
	    id: string;				<-- This is the UID
		title: string;
		productItem: [{
		    id: string;			<-- This is the UID
		    name: string;
		    image:string;
		}];
	*/

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a Wishlist (and all its items)
*
* @param {uuid} WishlistUID - The UUID for the Wishlist to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.WishlistGetById;
DELIMITER //
CREATE PROCEDURE store.WishlistGetById(
  IN _WishlistUID varchar(36)
)
BEGIN

  SELECT JSON_ARRAYAGG(JSON_OBJECT(
    'id', UID,
    'title', Title,
    'productItem', (SELECT JSON_ARRAYAGG(JSON_OBJECT(
        'id', Product.UID,
        'name', Product.Name,
        'image', Product.Image
      ))
      FROM store.Product
      JOIN store.Wishlist ON Wishlist.AccountID = Account.ID
    )
  ))
  FROM store.Wishlist
  WHERE `UID` = UUID_TO_BIN(_WishlistUID);

	/*
	JSON:
	    id: string;				<-- This is the UID
		title: string;
		productItem: [{
		    id: string;			<-- This is the UID
		    name: string;
		    image:string;
		}];
	*/

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Wishlist
*
* @param {uuid} WishlistUID - The UUID of the Wishlist
* @param {uuid} AccountUID - The UUID of the Account
* @param {string} Title - A string containing Wishlist title
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.WishlistCreate;
DELIMITER //
CREATE PROCEDURE store.WishlistCreate(
  IN _WishlistUID varchar(36),
  IN _AccountUID varchar(36),
  IN _Title varchar(100)
)
BEGIN

  INSERT INTO store.Wishlist (
    `UID`, `AccountID`, `Title`
  ) VALUES (
    UUID_TO_BIN(_WishlistUID), UUID_TO_BIN(_AccountUID), _Title
  );

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a Wishlist
*
* @param {uuid} WishlistUID - The UUID of the Wishlist
* @param {string} Title - A string containing Wishlist title
* @param {string} Description - A string containing the Wishlist dascription
*/
DROP PROCEDURE IF EXISTS store.WishlistUpdate;
DELIMITER //
CREATE PROCEDURE store.WishlistUpdate()
BEGIN

	#TODO

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a Wishlist
*
* @param {uuid} WishlistUID - The UUID for the Wishlist to be deleted
*/
DROP PROCEDURE IF EXISTS store.WishlistDelete;
DELIMITER //
CREATE PROCEDURE store.WishlistDelete(
 	IN _WishlistUID varchar(36)
)
BEGIN

  -- get rid of all wishlist items
  DELETE FROM WishlistItem
  WHERE `WishlistID` = _WishlistUID;

  -- remove the wishlist itself
  DELETE FROM Wishlist
  WHERE `UID` = UUID_TO_BIN(_WishlistUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Wishlist Item (adds it to a wishlist)
*
* @param {uuid} WishlistItemUID - The UUID of the Wishlist Item
* @param {uuid} WishlistUID - The UUID of the Wishlist
* @param {uuid} productItemUID - The UUID of a ProductItem
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.WishlistItemCreate;
DELIMITER //
CREATE PROCEDURE store.WishlistItemCreate(
  IN _WishlistItemUID varchar(36),
  IN _WishlistUID varchar(36),
  IN _ProductItemUID varchar(36)
)
BEGIN

  INSERT INTO WishlistItemCreate
  (`UID`, `WishlistID`, `ProductItemID`)
    VALUES
  (UUID_TO_BIN(_WishlistItemUID), UUID_TO_BIN(_WishlistUID), UUID_TO_BIN(_ProductItemUID));

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a Wishlist Item
*
* @param {uuid} WishlistItemUID - The UUID for the Wishlist item to be deleted
*/
DROP PROCEDURE IF EXISTS store.WishlistItemDelete;
DELIMITER //
CREATE PROCEDURE store.WishlistItemDelete(
 	IN _WishlistItem varchar(36)
)
BEGIN

  DELETE FROM WishlistItem
  WHERE `UID` = UUID_TO_BIN(WishlistItem);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all addresses (for an account)
*
* @param {uuid} AccountUID - The UUID for the Account containing the addresses
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.AddressGetAll;
DELIMITER //
CREATE PROCEDURE store.AddressGetAll(
	IN _AccountUID varchar(36)
)
BEGIN
	
	SELECT 
		JSON_ARRAYAGG(GetAddressJSON(Address.ID))
	FROM
		Account, Address
	WHERE
		Account.ID = Address.AccountID AND
		Account.UID = UUID_TO_BIN(_AccountUID)
	ORDER BY
		Address.Description;	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves the primary (default) address (for an account)
*
* @param {uuid} AccountUID - The UUID for the Account containing the address
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.AddressGetPrimary;
DELIMITER //
CREATE PROCEDURE store.AddressGetPrimary(
	IN _AccountUID varchar(36)
)
BEGIN
	SELECT 
		GetAddressJSON(Address.ID)
	FROM
		Account, Address
	WHERE
		Account.UID = UUID_TO_BIN(_AccountUID) AND
		Address.ID = Account.AddressID ;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates the primary (default) address (for an account)
*
* @param {uuid} AccountUID - The UUID of the Account
* @param {uuid} AddressUID - The UUID of the address to make primary
*/
DROP PROCEDURE IF EXISTS store.AddressUpdatePrimary;
DELIMITER //
CREATE PROCEDURE store.AddressUpdatePrimary(
  IN _AccountUID varchar(36),
  IN _AddressUID varchar(36)
)
BEGIN

  UPDATE 
  	Account 
  SET 
    AddressID = ID_FROM_UID(_AddressUID, 'Address')
  WHERE 
 		Account.UID = UUID_TO_BIN(_AccountUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all Orders (for an account)
*
* @param {uuid} AccountUID - The UUID for the Account containing the Reviews
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.AccountOrderGetAll;
DELIMITER //
CREATE PROCEDURE store.AccountOrderGetAll(
   IN _AccountUID varchar(36)
)
BEGIN
	
   SELECT JSON_ARRAYAGG(
	   GetOrderJSON(`Order`.ID)
   )
   FROM 
   		`Order`, Account
   WHERE 
   		Account.ID = `Order`.AccountID AND
  		Account.UID = UUID_TO_BIN(_AccountUID);
	
END //
DELIMITER ;

