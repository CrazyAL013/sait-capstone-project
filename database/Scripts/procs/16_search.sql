#-------------------------------------------------------------------------------
/*
* All operations related to (product) searches.
*
* Content:	SearchByKeyword
* 					SearchByCategoryKeyword
* 					SearchBySubcategoryKeyword
* 					SearchByCategory
* 					SearchBySubcategory
*/
#-------------------------------------------------------------------------------
/*
* Returns all products
*
* @param {string} Sort - A string containing the sort type
* @return {string} - A resultset of formatted Product JSON strings
* 
* NOTE: JSON_ARRAY_AGG function doesn't return ordered results so it can't be used here.
* 
*/
DROP PROCEDURE IF EXISTS store.SearchAll;
DELIMITER //
CREATE PROCEDURE store.SearchAll(
	IN _Sort varchar(50)
)
BEGIN
	
	SELECT
		GetCatalogItemJSON(ProductItem.ID) AS ProductJSON
	FROM
		ProductItem
	ORDER BY 
		CASE LOWER(_Sort) WHEN 'high' THEN ProductItem.UnitPrice END DESC,
		CASE LOWER(_Sort) WHEN 'low' THEN ProductItem.UnitPrice END ASC,
		CASE LOWER(_Sort) WHEN 'stock' THEN ProductItem.StockQuantity END DESC,
		CASE LOWER(_Sort) WHEN 'rating' THEN GetProductItemAverageRating(ProductItem.ID) END DESC; 
			
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by keyword
*
* @param {string} Keywords - A string containing the search terms
* @param {string} Sort - A string containing the sort type
* @return {string} - A resultset of formatted Product JSON strings
* 
* NOTE: JSON_ARRAY_AGG function doesn't return ordered results so it can't be used here.
* 
*/
DROP PROCEDURE IF EXISTS store.SearchByKeyword;
DELIMITER //
CREATE PROCEDURE store.SearchByKeyword(
	IN _Keywords varchar(200),
	IN _Sort varchar(50)
)
BEGIN
	
	SELECT
		GetCatalogItemJSON(ProductSearch.ProductItemID) AS ProductJSON
	FROM
		ProductSearch, ProductItem
	WHERE
		ProductSearch.ProductItemID = ProductItem.ID AND
	MATCH
		(ProductSearch.Keywords)
	AGAINST
		(_Keywords IN NATURAL LANGUAGE MODE)
	ORDER BY 
		CASE LOWER(_Sort) WHEN 'high' THEN ProductItem.UnitPrice END DESC,
		CASE LOWER(_Sort) WHEN 'low' THEN ProductItem.UnitPrice END ASC,
		CASE LOWER(_Sort) WHEN 'stock' THEN ProductItem.StockQuantity END DESC,
		CASE LOWER(_Sort) WHEN 'rating' THEN GetProductItemAverageRating(ProductSearch.ProductItemID) END DESC; 
			
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by category and keyword
*
* @param {uuid} CategoryUID - The UUID for the category
* @param {string} Keywords - A string containing the search terms
* @param {string} Sort - A string containing the sort type
* @return {string} - A resultset of formatted Product JSON strings
* 
* NOTE: JSON_ARRAY_AGG function doesn't return ordered results so it can't be used here.
* 
*/
DROP PROCEDURE IF EXISTS store.SearchByCategoryKeyword;
DELIMITER //
CREATE PROCEDURE store.SearchByCategoryKeyword(
	IN _CategoryUID varchar(36),
	IN _Keywords varchar(200),
	IN _Sort varchar(50)
)
BEGIN
	
	SELECT  
		ProductSearch.ProductItemID, 
		GetCatalogItemJSON(ProductSearch.ProductItemID) AS ProductJSON
	FROM
		ProductSearch, ProductItem, ProductCategory
	WHERE
		ProductSearch.ProductItemID = ProductItem.ID AND	
		ProductCategory.ProductID = ProductItem.ProductID AND
		ProductCategory.CategoryID = ID_FROM_UID(_CategoryUID, 'Category') AND
	MATCH
		(ProductSearch.Keywords)
	AGAINST
		(_Keywords IN NATURAL LANGUAGE MODE)
	GROUP BY
		ProductSearch.ProductItemID
	ORDER BY 
		CASE LOWER(_Sort) WHEN 'high' THEN ProductItem.UnitPrice END DESC,
		CASE LOWER(_Sort) WHEN 'low' THEN ProductItem.UnitPrice END ASC,
		CASE LOWER(_Sort) WHEN 'stock' THEN ProductItem.StockQuantity END DESC,
		CASE LOWER(_Sort) WHEN 'rating' THEN GetProductItemAverageRating(ProductSearch.ProductItemID) END DESC; 
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by subcategory and keyword
*
* @param {uuid} SubcategoryUID - The UUID for the subcategory
* @param {string} Keywords - A string containing the search terms
* @param {string} Sort - A string containing the sort type
* @return {string} - A resultset of formatted Product JSON strings
* 
* NOTE: JSON_ARRAY_AGG function doesn't return ordered results so it can't be used here.
* 
*/
DROP PROCEDURE IF EXISTS store.SearchBySubcategoryKeyword;
DELIMITER //
CREATE PROCEDURE store.SearchBySubcategoryKeyword(
	IN _SubcategoryUID varchar(36),
	IN _Keywords varchar(200),
	IN _Sort varchar(50)
)
BEGIN
	
	SELECT  
		ProductSearch.ProductItemID, 
		GetCatalogItemJSON(ProductSearch.ProductItemID) AS ProductJSON
	FROM
		ProductSearch, ProductItem, ProductCategory
	WHERE
		ProductSearch.ProductItemID = ProductItem.ID AND
		ProductCategory.ProductID = ProductItem.ProductID AND
		ProductCategory.SubcategoryID = ID_FROM_UID(_SubcategoryUID, 'Subcategory') AND
	MATCH
		(ProductSearch.Keywords)
	AGAINST
		(_Keywords IN NATURAL LANGUAGE MODE)
	ORDER BY 
		CASE LOWER(_Sort) WHEN 'high' THEN ProductItem.UnitPrice END DESC,
		CASE LOWER(_Sort) WHEN 'low' THEN ProductItem.UnitPrice END ASC,
		CASE LOWER(_Sort) WHEN 'stock' THEN ProductItem.StockQuantity END DESC,
		CASE LOWER(_Sort) WHEN 'rating' THEN GetProductItemAverageRating(ProductSearch.ProductItemID) END DESC; 

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by category (returns all for a category)
*
* @param {uuid} CategoryUID - The UUID for the category
* @param {string} Sort - A string containing the sort type
* @return {string} - A resultset of formatted Product JSON strings
* 
* NOTE: JSON_ARRAY_AGG function doesn't return ordered results so it can't be used here.
* 
*/
DROP PROCEDURE IF EXISTS store.SearchByCategory;
DELIMITER //
CREATE PROCEDURE store.SearchByCategory(
	IN _CategoryUID varchar(36),
	IN _Sort varchar(50)
)
BEGIN
	
	SELECT  
		ProductItem.ID, 
		GetCatalogItemJSON(ProductItem.ID) AS ProductJSON
	FROM
		ProductItem, ProductCategory
	WHERE
		ProductCategory.ProductID = ProductItem.ProductID AND
		ProductCategory.CategoryID = ID_FROM_UID(_CategoryUID, 'Category')
	GROUP BY 
		ProductItem.ID
	ORDER BY 
		CASE LOWER(_Sort) WHEN 'high' THEN ProductItem.UnitPrice END DESC,
		CASE LOWER(_Sort) WHEN 'low' THEN ProductItem.UnitPrice END ASC,
		CASE LOWER(_Sort) WHEN 'stock' THEN ProductItem.StockQuantity END DESC,
		CASE LOWER(_Sort) WHEN 'rating' THEN GetProductItemAverageRating(ProductItem.ID) END DESC; 
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Searches products by subcategory (returns all for a subcategory)
*
* @param {uuid} SubcategoryUID - The UUID for the subcategory
* @param {string} Sort - A string containing the sort type
* @return {string} - A resultset of formatted Product JSON strings
* 
* NOTE: JSON_ARRAY_AGG function doesn't return ordered results so it can't be used here.
* 
*/
DROP PROCEDURE IF EXISTS store.SearchBySubcategory;
DELIMITER //
CREATE PROCEDURE store.SearchBySubcategory(
	IN _SubcategoryUID varchar(36),
	IN _Sort varchar(50)
)
BEGIN
	
	SELECT  
		ProductItem.ID, 
		GetCatalogItemJSON(ProductItem.ID) AS ProductJSON
	FROM
		ProductItem, ProductCategory
	WHERE
		ProductCategory.ProductID = ProductItem.ProductID AND
		ProductCategory.SubcategoryID = ID_FROM_UID(_SubcategoryUID, 'Subcategory')
	GROUP BY 
		ProductItem.ID
	ORDER BY 
		CASE LOWER(_Sort) WHEN 'high' THEN ProductItem.UnitPrice END DESC,
		CASE LOWER(_Sort) WHEN 'low' THEN ProductItem.UnitPrice END ASC,
		CASE LOWER(_Sort) WHEN 'stock' THEN ProductItem.StockQuantity END DESC,
		CASE LOWER(_Sort) WHEN 'rating' THEN GetProductItemAverageRating(ProductItem.ID) END DESC; 
	
END //
DELIMITER ;
