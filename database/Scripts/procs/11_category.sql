#-------------------------------------------------------------------------------
/*
* All operations related to categories and subcategories.
*
* Content:	CategoryGetAll
* 			CategoryGetById
* 			CategoryCreate
* 			CategoryUpdate
* 			CategoryDelete
* 			SubcategoryCreate
* 			SubcategoryUpdate
* 			SubcategoryDelete
* 			SubcategoryCreateOrUpdate
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all categories and their subcategories
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.CategoryGetAll;
DELIMITER //
CREATE PROCEDURE store.CategoryGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(UID),
		'name', Name,
		'subcategories', (
			SELECT JSON_ARRAYAGG(JSON_OBJECT(
				'id', BIN_TO_UUID(UID),
				'name', Name
			))
			FROM
				Subcategory
			WHERE
				Category.ID = Subcategory.CategoryID
			ORDER BY Name
		)
	))
	FROM
		Category
	ORDER BY Name;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a single category and its subcategories
*
* @param {uuid} CategoryUID - The UUID for the category to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.CategoryGetById;
DELIMITER //
CREATE PROCEDURE store.CategoryGetById (
	IN _CategoryUID varchar(36)
)
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(UID),
		'name', Name,
		'subcategories', (
			SELECT JSON_ARRAYAGG(JSON_OBJECT(
				'id', BIN_TO_UUID(Subcategory.UID),
				'name', Subcategory.Name
			))
			FROM
				Category, Subcategory
			WHERE
				Category.ID = Subcategory.CategoryID AND
				Category.UID = UUID_TO_BIN(_CategoryUID)
			ORDER BY Subcategory.Name
		)
	))
	FROM
		Category
	WHERE
		UID = UUID_TO_BIN(_CategoryUID)
	ORDER BY Name;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a category
*
* @param {uuid} CategoryUID - The UUID of the category
* @param {string} Name - A string containing the category name
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.CategoryCreate;
DELIMITER //
CREATE PROCEDURE store.CategoryCreate (
	IN _CategoryUID varchar(36),
	IN _Name varchar(100),
	OUT _ID int
)
BEGIN
	INSERT INTO Category(
		UID,
		Name
	) VALUES (
		UUID_TO_BIN(_CategoryUID),
		_Name
	);
	SET _ID = LAST_INSERT_ID();
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a category
*
* @param {uuid} CategoryUID - The UUID for the category to be updated
* @param {string} Name - A string containing the category name
*/
DROP PROCEDURE IF EXISTS store.CategoryUpdate;
DELIMITER //
CREATE PROCEDURE store.CategoryUpdate (
	IN _CategoryUID varchar(36),
	IN _Name varchar(100)
)
BEGIN
	UPDATE Category
	SET
		Name = _Name
	WHERE
		UID = UUID_TO_BIN(_CategoryUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a category and any associated sub-categories
*
* @param {uuid} CategoryUID - The UUID for the category to be deleted
*/
DROP PROCEDURE IF EXISTS store.CategoryDelete;
DELIMITER //
CREATE PROCEDURE store.CategoryDelete (
	IN _CategoryUID varchar(36)
)
BEGIN
	DELETE
		Category,
		Subcategory
	FROM
		Category
	LEFT JOIN
		Subcategory
	ON
		Category.ID = Subcategory.CategoryID
	WHERE
		Category.UID = UUID_TO_BIN(_CategoryUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a sub-category
*
* @param {uuid} SubcategoryUID - The UUID of the sub-category
* @param {uuid} CategoryUID - The UUID of the parent category
* @param {string} Name - A string containing the sub-category name
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.SubcategoryCreate;
DELIMITER //
CREATE PROCEDURE store.SubcategoryCreate (
	IN _SubcategoryUID varchar(36),
	IN _CategoryUID varchar(36),
	IN _Name varchar(100),
	OUT _ID int
)
BEGIN
	INSERT INTO Subcategory(
		UID,
		CategoryID,
		Name
	)
	SELECT
		UUID_TO_BIN(_SubcategoryUID),
		Category.ID,
		_Name
	FROM
		Category
	WHERE
		Category.UID = UUID_TO_BIN(_CategoryUID);

	SET _ID = LAST_INSERT_ID();
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a sub-category
*
* @param {uuid} SubcategoryUID - The UUID of the sub-category to update
* @param {uuid} CategoryUID - The UUID of the parent category
* @param {string} Name - A string containing the sub-category name
*/
DROP PROCEDURE IF EXISTS store.SubcategoryUpdate;
DELIMITER //
CREATE PROCEDURE store.SubcategoryUpdate (
	IN _SubcategoryUID varchar(36),
	IN _CategoryUID varchar(36),
	IN _Name varchar(100)
)
BEGIN
	UPDATE Subcategory
	SET
		CategoryID = (SELECT ID FROM Category WHERE UID = UUID_TO_BIN(_CategoryUID)),
		Name = _Name
	WHERE
		UID = UUID_TO_BIN(_SubcategoryUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a sub-category
*
* @param {uuid} SubcategoryUID - The UUID for the sub-category to be deleted
*/
DROP PROCEDURE IF EXISTS store.SubcategoryDelete;
DELIMITER //
CREATE PROCEDURE store.SubcategoryDelete (
	IN _SubcategoryUID varchar(36)
)
BEGIN
	DELETE FROM
		Subcategory
	WHERE
		UID = UUID_TO_BIN(_subcategoryUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
*  Create or Update a sub-category
*
* @param {uuid} SubcategoryUID - The UUID of the sub-category
* @param {uuid} CategoryUID - The UUID of the parent category
* @param {string} Name - A string containing the sub-category name
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.SubcategoryCreateOrUpdate;
DELIMITER //
CREATE PROCEDURE store.SubcategoryCreateOrUpdate (
	IN _SubcategoryUID varchar(36),
	IN _CategoryUID varchar(36),
	IN _Name varchar(100),
	OUT _ID int
)
BEGIN
	IF EXISTS(SELECT UID FROM Subcategory WHERE UID = UUID_TO_BIN(_SubcategoryUID)) THEN
		CALL SubcategoryUpdate(_SubcategoryUID, _CategoryUID, _Name);
	ELSE
		CALL SubcategoryCreate(_SubcategoryUID, _CategoryUID, _Name, @id);
	END IF;

	SET _ID = LAST_INSERT_ID();
END //
DELIMITER ;
