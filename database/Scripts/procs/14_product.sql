#-------------------------------------------------------------------------------
/*
* All operations related to products.
*
* Content:	ProductGetAll
* 					ProductGetById
* 					ProductCreate
* 					ProductUpdate
* 					ProductDelete
* 					ProductReviewsGetAll
* 					ProductItemGetById
* 					ProductItemCreate
* 					ProductItemUpdate
* 					ProductItemDelete
* 					ProductImageGetAll
* 					ProductImageCreate
* 					ProductImageUpdate
* 					ProductImageDelete
* 					ProductItemImageCreate
* 					ProductItemImageDelete
* 					ProductItemAddSubCategory
* 					ProductItemRemoveSubCategory
* 					SpecificationCreate
* 					SpecificationDeleteByProductItem
* 					ProductStatsUpdate
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all Products (and associated ProductItems)
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.ProductGetAll;
DELIMITER //
CREATE PROCEDURE store.ProductGetAll()
BEGIN
	
	SELECT JSON_ARRAYAGG(
		GetProductJSON(Product.ID)
	)
	FROM
		Product;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a Product (and associated ProductItems)
*
* @param {uuid} ProductUID - The UUID of the product to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.ProductGetById;
DELIMITER //
CREATE PROCEDURE store.ProductGetById(
	IN _ProductUID varchar(36)
)
BEGIN
	
	SELECT 
		GetProductJSON(Product.ID)
	FROM
		Product
	WHERE
		Product.UID = UUID_TO_BIN(_productUID);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Product
*
* @param {uuid} ProductUID - The UUID of the Product
* @param {uuid} SupplierUID - The UUID of the Supplier
* @param {uuid} ManufacturerUID - The UUID of the Manufacturer
* @param {string} Name - A string containing the Product name
* @param {string} Description - A string containing the Product description
* @param {string} Keywords - A string containing the Product keywords
* @param {uuid} ImageUID - The UUID of an image file
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.ProductCreate;
DELIMITER //
CREATE PROCEDURE store.ProductCreate(
	IN _ProductUID varchar(36),
	IN _SupplierUID varchar(36),
	IN _ManufacturerUID varchar(36),
	IN _Name varchar(100),
	IN _Description text,
	IN _Keywords varchar(500),
	IN _ImageUID varchar(36),
	OUT _ID int
)
BEGIN
	INSERT INTO Product(
		UID,
		SupplierID,
		ManufacturerID,
		Name,
		Description,
		Keywords,
		ImageID
	)
	SELECT
		UUID_TO_BIN(_ProductUID),
		ID_FROM_UID(_SupplierUID, 'Supplier'),
		ID_FROM_UID(_ManufacturerUID, 'Manufacturer'),
		_Name,
		_Description,
		_Keywords,
		ID_FROM_UID(_ImageUID, 'Image');
		
	SET _ID = LAST_INSERT_ID();
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a Product
*
* @param {uuid} ProductUID - The UUID of the Product
* @param {uuid} SupplierUID - The UUID of the Supplier
* @param {uuid} ManufacturerUID - The UUID of the Manufacturer
* @param {string} Name - A string containing the Product name
* @param {string} Description - A string containing the Product description
* @param {string} Keywords - A string containing the Product keywords
* @param {uuid} ImageUID - The UUID of an image file
* @param {uuid} ProductUID - The UUID of the Product
*/
DROP PROCEDURE IF EXISTS store.ProductUpdate;
DELIMITER //
CREATE PROCEDURE store.ProductUpdate(
	IN _ProductUID varchar(36),
	IN _SupplierUID varchar(36),
	IN _ManufacturerUID varchar(36),
	IN _Name varchar(100),
	IN _Description text,
	IN _Keywords varchar(500),
	IN _ImageUID varchar(36)
)
BEGIN
	UPDATE Product
	SET
		SupplierID = ID_FROM_UID(_SupplierUID, 'Supplier'),
		ManufacturerID = ID_FROM_UID(_ManufacturerUID, 'Manufacturer'),
		Name = _Name,
		Description = _Description,
		Keywords = _Keywords,
		ImageID = ID_FROM_UID(_ImageUID, 'Image')
	WHERE
		UID = UUID_TO_BIN(_ProductUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a Product
*
* @param {uuid} ProductUID - The UUID for the Product to be deleted
*/
DROP PROCEDURE IF EXISTS store.ProductDelete;
DELIMITER //
CREATE PROCEDURE store.ProductDelete(
	IN _ProductUID varchar(36)
)
BEGIN
	DELETE
		Product,
		ProductItem
	FROM
		Product
	LEFT JOIN
		ProductItem
	ON
		Product.ID = ProductItem.ProductID
	WHERE
		Product.UID = UUID_TO_BIN(_ProductUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all reviews for a Product
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.ProductReviewsGetAll;
DELIMITER //
CREATE PROCEDURE store.ProductReviewsGetAll(
	IN _ProductUID varchar(36)
)
BEGIN
	
	SELECT JSON_ARRAYAGG(
		GetProductReviewJSON(Review.ID)
	)
	FROM
		Product, Review
	WHERE 
		Product.ID = Review.ProductID AND
		Product.UID = UUID_TO_BIN(_ProductUID);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves a ProductItem
*
* @param {uuid} ProductItemUID - The UUID of the ProductItem to be retrieved
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.ProductItemGetById;
DELIMITER //
CREATE PROCEDURE store.ProductItemGetById(
	IN _ProductItemUID varchar(36)
)
BEGIN
	SELECT 
		GetProductItemJSON(ProductItem.ID)	
	FROM
		ProductItem
	WHERE
		ProductItem.UID = UUID_TO_BIN(_ProductItemUID);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a ProductItem
*
* @param {uuid} ProductItemUID - The UUID of the ProductItem
* @param {uuid} ProductUID - The UUID of the Product
* @param {bool} Display - The display flag (true/false)
* @param {string} Model - A string contianing the ProductItem model
* @param {string} SKU - A string containing the ProductItem SKU
* @param {string} UPC - A string containing the ProductItem UPC
* @param {string} Name - A string containing the ProductItem name
* @param {string} Description - A string containing the ProductItem description
* @param {string} Keywords - A string containing the Product keywords
* @param {decimal} UnitPrice - The ProductItem unit price
* @param {decimal} Weight - The ProductItem weight
* @param {integer} StockQuantity - The ProductItem stock quantity
* @param {integer} ReorderLevel - The ProductItem reorder level
* @param {integer} TotalSold - The ProductItem total sold
* @return {integer} - The ID of the new record
*/
DROP PROCEDURE IF EXISTS store.ProductItemCreate;
DELIMITER //
CREATE PROCEDURE store.ProductItemCreate(
	IN _ProductItemUID varchar(36),
	IN _ProductUID varchar(36),
	IN _Display boolean,
	IN _Model varchar(50),
	IN _SKU varchar(12),
	IN _UPC char(12),
	IN _Name varchar(100),
	IN _Description text,
	IN _Keywords varchar(500),
	IN _UnitPrice decimal(15,2),
	IN _Weight decimal(15,2),
	IN _StockQuantity int,
	IN _ReorderLevel int,
	IN _TotalSold int,
	OUT _ID int
)
BEGIN
	INSERT INTO ProductItem(
		UID,
		ProductID,
		Display,
		Model,
		SKU,
		UPC,
		Name,
		Description,
		Keywords,
		UnitPrice,
		Weight,
		StockQuantity,
		ReorderLevel,
		TotalSold
	)
	SELECT
		UUID_TO_BIN(_ProductItemUID),
		ID_FROM_UID(_ProductUID, 'Product'),
		_Display,
		_Model,
		_SKU,
		_UPC,
		_Name,
		_Description,
		_Keywords,
		_UnitPrice,
		_Weight,
		_StockQuantity,
		_ReorderLevel,
		_TotalSold;

		SET _ID = LAST_INSERT_ID();
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates a ProductItem
*
* @param {uuid} ProductItemUID - The UUID of the ProductItem
* @param {uuid} ProductUID - The UUID of the Product
* @param {bool} Display - The display flag (true/false)
* @param {string} Model - A string contianing the ProductItem model
* @param {string} SKU - A string containing the ProductItem SKU
* @param {string} UPC - A string containing the ProductItem UPC
* @param {string} Name - A string containing the ProductItem name
* @param {string} Description - A string containing the ProductItem description
* @param {string} Keywords - A string containing the Product keywords
* @param {decimal} UnitPrice - The ProductItem unit price
* @param {decimal} Weight - The ProductItem weight
* @param {integer} StockQuantity - The ProductItem stock quantity
* @param {integer} ReorderLevel - The ProductItem reorder level
* @param {integer} TotalSold - The ProductItem total sold
*/
DROP PROCEDURE IF EXISTS store.ProductItemUpdate;
DELIMITER //
CREATE PROCEDURE store.ProductItemUpdate(
	IN _ProductItemUID varchar(36),
	IN _ProductUID varchar(36),
	IN _Display boolean,
	IN _Model varchar(50),
	IN _SKU varchar(12),
	IN _UPC char(12),
	IN _Name varchar(100),
	IN _Description text,
	IN _Keywords varchar(500),
	IN _UnitPrice decimal(15,2),
	IN _Weight decimal(15,2),
	IN _StockQuantity int,
	IN _ReorderLevel int,
	IN _TotalSold int
)
BEGIN
	
	IF NOT EXISTS (SELECT * FROM ProductItem WHERE ProductItem.UID = UUID_TO_BIN(_ProductItemUID)) 
	THEN
		CALL ProductItemCreate(_ProductItemUID,_ProductUID,_Display,_Model,_SKU,_UPC,_Name,_Description,_Keywords,_UnitPrice,_Weight,_StockQuantity,_ReorderLevel,_TotalSold, @OUT);
	ELSE
		UPDATE ProductItem
		SET
			ProductID = ID_FROM_UID(_ProductUID, 'Product'), 
			Display = _Display,
			Model = _Model,
			SKU = _SKU,
			UPC = _UPC,
			Name = _Name,
			Description = _Description,
			Keywords = _Keywords,
			UnitPrice = _UnitPrice,
			Weight = _Weight,
			StockQuantity = _StockQuantity,
			ReorderLevel = _ReorderLevel,
			TotalSold = _TotalSold
		WHERE
			UID = UUID_TO_BIN(_ProductItemUID);
	END IF;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a ProductItem
*
* @param {uuid} ProductItemUID - The UUID for the ProductItem to be deleted
*/
DROP PROCEDURE IF EXISTS store.ProductItemDelete;
DELIMITER //
CREATE PROCEDURE store.ProductItemDelete(
	IN _ProductItemUID varchar(36)
)
BEGIN
	DELETE
	FROM
		ProductItem
	WHERE
		UID = UUID_TO_BIN(_ProductItemUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all images for a product
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.ProductImageGetAll;
DELIMITER //
CREATE PROCEDURE store.ProductImageGetAll(
	IN _ProductUID varchar(36)
)
BEGIN
	SELECT JSON_ARRAYAGG(
		GetImageJSON(Image.ID)
	)
	FROM
		Image
	WHERE
		Image.ProductID = ID_FROM_UID(_ProductUID, 'product');
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates an Image
*
* @param {uuid} ImageUID - The UUID of the Image (unless it already exists)
* @param {uuid} ProductUID - The UUID of the Product
* @param {string} Type - A string containing the image type (png, jpg etc.)
* @param {string} Location - A string containing the path to the image file
*/
DROP PROCEDURE IF EXISTS store.ProductImageCreate;
DELIMITER //
CREATE PROCEDURE store.ProductImageCreate(
	IN _ImageUID varchar(36),
	IN _ProductUID varchar(36),
	IN _Type varchar(5),
	IN _Location varchar(200),
	OUT _ID int
)
BEGIN
	IF NOT EXISTS (SELECT * FROM Image WHERE UID = UUID_TO_BIN(_ImageUID)) THEN
		INSERT INTO Image(
			UID,
			ProductID,
			Type,
			Location
		)
		SELECT
			UUID_TO_BIN(_ImageUID),
			ID_FROM_UID(_ProductUID, 'product'),
			_Type,
			_Location;
	END IF;
		
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates an Image
*
* @param {uuid} ImageUID - The UUID of the Image
* @param {uuid} ProductUID - The UUID of the Product
* @param {string} Type - A string containing the image type (png, jpg etc.)
* @param {string} Location - A string containing the path to the image file
*/
DROP PROCEDURE IF EXISTS store.ProductImageUpdate;
DELIMITER //
CREATE PROCEDURE store.ProductImageUpdate(
	IN _ImageUID varchar(36),
	IN _ProductUID varchar(36),
	IN _Type varchar(5),
	IN _Location varchar(200)
)
BEGIN
	UPDATE Image
	SET
		ProductID = ID_FROM_UID(_ProductUID, 'product'),
		Type = _Type,
		Location = _Location
	WHERE
		UID = UUID_TO_BIN(_ImageUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes an Image
*
* @param {uuid} ImageUID - The UUID of the Image
*/
DROP PROCEDURE IF EXISTS store.ProductImageDelete;
DELIMITER //
CREATE PROCEDURE store.ProductImageDelete(
	IN _ImageUID varchar(36)
)
BEGIN
	DELETE FROM 
		Image
	WHERE
		Image.UID = UUID_TO_BIN(_ImageUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates (associates) an Image with a product item (unless it already exists)
*
* @param {uuid} ImageUID - The UUID of the Image
* @param {uuid} ProductItemUID - The UUID of the ProductItem
*/
DROP PROCEDURE IF EXISTS store.ProductItemImageCreate;
DELIMITER //
CREATE PROCEDURE store.ProductItemImageCreate(
	IN _ImageUID varchar(36),
	IN _ProductItemUID varchar(36),
	OUT _ID int
)
BEGIN
	IF NOT EXISTS (SELECT * FROM ProductImage WHERE ProductItemID = ID_FROM_UID(_ProductItemUID, 'productitem') AND ImageID = ID_FROM_UID(_ImageUID, 'image')) THEN
		INSERT INTO ProductImage(
			ProductItemID,
			ImageID
		)
		SELECT
			ID_FROM_UID(_ProductItemUID, 'productitem'),
			ID_FROM_UID(_ImageUID, 'image');
	END IF;	
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes an association between an Image and a product
*
* @param {uuid} ImageUID - The UUID of the Image
* @param {uuid} ProductItemUID - The UUID of the ProductItem
*/
DROP PROCEDURE IF EXISTS store.ProductItemImageDelete;
DELIMITER //
CREATE PROCEDURE store.ProductItemImageDelete(
	IN _ImageUID varchar(36),
	IN _ProductItemUID varchar(36)
)
BEGIN
	DELETE FROM 
		ProductImage
	WHERE
		ProductItemID = ID_FROM_UID(_ProductItemUID, 'productitem') AND
		ImageID = ID_FROM_UID(_ImageUID, 'image');
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Adds a Product to a Subcategory (unless it already exists)
*
* @param {uuid} ProductUID - The UUID of the Product
* @param {uuid} CategoryUID - The UUID of the Category
* @param {uuid} SubCategoryUID - The UUID of the Subcategory
*/
DROP PROCEDURE IF EXISTS store.ProductAddSubCategory;
DELIMITER //
CREATE PROCEDURE store.ProductAddSubCategory(
	IN _ProductUID varchar(36),
	IN _CategoryUID varchar(36),
	IN _SubcategoryUID varchar(36)
)
BEGIN
	
	IF NOT EXISTS (SELECT * FROM ProductCategory WHERE ProductID = ID_FROM_UID(_ProductUID, 'Product') AND SubcategoryID = ID_FROM_UID(_SubcategoryUID, 'Subcategory')) THEN
		INSERT INTO ProductCategory(
			ProductID,
			CategoryID,
			SubcategoryID
		)
		SELECT
			ID_FROM_UID(_ProductUID, 'Product'),
			ID_FROM_UID(_CategoryUID, 'Category'),
			ID_FROM_UID(_SubcategoryUID, 'Subcategory');
	END IF;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Removes a Product from a Subcategory
*
* @param {uuid} ProductUID - The UUID of the Product
* @param {uuid} SubCategoryUID - The UUID of the Subcategory
*/
DROP PROCEDURE IF EXISTS store.ProductRemoveSubCategory;
DELIMITER //
CREATE PROCEDURE store.ProductRemoveSubCategory(
	IN _ProductUID varchar(36),
	IN _SubcategoryUID varchar(36)
)
BEGIN

	DELETE FROM ProductCategory
	WHERE
		ProductID = ID_FROM_UID(_ProductUID, 'Product') AND
		SubcategoryID = ID_FROM_UID(_SubcategoryUID, 'Subcategory');
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates a Product Item Specification (unless it already exists)
*
* @param {uuid} ProductItemUID - The UUID of the ProductItem
* @param {string} Name - A string contianing the specification name
* @param {string} Value - A string containing the specification value
*/
DROP PROCEDURE IF EXISTS store.SpecificationCreate;
DELIMITER //
CREATE PROCEDURE store.SpecificationCreate(
	IN _ProductItemUID varchar(36),
	IN _Name varchar(50),
	IN _Value varchar(50)
)
BEGIN
	
	IF NOT EXISTS (SELECT * FROM Specification WHERE ProductItemID = ID_FROM_UID(_ProductItemUID, 'ProductItem') AND Name = _Name AND Value = _Value) THEN
		INSERT INTO Specification(
			ProductItemID,
			Name,
			Value
		)
		SELECT
			ID_FROM_UID(_ProductItemUID, 'ProductItem'),
			_Name,
			_Value;
	END IF;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes all specifications for a ProductItem
*
* @param {uuid} ProductItemUID - The UUID for the ProductItem specifications to be deleted
*/
DROP PROCEDURE IF EXISTS store.SpecificationDeleteByProductItem;
DELIMITER //
CREATE PROCEDURE store.SpecificationDeleteByProductItem(
	IN _ProductItemUID varchar(36)
)
BEGIN
	DELETE
	FROM
		Specification
	WHERE
		ProductItemID = (SELECT ID FROM ProductItem WHERE UID = UUID_TO_BIN(_ProductItemUID));
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates statistical information for all Products/ProductItems
* This proc is run periodically by an event
*
*/
DROP PROCEDURE IF EXISTS store.ProductUpdateStats;
DELIMITER //
CREATE PROCEDURE store.ProductUpdateStats(
	IN _LastExecuted datetime
)
BEGIN
	#TODO: Postponed until time permits
	
	#DECLARE _done BOOLEAN DEFAULT 0;
	#DECLARE _OrderID int
	#DECLARE _NewOrders CURSOR FOR SELECT Order.ID FROM `Order` WHERE Order.Date >= _LastExecuted;

	#DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET _done=1;
	#OPEN _NewOrders;
	#REPEAT
	#	FETCH _NewOrders INTO _OrderID;
	
	#UNTIL _done END REPEAT;
	
	#CLOSE NewOrders
	
	#INSERT INTO store.Test (Data) VALUES (_LastExecuted);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------