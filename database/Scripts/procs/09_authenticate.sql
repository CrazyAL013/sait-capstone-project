#-------------------------------------------------------------------------------
/*
* All operations related to user authentication.
*
* Content:	AccountLogin
* 					AccountUserExists
*						AccountRoleValidate
*						AccountChangeUserName
*						AccountChangePassword
*						AccountActivate
*						AccountDeactivate
*						AccountVerificationCodeUpdate
*						AccountVerificationCodeValidate
* 					RefreshTokenGet
* 					RefreshTokenValidate
* 					RefreshTokenCreate
* 					RefreshTokenUpdate
* 					RefreshTokenDelete
* 					RefreshTokenDeleteAll
*/
#-------------------------------------------------------------------------------
/*
* Authenticates an account login
*
* @param {string} UserName - A string containing the login user name (e-mail)
* @return {resultset} - A resultset
*/
DROP PROCEDURE IF EXISTS store.AccountLogin;
DELIMITER //
CREATE PROCEDURE store.AccountLogin(
  IN _UserName varchar(100)
)
BEGIN

	SELECT 
		Account.ID,
		BIN_TO_UUID(Account.UID),
		User.UserName,
		User.PasswordHash
	FROM
		Account, User
	WHERE
		Account.ID = User.AccountID AND
		User.UserName = _UserName;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Verifies if a user name exists
*
* @param {string} UserName - A string containing the login user name (e-mail)
* @return {resultset} - True (1) if the user name exists otherwise false (0)
*/
DROP PROCEDURE IF EXISTS store.AccountUserExists;
DELIMITER //
CREATE PROCEDURE store.AccountUserExists(
  IN _UserName varchar(100)
)
BEGIN

	DECLARE _Result boolean DEFAULT FALSE;

	IF EXISTS(
		SELECT 
			* 
		FROM
			User
		WHERE
			User.UserName = LOWER(_UserName)   
	)
	THEN
		SET _Result = TRUE;
	END IF;
	
	SELECT _Result AS 'UserExists';

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Validates the user role for an account
*
* @param {uuid} AccountUID - The UUID of the account
* @param {string} RoleName - A string containing the role name to check
* @return {boolean} - True (1) if the account is in the role otherwise false (0)
*/
DROP PROCEDURE IF EXISTS store.AccountRoleValidate;
DELIMITER //
CREATE PROCEDURE store.AccountRoleValidate(
  IN _AccountUID varchar(36),
  IN _JsonWebToken varchar(200),
  IN _RoleName varchar(100)
)
BEGIN
	DECLARE _Result boolean DEFAULT FALSE;
	DECLARE _ErrorMsg varchar(100);
	
	# Validate the role name to ensure it exists
	IF NOT EXISTS(SELECT * FROM Role WHERE LOWER(Name) = LOWER(_RoleName)) THEN
		SET _ErrorMsg = CONCAT("[ERROR] AccountValidateRole: Unknown role name '", _RoleName, "'");
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = _ErrorMsg;
	END IF;
	
	# Verify account/user has the required role and validate the JWT
	IF EXISTS(
		SELECT 
			* 
		FROM
			Account, User, Role, UserRole
		WHERE
			Account.ID = User.AccountID AND 
			User.ID = UserRole.UserID AND 
			Role.ID = UserRole.RoleID AND 
			LOWER(Role.Name) = LOWER(_RoleName) AND 
			Account.UID = UUID_TO_BIN(_AccountUID)
	)
	THEN
		SET _Result = TRUE;
	END IF;
	
	SELECT _Result AS 'IsInRole';

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates (changes) the user name (login) for an account
*
* @param {uuid} AccountUID - The UUID of the account
* @param {string} UserName - A string containing the user name
*/
DROP PROCEDURE IF EXISTS store.AccountUserNameChange;
DELIMITER //
CREATE PROCEDURE store.AccountUserNameChange(
  IN _AccountUID varchar(36),
	IN _UserName varchar(100)
)
BEGIN

	UPDATE 
		User
	SET 
		UserName = LOWER(_UserName)
	WHERE 
		User.AccountID = ID_FROM_UID(_AccountUID, 'Account');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates (changes) the password hash for an account
*
* @param {uuid} AccountUID - The UUID of the account
* @param {string} PasswordHash - A string containing the hashed password
*/
DROP PROCEDURE IF EXISTS store.AccountPasswordChange;
DELIMITER //
CREATE PROCEDURE store.AccountPasswordChange(
  IN _AccountUID varchar(36),
	IN _PasswordHash char(98)
)
BEGIN

	UPDATE 
		User
	SET 
		PasswordHash = _PasswordHash
	WHERE 
		User.AccountID = ID_FROM_UID(_AccountUID, 'Account');
	
	CALL RefreshTokenDeleteAll(_AccountUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Activates an account
*
* @param {uuid} AccountUID - The UUID of the account to be activated
*/
DROP PROCEDURE IF EXISTS store.AccountActivate;
DELIMITER //
CREATE PROCEDURE store.AccountActivate(
  IN _AccountUID varchar(36)
)
BEGIN

	UPDATE 
		Account
	SET 
		Active = True
	WHERE 
		Account.UID = UUID_TO_BIN(_AccountUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deactivates an account
*
* @param {uuid} AccountUID - The UUID of the account to be deactivate
*/
DROP PROCEDURE IF EXISTS store.AccountDeactivate;
DELIMITER //
CREATE PROCEDURE store.AccountDeactivate(
  IN _AccountUID varchar(36)
)
BEGIN

	UPDATE 
		Account
	SET 
		Active = False
	WHERE 
		Account.UID = UUID_TO_BIN(_AccountUID);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates (sets) a verification code for an account
*
* @param {uuid} AccountUID - The UUID of the account
* @param {string} Code - A string containing the verification code
*/
DROP PROCEDURE IF EXISTS store.AccountVerificationCodeUpdate;
DELIMITER //
CREATE PROCEDURE store.AccountVerificationCodeUpdate(
  IN _AccountUID varchar(36),
	IN _VerificationCode char(100)
)
BEGIN

	UPDATE 
		User
	SET 
		VerificationCode = _VerificationCode
	WHERE 
		User.AccountID = ID_FROM_UID(_AccountUID, 'Account');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Validates a verification code for an account
*
* @param {uuid} AccountUID - The UUID for the account
* @param {string} Code - The verification code to validate
* @return {resultset} True (1) if the verification code is valid toherwise false (0)
*/
DROP PROCEDURE IF EXISTS store.AccountVerificationCodeValidate;
DELIMITER //
CREATE PROCEDURE store.AccountVerificationCodeValidate(
  IN _AccountUID varchar(36),
  IN _VerificationCode varchar(4096)
)
BEGIN

	DECLARE _Result boolean DEFAULT FALSE;

	IF EXISTS(
		SELECT 
			* 
		FROM
			User
		WHERE
			User.AccountID = ID_FROM_UID(_AccountUID, 'Account') AND
			User.VerificationCode = _VerificationCode
	)
	THEN
		SET _Result = TRUE;
	END IF;
	
	SELECT _Result AS 'IsValidCode';

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all refresh tokens for an account
*
* @param {uuid} AccountUID - The UUID for the account
* @return {resultset} The refresh tokens as a resultset 
*/
DROP PROCEDURE IF EXISTS store.RefreshTokensGet;
DELIMITER //
CREATE PROCEDURE store.RefreshTokensGet(
  IN _AccountUID varchar(36)
)
BEGIN

	SELECT 
		RefreshToken.ID,
		Token	
  FROM 
  	Account, RefreshToken
  WHERE 
  	Account.ID = RefreshToken.AccountID AND
 		Account.ID = ID_FROM_UID(_AccountUID, 'Account');

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Validates a refresh token for an account
*
* @param {uuid} AccountUID - The UUID for the account
* @param {string} Token - The refresh token to validate
* @return {resultset} True (1) if the refresh token is valid toherwise false (0)
*/
DROP PROCEDURE IF EXISTS store.RefreshTokenValidate;
DELIMITER //
CREATE PROCEDURE store.RefreshTokenValidate(
  IN _AccountUID varchar(36),
  IN _Token varchar(4096)
)
BEGIN

	DECLARE _Result boolean DEFAULT FALSE;

	IF EXISTS(
		SELECT 
			* 
		FROM
			Account, RefreshToken
		WHERE
			Account.ID = RefreshToken.AccountID AND
			Account.ID = ID_FROM_UID(_AccountUID, 'Account') AND
			RefreshToken.Token = _Token
	)
	THEN
		SET _Result = TRUE;
	END IF;
	
	SELECT _Result AS 'IsValidToken';

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Creates (adds) a refresh token to an account
*
* @param {uuid} AccountUID - The UUID for the account
* @param {string} Token - The refresh token to add
*/
DROP PROCEDURE IF EXISTS store.RefreshTokenCreate;
DELIMITER //
CREATE PROCEDURE store.RefreshTokenCreate(
  IN _AccountUID varchar(36),
  IN _Token varchar(4096)
)
BEGIN

	INSERT INTO RefreshToken
		(AccountID, Token)
	SELECT
		ID_FROM_UID(_AccountUID, 'Account'),
		_Token;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Updates (replaces) a refresh token for an account
*
* @param {string} OldToken - The old refresh token
* @param {string} NewToken - The new refresh token
*/
DROP PROCEDURE IF EXISTS store.RefreshTokenUpdate;
DELIMITER //
CREATE PROCEDURE store.RefreshTokenUpdate(
  IN _AccountUID varchar(36),
  IN _OldToken varchar(4096),
  IN _NewToken varchar(4096)
)
BEGIN

	UPDATE 
		RefreshToken
	SET 
		Token = _NewToken
	WHERE 
 		RefreshToken.AccountID = ID_FROM_UID(_AccountUID, 'Account') AND
 		RefreshToken.Token = _OldToken;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes (revokes) a refresh token for an account
*
* @param {uuid} AccountUID - The UUID for the account
* @param {string} Token - The refresh token to revoke
*/
DROP PROCEDURE IF EXISTS store.RefreshTokenDelete;
DELIMITER //
CREATE PROCEDURE store.RefreshTokenDelete(
  IN _AccountUID varchar(36),
  IN _Token varchar(4096)
)
BEGIN

	DELETE FROM 
		RefreshToken
	WHERE 
 		RefreshToken.AccountID = ID_FROM_UID(_AccountUID, 'Account') AND
 		RefreshToken.Token = _Token;

END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes (revokes) all refresh tokens for an account
*
* @param {uuid} AccountUID - The UUID for the account
*/
DROP PROCEDURE IF EXISTS store.RefreshTokenDeleteAll;
DELIMITER //
CREATE PROCEDURE store.RefreshTokenDeleteAll(
  IN _AccountUID varchar(36)
)
BEGIN

	DELETE FROM 
		RefreshToken
	WHERE 
 		RefreshToken.AccountID = ID_FROM_UID(_AccountUID, 'Account');

END //
DELIMITER ;
