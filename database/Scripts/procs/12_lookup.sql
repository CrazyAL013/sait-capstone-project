#-------------------------------------------------------------------------------
/*
* All operations related to lookup tables
* 
* Content:	CountryGetAll
* 					OrderStatusGetAll
* 					UserRoleGetAll
* 					DiscountTypeGetAll
* 					PaymentProcessorGetAll
* 					SupplierGetAll
* 					ManufacturerGetAll
* 
*/
#-------------------------------------------------------------------------------
/*
* Retrieves all countries and regions
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.CountryGetAll;
DELIMITER //
CREATE PROCEDURE store.CountryGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', Country.ID,
		'name', Country.Name,
		'code', Country.Code,
		'currencyCode', Country.CurrencyCode,
		'currencySymbol', Country.CurrencySymbol,
		'tax', Country.Tax,
		'regions', (SELECT JSON_ARRAYAGG(JSON_OBJECT(
						'id', Region.ID,
						'countryId', Region.CountryID,
						'name', Region.Name,
						'code', Region.Code,
						'tax', Region.Tax
					))
					FROM 
						Region
					WHERE
						Region.CountryID = Country.Id
    	)
	))
	FROM
		Country
	ORDER BY
		Country.Name;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all order statuses
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.OrderStatusGetAll;
DELIMITER //
CREATE PROCEDURE store.OrderStatusGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(OrderStatus.UID),
		'title', OrderStatus.Title,
		'description', OrderStatus.Description
	))
	FROM
		OrderStatus
	ORDER BY
		OrderStatus.ID;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all user roles
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.UserRoleGetAll;
DELIMITER //
CREATE PROCEDURE store.UserRoleGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(Role.UID),
		'name', Role.Name
	))
	FROM
		Role;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all discount types
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.DiscountTypeGetAll;
DELIMITER //
CREATE PROCEDURE store.DiscountTypeGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(DiscountType.UID),
		'name', DiscountType.Name,
		'description', DiscountType.Description
	))
	FROM
		DiscountType;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all payment processors
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.PaymentProcessorGetAll;
DELIMITER //
CREATE PROCEDURE store.PaymentProcessorGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(PaymentProcessor.UID),
		'name', PaymentProcessor.Name,
		'logo', PaymentProcessor.Logo
	))
	FROM
		PaymentProcessor;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all suppliers
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.SupplierGetAll;
DELIMITER //
CREATE PROCEDURE store.SupplierGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(Supplier.UID),
		'name', Supplier.Name,
		'email', Supplier.Email,
		'phone', Supplier.Phone,
		'fax', Supplier.Fax,
		'address', Supplier.Address,
		'contacts', Supplier.Contacts
	))
	FROM
		Supplier
	ORDER BY
		Supplier.ID;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Retrieves all manufacturers
*
* @return {string} - The results formatted as a JSON string
*/
DROP PROCEDURE IF EXISTS store.ManufacturerGetAll;
DELIMITER //
CREATE PROCEDURE store.ManufacturerGetAll()
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'id', BIN_TO_UUID(Manufacturer.UID),
		'name', Manufacturer.Name,
		'website', Manufacturer.website
	))
	FROM
		Manufacturer
	ORDER BY
		Manufacturer.ID;
END //
DELIMITER ;
#-------------------------------------------------------------------------------
