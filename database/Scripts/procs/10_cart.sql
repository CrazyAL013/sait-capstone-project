#-------------------------------------------------------------------------------
/*
* All operations related to the shopping cart.
*
* Content:	CartUpdate
* 					CartDelete
*/
#-------------------------------------------------------------------------------
/*
* Updates the saved cart (for an account)
*
* @param {uuid} AccountUID - The UUID of the account
* @param {string} Cart - A (JSON) string containing the cart items
*/
DROP PROCEDURE IF EXISTS store.CartUpdate;
DELIMITER //
CREATE PROCEDURE store.CartUpdate(
	IN _AccountUID varchar(36),
	IN _Cart text
)
BEGIN
	UPDATE 
		Account
	SET 
		Cart = _Cart
	WHERE
		UID = UUID_TO_BIN(_AccountUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Deletes a saved cart (for an account)
*
* @param {uuid} AccountUID - The UUID of the account
*/
DROP PROCEDURE IF EXISTS store.CartDelete;
DELIMITER //
CREATE PROCEDURE store.CartDelete(
	IN _AccountUID varchar(36)
)
BEGIN
	UPDATE 
		Account
	SET
		Cart = NULL
	WHERE
		UID = UUID_TO_BIN(_AccountUID);
END //
DELIMITER ;
#-------------------------------------------------------------------------------
/*
* Fetches the product items for a cart
*
* @param {uuid,uuid...} ProductItemUIDList - A comma-delimited list of product item UUID's
* @return {string} - The results formatted as a JSON string (array)
*/
DROP PROCEDURE IF EXISTS store.CartGetItems;
DELIMITER //
CREATE PROCEDURE store.CartGetItems(
	IN _ProductItemUIDList text
)
BEGIN

	SET @sql = CONCAT(
		"SELECT JSON_ARRAYAGG(
			GetCartItemJSON(ProductItem.ID)
		)
		FROM 
			ProductItem
		WHERE 
			BIN_TO_UUID(ProductItem.UID) IN (", _ProductItemUIDList, ")"
	);

	PREPARE statement FROM @sql;
	EXECUTE statement;
	DEALLOCATE PREPARE statement;
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------