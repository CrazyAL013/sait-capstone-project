USE store;

select ID_FROM_UID('e7377a32-5ad3-11eb-b72c-9bddd5662652','Product');

select ID,  BIN_TO_UUID(UID) from Image i;

#-------------------------------------------------------------------------------
# Testing Full-Text Search
#-------------------------------------------------------------------------------

# Keyword search
CALL SearchByKeyword('', '');
CALL SearchByKeyword('danner ap_3', 'high');
CALL SearchByKeyword('danner ap_3', 'low');
CALL SearchByKeyword('danner ap_3', 'stock');
CALL SearchByKeyword('danner ap_3', 'rating');

# Keyword+Category search
CALL SearchByCategoryKeyword('dd6b54f2-6199-11eb-ae93-0242ac130002','danner ap_3', '');
CALL SearchByCategoryKeyword('dd6b54f2-6199-11eb-ae93-0242ac130002','danner ap_3', 'high');
CALL SearchByCategoryKeyword('dd6b54f2-6199-11eb-ae93-0242ac130002','danner ap_3', 'low');
CALL SearchByCategoryKeyword('dd6b54f2-6199-11eb-ae93-0242ac130002','danner ap_3', 'stock');
CALL SearchByCategoryKeyword('dd6b54f2-6199-11eb-ae93-0242ac130002','danner ap_3', 'rating');

# Keyword+Subcategory search
CALL SearchBySubcategoryKeyword('9e61e9e9-1bdb-403d-b287-b28ca6f58c76','danner ap_3', '');
CALL SearchBySubcategoryKeyword('9e61e9e9-1bdb-403d-b287-b28ca6f58c76','danner ap_3', 'high');
CALL SearchBySubcategoryKeyword('9e61e9e9-1bdb-403d-b287-b28ca6f58c76','danner ap_3', 'low');
CALL SearchBySubcategoryKeyword('9e61e9e9-1bdb-403d-b287-b28ca6f58c76','danner ap_3', 'stock');
CALL SearchBySubcategoryKeyword('9e61e9e9-1bdb-403d-b287-b28ca6f58c76','danner ap_3', 'rating');

# Category Search
CALL SearchByCategory('dd6b54f2-6199-11eb-ae93-0242ac130002', '');
CALL SearchByCategory('dd6b54f2-6199-11eb-ae93-0242ac130002', 'high');
CALL SearchByCategory('dd6b54f2-6199-11eb-ae93-0242ac130002', 'low');
CALL SearchByCategory('dd6b54f2-6199-11eb-ae93-0242ac130002', 'stock');
CALL SearchByCategory('dd6b54f2-6199-11eb-ae93-0242ac130002', 'rating');

# Subcategory Search
CALL SearchBySubcategory('9e61e9e9-1bdb-403d-b287-b28ca6f58c76', '');
CALL SearchBySubcategory('9e61e9e9-1bdb-403d-b287-b28ca6f58c76', 'high');
CALL SearchBySubcategory('9e61e9e9-1bdb-403d-b287-b28ca6f58c76', 'low');
CALL SearchBySubcategory('9e61e9e9-1bdb-403d-b287-b28ca6f58c76', 'stock');
CALL SearchBySubcategory('9e61e9e9-1bdb-403d-b287-b28ca6f58c76', 'rating');

#-------------------------------------------------------------------------------
# Testing Account
#-------------------------------------------------------------------------------
CALL AccountGetAll();
CALL AccountGetById('e7377a32-5ad3-11eb-b72c-9bddd5662652');
CALL AddressGetAll('51640cb5-a2f0-442e-9725-7c8b546dc261');
CALL AccountOrderGetAll('e7377a32-5ad3-11eb-b72c-9bddd5662652');
	
CALL AddressCreate('5bee59dc-5578-41c0-9083-e96b403ac1cd', '51640cb5-a2f0-442e-9725-7c8b546dc261', 'Home', '123 house', null, 'Ceres', '123456', 7, 1, @ret);
SELECT BIN_TO_UUID(UID), FullName FROM Account;
SELECT FullName,ISNULL(Account.AddressID) FROM Account WHERE Account.UID = UUID_TO_BIN('51640cb5-a2f0-442e-9725-7c8b546dc261');

#-------------------------------------------------------------------------------
# Testing Authentication
#-------------------------------------------------------------------------------
CALL AccountUserExists('1@2.3');
CALL AccountRoleValidate('e7377a32-5ad3-11eb-b72c-9bddd5662652','test','user');
CALL RefreshTokenCreate('e7377a32-5ad3-11eb-b72c-9bddd5662652', '<TEST__REFRESH__TOKEN_2>');
CALL RefreshTokensGet('e7377a32-5ad3-11eb-b72c-9bddd5662652');
CALL RefreshTokenValidate('e7377a32-5ad3-11eb-b72c-9bddd5662652', '<TEST__REFRESH__TOKEN>');
CALL RefreshTokenUpdate('e7377a32-5ad3-11eb-b72c-9bddd5662652', '<TEST__REFRESH__TOKEN>','<UPDATE__REFRESH__TOKEN>');
CALL RefreshTokenDelete('e7377a32-5ad3-11eb-b72c-9bddd5662652', '<UPDATE__REFRESH__TOKEN>');
CALL RefreshTokenDeleteAll('e7377a32-5ad3-11eb-b72c-9bddd5662652');

CALL AccountVerificationCodeUpdate('e7377a32-5ad3-11eb-b72c-9bddd5662652','TEST__VERIFY__CODE_1');
CALL AccountVerificationCodeValidate('e7377a32-5ad3-11eb-b72c-9bddd5662652','TEST__VERIFY__CODE_1');

CALL AccountUserNameChange('e7377a32-5ad3-11eb-b72c-9bddd5662652','a@b.c');
CALL AccountPasswordChange('e7377a32-5ad3-11eb-b72c-9bddd5662652','NEW__PASSWORD__HASH');

CALL AccountActivate('e7377a32-5ad3-11eb-b72c-9bddd5662652');
CALL AccountDeactivate('e7377a32-5ad3-11eb-b72c-9bddd5662652');
SELECT * FROM User;
SELECT * FROM Account;

#-------------------------------------------------------------------------------
# Testing Lookups
#-------------------------------------------------------------------------------
CALL CountryGetAll();
CALL OrderStatusGetAll();
CALL UserRoleGetAll();
CALL DiscountTypeGetAll();
CALL PaymentProcessorGetAll();
CALL SupplierGetAll();
CALL ManufacturerGetAll();

#-------------------------------------------------------------------------------
# Testing Product:
#-------------------------------------------------------------------------------
CALL ProductGetByID("6cd4de5c-5ad2-11eb-8916-6339784c3517");

SELECT GetProductJSON(2);
SELECT GetSupplierJSON(7);

SELECT GetProductItemImagesJSON(1);

CALL ProductAddSubCategory("6cd4de5c-5ad2-11eb-8916-6339784c3517", "06a21467-7ed7-4b25-809f-fff8c91341dd", "3e42dceb-3a7b-4551-8ea5-ef5c3fadd59c");
CALL ProductRemoveSubCategory("6cd4de5c-5ad2-11eb-8916-6339784c3517", "3e42dceb-3a7b-4551-8ea5-ef5c3fadd59c");

CALL ProductItemCreate(
	'a777545c-9907-4183-9eb1-bb8c126e94e5',
	'6cd4de5c-5ad2-11eb-8916-6339784c3517',
	false,
	'',
	'',
	'',
	'',
	'',
	'test',
	1.99,
	4,
	2,
	1,
	1,
	@ret
);
CREATE PROCEDURE store.ProductItemCreate(
	IN _ProductItemUID varchar(36),
	IN _ProductUID varchar(36),
	IN _Display boolean,
	IN _Model varchar(50),
	IN _SKU varchar(12),
	IN _UPC char(12),
	IN _Name varchar(100),
	IN _Description text,
	IN _Keywords varchar(500),
	IN _UnitPrice decimal(15,2),
	IN _Weight decimal(15,2),
	IN _StockQuantity int,
	IN _ReorderLevel int,
	IN _TotalSold int,
	OUT _ID int
);
#-------------------------------------------------------------------------------
# Testing Cart:
#-------------------------------------------------------------------------------
CALL store.CartGetItems('"5563e9ba-5ad3-11eb-92f1-57c53480831a","52d8ae42-5ad3-11eb-90c7-a34765493837","4e009290-5ad3-11eb-8173-0f02e9613d92","49f8fc14-5ad3-11eb-9526-17c8e3dbfd8e","46e289a0-5ad3-11eb-91c9-7759eb6abcdf","4456a982-5ad3-11eb-a351-3fc1e4ba964d","4208eabe-5ad3-11eb-ac85-5b088aad8d46","3f5e62bc-5ad3-11eb-9150-f79f9b51b2c2","c5c3be40-6066-11eb-ae93-0242ac130002","5dd103f0-6067-11eb-ae93-0242ac130002","62c92dc4-6067-11eb-ae93-0242ac130002","395cc71e-5ad3-11eb-b935-17a08d9a9b9b"');

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Testing Functions
# NOTE: This doesn't test accuracy of results just that the queries execute.
#-------------------------------------------------------------------------------
SELECT 
	GetAccountJSON(4),
	GetUserRolesJSON(1),
	GetAddressJSON(1),
	GetDiscountJSON(1),
	GetShipmentJSON(1),
	GetPaymentProcessorJSON(1),
	GetOrderStatusJSON(1),
	GetOrderJSON(1),
	GetOrderItemsJSON(1),
	GetSupplierJSON(1),
	GetManufacturerJSON(1),
	GetProductAverageRating(1),
	GetProductItemAverageRating(1),
	GetProductJSON(1),
	GetProductItemsJSON(1),
	GetCartItemJSON(1),
	GetProductCategoriesJSON(4);

#-------------------------------------------------------------------------------

SELECT * FROM INFORMATION_SCHEMA.events;
DESCRIBE INFORMATION_SCHEMA.events;

SELECT LAST_EXECUTED FROM INFORMATION_SCHEMA.events WHERE EVENT_NAME="MaintenanceEvent";


SELECT ID, BIN_TO_UUID(UID) FROM `Order`;
CALL OrderGetById("02158504-b1fe-4b87-b60d-9b059a407569");
CALL OrderGetALL();
#-------------------------------------------------------------------------------
# Testing Orders: WARNING - DELETES ALL ORDERS/ORDER ITEMS/SHIPMENTS
#------------------------------------------------------------------------------

SELECT ID, BIN_TO_UUID(UID) FROM `Order`;


DELETE FROM `Order`;
DELETE FROM OrderItem;
DELETE FROM Shipment;

# Create a shipment
CALL OrderShipmentCreate(
	"df173bf0-94dc-4f9f-b394-41ea61ef4aed",		#ShipmentUID
	"rate_0cf466d2ac6f49f487b45a9514127bdc",	#RateID
	"shp_56b39dc880b44bd6ace38006e94ba3be",		#ShipmentID
  "DHL",																		#Carrier
  "Express",																#Service
  19.99,																		#Rate
  NULL,																			#TrackingNumber
  14,																				#DeliveryDays
  NULL,																			#ShipDate
  @result
);
SELECT * FROM Shipment;

CALL OrderShipmentUpdate(
	"df173bf0-94dc-4f9f-b394-41ea61ef4aed",		#ShipmentUID
	"rate_0cf466d2ac6f49f487b45a9514127bdc",	#RateID
	"shp_56b39dc880b44bd6ace38006e94ba3be",		#ShipmentID
  "DHL",																		#Carrier
  "Express",																#Service
  99.99,																		#Rate
  NULL,																			#TrackingNumber
  99,																				#DeliveryDays
  NULL																			#ShipDate
);
SELECT * FROM Shipment;


# Create an order
CALL OrderCreate(
	"4c4cef2c-7c7c-4e47-86c3-94b8cc965eeb",		#OrderUID
	"e7377a32-5ad3-11eb-b72c-9bddd5662652",		#AccountUID
	"34819C92-B35F-C1F3-B021-749CAAE1BCD3",		#BillingAddressUID
	"B5CB5B3C-B770-4D6F-9BAA-649238C16B76",		#DeliveryAddressUID
	NULL,																			#DiscountUID
	"df173bf0-94dc-4f9f-b394-41ea61ef4aed",		#ShipmentUID
	"8fe47cc2-6ff2-4918-8820-73c3bcf5359a",		#PaymentProcessorUID
	NULL,																			#TransactionID
	19.99,																		#ShipTotal
	2.99,																			#TaxTotal
	0,																				#DiscountTotal
	62.97,																		#GrandTotal
	@result
);
SELECT * FROM `Order`;

# Create order items
CALL OrderItemCreate(
	"f59ae0fe-c21c-4cac-96cc-5af8df1a1771",		#OrderItemUID
	"4c4cef2c-7c7c-4e47-86c3-94b8cc965eeb",		#OrderUID
	"52d8ae42-5ad3-11eb-90c7-a34765493837",		#ProductItemUID
	2,																				#Quantity
	19.99,																		#UnitPrice
	@result
);
SELECT * FROM OrderItem;

# Update order
CALL OrderUpdate(
	"4c4cef2c-7c7c-4e47-86c3-94b8cc965eeb",		#OrderUID
	"e7377a32-5ad3-11eb-b72c-9bddd5662652",		#AccountUID
	"ce71a44e-6199-13eb-ae93-0242ac130062",		#OrderStatusUID	
	"34819C92-B35F-C1F3-B021-749CAAE1BCD3",		#BillingAddressUID
	"B5CB5B3C-B770-4D6F-9BAA-649238C16B76",		#DeliveryAddressUID
	NULL,																			#DiscountUID
	"df173bf0-94dc-4f9f-b394-41ea61ef4aed",		#ShipmentUID
	"8fe47cc2-6ff2-4918-8820-73c3bcf5359a",		#PaymentProcessorUID
	NULL,																			#TransactionID
	99.99,																		#ShipTotal
	9.99,																			#TaxTotal
	0,																				#DiscountTotal
	999.99 																		#GrandTotal
);
SELECT * FROM `Order`;

# Update order item
CALL OrderItemUpdate(
	"f59ae0fe-c21c-4cac-96cc-5af8df1a1771",		#OrderItemUID
	"4c4cef2c-7c7c-4e47-86c3-94b8cc965eeb",		#OrderUID
	"52d8ae42-5ad3-11eb-90c7-a34765493837",		#ProductItemUID
	99,																				#Quantity
	99.99																			#UnitPrice
);
SELECT * FROM OrderItem;
#-------------------------------------------------------------------------------
# Testing Reviews
#-------------------------------------------------------------------------------
CALL AccountReviewGetAll('e7377a32-5ad3-11eb-b72c-9bddd5662652');
CALL ProductReviewsGetAll('5dbbb7f6-5ad2-11eb-8c12-2b01a4dddde0');

#-------------------------------------------------------------------------------

