#-------------------------------------------------------------------------------
/*
* Populate lookup tables with data
* NOTE: The ID/UID values used here are also enumerated in the code - DO NOT
* 			change the ID/UID values below without also updating the code.
* 
* Content:	OrderStatus
* 					Role
* 					PaymentProcessor
* 					DiscountType
* 					Category
* 					Subcategory
* 					Region
* 					Country
*/
#-------------------------------------------------------------------------------
INSERT INTO store.OrderStatus
	(ID, UID, Title, Description)
VALUES
	(1, (UUID_TO_BIN('41509b3c-00fd-46ba-a389-ccd9f184a37d')), 'New', 'Your order has been received.'),
	(2, (UUID_TO_BIN('ce71a44e-6199-13eb-ae93-0242ac130062')), 'In Progress', 'We are preparing your order for shipment.'),
	(3, (UUID_TO_BIN('dd6b54f1-6199-17eb-ae93-0242ac140002')), 'Shipped', 'Your order has been shipped.'),
	(4, (UUID_TO_BIN('014d3aae-619a-18eb-ae93-0242ac130502')), 'Delivered', 'Your order has been delivered.'),
	(5, (UUID_TO_BIN('014d3aae-619a-18eb-ae93-0242ac130502')), 'Cancelled', 'Your order has been cancelled and payment refunded.'),
	(6, (UUID_TO_BIN('06a23467-7ed7-2b25-809f-fff8c91342dd')), 'Delayed', 'There was a problem with your order.');
#-------------------------------------------------------------------------------
INSERT INTO store.Role
	(ID, UID, Name)
VALUES
	(1,(UUID_TO_BIN("2c1b718b-9da0-40f5-9b3c-061c2dde4396")),'user'),
	(2,(UUID_TO_BIN("99052f08-1316-4de4-90f7-f85145e8af97")),'admin'),
	(3,(UUID_TO_BIN("fbbde3f4-721b-40d9-9819-aeeffb152617")),'super');
#-------------------------------------------------------------------------------
INSERT INTO store.PaymentProcessor
	(ID, UID, Name, Logo)
VALUES
	(1,(UUID_TO_BIN("8fe47cc2-6ff2-4918-8820-73c3bcf5359a")),'Square','square.png');
#-------------------------------------------------------------------------------
INSERT INTO store.DiscountType
    (ID, UID, Name, Description)
VALUES
	(1,(UUID_TO_BIN("4f64e5f8-20a7-41d5-a9b5-edc777bdfec1")),'SHIPPING', 'Discount applied to shipping cost'),
	(2,(UUID_TO_BIN("be210eba-8a0d-42a5-a6f4-972dc6887307")),'NET', 'Discount applied to purchases before tax/shipping');
#-------------------------------------------------------------------------------
INSERT INTO store.Category
    (ID, UID, Name)
VALUES
	(1, (UUID_TO_BIN('ce71a24e-6199-11eb-ae93-0242ac130002')), 'Additives & Media'),
	(2, (UUID_TO_BIN('d6dae440-6199-11eb-ae93-0242ac130002')), 'Air Pump Supplies'),
	(3, (UUID_TO_BIN('dd6b54f2-6199-11eb-ae93-0242ac130002')), 'Air Pumps'),
	(4, (UUID_TO_BIN('fa1e85ba-6199-11eb-ae93-0242ac130002')), 'Aquariums'),
	(5, (UUID_TO_BIN('fddbbff6-6199-11eb-ae93-0242ac130002')), 'Cleaning Supplies'),
	(6, (UUID_TO_BIN('014d1aae-619a-11eb-ae93-0242ac130002')), 'Controllers & Monitors'),
	(7, (UUID_TO_BIN('0640d366-619a-11eb-ae93-0242ac130002')), 'Filter Media'),
	(8, (UUID_TO_BIN('0a617216-619a-11eb-ae93-0242ac130002')), 'Food & Vitamins'),
	(9, (UUID_TO_BIN('7f3a0184-3ec1-478e-a7c4-282512a4da08')), 'Heaters'),
	(10, (UUID_TO_BIN('06a21467-7ed7-4b25-809f-fff8c91341dd')), 'Lighting - LED');
#-------------------------------------------------------------------------------
INSERT INTO store.Subcategory
    (ID, UID, CategoryID, Name)
VALUES
	(1,UUID_TO_BIN('d1669409-69ad-456c-8f78-e87cd44d0bc2'),1,'2-Part Additives'),
	(2,UUID_TO_BIN('75540857-2514-400f-9fc5-cfc576ab2ebe'),1,'Aquaforest System'),
	(3,UUID_TO_BIN('ed0a85d2-3936-4da8-93a5-ffbaa26eb5be'),1,'Bacterial Additives'),
	(4,UUID_TO_BIN('60e88db1-78b9-441f-a06c-89dcf6854dbf'),1,'Bulk Chemicals'),
	(5,UUID_TO_BIN('53445fb4-fe35-4806-bb69-11984316cbba'),1,'Carbon'),
	(6,UUID_TO_BIN('d44042fc-0929-4f86-a1ee-ad13a998f75d'),1,'Specialized Additives'),
	(7,UUID_TO_BIN('c7d91bbb-b72a-467a-8198-149248b40bdd'),1,'Freshwater Additives'),
	(8,UUID_TO_BIN('ab16416f-d59b-4de8-8fa7-589e33db2c85'),1,'Major Element Additives'),
	(9,UUID_TO_BIN('72dbbb4e-fb47-4515-b9ea-aa7c2f33e004'),1,'Minor Element Additives'),
	(10,UUID_TO_BIN('7b9d9645-a81a-4b72-a8ac-3f8573d2ce60'),1,'Specialized Media'),
	(11,UUID_TO_BIN('470fdfc6-720e-4a64-a63f-4cfe4b5c4209'),1,'Zeovit Additives'),
	(12,UUID_TO_BIN('63f81d8e-c107-4ba7-9fb8-b1f81fc64657'),2,'Airline Tubing & Fittings'),
	(13,UUID_TO_BIN('0f34eca1-98eb-4a90-99d6-dd813dbdada0'),2,'Airstones & Diffusers'),
	(14,UUID_TO_BIN('dc45e352-0e85-4f4e-b6f9-1d963be1f111'),2,'Other Air Pump Supplies'),
	(15,UUID_TO_BIN('9e61e9e9-1bdb-403d-b287-b28ca6f58c76'),3,'Aquarium Air Pumps'),
	(16,UUID_TO_BIN('9b0062a9-5100-46d2-9091-c73dafb08ed8'),3,'Heavy Duty Air Pumps'),
	(17,UUID_TO_BIN('e4aa8623-703b-4ebf-ac09-fd13d337a122'),3,'Airline Tubing & Fittings'),
	(18,UUID_TO_BIN('9ba0e600-06b6-4493-8881-99b0fb6fc6c3'),4,'Aquarium Accessories'),
	(19,UUID_TO_BIN('64e2b858-42a6-4da3-8890-f6c7ee6240b5'),4,'Aquariums & Packages'),
	(20,UUID_TO_BIN('5281c14e-4b5b-4f9b-a288-734e8ebd1169'),4,'Aquarium Furniture'),
	(21,UUID_TO_BIN('d57dc386-3a90-4ba0-8ab3-3ed7af26bba8'),5,'Algae Scrapers & Pads'),
	(22,UUID_TO_BIN('8877c5e1-9035-483d-a251-7628e45d4639'),5,'Cleaning Brushes'),
	(23,UUID_TO_BIN('dec677de-b18b-469f-8bcf-efa742aa3a57'),5,'Gravel Cleaners'),
	(24,UUID_TO_BIN('bbe9d485-495e-4d08-beef-01655927fb14'),5,'Magnets & Accessories'),
	(25,UUID_TO_BIN('4e795d52-acfb-4dc2-bbdb-20e7398420ff'),5,'Other Cleaning Supplies'),
	(26,UUID_TO_BIN('502d2b55-cd8a-46ed-be33-a92a6688dfee'),6,'Aquarium Controllers'),
	(27,UUID_TO_BIN('23575414-66b1-4a4a-92e3-216b68e22fed'),6,'Controller Modules'),
	(28,UUID_TO_BIN('9283a7c6-6997-4d5b-b9ef-9d662fc60cb1'),6,'Controller Packages'),
	(29,UUID_TO_BIN('7c3de62f-a294-4e7c-8315-5a73e3ea3004'),6,'Controller Accessories'),
	(30,UUID_TO_BIN('3955d155-e666-4bb4-b54c-de337b333f2f'),6,'Replacement Probes & Parts'),
	(31,UUID_TO_BIN('873a526d-667c-4dbf-84dd-e5388581320c'),6,'Digital Monitors'),
	(32,UUID_TO_BIN('4365ac7e-0596-49de-bf6b-8802f84235c1'),6,'Hydros Controllers'),
	(33,UUID_TO_BIN('3bc66cf0-ff98-487f-9ea1-06d740ba4dc4'),7,'Canister Filter Media'),
	(34,UUID_TO_BIN('7f1d237b-c697-4318-a595-2c776792f6d0'),7,'Filter Bags & Socks'),
	(35,UUID_TO_BIN('1e2599d9-2213-4720-9e6d-a23039eccc4d'),7,'Hang-On Filter Media'),
	(36,UUID_TO_BIN('558e2396-2cc8-4c43-9ea8-31aafc24c5d2'),7,'Specialized Filter Media'),
	(37,UUID_TO_BIN('43263ea1-51bb-43fb-99e8-3e89053e7957'),7,'Other Filter Media'),
	(38,UUID_TO_BIN('dd275809-587a-4ee6-9d35-2aa334fd617e'),8,'Coral Food'),
	(39,UUID_TO_BIN('e50c883d-4ac6-4118-9e17-ad04380c60ae'),8,'Dried Fish Food'),
	(40,UUID_TO_BIN('c437c3c3-29e7-41f3-a583-6f749f3395b6'),8,'Feeders & Accessories'),
	(41,UUID_TO_BIN('af51ef0a-10c1-488c-a786-2d23ef0a317c'),8,'Food Additives & Vitamins'),
	(42,UUID_TO_BIN('5bdb0e6c-c45e-4bc8-aa26-1afc505891aa'),8,'Frozen & Refrigerated Food'),
	(43,UUID_TO_BIN('01c02771-8182-4259-9f17-0b54cddb3ed6'),8,'Pellet & Flake Fish Food'),
	(44,UUID_TO_BIN('f331afc4-1578-4a9c-8d99-9f90af212f25'),9,'Controllable Heaters'),
	(45,UUID_TO_BIN('332c6561-0e1f-49bb-83aa-489b57efea57'),9,'Submersible Heaters'),
	(46,UUID_TO_BIN('dc4b886d-556c-4963-80cc-427ee7311f9a'),9,'Titanium Heaters'),
	(47,UUID_TO_BIN('9b9ad38f-9b0c-45d1-ab5c-43e8d8643019'),10,'ATI LED Lighting'),
	(48,UUID_TO_BIN('9dea79d8-a5ec-44d5-bfbb-2cf7f9aa2166'),10,'AI LED Fixtures'),
	(49,UUID_TO_BIN('3236ab5e-536f-4538-8c85-6244b930c2e0'),10,'Current USA LED Lighting'),
	(50,UUID_TO_BIN('d9402056-5090-4d5a-b2b8-b211dd9188a2'),10,'Ecotech Radion Fixtures'),
	(51,UUID_TO_BIN('c86b5356-bb45-4738-9c11-26abe5420581'),10,'Kessil LED Lighting'),
	(52,UUID_TO_BIN('90cf3bad-1ee1-473a-bd20-d6deb2329db0'),10,'Maxspect Led Lights'),
	(53,UUID_TO_BIN('b0bb5ede-bd01-4796-a48a-282039dbfcce'),10,'Reef Brite LED'),
	(54,UUID_TO_BIN('3e42dceb-3a7b-4551-8ea5-ef5c3fadd59c'),10,'Red Sea ReefLED');
#-------------------------------------------------------------------------------
INSERT INTO store.Region
    (CountryID, Name, Code, Tax)
VALUES
    (1,'Alberta','AB',0),
    (1,'British Columbia','BC',7.0),
    (1,'Manitoba','MB',8.0),
    (1,'New Brunswick','NB',10.0),
    (1,'Newfoundland & Labrador','NL',10.0),
    (1,'Northwest Territories','NT',0),
    (1,'Nova Scotia','NS',10.0),
    (1,'Nunavut','NT',0),
    (1,'Ontario','ON',8.0),
    (1,'Prince Edward Island','PE',10.0),
    (1,'Quebec','QC',9.975),
    (1,'Saskatchewan','SK',6.0),
    (1,'Yukon','YT',0),
    (2,'Alabama','AL',0),
    (2,'Alaska','AK',0),
    (2,'Arizona','AZ',0),
    (2,'Arkansas','AR',0),
    (2,'California','CA',0),
    (2,'Colorado','CO',0),
    (2,'Connecticut','CT',0),
    (2,'Delaware','DE',0),
    (2,'Florida','FL',0),
    (2,'Georgia','GA',0),
    (2,'Hawaii','HI',0),
    (2,'Idaho','ID',0),
    (2,'Illinois','IL',0),
    (2,'Indiana','IN',0),
    (2,'Iowa','IA',0),
    (2,'Kansas','KS',0),
    (2,'Kentucky','KY',0),
    (2,'Louisiana','LA',0),
    (2,'Maine','ME',0),
    (2,'Maryland','MD',0),
    (2,'Massachusetts','MA',0),
    (2,'Michigan','MI',0),
    (2,'Minnesota','MN',0),
    (2,'Mississippi','MS',0),
    (2,'Missouri','MO',0),
    (2,'Montana','MT',0),
    (2,'Nebraska','NE',0),
    (2,'Nevada','NV',0),
    (2,'New Hampshire','NH',0),
    (2,'New Jersey','NJ',0),
    (2,'New Mexico','NM',0),
    (2,'New York','NY',0),
    (2,'North Carolina','NC',0),
    (2,'North Dakota','ND',0),
    (2,'Ohio','OH',0),
    (2,'Oklahoma','OK',0),
    (2,'Oregon','OR',0),
    (2,'Pennsylvania','PA',0),
    (2,'Rhode Island','RI',0),
    (2,'South Carolina','SC',0),
    (2,'South Dakota','SD',0),
    (2,'Tennessee','TN',0),
    (2,'Texas','TX',0),
    (2,'Utah','UT',0),
    (2,'Vermont','VT',0),
    (2,'Virginia','VA',0),
    (2,'Washington','WA',0),
    (2,'West Virginia','WV',0),
    (2,'Wisconsin','WI',0),
    (2,'Wyoming','WY',0);
#-------------------------------------------------------------------------------
INSERT INTO store.Country
    (ID, Name, Code, CurrencyCode, CurrencySymbol, Tax)
VALUES
    (1,'Canada','CA', 'CAD', '$', 5.0),
    (2,'United States','US', 'USD', '$', 0);
#-------------------------------------------------------------------------------