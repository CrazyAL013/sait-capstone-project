#-------------------------------------------------------------------------------
/*
* Load test data
*/
#-------------------------------------------------------------------------------

INSERT INTO store.Account
	(ID, UID, AddressID, Active, FullName, PreferredName, EMail, JoinDate, PhoneNumber, Cart)
VALUES
	(1,(UUID_TO_BIN("e7377a32-5ad3-11eb-b72c-9bddd5662652")),2,true,"Homer Simpson","Homer","1@2.3","2020-04-03 03:37:31","(372) 279-6047",'[["5563e9ba-5ad3-11eb-92f1-57c53480831a",{"id":"f3c10eb9-3cde-4465-be4e-bd24f9b2a845","quantity":4}]]'),
	(2,(UUID_TO_BIN("ea4e51fa-5ad3-11eb-a201-a7261ebdda82")),5,true,"Philip J. Fry","Fry","2@3.4","2021-03-19 13:57:59","(574) 878-8636",NULL),
	(3,(UUID_TO_BIN("286d2626-5adb-11eb-8567-6f6f96ddd047")),8,true,"Bender Bending Rodríguez","Bender","9@9.9","2020-09-27 17:06:37","(879) 986-2155",NULL),
	(4,(UUID_TO_BIN("5e47e600-2d43-4f71-a3a9-4cfe7c457ae1")),NULL,1,'Bobby Bob Bobster','Dave','4@5.6','2021-03-24 21:59:56',NULL,NULL);

#-------------------------------------------------------------------------------
INSERT INTO store.User
    (AccountID, userName, PasswordHash)
VALUES
	(1,'1@2.3','$argon2i$v=19$m=1048576,t=4,p=8$nBK6IxV3OYPHN3n1S/1j/g$eDe3WpsVZL325CQFnZsFVsT8QYvtTTHxyCBjJo94IrQ'),
	(2,'2@3.4','$argon2i$v=19$m=1048576,t=4,p=8$l4Xt80qJexSDur0uEEaRAg$wu+BWOf3Vu/Xo231EHQKA5cJ6iID1uHnzKeMd7Vpa64'),
	(3,'9@9.9','$argon2i$v=19$m=1048576,t=4,p=8$Uu1TdlZX92rSFzcWlqqygQ$Vrjvl6vNsqDANfoYp3+ULso4p8F2eh9Y/Eb+z4QMkT0'),	
	(4,'4@5.6','$argon2i$v=19$m=1048576,t=4,p=8$yNVw210Lqj2pHLnlzbCVvQ$fhzQehAX89RaJpaAFJdiZawbm5Q2zIG6yr+XL6awmKM');
#-------------------------------------------------------------------------------
INSERT INTO store.UserRole
    (UserID, RoleID)
VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(3, 2),
	(3, 3),
	(4, 1);
#-------------------------------------------------------------------------------
INSERT INTO store.Address
	(ID, UID, AccountID, Description, Line1, City, PostalZip, RegionID, CountryID)
VALUES
	(1,(UUID_TO_BIN("F79171CE-7AEC-E51D-390A-12FC09116487")),1,"Mom & Dad","300 Stewart Green SW","Calgary","T3H 3C8",1,1),
	(2,(UUID_TO_BIN("34819C92-B35F-C1F3-B021-749CAAE1BCD3")),1,"Home","7111 Elmbridge Way","Richmond","V6X 3J7",2,1),
	(3,(UUID_TO_BIN("B5CB5B3C-B770-4D6F-9BAA-649238C16B76")),1,"Uncle Bob","1722 Preston Avenue N","Saskatoon","S7N 4Y1",12,1),
	(4,(UUID_TO_BIN("05523FCD-D6B4-1911-E8E2-6A3EDD5AAF06")),1,"Office","8 Welwood Drive","Uxbridge","L9P 1Z7",9,1),
	(5,(UUID_TO_BIN("2ED947E9-F591-E700-7119-172599893075")),2,"Office","465 du Royaume Ouest","Chicoutimi","G7H 5B1",11,1),
	(6,(UUID_TO_BIN("683EA75C-3399-AAEC-1E34-A9EBCBF7A947")),2,"Home","6055 Almon St","Halifax","B3K 1T9",7,1),
	(7,(UUID_TO_BIN("B0A70147-6C04-2754-776B-1502CB65ABD4")),2,"Grampa","6912 29th Avenue NW","Calgary","T3B 0J4",1,1),
	(8,(UUID_TO_BIN("9813242C-B5CF-DD06-AA26-5AB031599465")),2,"Mom & Dad","1636 Kenaston Blvd","Winnipeg","R3P 2M6",3,1),
	(9,(UUID_TO_BIN("ea4e51fa-5ad3-11eb-a201-a7261ebdda82")),3,"Home","1350 Alpha Lake Road","Whistler","V8E 0H9",2,1);
#-------------------------------------------------------------------------------
INSERT INTO store.Supplier
	(ID, UID, Name, EMail, Phone, Fax, Address, Contacts)
VALUES
	(1, (UUID_TO_BIN("a8ffa1c1-2580-4b22-827d-12550addf3e9")), "Curabitur Consequat Ltd","elit@congueelit.org","(185) 236-7131","(243) 207-9537","795-7603 Arcu. Ave|Springdale|0400|Denmark","Inez Levine|Hadassah Mcfarland|Allen Harrington"),
	(2, (UUID_TO_BIN("82c11bcf-6fc2-4ddd-ba8a-d255198d4d0a")), "Tincidunt Neque Associates","neque@odiotristiquepharetra.ca","(229) 900-1353","(950) 957-0964","7852 Viverra. Avenue|Ciudad Victoria|40606|United States","Scott Rivera"),
	(3, (UUID_TO_BIN("b11faaed-58f1-4844-addd-3ab523c9b8f9")), "Aliquam Erat Limited","dignissim.pharetra.Nam@sapien.edu","(929) 880-9581","(756) 991-9110","Ap #154-147 Odio Av.|Hualañé|03452-312|Netherlands","Fitzgerald Hewitt|Lucy Bean"),
	(4, (UUID_TO_BIN("a724fcea-fa97-45cf-aa96-ccf2ed323811")), "Eget Tincidunt Company","tristique.aliquet@accumsan.ca","(703) 423-6559","(585) 623-1529","130-9674 Tristique Rd.|Meeuwen-Gruitrode|811240|Mexico","Bruce Reyes|Macaulay Sosa"),
	(5, (UUID_TO_BIN("8ee3ad2b-3c74-42de-8d8d-cc560a5030ea")), "Diam Luctus Inc.","gravida@utlacusNulla.ca","(695) 577-4045","(577) 929-9761","4535 Elit Ave|Bozeman|17008|Australia","Brenden Rogers|Pamela Talley|Iris Gates|Astra Doyle"),
	(6, (UUID_TO_BIN("1c7c6f6a-3dce-4ef0-9a85-b1fa1ec8f1b5")), "Dignissim Tempor LLC","erat.Sed.nunc@lacusvestibulumlorem.ca","(823) 689-8481","(842) 932-2687","Ap #871-8732 Nam Rd.|San Fabián|75750|Germany","Martin Day|Nash Evans"),
	(7, (UUID_TO_BIN("0618aa77-593e-4ce3-9a72-73bf5ae0f1fb")), "Phasellus Inc.","Quisque@Nunc.net","(352) 567-0383","(272) 335-6339","Ap #528-502 Nulla Ave|York|16118|Hong Kong","Gil Kinney|Darryl Carr|Brian Donovan|Aphrodite Beasley|Malachi Wyatt"),
	(8, (UUID_TO_BIN("7d8ace22-3e3d-413f-a118-f663a0960fc1")), "Nunc Risus LLC","lacus@Craseu.org","(512) 784-2583","(936) 206-6319","9731 Vel Road|Serrungarina|87720-455|Ireland","Jacob Lyons|Daryl Mclaughlin|Ifeoma Gilbert"),
	(9, (UUID_TO_BIN("95363929-9569-4994-9a0a-8cb0b4b3c4db")), "Sapien Ltd","semper.tellus@necimperdiet.com","(867) 315-4941","(408) 876-3432","Ap #694-2052 Aliquet Road|Mosciano SantAngelo|28439|Canada","Sylvester Mckinney|Brynn Mcguire"),
	(10,(UUID_TO_BIN("b72bb0ee-3451-4c34-aa8f-be9106871bdb")), "Quisque LLP","neque@ametornarelectus.net","(307) 375-3784","(102) 412-8684","P.O. Box 778, 7015 Vitae Street|Opprebais|15122|Switzerland","Aladdin York|Renee Jensen");
#-------------------------------------------------------------------------------
INSERT INTO store.Manufacturer
	(ID, UID, Name, Website)
VALUES
	(1,(UUID_TO_BIN("f28ec1b7-cf5f-42dd-bba5-faf6341b5c1d")), "SevenPorts", "https://sevenports.com");
#-------------------------------------------------------------------------------
INSERT INTO store.Image
	(ID, UID, ProductID, Type, Location)
VALUES
	(1,(UUID_TO_BIN('5dbbb7f6-5ad2-11eb-8c12-2b01a4dddde0')),1,'jpeg','/'),
	(2,(UUID_TO_BIN('6cd4de5c-5ad2-11eb-8916-6339784c3517')),2,'jpeg','/'),
	(3,(UUID_TO_BIN('eb114a44-5ad2-11eb-b5b2-4fa53be071dc')),3,'jpeg','/'),
	(4,(UUID_TO_BIN('ede598b0-5ad2-11eb-a73f-d7eacab1ce14')),4,'jpeg','/'),
	(5,(UUID_TO_BIN('4e009290-5ad3-11eb-8173-0f02e9613d92')),1,'jpeg','/'),
	(6,(UUID_TO_BIN('5563e9ba-5ad3-11eb-92f1-57c53480831a')),1,'jpeg','/'),
	(7,(UUID_TO_BIN('52d8ae42-5ad3-11eb-90c7-a34765493837')),1,'jpeg','/'),
	(8,(UUID_TO_BIN('63193fbb-6ec8-4a99-a522-f5a75f030aeb')),1,'jpeg','/'),
	(9,(UUID_TO_BIN('3ac97c98-3db0-4c1b-bdf6-1463da3b08de')),5,'jpeg','/'),
	(10,(UUID_TO_BIN('e804e1fa-0ed0-4b95-9603-92a08d58470a')),5,'jpeg','/'),
	(11,(UUID_TO_BIN('e094bd3f-86bf-4500-a418-784ebb1cafd6')),5,'jpeg','/'),
	(12,(UUID_TO_BIN('35ddeba4-8b40-4240-b162-e88c8d83348f')),5,'jpeg','/'),
	(13,(UUID_TO_BIN('2960b633-a92a-46eb-ba82-839ae72dac0b')),6,'jpeg','/'),
	(14,(UUID_TO_BIN('ea064769-a275-4dd5-9fa6-1db29d3ea784')),6,'jpeg','/'),
	(15,(UUID_TO_BIN('490477c1-cc62-49c2-a305-ae4b8e63b408')),7,'jpeg','/'),
	(16,(UUID_TO_BIN('1bf56321-ba40-49c9-8ddc-842422c32b1e')),7,'jpeg','/');

INSERT INTO store.ProductImage
	(ProductItemID, ImageID)
VALUES
	(1,6),
	(2,7),
	(3,5),
	(4,1),
	(5,2),
	(6,2),
	(7,2),
	(8,2),
	(9,3),
	(10,3),
	(11,3),
	(12,4),
	(1,8),
	(13,9),
	(14,11),
	(15,12),
	(13,10),
	(14,10),
	(15,10),
	(16,13),
	(17,14),
	(18,15),
	(19,16),
	(16,14),
	(17,13),
	(18,16),
	(19,15);


#-------------------------------------------------------------------------------
INSERT INTO store.Product
	(ID, UID, SupplierID, ManufacturerID, Name, Description, Keywords, ImageID)
VALUES
  (1,(UUID_TO_BIN("5dbbb7f6-5ad2-11eb-8c12-2b01a4dddde0")),1,1,"Danner Aqua-Supreme Air Pump","Energy-efficient recirculating Aqua-Supreme Air Pumps in 4 convenient sizes easily aerate single or multiple aquariums. Aqua-Supreme air pumps are ideal for most fresh and saltwater aquarium applications. In addition to aerating single or multiple aquariums, Aqua-Supreme air pumps are great for oxygenating indoor ponds and water gardens, aerating single or multiple terrariums, and oxygenating indoor water features. Easy-to-use Aqua-Supreme air pumps accept standard airline tubing. Advanced design and soft rubber feet ensure quiet operation. Air pump motor needs no lubrication. Energy-Efficient Motor, Easy to Use, Quiet Operation, Accepts Standard Airline Tubing. UL-listed.","air pump danner aqua supreme", 1),
  (2,(UUID_TO_BIN("6cd4de5c-5ad2-11eb-8916-6339784c3517")),7,1,"Tetra Whisper Air Pump","Minimal noise, maximum air flow. The Whisper® Air Pump’s dome shape actually flattens sound wave frequencies. In addition, each model features sound-dampening chambers designed specifically for that model. And the air pump's rubber feet and suspended motor prevent sound waves from reflecting off surfaces such as tables and shelves. Available in models for tanks from 10 to 100 gallons, Whisper Air Pumps provide a powerful flow of air. UL listed. ","air pump tetra whisper", 2),
	(3,(UUID_TO_BIN("eb114a44-5ad2-11eb-b5b2-4fa53be071dc")),9,1,"Seachem Reef Glue","Reef Glue is a superior cyanoacrylate gel for gluing and mounting coral frags and colonies to reef rock or plugs. It bonds within seconds and has excellent control, hold and durability characteristics. It can even be used underwater. Reef Glue can be used for any aquascaping in freshwater or saltwater aquariums or for any plastics repairs. Reef Glue is in an aluminum tube for extended stability life beyond products contained in plastic bottles - it won't dry up in the tube.","seachem reef glue", 3),
	(4,(UUID_TO_BIN("ede598b0-5ad2-11eb-a73f-d7eacab1ce14")),3,1,"Coralife Digital Thermometer","The Coralife Digital Thermometer features an easy to read digital display with temperature in either Fahrenheit or Celsius depending on your choice.","coralife digitial thermometer", 4),
	(5,(UUID_TO_BIN("caacb46c-2092-4be7-88f5-fd91c6e4c234")),10,1,"JBJ Cubey Aquarium","BIG TANK performance!<br/><br/>JBJ-USA is proudly introducing to you our newest product, the Nano Cubey. This JBJ’s new Cubey is the result of a redesign on the original Nano-Cube to fit today’s modern décor and lifestyle.<br/><br/>The All-In-One Cubeys come in black or white with high clarity flat panel glass, black silicon and integrated dimmable two channels LED canopy. Complete the contemporary look with our 36 inch high gloss painted stand in matching color.<br/><br/>Available in Black and White.<br/><br/>Whether you’re a family looking for an entry level freshwater aquarium or a moderate to advanced marine aquarist looking for the challenge of a nanoreef, the CUBEY is truly the one size that fits all.","cubey aquarium jbj tank kit",9),
	(6,(UUID_TO_BIN("77531fbd-fc00-4edd-a3a4-e4e3b7906173")),9,1,"Fluval FLEX Aquarium Kit (Black)","The Fluval Flex not only offers contemporary styling with its distinctive curved front, but is also equipped with powerful multi-stage filtration and brilliant LED lighting that allows the user to customize several settings via remote control.<br/><ul><li>7500K LED lamp supports plant growth and enhances fish colors</li><li>Fully adjustable White + RGB LEDs for endless color blends</li><li>FLEXPad remote can also control fun special effects (i.e. fading cloud cover, lightning bolts)</li><li>Powerful 3-stage filtration for superior water quality</li><li>Oversized mechanical (foam), chemical (carbon) and biological (Biomax) media included</li><li>Multi-directional dual outputs for customized water flow</li><li>Hidden rear filter compartment</li><li>Stylish honeycomb wrap conceals water line and sides of rear compartment</li><li>Easy feed top cover opening</li><li>Bold curved front design</li><li>For freshwater use only</li></ul>","fluval flex aquarium tank kit",13),
	(7,(UUID_TO_BIN("1180ee0f-b404-40b1-8efa-d84a4102771b")),9,1,"Fluval FLEX Aquarium Kit (White)","The Fluval Flex not only offers contemporary styling with its distinctive curved front, but is also equipped with powerful multi-stage filtration and brilliant LED lighting that allows the user to customize several settings via remote control.<br/><ul><li>7500K LED lamp supports plant growth and enhances fish colors</li><li>Fully adjustable White + RGB LEDs for endless color blends</li><li>FLEXPad remote can also control fun special effects (i.e. fading cloud cover, lightning bolts)</li><li>Powerful 3-stage filtration for superior water quality</li><li>Oversized mechanical (foam), chemical (carbon) and biological (Biomax) media included</li><li>Multi-directional dual outputs for customized water flow</li><li>Hidden rear filter compartment</li><li>Stylish honeycomb wrap conceals water line and sides of rear compartment</li><li>Easy feed top cover opening</li><li>Bold curved front design</li><li>For freshwater use only</li></ul>","fluval flex aquarium tank kit",16);
#-------------------------------------------------------------------------------
INSERT INTO store.ProductItem
	(ID, UID, ProductID, Display, Model, SKU, UPC, Name, Description, Keywords, UnitPrice, Weight, StockQuantity, ReorderLevel, TotalSold)
VALUES
	(1,(UUID_TO_BIN("5563e9ba-5ad3-11eb-92f1-57c53480831a")),1,false,"AP-2","","","1.5 Watt 73cu. in/min","","1_5 watt AP_2",13.99, 0, 9, 10, 5),
	(2,(UUID_TO_BIN("52d8ae42-5ad3-11eb-90c7-a34765493837")),1,true,"AP-3","","","2.8 Watt 110cu. in/min","","2_8 watt AP_3",19.99, 0, 55, 10, 7),
	(3,(UUID_TO_BIN("4e009290-5ad3-11eb-8173-0f02e9613d92")),1,false,"AP-4","","","3.5 Watt 275 cu. in/min","","3_5 watt AP_4",33.99, 0, 23, 6, 2),
	(4,(UUID_TO_BIN("49f8fc14-5ad3-11eb-9526-17c8e3dbfd8e")),1,false,"AP-8","","","7 Watt 549 cu. in/min","","7 watt AP_8",48.99, 0, 13, 6, 9),
	(5,(UUID_TO_BIN("46e289a0-5ad3-11eb-91c9-7759eb6abcdf")),2,false,"GT645-20","","","20 gallons","For tanks up to 20 gallons in size, 1 air outlet, Minimal noise and maximum air flow, Features sound-dampening chambers.","20 GT645_20",23.99, 0, 32, 5, 6),
	(6,(UUID_TO_BIN("4456a982-5ad3-11eb-a351-3fc1e4ba964d")),2,true,"GT645-40","","","40 gallons","For tanks up to 40 gallons in size, 1 air outlet, Minimal noise and maximum air flow, Features sound-dampening chambers.","40 GT645_40",27.99, 0, 20, 4, 10),
	(7,(UUID_TO_BIN("4208eabe-5ad3-11eb-ac85-5b088aad8d46")),2,false,"GT645-60","","","60 gallons","For tanks up to 60 gallons in size, 2 air outlets, Minimal noise and maximum air flow, Features sound-dampening chambers.","60 GT645_60",35.99, 0, 11, 3, 1),
	(8,(UUID_TO_BIN("3f5e62bc-5ad3-11eb-9150-f79f9b51b2c2")),2,false,"GT645-100","","","100 gallons","For tanks up to 100 gallons in size, 2 air outlets, Minimal noise and maximum air flow, Features sound-dampening chambers.","100 GT645_100",48.99, 0, 7, 2, 4),
	(9,(UUID_TO_BIN("c5c3be40-6066-11eb-ae93-0242ac130002")),3,true,"46488-60","","","60ml","60ml tube.","60ml 46488_60",9.99, 0, 0, 25, 10),
	(10,(UUID_TO_BIN("5dd103f0-6067-11eb-ae93-0242ac130002")),3,false,"46488-100","","","100ml","100ml tube.","100ml 46488_100",17.99, 0, 50, 25, 5),
	(11,(UUID_TO_BIN("62c92dc4-6067-11eb-ae93-0242ac130002")),3,false,"46488-120","","","120ml","120ml tube.","120ml 46488_120",19.99, 0, 14, 25, 12),
	(12,(UUID_TO_BIN("395cc71e-5ad3-11eb-b935-17a08d9a9b9b")),4,true,"CDT737676","","","Black","Attaches to aquarium via small suction cup, Battery included, 3 foot long temperature probe for large size aquariums.","CDT737676",11.99, 0, 100, 15, 20),
	(13,(UUID_TO_BIN("4f105dcd-ac25-4d77-b059-70e81bd93356")),5,false,"AQ-JBJ-CN-10-B","","","10 Gal, Black","10 Gal Cubey All in One Mid-size Aquarium.<br/>Kit Contents: Smart Touch LED Canopy, 2 channel RGB w/ independent power cords, Low Iron Glass, Crystal Clear Viewing, All-In-One 3 Stage Filter, Designated Heater Column in Filter.","10 AQ-JBJ-CN-10-B",249.99,0,23,10,14),
	(14,(UUID_TO_BIN("68dbf8c6-5713-4e6e-ad94-df5cb8d3d82a")),5,true,"AQ-JBJ-CN-15-B","","","15 Gal, Black","20 Gal Cubey All in One Mid-size Aquarium.<br/>Kit Contents: Smart Touch LED Canopy, 2 channel RGB w/ independent power cords, Low Iron Glass, Crystal Clear Viewing, All-In-One 3 Stage Filter, Designated Heater Column in Filter.","15 AQ-JBJ-CN-15-B",284.99,0,42,10,16),
	(15,(UUID_TO_BIN("dedcb70a-7551-4111-9c1c-dd43f593aa75")),5,false,"AQ-JBJ-CN-20-B","","","20 Gal, Black","20 Gal Cubey All in One Mid-size Aquarium.<br/>Kit Contents: 20 Gallon All-In-One Aquarium, 3-Stage Filtration with media, (2) Accela FP-1500 Pumps, Orion SL-140 LED, Protein Skimmer.","20 AQ-JBJ-CN-20-B",308.49,0,25,15,25),
	(16,(UUID_TO_BIN("d8449013-ed50-4118-b69e-8e1cb29ee51b")),6,false,"AQ-A15004","","","9 gallon (34L)","","AQ-A15004",119.95,0,4,10,52),
	(17,(UUID_TO_BIN("62a7e33a-8891-408d-91cb-ab483f28cc99")),6,true,"AQ-A15006","","","15 gallon (57L)","","AQ-A15004",149.95,0,8,5,13),
	(18,(UUID_TO_BIN("8cb90022-0443-47b0-80d3-a675c6046998")),7,false,"AQ-A15008","","","9 gallon (34L)","","AQ-A15004",119.95,0,7,9,15),
	(19,(UUID_TO_BIN("9c16147f-76b1-408c-b85a-0fab1db479cf")),7,false,"AQ-A15010","","","15 gallon (57L)","","AQ-A15004",149.95,0,32,10,123);
#-------------------------------------------------------------------------------
INSERT INTO store.Specification
	(ProductItemID, Name, Value)
VALUES
	(1, "Power", "1.5 Watt"),
	(1, "Air Flow", "73 cu. in/min"),
	(1, "Pressure", "1.45 psi"),
	(1, "Max Water Depth", "3 feet"),
	(2, "Power", "2.8 Watt"),
	(2, "Air Flow", "110 cu. in/min"),
	(2, "Pressure", "1.45 psi"),
	(2, "Max Water Depth", "3.3 feet"),
	(3, "Power", "3.5 Watt"),
	(3, "Air Flow", "275 cu. in/min"),
	(3, "Pressure", "2.00 psi"),
	(3, "Max Water Depth", "2 feet"),
	(4, "Power", "7 Watt"),
	(4, "Air Flow", "549 cu. in/min"),
	(4, "Pressure", "2.00 psi"),
	(4, "Max Water Depth", "4.5 feet"),
	(5, "Max Tank Size", "20 gallons"),
	(5, "Air Outlets", "1"),
	(6, "Max Tank Size", "40 gallons"),
	(6, "Air Outlets", "1"),
	(7, "Max Tank Size", "60 gallons"),
	(7, "Air Outlets", "2"),
	(8, "Max Tank Size", "100 gallons"),
	(8, "Air Outlets", "2"),
	(9, "Size", "60ml"),
	(10, "Size", "100ml"),
	(11, "Size", "120ml"),
	(12, "Battery", "9V PP3 (included)"),
	(12, "Probe length", "3 feet"),
	(12, "Dimensions", "L2.25 x W.5 x H1.5"),
	(13, "Tank","15.78x12.25x14.68in"),
	(13, "Stand","15.78x12.15x36in"),
	(13, "Pump Power","12W"),
	(13, "Pump Flow","196gph"),
	(13, "Light Power","DC12V"),
	(13, "Light Watt","10W"),
	(14, "Tank","18.5x18x18in"),
	(14, "Stand","18.5x18x36in"),
	(14, "Pump Power","SP1-1000"),
	(14, "Pump Flow","266gph"),
	(14, "Light Power","DC12V"),
	(14, "Light Watt","15W"),
	(15, "Tank","36x21x20in"),
	(15, "Stand","36x21x36in"),
	(15, "Pump Power","SP1-1000(2)"),
	(15, "Pump Flow","266gph"),
	(15, "Light Power","DC12V"),
	(15, "Light Watt","20W"),
	(16, "Size","14x13x13in"),
	(16, "Light","Included"),
	(16, "Pump","Included"),
	(16, "Filtration","Included"),
	(16, "Heater","Sold separately"),
	(17, "Size","14x13x13in"),
	(17, "Light","Included"),
	(17, "Pump","Included"),
	(17, "Filtration","Included"),
	(17, "Heater","Sold separately"),
	(18, "Size","14x13x13in"),
	(18, "Light","Included"),
	(18, "Pump","Included"),
	(18, "Filtration","Included"),
	(18, "Heater","Sold separately"),
	(19, "Size","14x13x13in"),
	(19, "Light","Included"),
	(19, "Pump","Included"),
	(19, "Filtration","Included"),
	(19, "Heater","Sold separately");

#-------------------------------------------------------------------------------
INSERT INTO store.ProductCategory
	(ProductID, CategoryID, SubcategoryID)
VALUES
	(1,3,15),
	(2,3,15),
	(2,3,16),
	(3,4,18),
	(4,4,18),
	(4,6,31),
	(5,4,19),
	(6,4,19),
	(7,4,19);
#-------------------------------------------------------------------------------
INSERT INTO store.`Order` 
	(UID,AccountID,OrderStatusID,BillingAddressID,DeliveryAddressID,DiscountID,ShipmentID,PaymentProcessorID,TransactionID,`Date`,SubTotal,BeforeTaxTotal,ShipTotal,TaxTotal,DiscountTotal,GrandTotal,PaymentCardBrand,PaymentCardLast4Digits,PaymentCardExpiryMonth,PaymentCardExpiryYear) 
VALUES
	 ((UUID_TO_BIN("18973400-5893-4b76-aa9c-ba3f3844cd51")),1,1,2,2,0,1,1,'r9gSkTzvENwHljCNPumH2JLU83PZY','2021-04-01 21:51:17',47.98,144.91,96.93,21.74,0.00,166.65,'Visa',1111,12,21),
	 ((UUID_TO_BIN("e36ad233-b0ac-446e-a668-db717591bdac")),1,4,2,2,0,2,1,'33uUYZKl4JnXMDdJ51KYg7BL7qZZY','2020-03-11 21:52:58',73.96,143.94,69.98,21.59,0.00,165.53,'Visa',1111,12,21),
	 ((UUID_TO_BIN("c93aa23d-071e-48ac-b094-cfb9d909c227")),1,4,2,2,0,3,1,'bJe2FTQWKvp96YSR3cZhCNAhlsVZY','2020-09-26 21:53:23',19.99,67.83,47.84,16.17,0.00,123.98,'Visa',1111,12,21);

INSERT INTO store.OrderItem 
	(UID,OrderID,ProductItemID,Quantity,UnitPrice) 
VALUES
	 ((UUID_TO_BIN("4aec9ecb-977d-491a-a20a-67a14188d958")),1,6,1,27.99),
	 ((UUID_TO_BIN("22ffcdaf-8ddb-4962-a3d0-446444bb7880")),1,11,1,19.99),
	 ((UUID_TO_BIN("4880dac0-f03a-4f46-8318-f7fe861c6376")),2,6,1,27.99),
	 ((UUID_TO_BIN("3e5a79f8-0b9f-4a01-9e8a-484da0832a08")),2,11,1,19.99),
	 ((UUID_TO_BIN("a3379d98-28d3-4852-af65-3c069a331c0c")),2,12,1,11.99),
	 ((UUID_TO_BIN("bd2d16a7-b270-43ce-ba15-fa31aa3debf8")),2,1,1,13.99),
	 ((UUID_TO_BIN("8a1281db-4c14-4252-97e7-07c961af7eda")),3,11,3,19.99);
	
INSERT INTO store.Shipment 
	(UID,RateID,ShipmentID,Carrier,Service,Rate,TrackingNumber,DeliveryDays,ShipDate) 
VALUES
	 ((UUID_TO_BIN("26179f38-864e-4dca-8dd2-979e218fd903")),'shp_8e659ef90d424ad2a3b55c0c7655cb19','rate_828d27f18b514fa6b3c05acc6f010b4f','FedEx','PRIORITY_OVERNIGHT',96.00,NULL,5,'2021-04-01 00:00:00'),
	 ((UUID_TO_BIN("4ac6e2ce-f973-4857-abd6-7f55120d7581")),'shp_7752e5bb79e74c3aad925cbf8d971c4e','rate_58aa2904a7df44f29acf51ca759c09cd','FedEx','FEDEX_2_DAY',69.00,NULL,6,'2021-04-01 00:00:00'),
	 ((UUID_TO_BIN("096fda19-34a3-4d00-bfb5-1c56784c3c14")),'shp_6d014e0558854889a20f39c2f026d247','rate_cca2e9d09c8c423f9280048f6e6c4ac1','FedEx','FEDEX_EXPRESS_SAVER',47.00,NULL,0,'2021-04-01 00:00:00');
#-------------------------------------------------------------------------------
INSERT INTO store.Review
  (UID,AccountID,ProductID,ProductItemID,Title,Description,Rating,`Date`,VerifiedPurchase)
VALUES
	((UUID_TO_BIN("c71514b9-2bd9-4f28-a91e-854472e91909")),1,1,1,"Nemo loves it!","I've named it 'Pumpy McPumpFace' since it's such a great pump.",5,'2021-04-01',true),
	((UUID_TO_BIN("77368d62-a6f8-407b-8dae-a63b50a73596")),2,1,2,"Perfect air pump","Very impressed with this air pump. The adjustment for air volume is perfect for our 1.5gal Betta tank. I can adjust to get just the right amount needed. There is no vibration noise or pump noise. It truly is whisper quiet. The only reason I know it’s still working is I can see the air bubbles. Perfect air pump!",3,'2021-04-01',true),
	((UUID_TO_BIN("09115816-f282-4c8b-ae41-5b451cd415ff")),1,2,3,"Very nice. Happy with it. Wish I could spell properly","So i was expecting to get a claim from the seller saying quiet and is a kit....but guess what this thing is silent the actul pump makes zero noise. Aslong as its on the runner mounts. And it had evyjing you need to set it up. Took 5min and 2 snips with the scissors. Its amazing and the price. 100% better then any pet store could do. The chepest pump (with out tubing or air stone or check valve) is 18 CAD here (petsmart) and this is 17$ with everything. And is way quieter. Im so happy with this purchase. And has a nice steady stream for my rainbow shark.",4,'2020-02-21',true),
	((UUID_TO_BIN("eed64724-aba8-47e1-b3ad-72e171ac7338")),2,2,4,"Not as noisy as I thought","I was hesitant of the mixed reviews on how loud this pump is because my fish tank is in my bedroom. I will admit that at first I thought this pump was noisy, but I would say that it's a medium hum which was easy enough to get used to within a couple days. I also put a towel underneath it so it wouldn't vibrate on the tank stand. Now I have to be right beside it and really listen in order to hear it. It's not any worse than the sound of a fan.",2,'2020-09-11',true),
	((UUID_TO_BIN("eb648d37-2f46-40d2-a027-9442d616c275")),1,3,10,"It's a sticky situation","Really good glue. Does exactly what it says on the packet. Some advice though: make sure you clean it off your hands before typing a reviewwwwwwwwwwwwwwwwwwwwwwwwwwww",4,'2021-02-19',true);
	
