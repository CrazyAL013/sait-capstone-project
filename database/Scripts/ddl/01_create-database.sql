#-------------------------------------------------------------------------------
/*
* Create database, default API user, and tables.
* 
*/
#-------------------------------------------------------------------------------

-- Create a new database (dropping any existing database of the same name)
DROP DATABASE IF EXISTS store;
CREATE DATABASE store;
USE store;
#-------------------------------------------------------------------------------
-- Add API user, grant permissions, and LOCK ACCOUNT
-- IMPORTANT: PASSWORD IS NOT SET. DO NOT USE BLANK PASSWORD!
-- Password must be changed manually
-- The account is locked so that the password can be changed later
CREATE USER IF NOT EXISTS 'api'@'%' IDENTIFIED BY '';
GRANT SELECT, INSERT, UPDATE, DELETE ON store.* TO 'api'@'%';
FLUSH PRIVILEGES;
ALTER USER 'api'@'%' ACCOUNT LOCK;
#-------------------------------------------------------------------------------
-- Countries (lookup)
CREATE TABLE store.Country (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Code` varchar(10) NOT NULL,
  `CurrencyCode` varchar(3) NOT NULL,
  `CurrencySymbol` varchar(1) NOT NULL,
  `Tax` decimal(5,3) NOT NULL DEFAULT '0',	# % national sales tax rate
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Country_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Province/State etc.  (lookup)
CREATE TABLE store.Region (
  `ID` int NOT NULL AUTO_INCREMENT,
  `CountryID` int NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Code` varchar(10) NOT NULL,
  `Tax` decimal(5,3) NOT NULL DEFAULT '0',	# % regional sales tax rate
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Region_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Payment Processors (lookup)
CREATE TABLE store.PaymentProcessor (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `Name` varchar(50) NOT NULL,
  `Logo` varchar(100) NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PaymentProcessor` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Display information for discount types (lookup)
CREATE TABLE store.DiscountType (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `Name` varchar(20) NOT NULL,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DiscountType_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- User Role (lookup)
CREATE TABLE store.Role (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),  
  `Name` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Role_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Order Status (lookup)
CREATE TABLE store.OrderStatus (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
	`Title` varchar(100) NOT NULL,
  `Description` varchar(200) NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `OrderStatus_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Customer account
CREATE TABLE store.Account (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `AddressID` int NULL,
  `Active` boolean DEFAULT '1' NOT NULL,
  `FullName` varchar(100) NOT NULL,
  `PreferredName` varchar(50) NOT NULL,
  `EMail` varchar(100) NOT NULL,
  `JoinDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PhoneNumber` varchar(30) NULL,
  `Cart` text NULL,						#JSON string containing saved cart items
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Account_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- User login (associated with an account)
CREATE TABLE store.User (
  `ID` int NOT NULL AUTO_INCREMENT,
  `AccountID` int NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `PasswordHash` char(98) NOT NULL,
  `VerificationCode` varchar(100) NULL,			# Used to verify account (via e-mail)
  PRIMARY KEY (`ID`),
  UNIQUE KEY `User_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Refresh Token (associated with an account)
CREATE TABLE store.RefreshToken (
  `ID` int NOT NULL AUTO_INCREMENT,
  `AccountID` int NOT NULL,
  `Token` varchar(4096) NULL,				
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RefreshToken_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- A user can belong to many roles
-- Junction table: User <-> Role
CREATE TABLE store.UserRole (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UserID` int NOT NULL,
  `RoleID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UserRole_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Customer addresses
CREATE TABLE store.Address (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `AccountID` int NOT NULL,
  `Description` varchar(50) NOT NULL,
  `Line1` varchar(100) NOT NULL,
  `Line2` varchar(100) NULL,
  `City` varchar(100) NOT NULL,
  `PostalZip` char(15) NOT NULL,
  `RegionID` int NOT NULL,
  `CountryID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Address_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Customer wishlist
CREATE TABLE store.Wishlist (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `AccountID` int NOT NULL,
  `Title` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Wishlist_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.WishlistItem (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `WishlistID` int NOT NULL,
  `ProductItemID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `WishlistItem_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Customer reviews
CREATE TABLE store.Review (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `AccountID` int NOT NULL,
  `ProductID` int NOT NULL,
  `ProductItemID` int NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `Rating` tinyint NOT NULL,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `VerifiedPurchase` boolean DEFAULT '0' NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Review_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Discount definition
-- See also: Order, SaleItem
CREATE TABLE store.Discount (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `DiscountTypeID` int NOT NULL,
  `Code` varchar(20) NOT NULL,
  `Description` varchar(20) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `FixedAmount` decimal(15,2) NOT NULL DEFAULT '0',
  `Percentage` decimal(5,2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Discount_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Customer order
CREATE TABLE store.Order (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `AccountID` int NOT NULL,
  `OrderStatusID` int NOT NULL DEFAULT '1',
  `BillingAddressID` int NOT NULL,
  `DeliveryAddressID` int NOT NULL,
  `DiscountID` int NULL,
  `ShipmentID` int NULL,
  `PaymentProcessorID` int NOT NULL,
  `TransactionID` varchar(500) NULL,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SubTotal` decimal(15,2) NOT NULL DEFAULT '0',
  `BeforeTaxTotal` decimal(15,2) NOT NULL DEFAULT '0',
  `ShipTotal` decimal(15,2) NOT NULL DEFAULT '0',
  `TaxTotal` decimal(15,2) NOT NULL DEFAULT '0',
  `DiscountTotal` decimal(15,2) NOT NULL DEFAULT '0',
  `GrandTotal` decimal(15,2) NOT NULL DEFAULT '0',
  `PaymentCardBrand` varchar(50) NULL,
  `PaymentCardLast4Digits` int NULL,
  `PaymentCardExpiryMonth` int NULL,
  `PaymentCardExpiryYear` int NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Item in an order
CREATE TABLE store.OrderItem (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `OrderID` int NOT NULL,
  `ProductItemID` int NOT NULL,
  `Quantity` int NOT NULL DEFAULT '0',
  `UnitPrice` decimal(15,2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `OrderItem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.Shipment (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `RateID` varchar(100) NOT NULL,
  `ShipmentID` varchar(37) NOT NULL,
  `Carrier` varchar(37) NOT NULL,
  `Service` varchar(100) NOT NULL,
  `Rate` decimal(15,2) NOT NULL DEFAULT '0',
  `TrackingNumber` varchar(500) NULL,
  `DeliveryDays` int NOT NULL DEFAULT '0',
  `ShipDate` datetime NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Shipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Information for a sale
CREATE TABLE store.Sale (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `Name` varchar(100) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Sale` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Sale item (product) and discount
CREATE TABLE store.SaleItem (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `ProductItemID` int NOT NULL,
  `DiscountID` int NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SaleItem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Facilitates full-text searches for products
CREATE TABLE store.ProductSearch (
  `ID` int NOT NULL AUTO_INCREMENT,
  `ProductID` int NOT NULL,
  `ProductItemID` int NOT NULL,
  `Keywords` longtext NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ProductSearch_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE store.ProductSearch ADD FULLTEXT (Keywords);
#-------------------------------------------------------------------------------
CREATE TABLE store.Manufacturer (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `Name` varchar(100) NOT NULL,
  `Website` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Manufacturer_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.Supplier (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `Name` varchar(100) NOT NULL,
  `EMail` varchar(1000) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Fax` varchar(15) NOT NULL,
  `Address` varchar(250) NOT NULL,
  `Contacts` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Supplier_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.StockOrder (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `SupplierID` int NOT NULL,
  `Date` datetime NOT NULL,
  `DeliveryDate` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Order_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.StockOrderItem (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `StockOrderID` int NOT NULL,
  `ProductItemID` int NOT NULL,
  `Quantity` int NOT NULL DEFAULT '0',
  `UnitPrice` decimal(15,2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `OrderItem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Category groups subcategories
CREATE TABLE store.Category (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `Name` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Category_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- Subcategory is associated with products
-- See also: ProductCategory
CREATE TABLE store.Subcategory (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `CategoryID` int NOT NULL,
  `Name` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Subcategory_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
-- A product can belong to many (sub) categories
-- Junction table: Product <-> Subcategory
-- See also: Product, Subcategory
CREATE TABLE store.ProductCategory (
  `ID` int NOT NULL AUTO_INCREMENT,
  `ProductID` int NOT NULL,
  `CategoryID` int NOT NULL,
  `SubcategoryID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ProductCategory_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.Product (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `SupplierID` int NOT NULL,
  `ManufacturerID` int NOT NULL,
  `Name` varchar(100) NOT NULL,						-- Overall product name
  `Description` text NOT NULL,						-- <=65535 characters - code must check length!
  `Keywords` varchar(500) NOT NULL,				-- space delimited string of keywords
  `ImageID` int NULL,							-- single image for primary image
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Product_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.ProductItem (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `ProductID` int NOT NULL,
  `Display` boolean DEFAULT '1' NOT NULL,				-- The Display item is used when displaying a (web) catalog
  `Model` varchar(50) NOT NULL,
  `SKU` varchar(12) NULL,
  `UPC` char(12) NULL,
  `Name` varchar(100) NOT NULL,									-- Item name (appended to overall product name)
  `Description` text NOT NULL,									-- <=65535 characters - code must check length!
  `Keywords` varchar(500) NOT NULL,							-- space delimited string of keywords
  `UnitPrice` decimal(15,2) NOT NULL DEFAULT '0',
  `Weight` decimal(15,2) NOT NULL DEFAULT '0',
  `StockQuantity` int NOT NULL DEFAULT '0',
  `ReorderLevel` int NOT NULL DEFAULT '0',
  `TotalSold` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ProductItem_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.Specification (
  `ProductItemID` int NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Value` varchar(50) NOT NULL,
  KEY (`ProductItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.ProductImage (
  `ProductItemID` int NOT NULL,
  `ImageID` int NOT NULL,
  PRIMARY KEY (`ProductItemID`, `ImageID`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
CREATE TABLE store.Image (
  `ID` int NOT NULL AUTO_INCREMENT,
  `UID` binary(16) DEFAULT (UUID_TO_BIN(UUID())),
  `ProductID` int NOT NULL,
  `Type` varchar(5) NOT NULL,
  `Location` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IMAGE_UN` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#-------------------------------------------------------------------------------
