#-------------------------------------------------------------------------------
/*
* Events
* 
* Content:	MaintenanceEvent
* 
* TODO: Change timing to 
*/
#-------------------------------------------------------------------------------

DROP EVENT IF EXISTS store.MaintenanceEvent;
DELIMITER //
CREATE EVENT store.MaintenanceEvent
	ON SCHEDULE EVERY 60 SECOND		#TODO: MINUTE
	DO
		BEGIN
			#TODO: Postponed until time permits
			#SET @LastExecuted = (SELECT LAST_EXECUTED FROM INFORMATION_SCHEMA.events WHERE EVENT_NAME="MaintenanceEvent"); 
			#CALL store.ProductUpdateStats(@LastExecuted);
		END //
DELIMITER ;
#-------------------------------------------------------------------------------
ALTER EVENT store.MaintenanceEvent ENABLE;		
ALTER EVENT store.MaintenanceEvent DISABLE;
	

