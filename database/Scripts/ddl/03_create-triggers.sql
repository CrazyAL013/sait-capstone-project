#-------------------------------------------------------------------------------
/*
* Triggers
* 
* Content:	ProductItemInsert
* 					ProductItemUpdate
* 					ProductItemDelete
*/
#-------------------------------------------------------------------------------
-- Triggers used to update the full-text search table (ProductSearch)
-- whenever changes are made to the ProductItem table.
DROP TRIGGER IF EXISTS store.ProductItemInsert;
CREATE TRIGGER store.ProductItemInsert
	AFTER INSERT ON ProductItem FOR EACH ROW
		INSERT INTO ProductSearch
			(ProductID, ProductItemID, Keywords)
		SELECT
			NEW.ProductID,
			NEW.ID,
			CONCAT(LOWER(P.Keywords), " ", LOWER(NEW.Keywords))
		FROM
			Product P, ProductItem PI
		WHERE
			P.ID = PI.ProductID AND
			P.ID = NEW.ProductID AND
			PI.ID = NEW.ID;
#-------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS store.ProductItemUpdate;
CREATE TRIGGER store.ProductItemUpdate
	AFTER UPDATE ON ProductItem FOR EACH ROW
		UPDATE ProductSearch
		SET Keywords =
		(
			SELECT
				CONCAT(LOWER(P.Keywords), " ", LOWER(NEW.Keywords))
			FROM
				Product P, ProductItem PI
			WHERE
				P.ID = PI.ProductID AND
				PI.ID = NEW.ID
		)
		WHERE ID = NEW.ID;
#-------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS store.ProductItemDelete;
CREATE TRIGGER store.ProductItemDelete
	AFTER DELETE ON ProductItem FOR EACH ROW
		DELETE FROM ProductSearch
		WHERE ProductItemID = OLD.ID;
#-------------------------------------------------------------------------------
