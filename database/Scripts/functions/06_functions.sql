#-------------------------------------------------------------------------------
/*
*	Helper Functions 
* 
* Content:	GetCategoryFromSubcategory
* 					GetAccountJSON
* 					GetUserRolesJSON
* 					GetAddressJSON
* 					GetDiscountJSON
* 					GetShipmentJSON
* 					GetPaymentProcessorJSON
* 					GetOrderStatusJSON
* 					GetOrderJSON
* 					GetOrderItemsJSON
* 					GetSupplierJSON
* 					GetManufacturerJSON
* 					GetProductAverageRating
* 					GetProductItemAverageRating
* 					GetProductJSON
* 					GetProductReviewJSON
* 					GetProductItemsJSON
* 					GetProductItemJSON
* 					GetCatalogItemJSON
* 					GetCartItemJSON
* 					GetReviewJSON
* 					GetProductCategoriesJSON
* 					GetImageJSON 
* 					GetProductItemImagesJSON
* 					ID_FROM_UID
*/
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetCategoryFromSubcategory;
DELIMITER //
CREATE FUNCTION store.GetCategoryFromSubcategory(_SubcategoryID int) RETURNS int
DETERMINISTIC
BEGIN
		
	RETURN (
		SELECT 
			Category.ID
		FROM
			Category, Subcategory
		WHERE
			Category.ID = Subcategory.CategoryID AND
			Subcategory.ID = _SubcategoryID
	);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetAccountJSON;
DELIMITER //
CREATE FUNCTION store.GetAccountJSON(_AccountID int) RETURNS JSON
DETERMINISTIC
BEGIN
		
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Account.UID),
			'roles', GetUserRolesJSON(User.ID),
			'active', Account.Active,
			'username', User.UserName,
			'fullName', Account.FullName,
			'preferredName', Account.PreferredName,
			'email', Account.EMail,
			'joinDate', Account.JoinDate,
			'phoneNumber', Account.PhoneNumber,
			'cart', Account.Cart,
			'address', GetAddressJSON(Account.AddressID),
			'accessToken', ''
		)
	FROM
		Account, User
	WHERE
		Account.ID = User.AccountID AND
		Account.ID = _AccountID
	);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetUserRolesJSON;
DELIMITER //
CREATE FUNCTION store.GetUserRolesJSON(_UserID int) RETURNS JSON
DETERMINISTIC
BEGIN
		
	RETURN (
		SELECT JSON_ARRAYAGG(JSON_OBJECT(
			'id', BIN_TO_UUID(Role.UID),
			'name', Role.Name
		))
		FROM
			Role, UserRole
		WHERE
			Role.ID = UserRole.RoleID AND
			UserRole.UserID = _UserID
	);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetAddressJSON;
DELIMITER //
CREATE FUNCTION store.GetAddressJSON(_AddressID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Address.UID),
			'description', Address.Description,
			'line1', Address.Line1,
			'line2', Address.Line2,
			'city', Address.City,
			'postalZip', Address.PostalZip,
			'region', (
				SELECT JSON_OBJECT(
					'id', Region.ID,
					'countryId', Region.CountryID,
					'name', Region.Name,
					'code', Region.Code,
					'tax', Region.Tax
				)
				FROM 
					Region
				WHERE
					Region.ID = Address.RegionId
	    ),
			'country', (
				SELECT JSON_OBJECT(
					'id', Country.ID,
					'name', Country.Name,
					'code', Country.Code,
					'currencyCode', Country.CurrencyCode,
					'currencySymbol', Country.CurrencySymbol,	
					'tax', Country.Tax
				)
				FROM 
					Country
				WHERE
					Country.ID = Address.CountryId
	    	)
		)
		FROM
			Address
		WHERE
			Address.ID = _AddressID
	);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetDiscountJSON;
DELIMITER //
CREATE FUNCTION store.GetDiscountJSON(_DiscountID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Discount.UID),
			'type', (
				SELECT JSON_OBJECT(
					'id', DiscountType.ID,
					'name', DiscountType.Name,
					'description', DiscountType.Description
				)
				FROM 
					DiscountType
				WHERE
					DiscountType.ID = Discount.DiscountTypeID
	    ),
	    'code', Discount.Code,
	    'description', Discount.Description,
	    'startDate', Discount.StartDate,
	    'endDate', Discount.EndDate,
	    'fixedAmount', Discount.FixedAmount,
	    'percentage', Discount.Percentage
		)
		FROM
			Discount
		WHERE
			Discount.ID = _DiscountID
	);

	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetShipmentJSON;
DELIMITER //
CREATE FUNCTION store.GetShipmentJSON(_ShipmentID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Shipment.UID),
			'carrier', Shipment.Carrier,
			'service', Shipment.Service,
			'rate', Shipment.Rate,
			'trackingNumber', Shipment.TrackingNumber,
			'deliveryDays', Shipment.DeliveryDays,
			'shipDate', Shipment.ShipDate
		)
		FROM
			Shipment
		WHERE
			Shipment.ID = _ShipmentID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetPaymentProcessorJSON;
DELIMITER //
CREATE FUNCTION store.GetPaymentProcessorJSON(_PaymentProcessorID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(PaymentProcessor.UID),
			'name', PaymentProcessor.Name,
			'logo', PaymentProcessor.Logo
		)
		FROM
			PaymentProcessor
		WHERE
			PaymentProcessor.ID = _PaymentProcessorID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetOrderStatusJSON;
DELIMITER //
CREATE FUNCTION store.GetOrderStatusJSON(_OrderStatusID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'title', OrderStatus.Title,
			'description', OrderStatus.Description
		)
		FROM
			OrderStatus
		WHERE
			OrderStatus.ID = _OrderStatusID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetOrderJSON;
DELIMITER //
CREATE FUNCTION store.GetOrderJSON(_OrderID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(`Order`.UID),
			'status', GetOrderStatusJSON(OrderStatusID),
			'billingAddress', GetAddressJSON(BillingAddressID),
			'deliveryAddress', GetAddressJSON(DeliveryAddressID),
			'discountId', GetDiscountJSON(DiscountID),
			'shipmentId', GetShipmentJSON(ShipmentID),
			'paymentProcessor', GetPaymentProcessorJSON(PaymentProcessorID),
			'transactionId', transactionID,
			'date', `Date`,
			'subTotal', SubTotal,
			'beforeTaxTotal', BeforeTaxTotal,
			'shipTotal', ShipTotal,
			'taxTotal', TaxTotal,
			'discountTotal', DiscountTotal,
			'grandTotal', GrandTotal,
			'paymentCardBrand', PaymentCardBrand,
			'paymentCardLast4Digits', PaymentCardLast4Digits,
			'paymentCardExpiryMonth', PaymentCardExpiryMonth,
			'paymentCardExpiryYear', PaymentCardExpiryYear,
			'items', GetOrderItemsJSON(`Order`.ID)
		)
		FROM 
			`Order`
		WHERE
			`Order`.ID = _OrderID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetOrderItemsJSON;
DELIMITER //
CREATE FUNCTION store.GetOrderItemsJSON(_OrderID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_ARRAYAGG(JSON_OBJECT(
			'id', BIN_TO_UUID(OrderItem.UID),
			'quantity', OrderItem.Quantity,
			'unitPrice', OrderItem.UnitPrice,
			'productId', BIN_TO_UUID(Product.UID),
			'productName', Product.Name,
			'productImage', GetImageJSON(Product.ImageID),
	    'productItem', (
				SELECT JSON_OBJECT(
					'id', BIN_TO_UUID(ProductItem.UID),
					'name', ProductItem.Name,
					'model', ProductItem.Model
				)
				FROM 
					ProductItem
				WHERE
					ProductItem.ID = OrderItem.ProductItemID
	    ),
	    'quantity', OrderItem.Quantity,
	    'unitPrice', OrderItem.UnitPrice
		))
		FROM
			OrderItem, Product, ProductItem
		WHERE
			OrderItem.ProductItemID = ProductItem.ID AND 
			Product.ID = ProductItem.ProductID AND
			OrderItem.OrderID = _OrderID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetSupplierJSON;
DELIMITER //
CREATE FUNCTION store.GetSupplierJSON(_SupplierID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Supplier.UID),
		  'name', Supplier.Name,
  		'email', Supplier.EMail,
  		'phone', Supplier.Phone,
  		'fax', Supplier.Fax,
  		'address', Supplier.Address,
  		'contacts', Supplier.Contacts			
		)
		FROM
			Supplier
		WHERE
			Supplier.ID = _SupplierID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetManufacturerJSON;
DELIMITER //
CREATE FUNCTION store.GetManufacturerJSON(_ManufacturerID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Manufacturer.UID),
		  'name', Manufacturer.Name,
  		'website', Manufacturer.Website
		)
		FROM
			Manufacturer
		WHERE
			Manufacturer.ID = _ManufacturerID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductAverageRating;
DELIMITER //
CREATE FUNCTION store.GetProductAverageRating(_ProductID int) RETURNS int
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT 
			AVG(Review.Rating)
		FROM
			Review
		WHERE
			Review.ProductID = _ProductID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductItemAverageRating;
DELIMITER //
CREATE FUNCTION store.GetProductItemAverageRating(_ProductItemID int) RETURNS int
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT 
			AVG(Review.Rating)
		FROM
			Review
		WHERE
			Review.ProductItemID = _ProductItemID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductJSON;
DELIMITER //
CREATE FUNCTION store.GetProductJSON(_ProductID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Product.UID),  		  		
			'supplier', GetSupplierJSON(Product.SupplierID),
			'manufacturer', GetManufacturerJSON(Product.ManufacturerID),
			'name', Product.Name,
			'description', Product.Description,
			'keywords', Product.Keywords,
			'averageRating', GetProductAverageRating(Product.ID),
			'image', GetImageJSON(Product.ImageID),
			'categories', GetProductCategoriesJSON(Product.ID),
			'productItems', GetProductItemsJSON(Product.ID)
		)
		FROM
			Product
		WHERE
			Product.ID = _ProductID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductReviewJSON;
DELIMITER //
CREATE FUNCTION store.GetProductReviewJSON(_ReviewID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Review.UID),
			'title', Review.Title,
			'description', Review.Description,
			'rating', Review.Rating,
			'date', Review.Date,
			'verifiedPurchase', Review.VerifiedPurchase,
			'name', Account.PreferredName,
			'region', Region.Name,
			'country', Country.Name
		)
		FROM
			Review, Account, Address, Region, Country
		WHERE
			Account.ID = Review.AccountID AND
			Account.AddressID = Address.ID AND
			Address.RegionID = Region.ID AND
			Address.CountryID = Country.ID AND
			Review.ID = _ReviewID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductItemsJSON;
DELIMITER //
CREATE FUNCTION store.GetProductItemsJSON(_ProductID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_ARRAYAGG(JSON_OBJECT(
			'id', BIN_TO_UUID(ProductItem.UID),	
			'display', ProductItem.Display,
			'model', ProductItem.Model,
			'sku', ProductItem.SKU,
			'upc', ProductItem.UPC,
			'name', ProductItem.Name,
			'description', ProductItem.Description,
			'specifications', GetProductItemSpecificationsJSON(ProductItem.ID),
			'keywords', ProductItem.Keywords,
			'images', GetProductItemImagesJSON(ProductItem.ID),
			'unitPrice', ProductItem.UnitPrice,
			'weight', ProductItem.Weight,
			'stockQuantity', ProductItem.StockQuantity,
			'reorderLevel', ProductItem.ReorderLevel,
			'totalSold', ProductItem.TotalSold,
			'averageRating', GetProductItemAverageRating(ProductItem.ID)
		))
		FROM
			ProductItem
		WHERE
			ProductItem.ProductID = _ProductID
		ORDER BY 
			ProductItem.TotalSold
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductItemJSON;
DELIMITER //
CREATE FUNCTION store.GetProductItemJSON(_ProductItemID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(ProductItem.UID),	
		  'display', ProductItem.Display,
		  'model', ProductItem.Model,
			'sku', ProductItem.SKU,
			'upc', ProductItem.UPC,
			'name', ProductItem.Name,
			'description', ProductItem.Description,
			'specifications', GetProductItemSpecificationsJSON(ProductItem.ID),
			'keywords', ProductItem.Keywords,
			'images', GetProductItemImagesJSON(ProductItem.ID),
			'unitPrice', ProductItem.UnitPrice,
			'weight', ProductItem.Weight,
			'stockQuantity', ProductItem.StockQuantity,
			'reorderLevel', ProductItem.ReorderLevel,
			'totalSold', ProductItem.TotalSold,
			'averageRating', GetProductItemAverageRating(ProductItem.ID)
		)
		FROM
			ProductItem
		WHERE
			ProductItem.ID = _ProductItemID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductItemSpecificationsJSON;
DELIMITER //
CREATE FUNCTION store.GetProductItemSpecificationsJSON(_ProductItemID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_ARRAYAGG(JSON_OBJECT(
			'name', Specification.Name,
			'value', Specification.Value
		))
		FROM
			Specification
		WHERE
			Specification.ProductItemID = _ProductItemID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetCatalogItemJSON;
DELIMITER //
CREATE FUNCTION store.GetCatalogItemJSON(_ProductItemID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Product.UID),  		  		
			'productName', Product.Name,
			'image', GetImageJSON(Product.ImageID),
			'productItem', (
				SELECT JSON_OBJECT(
					'id', BIN_TO_UUID(ProductItem.UID),	
				  'model', ProductItem.Model,
					'name', ProductItem.Name,
					'description', ProductItem.Description,
					'unitPrice', ProductItem.UnitPrice,
					'stockQuantity', ProductItem.StockQuantity,
					'reorderLevel', ProductItem.ReorderLevel,
					'totalSold', ProductItem.TotalSold,
					'averageRating', GetProductItemAverageRating(ProductItem.ID)
				)
				FROM 
					ProductItem
				WHERE
					ProductItem.ID = _ProductItemID
	    )
		)
		FROM
			Product, ProductItem
		WHERE
			Product.ID = ProductItem.ProductID AND
			ProductItem.ID = _ProductItemID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetCartItemJSON;
DELIMITER //
CREATE FUNCTION store.GetCartItemJSON(_ProductItemID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'productId', BIN_TO_UUID(Product.UID),  		  		
			'productName', Product.Name,
			'productImage', GetImageJSON(Product.ImageID),
			'productItem', (
				SELECT JSON_OBJECT(
					'id', BIN_TO_UUID(ProductItem.UID),
					'name', ProductItem.Name,
					'model', ProductItem.Model,
					'weight', ProductItem.Weight,
					'unitPrice', ProductItem.UnitPrice,
					'stockQuantity', ProductItem.StockQuantity,
					'reorderLevel', ProductItem.ReorderLevel
				)
				FROM 
					ProductItem
				WHERE
					ProductItem.ID = _ProductItemID
	    )
		)
		FROM
			Product, ProductItem
		WHERE
			Product.ID = ProductItem.ProductID AND
			ProductItem.ID = _ProductItemID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetReviewJSON;
DELIMITER //
CREATE FUNCTION store.GetReviewJSON(_ReviewID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Review.UID),  		  		
			'title', Review.Title,
			'description', Review.Description,
			'rating', Review.Rating,
			'date', Review.Date,
			'verifiedPurchase', Review.VerifiedPurchase,
			'product', (
				SELECT JSON_OBJECT(
					'id', BIN_TO_UUID(Product.UID),
					'name', Product.Name,
					'image', GetImageJSON(Product.ImageID),
					'item', (
						SELECT JSON_OBJECT(
							'id', BIN_TO_UUID(ProductItem.UID),
							'name', ProductItem.Name,
							'model', ProductItem.Model
						)
						FROM 
							ProductItem
						WHERE
							ProductItem.ID = Review.ProductItemID
					)
				)
				FROM 
					Product
				WHERE
					Product.ID = Review.ProductID
			)
		)
		FROM
			Review
		WHERE
			Review.ID = _ReviewID
	);

END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductCategoriesJSON;
DELIMITER //
CREATE FUNCTION store.GetProductCategoriesJSON(_ProductID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_ARRAYAGG(JSON_OBJECT(
			'id', BIN_TO_UUID(Category.UID),
			'name', Category.Name,
			'subcategories', (
				SELECT JSON_ARRAYAGG(JSON_OBJECT(
					'id', Subcategory.ID,
					'name', Subcategory.Name
				))
				FROM 
					Product, Subcategory, ProductCategory
				WHERE 
					ProductCategory.SubcategoryID = Subcategory.ID AND
					ProductCategory.ProductID = Product.ID AND
					ProductCategory.CategoryID = Category.ID AND
					Product.ID = _ProductID	
			)
		))
		FROM 
			Category
		WHERE
			Category.ID IN (
			SELECT 
				Category.ID
			FROM 
				Product, Category, ProductCategory
			WHERE
				ProductCategory.CategoryID = Category.ID AND
				ProductCategory.ProductID = Product.ID AND
				Product.ID = _ProductID
		)
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetImageJSON;
DELIMITER //
CREATE FUNCTION store.GetImageJSON(_ImageID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_OBJECT(
			'id', BIN_TO_UUID(Image.UID),
			'type', Image.Type,
			'location', Image.Location,
			'url', CONCAT(Image.Location, BIN_TO_UUID(Image.UID), '.', Image.Type)
		)
		FROM 
			Image
		WHERE
			Image.ID = _ImageID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.GetProductItemImagesJSON;
DELIMITER //
CREATE FUNCTION store.GetProductItemImagesJSON(_ProductItemID int) RETURNS JSON
DETERMINISTIC
BEGIN
	
	RETURN (
		SELECT JSON_ARRAYAGG(
			GetImageJSON(Image.ID)
		)
		FROM 
			Image, ProductImage
		WHERE
			Image.ID = ProductImage.ImageID AND 
			ProductImage.ProductItemID = _ProductItemID
	);
	
END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.ID_FROM_UID;
DELIMITER //
CREATE FUNCTION store.ID_FROM_UID(_UID varchar(36), _Table varchar(64)) RETURNS int
DETERMINISTIC
BEGIN

	DECLARE _ID int DEFAULT 0;

	IF (SELECT LOWER(_Table) = 'account') THEN 
		SELECT ID INTO _ID FROM Account WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'address') THEN
		SELECT ID INTO _ID FROM Address WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'category') THEN
		SELECT ID INTO _ID FROM Category WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'discount') THEN
		SELECT ID INTO _ID FROM Discount WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'discounttype') THEN
		SELECT ID INTO _ID FROM DiscountType WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'image') THEN
		SELECT ID INTO _ID FROM Image WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'manufacturer') THEN
		SELECT ID INTO _ID FROM Manufacturer WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'order') THEN
		SELECT ID INTO _ID FROM `Order` WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'orderitem') THEN
		SELECT ID INTO _ID FROM OrderItem WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'orderstatus') THEN
		SELECT ID INTO _ID FROM OrderStatus WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'paymentprocessor') THEN
		SELECT ID INTO _ID FROM PaymentProcessor WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'product') THEN
		SELECT ID INTO _ID FROM Product WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'productitem') THEN
		SELECT ID INTO _ID FROM ProductItem WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'review') THEN
		SELECT ID INTO _ID FROM Review WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'role') THEN
		SELECT ID INTO _ID FROM Role WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'sale') THEN
		SELECT ID INTO _ID FROM Sale WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'saleitem') THEN
		SELECT ID INTO _ID FROM SaleItem WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'shipment') THEN
		SELECT ID INTO _ID FROM Shipment WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'stockorder') THEN
		SELECT ID INTO _ID FROM StockOrder WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'subcategory') THEN
		SELECT ID INTO _ID FROM Subcategory WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'supplier') THEN
		SELECT ID INTO _ID FROM Supplier WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'wishlist') THEN
		SELECT ID INTO _ID FROM Wishlist WHERE UID = UUID_TO_BIN(_UID);
	ELSEIF (SELECT LOWER(_Table) = 'wishlistitem') THEN
		SELECT ID INTO _ID FROM WishlistItem WHERE UID = UUID_TO_BIN(_UID);

	END IF;

	RETURN (_ID);	

END //
DELIMITER ;
#-------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS store.UID_FROM_ID;
DELIMITER //
CREATE FUNCTION store.UID_FROM_ID(_ID int, _Table varchar(64)) RETURNS varchar(36)
DETERMINISTIC
BEGIN

	DECLARE _UID int DEFAULT '';

	IF (SELECT LOWER(_Table) = 'account') THEN 
		SELECT UID INTO _UID FROM Account WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'address') THEN
		SELECT UID INTO _UID FROM Address WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'category') THEN
		SELECT UID INTO _UID FROM Category WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'discount') THEN
		SELECT UID INTO _UID FROM Discount WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'discounttype') THEN
		SELECT UID INTO _UID FROM DiscountType WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'image') THEN
		SELECT UID INTO _UID FROM Image WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'manufacturer') THEN
		SELECT UID INTO _UID FROM Manufacturer WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'order') THEN
		SELECT UID INTO _UID FROM `Order` WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'orderitem') THEN
		SELECT UID INTO _UID FROM OrderItem WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'orderstatus') THEN
		SELECT UID INTO _UID FROM OrderStatus WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'paymentprocessor') THEN
		SELECT UID INTO _UID FROM PaymentProcessor WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'product') THEN
		SELECT UID INTO _UID FROM Product WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'productitem') THEN
		SELECT UID INTO _UID FROM ProductItem WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'review') THEN
		SELECT UID INTO _UID FROM Review WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'role') THEN
		SELECT UID INTO _UID FROM Role WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'sale') THEN
		SELECT UID INTO _UID FROM Sale WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'saleitem') THEN
		SELECT UID INTO _UID FROM SaleItem WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'shipment') THEN
		SELECT UID INTO _UID FROM Shipment WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'stockorder') THEN
		SELECT UID INTO _UID FROM StockOrder WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'subcategory') THEN
		SELECT UID INTO _UID FROM Subcategory WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'supplier') THEN
		SELECT UID INTO _UID FROM Supplier WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'wishlist') THEN
		SELECT UID INTO _UID FROM Wishlist WHERE ID = _ID;
	ELSEIF (SELECT LOWER(_Table) = 'wishlistitem') THEN
		SELECT UID INTO _UID FROM WishlistItem WHERE ID = _ID;

	END IF;

	RETURN (_ID);	

END //
DELIMITER ;
#-------------------------------------------------------------------------------


